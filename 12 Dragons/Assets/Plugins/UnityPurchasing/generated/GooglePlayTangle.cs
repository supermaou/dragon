#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class GooglePlayTangle
    {
        private static byte[] data = System.Convert.FromBase64String("BLY1FgQ5Mj0esnyywzk1NTUxNDc+OJ5KZNqku/aew9sh1vLhJVgGpzJ4n1jpBCGcXnd1OfdIjjySdughstbseLiaVqgkXGmIVRHh1ertwLobCDDDEp02/9V/DQ0iAKE+IoU6kADQdUnzgeH76YFZ0GFYL5bSE3sxtjU7NAS2NT42tjU1NNG46ho97LQXiTMRDVMO/ydHzpFiR1E0a/i+sSppk5pa7QXgCr5jHBYSlHO2I/O4kI90TB+vmTrElPXwvWEJSaLdZVlpz8/YGTy7ty+et7WtDvkFDGk65k67Ot7+2MHXnD1auykW5kaeZj6AzY8eFwIBlpx/4ivZ75YN0nNpOS/oa8vuWjN9m1WsrKxQWR/vn3EAuHDORZsXKCa/CTY3NTQ1");
        private static int[] order = new int[] { 0,9,3,4,10,6,9,12,12,11,11,13,12,13,14 };
        private static int key = 52;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
