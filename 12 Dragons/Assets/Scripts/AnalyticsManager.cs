﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Facebook.Unity;

public class AnalyticsManager : MonoBehaviour {

	public static AnalyticsManager instance = null;

	void Awake(){
		if (instance == null) {
			instance = this;
		} else {
			Destroy (gameObject);
		}

		DontDestroyOnLoad (gameObject);

	}

	void OnApplicationPause (bool pauseStatus)
	{
		// Check the pauseStatus to see if we are in the foreground
		// or background
		if (!pauseStatus) {
			//app resume
			if (FB.IsInitialized) {
				FB.ActivateApp();
			} else {
				//Handle FB.Init
				FB.Init( () => {
					FB.ActivateApp();
				});
			}
		}
	}

	/*
	public void OnSceneWasLoaded(string scene){
		var parameters = new Dictionary<string, object>();
		parameters["scene_load"] = scene;
		FB.LogAppEvent(
			Facebook.Unity.AppEventName.,
			(float)1,
			parameters
		);
	}*/

	public void CompleteDungeon(string dunName){
		var parameters = new Dictionary<string,object> ();
		parameters ["dungeon_complete"] = dunName;
		FB.LogAppEvent (
			Facebook.Unity.AppEventName.AchievedLevel,
			1f,
			parameters
		);
	}

	public void OpenGacha(string type){
		var parameters = new Dictionary<string,object> ();
		parameters ["open_gacha"] = type;
		FB.LogAppEvent (
			Facebook.Unity.AppEventName.Purchased,
			1f,
			parameters
		);
	}

	public void QuestClaimLogEvent(string shortCode){
		var parameters = new Dictionary<string, object>();
		parameters["achievement_claim"] = shortCode;
		FB.LogAppEvent(
			Facebook.Unity.AppEventName.UnlockedAchievement,
			1f,
			parameters
		);
	}

	public void SpendGems(string item,int spending){
		var softPurchaseParameters = new Dictionary<string, object>();
		softPurchaseParameters["gems_spending"] = item;
		FB.LogAppEvent(
			Facebook.Unity.AppEventName.SpentCredits,
			(float)spending,
			softPurchaseParameters
		);
	}

	public void SpendGold(string item,int spending){
		var softPurchaseParameters = new Dictionary<string, object>();
		softPurchaseParameters["gold_spending"] = item;
		FB.LogAppEvent(
			Facebook.Unity.AppEventName.SpentCredits,
			(float)spending,
			softPurchaseParameters
		);
	}

	public void ReceiveGems(string source,int amount){
		var ReceiveParameters = new Dictionary<string, object>();
		ReceiveParameters["receive_gems"] = source;
		FB.LogAppEvent(
			Facebook.Unity.AppEventName.SpentCredits,
			(float)amount,
			ReceiveParameters
		);
	}
}
