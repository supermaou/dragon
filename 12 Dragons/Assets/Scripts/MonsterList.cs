﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class MonsterList : MonoBehaviour {

	public static MonsterList instance = null;

	public MonsterModel[] monsterModels;

	public List<MonsterModel> monsterModelList = new List<MonsterModel> ();

	private string resolution = "HD";

	// Use this for initialization
	void Awake () {
		if (instance == null) {
			instance = this;
		} else {
			Destroy (gameObject);
			return;
		}
		DontDestroyOnLoad (gameObject);

		if (Screen.width >= 500f) {
			resolution = "HD";
		}
			
	}

	void Start(){
		Resources.UnloadUnusedAssets ();
	}
		
	public GameObject GetPrefab(string shortCode){
		for (int i = 0; i < monsterModels.Length; i++) {
			if (monsterModels [i].shortCode == shortCode) {
				GameObject g = Resources.Load ("MonsterPrefabs/p" + monsterModels [i].id, typeof(GameObject)) as GameObject;
				if (g != null) {
					return g;
				} else {
					print ("Prefab error for: " + shortCode);
					return null;
				}
			}
		}

		return null;
	}

	public Sprite GetThumbnail(string shortCode){
		for (int i = 0; i < monsterModels.Length; i++) {
			if (monsterModels [i].shortCode == shortCode) {
				//return monsterModels [i].thumbnail;

				return Resources.Load<Sprite>("Thumbnails/" + resolution + "/m" + monsterModels[i].id);
			}
		}

		return null;
	}

	public bool IsShortCodeValid(string shortCode){
		for (int i = 0; i < monsterModels.Length; i++) {
			if (monsterModels [i].shortCode == shortCode) {
				return true;
			}
		}

		return false;
	}

	/*
	public IEnumerator CheckEvolveMaterials(){
		print ("Start checking");
		for (int i = 0; i < monsterModels.Length; i++) {
			string shortCode = monsterModels [i].shortCode;
			MonsterData m = new MonsterData ();
			m.shortCode = shortCode;
			GameSparksManager.instance.GetMonsterData (m);

			while (!PersistentData.instance.loadedMonsterData || m.stars == 0) {
				print (i.ToString() + " check");
				yield return null;
			}

			for (int j = 0; j < m.evolveMaterials.Count; j++) {
				bool isMatch = false;
				for (int k = 0; k < monsterModels.Length; k++) {
					if (m.evolveMaterials [j] == monsterModels [k].shortCode) {
						isMatch = true;
						break;
					}
				}

				if (!isMatch) {
					print ("Error at " + shortCode);
				}
			}
		}

		print ("Check complete");
	}*/
}

[Serializable]
public class MonsterModel{
	public string shortCode;
	public string id;
}
