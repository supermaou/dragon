﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StoryManager : MonoBehaviour {

	List<string> dialog = new List<string>(){
		"Guinevere: Are you fucking serious?? ha",
		"Gawain: I dont know what your want to do at all",
		"Guinevere: I dont know about it too.",
		"Gawain: And I dont know.."
	};
	int currentIndex = 0;

	public Text nameText;
	public Text dialogText;

	public GameObject tapToContinue;

	private GameObject npc;
	private IEnumerator coroutine;

	void Awake(){

		coroutine = RunDialog ();
		StartCoroutine (coroutine);
	}

	void Update(){
		if (Input.GetMouseButtonDown(0)) {
			if (tapToContinue.activeSelf == true) {
				coroutine = RunDialog ();
				StartCoroutine (coroutine);

			} else {
				StopCoroutine (coroutine);

				if (currentIndex < dialog.Count) {
					string[] s = dialog [currentIndex].Split (':');
					dialogText.text = s [1];
					currentIndex++;
					tapToContinue.SetActive (true);
				}
			}
		}
	}

	IEnumerator RunDialog(){
		
		tapToContinue.SetActive (false);

		if (currentIndex >= dialog.Count) {
			//Go to game scene
			yield break;
		}

		string[] s = dialog [currentIndex].Split (':');
		nameText.text = s [0];

		if (npc != null) {
			StartCoroutine (MovingOut (npc, 0.5f));
		}

		//Instantiate character image
		npc = Instantiate (Resources.Load ("Characters/" + s [0], typeof(GameObject)) as GameObject);
		StartCoroutine (MovingIn (npc, 0.5f));

		string descText = "";
		foreach (char c in s[1]) {
			descText = descText + c.ToString ();
			dialogText.text = descText;
			yield return new WaitForSeconds (0.03f);
		}

		currentIndex++;

		tapToContinue.SetActive (true);
	}

	IEnumerator MovingIn(GameObject g,float time){

		SpriteRenderer sr = g.GetComponent<SpriteRenderer> ();

		float t = 0;

		Vector3 stop = g.transform.position;
		Vector3 start = new Vector3 (-5f, g.transform.position.y, g.transform.position.z);

		if (g.transform.position.x > 0f) {
			start = new Vector3 (5f, g.transform.position.y, g.transform.position.z);
		}

		while (t < time) {
			sr.color = Color.Lerp (Color.clear, Color.white, t / time);
			g.transform.position = Vector3.Lerp (start, stop, t / time);

			t += Time.deltaTime;
			yield return null;
		}
	}

	IEnumerator MovingOut(GameObject g,float time){

		SpriteRenderer sr = g.GetComponent<SpriteRenderer> ();

		float t = 0;

		Vector3 start = g.transform.position;
		Vector3 stop = new Vector3 (-5f, g.transform.position.y, g.transform.position.z);

		if (g.transform.position.x > 0f) {
			stop = new Vector3 (5f, g.transform.position.y, g.transform.position.z);
		}

		while (t < time) {
			sr.color = Color.Lerp (Color.white, Color.clear, t / time);
			g.transform.position = Vector3.Lerp (start, stop, t / time);
			t += Time.deltaTime;
			yield return null;
		}

		Destroy (g);
	}
}
