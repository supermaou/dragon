﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System;

public class SaveLoad : MonoBehaviour {

	public static UserData userData = new UserData ();

	public static void Save() {
		BinaryFormatter bf = new BinaryFormatter();
		FileStream file = File.Create (Application.persistentDataPath + "/systemdata26.gd");
		bf.Serialize(file, SaveLoad.userData);
		file.Close();
	}   

	public static void Load() {
		if(File.Exists(Application.persistentDataPath + "/systemdata26.gd")) {
			BinaryFormatter bf = new BinaryFormatter();
			FileStream file = File.Open(Application.persistentDataPath + "/systemdata26.gd", FileMode.Open);
			SaveLoad.userData = (UserData)bf.Deserialize(file);
			file.Close();
		}
	}
}

[System.Serializable]
public class UserData {

	//check lose count to show starter pack promo
	//public bool isBuyStarterPack = false; //check to make sure player can only buy one of this //Should save in server
	public int loseCount = 0;

	//Noti things
	public int inventoryNoti = 0;

	//review things
	public List<string> reviewVersions = new List<string>();

	//monster book
	//public List<string> monsterFounded = new List<string>(); //Should save in server

	//gacha table things
	public int gachaStartTime = 0;
	public string featureMon;
	public List<List<string>> gachaOpenData = new List<List<string>>(); //Store as shortCode
	public List<string> gachaList = new List<string>();
	public List<string> featureList = new List<string>();
	public List<string> featurePool = new List<string> ();

	//daily dungeon things
	public List<string> dailyDungeons = new List<string>(); //for checking if the dungeon should be reset
	public long timeStamp;

	//offer things
	public List<string> sundayMaterialOffer = new List<string>();
	public List<string> sundayGemOffer = new List<string>();

	public UserData(){

		for (int i = 0; i < 6; i++) {
			gachaOpenData.Add (new List<string> ());
			for (int j = 0; j < 6; j++) {
				gachaOpenData [i].Add ("0");
			}
		}
	}
}