﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ArenaResult : MonoBehaviour {

	public Text title;
	public Text coinText;
	public Text arenaScoreText;
	public Text currentScoreText;

	void Start(){
		/*
		if (PersistentData.instance.arenaResult == "win") {
			title.text = "คุณชนะ";
			StartCoroutine (CountTo( coinText, 1000, 0.5f));
			GameSparksManager.instance.playerData.gold += 1000;
			arenaScoreText.text = "+" + (PersistentData.instance.ratingWin - GameSparksManager.instance.playerData.arenaScore).ToString ();
			StartCoroutine (CountTo (currentScoreText, PersistentData.instance.ratingWin, 0.5f));
			GameSparksManager.instance.playerData.arenaScore = PersistentData.instance.ratingWin;
			GameSparksManager.instance.SavePlayerData ();

		} else if (PersistentData.instance.arenaResult == "lose") {
			title.text = "คุณแพ้";
			StartCoroutine (CountTo( coinText, 100, 0.5f));
			GameSparksManager.instance.playerData.gold += 100;
			arenaScoreText.text = "-" + (GameSparksManager.instance.playerData.arenaScore - PersistentData.instance.ratingLose).ToString ();
			StartCoroutine (CountTo (currentScoreText, PersistentData.instance.ratingLose, 0.5f));
			GameSparksManager.instance.playerData.arenaScore = PersistentData.instance.ratingLose;
			GameSparksManager.instance.SavePlayerData ();
		}*/
	}

	public void Menu(){
		Initiate.Fade("main",new Color(0.1f,0.1f,0.1f),1.5f);
	}

	IEnumerator CountTo(Text txt,int target, float duration){
		int start = System.Int32.Parse (txt.text);
		for (float timer = 0; timer < duration; timer += Time.deltaTime) {
			float progress = timer / duration;
			int score = (int)Mathf.Lerp (start, target, progress);
			txt.text = score.ToString ();
			yield return null;
		}

		txt.text = target.ToString ();
	}
}
