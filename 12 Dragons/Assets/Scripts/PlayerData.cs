﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSparks.Core;

public class PlayerData {

	public string displayName = "";
	public SafeInt bestWorldBossScore = new SafeInt (0);

	public SafeInt xp = new SafeInt (0);
	public SafeInt gold = new SafeInt(100);
	public SafeInt gems = new SafeInt (0);
	public SafeInt stamina = new SafeInt (40);
	public SafeInt maxStamina = new SafeInt (40);
	public SafeInt rank = new SafeInt(1);
	public SafeInt maxInventory = new SafeInt(100);
	public SafeInt maxFriends = new SafeInt (5);
	public int currentTeam = 0;
	public SafeInt dailyLogin = new SafeInt (0);
	public SafeInt loginCount = new SafeInt (0);
	public SafeInt dungeon = new SafeInt (0);
	public SafeInt tower = new SafeInt (0);

	public List<string> dailyDungeonCompleted = new List<string> ();

	//get from another database
	public List<int> stars = new List<int>();

	public SafeInt dailyGacha = new SafeInt (0);
	public SafeInt specialGacha = new SafeInt (0);

	//quest parameter
	public SafeInt login = new SafeInt(0);
	public SafeInt clear = new SafeInt(0);
	public SafeInt gacha = new SafeInt(0);
	public SafeInt evolve = new SafeInt(0);
	public SafeInt fuse = new SafeInt(0);
	public SafeInt beat = new SafeInt(0);
	public SafeInt dailydun = new SafeInt(0);
	public SafeInt bossraid = new SafeInt(0);

	//World boss data
	public string worldBoss_reward = "";
	public SafeInt dailyRaid = new SafeInt(0);

	public List<string> unclaimAchievement = new List<string> ();
	public List<string> friendRequests = new List<string>();
	public List<string> friendList = new List<string>();
	public List<int> friendEnergy = new List<int>();
	public List<int> friendDailySend = new List<int>();

	//daily gems
	public SafeInt dailyGems = new SafeInt(-1);
	public SafeInt isReceived = new SafeInt(0);

	//promotion (starter pack)
	public SafeInt isBuyStarterPack = new SafeInt(0);

	//refer code
	public SafeInt referReward = new SafeInt(0);

	//daily quests things
	public List<string> dailyQuestsToClaim = new List<string>();
	public List<string> dailyQuestsComplete = new List<string>();

	public GSRequestData ToJsonRequestData(){
		GSRequestData jsonDataToSend = new GSRequestData();
		jsonDataToSend.AddString ("displayName", displayName);
		jsonDataToSend.AddNumber ("bestWorldBossScore", bestWorldBossScore.Value);
		jsonDataToSend.AddNumber ("xp", xp.Value);
		jsonDataToSend.AddNumber ("gold", gold.Value);
		jsonDataToSend.AddNumber ("gems", gems.Value);
		jsonDataToSend.AddNumber ("stamina", stamina.Value);
		jsonDataToSend.AddNumber ("maxStamina", maxStamina.Value);
		jsonDataToSend.AddNumber ("rank", rank.Value);
		jsonDataToSend.AddNumber ("maxInventory", maxInventory.Value);
		jsonDataToSend.AddNumber ("maxFriends", maxFriends.Value);
		jsonDataToSend.AddNumber ("currentTeam", currentTeam);
		jsonDataToSend.AddNumber ("dailyLogin", dailyLogin.Value);
		jsonDataToSend.AddNumber ("loginCount", loginCount.Value);
		jsonDataToSend.AddNumber ("dungeon", dungeon.Value);
		jsonDataToSend.AddNumber ("tower", tower.Value);
		jsonDataToSend.AddStringList ("unclaimAchievement", unclaimAchievement);

		jsonDataToSend.AddStringList ("dailyDungeonCompleted", dailyDungeonCompleted);

		jsonDataToSend.AddNumber ("dailyGacha", dailyGacha.Value);
		jsonDataToSend.AddNumber ("specialGacha", specialGacha.Value);

		//friend things
		jsonDataToSend.AddStringList ("friendRequests", friendRequests);
		jsonDataToSend.AddStringList ("friendList", friendList);
		jsonDataToSend.AddNumberList ("friendEnergy", friendEnergy);
		jsonDataToSend.AddNumberList ("friendDailySend", friendDailySend);

		//quest variables
		jsonDataToSend.AddNumber("login",login.Value);
		jsonDataToSend.AddNumber("clear",clear.Value);
		jsonDataToSend.AddNumber("gacha",gacha.Value);
		jsonDataToSend.AddNumber("evolve",evolve.Value);
		jsonDataToSend.AddNumber ("fuse", fuse.Value);
		jsonDataToSend.AddNumber ("beat", beat.Value);
		jsonDataToSend.AddNumber ("dailydun", dailydun.Value);
		jsonDataToSend.AddNumber ("bossraid", bossraid.Value);

		//World boss data
		jsonDataToSend.AddString ("worldBoss_reward", worldBoss_reward);
		jsonDataToSend.AddNumber ("dailyRaid", dailyRaid.Value);

		//daily gems
		jsonDataToSend.AddNumber ("dailyGems", dailyGems.Value);
		jsonDataToSend.AddNumber ("isReceived", isReceived.Value);

		//starter pack
		jsonDataToSend.AddNumber("isBuyStarterPack",isBuyStarterPack.Value);

		//refer
		jsonDataToSend.AddNumber("referReward",referReward.Value);

		//daily quests things
		jsonDataToSend.AddStringList("dailyQuestsToClaim", dailyQuestsToClaim);
		jsonDataToSend.AddStringList ("dailyQuestsComplete", dailyQuestsComplete);

		return jsonDataToSend;
	}

	public void RankUp(){
		rank = rank + 1;
		maxStamina = maxStamina + 1;
		GameSparksManager.instance.playerData.stamina += maxStamina.Value;
		//GameSparksManager.instance.SavePlayerData ();
		//SaveLoad.Save ();
	}

	public int TotalExpRequireForNextRank(){
		float nextRankExp = 1700000 * Mathf.Pow ((rank.Value) / 98f, 2.5f);
		float currentRankExp = 1700000 * Mathf.Pow ((rank.Value - 1f) / 98f, 2.5f);
		return Mathf.RoundToInt (nextRankExp - currentRankExp);
	}

	public int ExpRequireForNextRank(){
		if (rank >= 500) {
			return -1;
		}

		float nextRankExp = 1700000 * Mathf.Pow ((rank.Value) / 98f, 2.5f);
		return Mathf.RoundToInt (nextRankExp - xp.Value);
	}
		
}
