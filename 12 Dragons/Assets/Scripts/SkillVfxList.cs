﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillVfxList : MonoBehaviour {

	public static SkillVfxList instance = null;
	public SkillData[] vfxs;

	void Awake(){
		if (instance == null) {
			instance = this;
		} else {
			Destroy (gameObject);
			return;
		}
		DontDestroyOnLoad (gameObject);
	}
}

[System.Serializable]
public class SkillData{
	public string skillName;
}
