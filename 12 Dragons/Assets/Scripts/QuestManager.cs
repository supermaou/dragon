﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using GameSparks.Core;

public class QuestManager : MonoBehaviour {

	public Transform content;

	public GameObject questPrefab;

	public Sprite gemIcon;
	public Sprite goldIcon;
	public Sprite energyIcon;
	public Sprite spaceIcon;
	public Sprite eggMonIcon;

	public GameObject messageDialog;

	public Image dailyQuestBtn;
	public Image normalQuestBtn;

	public Sprite highlightedBtn;
	public Sprite normalBtn;

	private string currentTab = "dailyQuest";

	void OnEnable(){
		UIManager.instance.navMenu [0].transform.GetChild (2).gameObject.SetActive (false);

		if (currentTab == "dailyQuest") {
			ShowDailyQuest ();
		} else {
			ShowQuests ();
		}
	}

	void SetSpriteToOriginalSize(Image img, float target){
		float width = img.sprite.rect.width;
		float height = img.sprite.rect.height;

		if (height > width) {
			float multiplier = target / height;

			img.rectTransform.sizeDelta = new Vector2 (width * multiplier, target);
		} else {
			float multiplier = target / width;

			img.rectTransform.sizeDelta = new Vector2 (target, height * multiplier);
		}
	}

	public void ShowDailyQuestButton(){
		if (currentTab == "dailyQuest") {
			return;
		}

		dailyQuestBtn.sprite = highlightedBtn;
		normalQuestBtn.sprite = normalBtn;

		dailyQuestBtn.transform.GetChild (0).GetComponent<Text> ().color = new Color (50f / 255f, 50f / 255f, 50f / 255f);
		normalQuestBtn.transform.GetChild (0).GetComponent<Text> ().color = Color.white;

		currentTab = "dailyQuest";
		ShowDailyQuest ();
	}

	public void ShowQuestButton(){
		if (currentTab == "normalQuest") {
			return;
		}

		dailyQuestBtn.sprite = normalBtn;
		normalQuestBtn.sprite = highlightedBtn;

		dailyQuestBtn.transform.GetChild (0).GetComponent<Text> ().color = Color.white;
		normalQuestBtn.transform.GetChild (0).GetComponent<Text> ().color = new Color (50f / 255f, 50f / 255f, 50f / 255f);

		currentTab = "normalQuest";
		ShowQuests ();
	}

	void ShowDailyQuest(){
		foreach (Transform t in content) {
			Destroy (t.gameObject);
		}

		new GameSparks.Api.Requests.LogEventRequest ().SetEventKey ("getDailyQuests").Send ((response) => {
			if(!response.HasErrors){
				List<GSData> data = response.ScriptData.GetGSDataList("data");

				for(int i=0;i<data.Count;i++){
					//string quest_id = data[i].GetGSData("_id").GetString("$oid");
					string shortCode = data[i].GetString("shortCode");
					int target = data[i].GetInt("target") ?? 1;
					List<string> reward = data[i].GetStringList("reward");

					GameObject g = Instantiate (questPrefab) as GameObject;
					g.transform.SetParent (content, false);
					g.SetActive(false);

					Image thumbnail = g.transform.GetChild (0).GetComponent<Image> ();
					Text title = g.transform.GetChild (1).GetComponent<Text> ();
					Text rewardText = g.transform.GetChild (2).GetComponent<Text> ();
					Text progress = g.transform.GetChild (3).GetComponent<Text> ();

					thumbnail.sprite = GetRewardThumbnail (reward[0]);
					SetSpriteToOriginalSize (thumbnail, 80f);

					if(shortCode == "dailyQuestClear"){
						title.text = "ทำภารกิจรายวันทั้งหมด";
					}else if(shortCode == "normalDungeonPlays"){
						title.text = "เล่นด่านผจญภัย " + target.ToString() + " ครั้ง"; 
					}else if(shortCode == "dailyDungeonPlays"){
						title.text = "เล่นดันเจี้ยนพิเศษ " + target.ToString() + " ครั้ง";
					}else if(shortCode == "worldBossPlay"){
						title.text = "สู้บอส " + target.ToString() + " ครั้ง";
					}else if(shortCode == "gachaTable"){
						title.text = "เปิดตารางสุ่มมอนสเตอร์ " + target.ToString() + " ครั้ง";
					}

					rewardText.text = "รางวัล: " + GetRewardText (reward[0]);

					if(GameSparksManager.instance.playerData.dailyQuestsComplete.Contains(shortCode)){
						
						Button b = g.AddComponent<Button> ();
						b.interactable = false;

						progress.text = "รับรางวัลแล้ว";

					} else if(GameSparksManager.instance.playerData.dailyQuestsToClaim.Contains(shortCode)){
						g.transform.GetChild (3).gameObject.SetActive (false);
						g.transform.GetChild (4).gameObject.SetActive (true);
						g.transform.GetChild (5).gameObject.SetActive (true);

						if(reward.Count == 1){
							Button b = g.AddComponent<Button> ();
							b.onClick.AddListener (() => ClaimDailyQuest(b,shortCode,reward[0]));
						}else{
							int rand = UnityEngine.Random.Range(0,reward.Count);
							Button b = g.AddComponent<Button> ();
							b.onClick.AddListener (() => ClaimDailyQuest(b,shortCode,reward[rand]));
						}
					} else{
						g.transform.GetChild (3).gameObject.SetActive (false);
					}

					g.SetActive(true);
				}

			}else{
				
			}
		});
	}

	void ShowQuests(){
		foreach (Transform t in content) {
			Destroy (t.gameObject);
		}

		List<QuestData> questData = GameSparksManager.instance.questData;

		for (int i = 0; i < GameSparksManager.instance.playerData.unclaimAchievement.Count; i++) {
			string s = GameSparksManager.instance.playerData.unclaimAchievement [i];
			string[] split = s.Split (';');

			GameObject g = Instantiate (questPrefab) as GameObject;
			g.transform.SetParent (content, false);

			Image thumbnail = g.transform.GetChild (0).GetComponent<Image> ();
			Text title = g.transform.GetChild (1).GetComponent<Text> ();
			Text reward = g.transform.GetChild (2).GetComponent<Text> ();

			thumbnail.sprite = GetRewardThumbnail (split [2]);
			SetSpriteToOriginalSize (thumbnail, 80f);

			if (split [1] == "login") {
				title.text = "ล็อกอินเข้าเกมส์ " + split [0] + " ครั้ง";
			} else if (split [1] == "clear") {
				title.text = "ผ่านด่าน " + split [0] + " ครั้ง";
			} else if (split [1] == "gacha") {
				title.text = "เปิดแผ่นป้ายมอนสเตอร์ " + split [0] + " ครั้ง";
			} else if (split [1] == "evolve") {
				title.text = "วิวัฒนาการมอนสเตอร์ " + split [0] + " ครั้ง";
			} else if (split [1] == "fuse") {
				title.text = "อัพเกรดมอนสเตอร์ " + split [0] + " ครั้ง";
			} else if (split [1] == "beat") {
				title.text = "กำจัดมอนสเตอร์ " + split [0] + " ตัว";
			} else if (split [1] == "cleardun") {
				title.text = "ชนะด่าน " + GameSparksManager.instance.dungeonData [Int32.Parse (split [0]) - 1].thaiName;
			} else if (split [1] == "dailydun") {
				title.text = "ผ่านดันเจี้ยนพิเศษ " + split [0] + " ครั้ง";
			} else if (split [1] == "bossraid") {
				title.text = "ล่าบอส " + split[0] + " ครั้ง";
			}

			reward.text = "รางวัล: " + GetRewardText (split [2]);
			g.transform.GetChild (3).gameObject.SetActive (false);
			g.transform.GetChild (4).gameObject.SetActive (true);
			g.transform.GetChild (5).gameObject.SetActive (true);

			Button b = g.AddComponent<Button> ();
			b.onClick.AddListener (() => ClaimReward (b, s));
		}

		for (int i = 0; i < questData.Count; i++) {
			GameObject g = Instantiate (questPrefab) as GameObject;
			g.transform.SetParent (content, false);

			Image thumbnail = g.transform.GetChild (0).GetComponent<Image> ();
			Text title = g.transform.GetChild (1).GetComponent<Text> ();
			Text reward = g.transform.GetChild (2).GetComponent<Text> ();
			Text progress = g.transform.GetChild (3).GetComponent<Text> ();

			thumbnail.sprite = GetRewardThumbnail (questData [i].reward [questData [i].currentTargetIndex].ToString ());
			SetSpriteToOriginalSize (thumbnail, 80f);

			if (questData [i].questType == "login") {
				title.text = "ล็อกอินเข้าเกมส์ " + questData [i].targetValue [questData [i].currentTargetIndex].ToString () + " ครั้ง";
			} else if (questData [i].questType == "clear") {
				title.text = "ผ่านด่าน " + questData [i].targetValue [questData [i].currentTargetIndex].ToString () + " ครั้ง";
			} else if (questData [i].questType == "gacha") {
				title.text = "เปิดแผ่นป้ายมอนสเตอร์ " + questData [i].targetValue [questData [i].currentTargetIndex].ToString () + " ครั้ง";
			} else if (questData [i].questType == "evolve") {
				title.text = "วิวัฒนาการมอนสเตอร์ " + questData [i].targetValue [questData [i].currentTargetIndex].ToString () + " ครั้ง";
			} else if (questData [i].questType == "fuse") {
				title.text = "อัพเกรดมอนสเตอร์ " + questData [i].targetValue [questData [i].currentTargetIndex].ToString () + " ครั้ง";
			} else if (questData [i].questType == "beat") {
				title.text = "กำจัดมอนสเตอร์ " + questData [i].targetValue [questData [i].currentTargetIndex].ToString () + " ตัว";
			} else if (questData [i].questType == "cleardun") {
				title.text = "ชนะด่าน " + GameSparksManager.instance.dungeonData [questData [i].targetValue [questData [i].currentTargetIndex] - 1].thaiName;
			} else if (questData [i].questType == "dailydun") {
				title.text = "ผ่านด่านพิเศษ " + questData [i].targetValue [questData [i].currentTargetIndex].ToString () + " ครั้ง";
			} else if (questData [i].questType == "bossraid") {
				title.text = "ล่าบอส " + questData [i].targetValue [questData [i].currentTargetIndex].ToString () + " ครั้ง";
			}
				
			reward.text = "รางวัล: " + GetRewardText (questData [i].reward [questData [i].currentTargetIndex].ToString ());
			progress.text = questData [i].progress.ToString () + "/" + questData [i].targetValue [questData [i].currentTargetIndex].ToString ();

			if (questData [i].questType == "cleardun") {
				progress.gameObject.SetActive (false);
			}
		}
	}

	void OnDisable(){
		foreach (Transform t in content) {
			Destroy (t.gameObject);
		}
	}

	Sprite GetRewardThumbnail(string shortCode){
		string[] s = shortCode.Split ('.');

		if (s [1] == "gems") {
			return gemIcon;
		} else if (s [1] == "gold") {
			return goldIcon;
		} else if (s [1] == "energy") {
			return energyIcon;
		} else if (s [1] == "spaces") {
			return spaceIcon;
		} else { //monster case
			return eggMonIcon;
		}

		return null;
	}

	string GetRewardText(string shortCode){
		string[] s = shortCode.Split ('.');

		if (s [1] == "gems") {
			return s [0] + " เพชร";
		} else if (s [1] == "gold") {
			return s [0] + " ทอง";
		} else if (s [1] == "energy") {
			return s [0] + " พลังงาน";
		} else if (s [1] == "spaces") {
			return s [0] + " ช่องเก็บของ";
		} else { //mon case
			return "วัตถุดิบเพิ่มเลเวล (สุ่มธาตุ)";
		}

		return "";
	}

	void ClaimDailyQuest(Button b,string shortCode,string reward){ //short code is type of quest
		if (!GameSparksManager.instance.playerData.dailyQuestsToClaim.Contains (shortCode)) {
			return;	
		}
			
		b.interactable = false;
		b.gameObject.transform.GetChild (4).gameObject.SetActive (false);
		b.gameObject.transform.GetChild (5).gameObject.SetActive (false);

		string[] ss = reward.Split ('.');

		string rewardText = "";

		if (ss [1] == "gems") {
			GameSparksManager.instance.playerData.gems += Int32.Parse (ss [0]);
			AnalyticsManager.instance.ReceiveGems ("quest", Int32.Parse (ss [0]));
			rewardText = Int32.Parse (ss [0]).ToString () + " เพชร";
		} else if (ss [1] == "gold") {
			GameSparksManager.instance.playerData.gold += Int32.Parse (ss [0]);
			rewardText = Int32.Parse (ss [0]).ToString () + " ทอง";
		} else if (ss [1] == "energy") {
			GameSparksManager.instance.playerData.stamina += Int32.Parse (ss [0]);
			rewardText = Int32.Parse (ss [0]).ToString () + " พลังงาน";
			//SaveLoad.Save ();
		} else if (ss [1] == "spaces") {
			GameSparksManager.instance.playerData.maxInventory += Int32.Parse (ss [0]);
			rewardText = Int32.Parse (ss [0]).ToString () + " ช่องเก็บของ";
		} else { //In case of monster
			GameSparksManager.instance.AddMonster(reward);
			rewardText = "วัตถุดิบเพิ่มเลเวล";
			if (ss [0] == "fireegg") {
				rewardText = rewardText + "ธาตุไฟ";
			} else if (ss [0] == "wateregg") {
				rewardText = rewardText + "ธาตุน้ำ";
			} else if (ss [0] == "woodegg") {
				rewardText = rewardText + "ธาตุดิน";
			} else if (ss [0] == "lightegg") {
				rewardText = rewardText + "ธาตุแสง";
			} else if (ss [0] == "darkegg") {
				rewardText = rewardText + "ธาตุความมืด";
			}
		}

		messageDialog.transform.GetChild (0).GetComponent<Text> ().text = "ได้รับ " + rewardText;
		messageDialog.SetActive (true);

		GameSparksManager.instance.playerData.dailyQuestsToClaim.Remove (shortCode);
		if (!GameSparksManager.instance.playerData.dailyDungeonCompleted.Contains (shortCode)) {
			GameSparksManager.instance.playerData.dailyQuestsComplete.Add (shortCode);
		}
		GameSparksManager.instance.SavePlayerData ();
		
		UIManager.instance.UpdateCurrencyTexts ();
		UIManager.instance.UpdateStaminaText ();
	}

	void ClaimReward(Button b,string s){
		b.interactable = false;

		string[] split = s.Split (';');

		string[] ss = split[2].Split('.');

		string rewardText = "";

		if (ss [1] == "gems") {
			GameSparksManager.instance.playerData.gems += Int32.Parse (ss [0]);
			AnalyticsManager.instance.ReceiveGems ("quest", Int32.Parse (ss [0]));
			rewardText = Int32.Parse (ss [0]).ToString () + " เพชร";
		} else if (ss [1] == "gold") {
			GameSparksManager.instance.playerData.gold += Int32.Parse (ss [0]);
			rewardText = Int32.Parse (ss [0]).ToString() + " ทอง";
		} else if (ss [1] == "energy") {
			GameSparksManager.instance.playerData.stamina += Int32.Parse (ss [0]);
			rewardText = Int32.Parse (ss [0]).ToString() + " พลังงาน";
			//SaveLoad.Save ();
		} else if (ss [1] == "spaces") {
			GameSparksManager.instance.playerData.maxInventory += Int32.Parse (ss [0]);
			rewardText = Int32.Parse (ss [0]).ToString() + " ช่องเก็บของ";
		}

		messageDialog.transform.GetChild (0).GetComponent<Text> ().text = "ได้รับ " + rewardText;
		messageDialog.SetActive (true);

		//clear quest in unclaim list
		GameSparksManager.instance.playerData.unclaimAchievement.Remove(s);

		GameSparksManager.instance.SavePlayerData ();

		UIManager.instance.UpdateCurrencyTexts ();
		UIManager.instance.UpdateStaminaText ();

		//b.gameObject.SetActive (false);
		StartCoroutine(ButtonCollapse(b.gameObject,0.28f));

		AnalyticsManager.instance.QuestClaimLogEvent (s);

	}

	IEnumerator ButtonCollapse(GameObject g, float time){

		//Set all alpha channel to 0
		Transform[] childTransforms;
		childTransforms = g.GetComponentsInChildren<Transform> ();
		foreach (Transform t in childTransforms) {
			Text text = t.gameObject.GetComponent<Text> ();
			Image img = t.gameObject.GetComponent<Image> ();

			if (text) {
				text.color = new Color (1f, 1f, 1f, 0f);
			} else if (img) {
				img.color = new Color (1f, 1f, 1f, 0f);	
			}
		}

		//Reduce size
		RectTransform rt = g.GetComponent<RectTransform> ();
		float start = rt.sizeDelta.y;
		float end = 0f;

		float ti = 0;
		while (ti < time) {
			float height = Mathf.Lerp (start, end, ti / time);
			rt.sizeDelta = new Vector2 (rt.sizeDelta.x, height);
			ti += Time.deltaTime;
			yield return null;
		}

		g.SetActive (false);
	}
}
