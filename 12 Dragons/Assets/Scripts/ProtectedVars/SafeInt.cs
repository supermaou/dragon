﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct SafeInt {

	private int value;
	private int offset;

	public int Value{
		get{
			return this.value - this.offset;
		}
		set{
			offset = Random.Range (1, 1000);
			this.value = value + offset;
		}
	}

	public override string ToString(){
		return (this.value - this.offset).ToString ();
	}

	public SafeInt(int value = 0){
		offset = Random.Range (1, 1000);
		this.value = value + offset;
	}

	public static SafeInt operator ++(SafeInt i1) {
		return new SafeInt (i1.Value + 1);
	}

	public static SafeInt operator --(SafeInt i1) {
		return new SafeInt (i1.Value - 1);
	}

	public static SafeInt operator +(SafeInt i1, SafeInt i2) {
		return new SafeInt (i1.Value + i2.Value);
	}

	public static SafeInt operator +(SafeInt i1, int i2) {
		return new SafeInt (i1.Value + i2);
	}

	public static SafeInt operator -(SafeInt i1, SafeInt i2) {
		return new SafeInt (i1.Value - i2.Value);
	}

	public static SafeInt operator -(SafeInt i1, int i2) {
		return new SafeInt (i1.Value - i2);
	}

	public static SafeInt operator *(SafeInt i1, SafeInt i2) {
		return new SafeInt (i1.Value * i2.Value);
	}

	public static SafeInt operator /(SafeInt i1, SafeInt i2) {
		return new SafeInt (i1.Value / i2.Value);
	}

	public static bool operator >(SafeInt i1, SafeInt i2) {
		if (i1.Value > i2.Value) {
			return true;
		} else {
			return false;
		}
	}

	public static bool operator >(SafeInt i1, int i2) {
		if (i1.Value > i2) {
			return true;
		} else {
			return false;
		}
	}

	public static bool operator <(SafeInt i1, SafeInt i2) {
		if (i1.Value < i2.Value) {
			return true;
		} else {
			return false;
		}
	}

	public static bool operator <(SafeInt i1, int i2) {
		if (i1.Value < i2) {
			return true;
		} else {
			return false;
		}
	}

	public static bool operator >=(SafeInt i1, SafeInt i2) {
		if (i1.Value >= i2.Value) {
			return true;
		} else {
			return false;
		}
	}

	public static bool operator >=(SafeInt i1, int i2) {
		if (i1.Value >= i2) {
			return true;
		} else {
			return false;
		}
	}

	public static bool operator <=(SafeInt i1, SafeInt i2) {
		if (i1.Value <= i2.Value) {
			return true;
		} else {
			return false;
		}
	}

	public static bool operator <=(SafeInt i1, int i2) {
		if (i1.Value <= i2) {
			return true;
		} else {
			return false;
		}
	}
}
