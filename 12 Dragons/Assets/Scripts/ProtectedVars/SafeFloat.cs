﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct SafeFloat{

	private float value;
	private float offset;

	public float Value{
		get{
			return this.value - this.offset;
		}
		set{
			offset = Random.Range (1, 1000);
			this.value = value + offset;
		}
	}

	public override string ToString(){
		return (this.value - this.offset).ToString ();
	}

	public SafeFloat(float value = 0){
		offset = Random.Range (1, 1000);
		this.value = value + offset;
	}

	public static SafeFloat operator ++(SafeFloat i1) {
		return new SafeFloat (i1.Value + 1);
	}

	public static SafeFloat operator --(SafeFloat i1) {
		return new SafeFloat (i1.Value - 1);
	}

	public static SafeFloat operator +(SafeFloat i1, SafeFloat i2) {
		return new SafeFloat (i1.Value + i2.Value);
	}

	public static SafeFloat operator +(SafeFloat i1, int i2) {
		return new SafeFloat (i1.Value + i2);
	}

	public static SafeFloat operator -(SafeFloat i1, SafeFloat i2) {
		return new SafeFloat (i1.Value - i2.Value);
	}

	public static SafeFloat operator -(SafeFloat i1, int i2) {
		return new SafeFloat (i1.Value - i2);
	}

	public static SafeFloat operator *(SafeFloat i1, SafeFloat i2) {
		return new SafeFloat (i1.Value * i2.Value);
	}

	public static SafeFloat operator /(SafeFloat i1, SafeFloat i2) {
		return new SafeFloat (i1.Value / i2.Value);
	}

	public static bool operator >(SafeFloat i1, SafeFloat i2) {
		if (i1.Value > i2.Value) {
			return true;
		} else {
			return false;
		}
	}

	public static bool operator >(SafeFloat i1, int i2) {
		if (i1.Value > i2) {
			return true;
		} else {
			return false;
		}
	}

	public static bool operator <(SafeFloat i1, SafeFloat i2) {
		if (i1.Value < i2.Value) {
			return true;
		} else {
			return false;
		}
	}

	public static bool operator <(SafeFloat i1, int i2) {
		if (i1.Value < i2) {
			return true;
		} else {
			return false;
		}
	}

	public static bool operator >=(SafeFloat i1, SafeFloat i2) {
		if (i1.Value >= i2.Value) {
			return true;
		} else {
			return false;
		}
	}

	public static bool operator >=(SafeFloat i1, int i2) {
		if (i1.Value >= i2) {
			return true;
		} else {
			return false;
		}
	}

	public static bool operator <=(SafeFloat i1, SafeFloat i2) {
		if (i1.Value <= i2.Value) {
			return true;
		} else {
			return false;
		}
	}

	public static bool operator <=(SafeFloat i1, int i2) {
		if (i1.Value <= i2) {
			return true;
		} else {
			return false;
		}
	}
}
