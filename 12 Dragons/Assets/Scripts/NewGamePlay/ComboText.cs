﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ComboText : MonoBehaviour {

	private TextMeshProUGUI thisText;

	public float t = 0f;

	void Awake(){
		thisText = GetComponent<TextMeshProUGUI> ();
	}

	void OnEnable(){
		StartCoroutine (FadeOut (2f));
	}

	IEnumerator FadeOut(float time){
		t = 0f;
		float start = 1f;
		float end = 0f;

		while (t < time) {
			thisText.color = new Color (thisText.color.r, thisText.color.g, thisText.color.b, Mathf.Lerp (start, end, t / time));
			t += Time.deltaTime;
			yield return null;
		}

		gameObject.SetActive (false);
	}
}
