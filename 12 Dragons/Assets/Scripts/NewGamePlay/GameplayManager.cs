﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;
using UnityEngine.EventSystems;

public class GameplayManager : MonoBehaviour {

	public static GameplayManager instance = null;

	//reference in scene
	public GameObject gameCanvas;
	public Transform alliesGroup;
	public Transform cooldownTexts;
	public GameObject enemyDetailPopup;
	public GameObject menuDialog;
	public GameObject gameOverDialog;
	public GameObject notEnoughGemDialog;
	public GameObject skillDescDialog;
	public GameObject skillErrorDialog;

	public Transform buffColumn;

	public SpriteRenderer background;
	public MovingFloor floor;

	public Image totalHpGauge;
	public Text totalHpText;

	public GameObject clearText;
	public Image gameOverPanel;
	public GameObject emptyPanel;
	public TextMeshProUGUI gameOverText;
	public GameObject targetIcon;

	//item drop things
	public GameObject coinIcon;
	public Text coinText;
	public GameObject eggIcon;
	public Text eggText;

	public List<Button> monsterThumbnails = new List<Button> ();

	//world boss things
	public GameObject scoreKey;
	public Text scoreValue;
	public int cumulativeDamage = 0;

	//prefabs
	//hp prefabs
	public GameObject enemyHp1;
	public GameObject enemyHp2;
	public GameObject enemyHp3;

	//buff prefabs
	public GameObject buffElementPrefab;
	public GameObject buffTeamPrefab;

	//drop item prefab
	public GameObject eggPrefab;
	public GameObject coinSpin;

	//projectile prefab;
	public GameObject fireProjectile;
	public GameObject waterProjectile;
	public GameObject plantProjectile;
	public GameObject lightProjectile;
	public GameObject darkProjectile;

	//badges for cooldown
	public Sprite fireEmptyBadge;
	public Sprite waterEmptyBadge;
	public Sprite plantEmptyBadge;
	public Sprite lightEmptyBadge;
	public Sprite darkEmptyBadge;

	//badges for element
	public Sprite fireBadge;
	public Sprite waterBadge;
	public Sprite plantBadge;
	public Sprite lightBadge;
	public Sprite darkBadge;

	public GameObject damageTextPrefab;
	public Sprite shieldSprite;

	//data in game
	private int monstersInTeam = 0;
	public List<MonsterController> monsterStatus = new List<MonsterController>(); //monster status
	public List<List<MonsterData>> enemiesData = new List<List<MonsterData>>(); //all enemies data
	public List<GameObject> enemies = new List<GameObject> (); //all alive enemies
	public List<int> monsterCooldown = new List<int>();
	public GameObject currentEnemy;

	//in game variable
	public int totalHp = 0;
	public int currentHp;
	public int totalRCV = 0;
	public int currentWave = 1;
	float timeTemp = 0f; //for short and long tap detection
	public bool gameOver = false;

	//fix variables
	float[] projectilePosX = new float[]{ -2.35f, -1.41f, -0.47f, 0.47f, 1.41f, 2.35f };

	//sound variables
	public AudioClip clearSound;
	public AudioClip gameOverSound;
	public AudioClip warningSound;
	public AudioClip projectileSound;

	//skill effect variables
	public int goldBonusPercent = 0;
	public int dropChanceIncrease = 0;

	//Tutorial things
	public GameObject howtoplayTutorial;
	public GameObject skillTutorial;
	public GameObject enemyDetailTutorial;
	public GameObject targetTutorial;
	public GameObject elementTutorial;

	void Awake(){
		instance = this;

		if (PersistentData.instance.gameType == "worldBoss") {
			coinIcon.SetActive (false);
			coinText.gameObject.SetActive (false);
			eggIcon.SetActive (false);
			eggText.gameObject.SetActive (false);

			scoreKey.SetActive (true);
			scoreValue.text = cumulativeDamage.ToString ();
			scoreValue.gameObject.SetActive (true);
		}
	}

	void InitGame(){

		int backgroundType = 0;
		if (PersistentData.instance.currentDungeon != null) {
			int rand = UnityEngine.Random.Range(0,5);
			SoundManager.instance.PlayMusic (Resources.Load<AudioClip> ("Musics/BattleMusics/" + rand.ToString ()));

			backgroundType = PersistentData.instance.currentDungeon.background;
		} else {
			SoundManager.instance.PlayMusic (Resources.Load<AudioClip> ("Musics/BattleMusics/0"));

			backgroundType = UnityEngine.Random.Range (0, 5);
		}
		background.sprite = Resources.Load<Sprite>("Backgrounds/bg" + backgroundType.ToString());
		floor.gameObject.GetComponent<MeshRenderer> ().material.mainTexture = Resources.Load ("Floors/floor" + backgroundType.ToString ()) as Texture2D;

		enemiesData = PersistentData.instance.enemiesData;

		monstersInTeam = GameSparksManager.instance.teamData [GameSparksManager.instance.playerData.currentTeam].Count;

		//set team thumbnails
		for (int i = 0; i < GameSparksManager.instance.teamData [GameSparksManager.instance.playerData.currentTeam].Count; i++) {
			GameObject monGroup = alliesGroup.GetChild (i).gameObject;

			GameObject g = new GameObject ();
			g.transform.SetParent (monGroup.transform, false);
			g.transform.localPosition = new Vector3 (0f, 0f, 0f);

			g.AddComponent<ScaleUpAndDown> ();

			Image img = g.AddComponent<Image> ();
			img.sprite = MonsterList.instance.GetThumbnail(GameSparksManager.instance.teamData[GameSparksManager.instance.playerData.currentTeam][i].shortCode);

			MonsterController m = g.AddComponent<MonsterController> ();
			m.CloneMonsterData(GameSparksManager.instance.teamData[GameSparksManager.instance.playerData.currentTeam][i]);
			m.orderInGame = i;
			monsterStatus.Add (m);

			totalHp += m.hitPoint.Value; //Add hitpoint
			totalRCV += m.recovery.Value; //Add RCV // defense == RCV for now

			//add thumbnail onclick event
			Button b = g.AddComponent<Button>();
			b.onClick.AddListener (() => ShowSkillDialog (m));

			ColorBlock newColorBlock = b.colors;
			newColorBlock.disabledColor = new Color (1f, 1f, 1f);
			b.colors = newColorBlock;

			monsterThumbnails.Add (b);

			//set cooldown badge;
			if(m.element == "fire"){
				cooldownTexts.GetChild (i).GetComponent<Image> ().sprite = fireEmptyBadge;
			}else if(m.element == "water"){
				cooldownTexts.GetChild (i).GetComponent<Image> ().sprite = waterEmptyBadge;
			}else if(m.element == "plant"){
				cooldownTexts.GetChild (i).GetComponent<Image> ().sprite = plantEmptyBadge;
			}else if(m.element == "light"){
				cooldownTexts.GetChild (i).GetComponent<Image> ().sprite = lightEmptyBadge;
			}else if(m.element == "dark"){
				cooldownTexts.GetChild (i).GetComponent<Image> ().sprite = darkEmptyBadge;
			}

			if (m.cooldown.Value == 0 || m.cooldown.Value >= 100) {
				cooldownTexts.GetChild (i).GetComponent<Image> ().color = new Color (1f, 1f, 1f, 0f);
				cooldownTexts.GetChild (i).GetChild (0).gameObject.SetActive (false);
				monsterCooldown.Add (10000);
			} else {
				cooldownTexts.GetChild (i).GetChild (0).GetComponent<Text> ().text = (m.cooldown - m.skillLevel + 1).ToString ();
				monsterCooldown.Add (m.cooldown.Value - m.skillLevel.Value + 1);
			}
		}

		for (int i = GameSparksManager.instance.teamData [GameSparksManager.instance.playerData.currentTeam].Count; i < 6; i++) {
			GameObject g = alliesGroup.GetChild (i).gameObject;
			Image img = g.GetComponent<Image> ();

			img.color = new Color (1f, 1f, 1f, 0f);

			//set cooldown badge alpha to 0;
			cooldownTexts.GetChild (i).GetComponent<Image> ().color = new Color(1f,1f,1f,0f);
			cooldownTexts.GetChild(i).GetChild(0).GetComponent<Text>().color = new Color(1f,1f,1f,0f);
		}

		//Set hp section
		totalHpText.text = totalHp.ToString() + "/" + totalHp.ToString ();
		currentHp = totalHp;

		//Move floor
		floor.Move ();
	}

	IEnumerator Start(){

		InitGame ();

		yield return null;

		for (int i = 0; i < GameSparksManager.instance.teamData[GameSparksManager.instance.playerData.currentTeam].Count; i++) {
			Image img = alliesGroup.GetChild (i).GetChild(0).GetComponent<Image> ();
			SetSpriteToOriginalSize (img, 150f);
		}

		//Trigger leader skill
		monsterStatus[0].leaderSkillBehavior.TriggerAbility (monsterStatus[0]);

	}

	void OnDisable(){
		SoundManager.instance.MuteMusic ();
		Resources.UnloadUnusedAssets ();
	}

	void SetSpriteToOriginalSize(Image img, float target){
		float width = img.sprite.rect.width;
		float height = img.sprite.rect.height;

		if (height > width) {
			float multiplier = target / height;

			img.rectTransform.sizeDelta = new Vector2 (width * multiplier, target);
		} else {
			float multiplier = target / width;

			img.rectTransform.sizeDelta = new Vector2 (target, height * multiplier);
		}
	}

	void ShowSkillDialog(MonsterController m){
		//Check for cooldown reach 0
		int index = 0;
		for (int i = 0; i < monsterStatus.Count; i++) {
			if (monsterStatus [i] == m) {
				index = i;
				break;
			}
		}

		if (monsterCooldown [index] > 0) {
			return;
		}

		skillDescDialog.SetActive (true);

		skillDescDialog.transform.GetChild (0).GetComponent<Text> ().text = m.activeSkillBehavior.aDescription;

		Button b = skillDescDialog.transform.GetChild (1).GetComponent<Button> ();
		b.onClick.RemoveAllListeners ();
		b.onClick.AddListener (() => UseSkill (m));
	}

	void UseSkill(MonsterController m){
		skillDescDialog.SetActive (false);

		int index = 0;
		for (int i = 0; i < monsterStatus.Count; i++) {
			if (monsterStatus [i].objectId == m.objectId) {
				index = i;
				break;
			}
		}

		if (monsterCooldown [index] > 0) {
			return;
		}

		m.activeSkillBehavior.TriggerAbility (m);
	}

	public void OnSkillError(){
		skillErrorDialog.SetActive (true);
	}

	public void ResetCooldown(MonsterController m){
		int index = 0;
		for (int i = 0; i < monsterStatus.Count; i++) {
			if (monsterStatus [i].objectId == m.objectId) {
				index = i;
				break;
			}
		}

		monsterCooldown [index] = m.cooldown.Value - m.skillLevel.Value + 1;
		cooldownTexts.GetChild (index).GetChild (0).GetComponent<Text> ().text = (m.cooldown - m.skillLevel + 1).ToString ();
		cooldownTexts.GetChild (index).GetChild (0).GetComponent<ScaleUpAndDown> ().isActive = false;
		monsterThumbnails [index].GetComponent<ScaleUpAndDown> ().isActive = false;
	}

	public IEnumerator SpawnEnemies(){

		float midRef = ((float) enemiesData[currentWave - 1].Count)/2f - 0.5f;

		//position the enemy in current wave
		int a = 0;
		foreach(MonsterData data in enemiesData[currentWave - 1]){
			GameObject e = Instantiate (MonsterList.instance.GetPrefab(data.shortCode));
			if (enemiesData [currentWave - 1].Count == 3 || enemiesData [currentWave - 1].Count == 1) { //in case of 1 or 3
				e.transform.localPosition = new Vector3 ((a - midRef) * 1.6f, 2f, 0f);
			} else { //In case of 2
				e.transform.localPosition = new Vector3 ((a - midRef) * 2.4f, 2f, 0f);
			}
			enemies.Add (e);
			e.tag = "enemy";
			Enemy enemyScript = e.AddComponent<Enemy> ();

			//add box collider
			BoxCollider2D bc2d = e.AddComponent<BoxCollider2D> ();
			bc2d.offset = new Vector2 (0f, 500f);
			bc2d.size = new Vector2 (1000f, 1000f);

			//set stat
			MonsterController m = e.AddComponent<MonsterController> ();
			m.CloneMonsterData (data);
			enemyScript.SetCurrentHp ();
			a++;
		}

		yield return new WaitForSeconds (0.1f);

		SearchNewEnemy ();

		//Spawn enemy hp bar
		if (enemies.Count == 1) {
			foreach (GameObject e in enemies) {
				GameObject hpBar = Instantiate (enemyHp1, gameCanvas.transform, false);
				hpBar.transform.position = e.transform.position - new Vector3 (0f, 0.2f, 0f);
				Enemy eScript = e.GetComponent<Enemy> ();
				eScript.hpLeft = hpBar.transform.GetChild (0).gameObject.GetComponent<Image> ();
				StartCoroutine (eScript.FadeIn (hpBar, 1f));

				//set cooldown badge
				MonsterController m = e.GetComponent<MonsterController> ();

				if (m.element == "fire") {
					hpBar.transform.GetChild (1).GetComponent<Image> ().sprite = fireEmptyBadge;
				} else if (m.element == "water") {
					hpBar.transform.GetChild (1).GetComponent<Image> ().sprite = waterEmptyBadge;
				} else if (m.element == "plant") {
					hpBar.transform.GetChild (1).GetComponent<Image> ().sprite = plantEmptyBadge;
				} else if (m.element == "light") {
					hpBar.transform.GetChild (1).GetComponent<Image> ().sprite = lightEmptyBadge;
				} else if (m.element == "dark") {
					hpBar.transform.GetChild (1).GetComponent<Image> ().sprite = darkEmptyBadge;
				}

				hpBar.transform.GetChild (1).GetChild (0).GetComponent<Text> ().text = eScript.cooldown.ToString ();
			}
		} else if (enemies.Count == 2) {
			foreach (GameObject e in enemies) {
				GameObject hpBar = Instantiate (enemyHp2, gameCanvas.transform, false);
				hpBar.transform.position = e.transform.position - new Vector3 (0f, 0.2f, 0f);
				Enemy eScript = e.GetComponent<Enemy> ();
				eScript.hpLeft = hpBar.transform.GetChild (0).gameObject.GetComponent<Image> ();
				StartCoroutine (eScript.FadeIn (hpBar, 1f));

				//set cooldown badge
				MonsterController m = e.GetComponent<MonsterController> ();

				if (m.element == "fire") {
					hpBar.transform.GetChild (1).GetComponent<Image> ().sprite = fireEmptyBadge;
				} else if (m.element == "water") {
					hpBar.transform.GetChild (1).GetComponent<Image> ().sprite = waterEmptyBadge;
				} else if (m.element == "plant") {
					hpBar.transform.GetChild (1).GetComponent<Image> ().sprite = plantEmptyBadge;
				} else if (m.element == "light") {
					hpBar.transform.GetChild (1).GetComponent<Image> ().sprite = lightEmptyBadge;
				} else if (m.element == "dark") {
					hpBar.transform.GetChild (1).GetComponent<Image> ().sprite = darkEmptyBadge;
				}

				hpBar.transform.GetChild (1).GetChild (0).GetComponent<Text> ().text = eScript.cooldown.ToString ();
			}
		} else if (enemies.Count == 3) {
			foreach (GameObject e in enemies) {
				GameObject hpBar = Instantiate (enemyHp3, gameCanvas.transform, false);
				hpBar.transform.position = e.transform.position - new Vector3 (0f, 0.2f, 0f);
				Enemy eScript = e.GetComponent<Enemy> ();
				eScript.hpLeft = hpBar.transform.GetChild (0).gameObject.GetComponent<Image> ();
				StartCoroutine (eScript.FadeIn (hpBar, 1f));

				//set cooldown badge
				MonsterController m = e.GetComponent<MonsterController> ();

				if (m.element == "fire") {
					hpBar.transform.GetChild (1).GetComponent<Image> ().sprite = fireEmptyBadge;
				} else if (m.element == "water") {
					hpBar.transform.GetChild (1).GetComponent<Image> ().sprite = waterEmptyBadge;
				} else if (m.element == "plant") {
					hpBar.transform.GetChild (1).GetComponent<Image> ().sprite = plantEmptyBadge;
				} else if (m.element == "light") {
					hpBar.transform.GetChild (1).GetComponent<Image> ().sprite = lightEmptyBadge;
				} else if (m.element == "dark") {
					hpBar.transform.GetChild (1).GetComponent<Image> ().sprite = darkEmptyBadge;
				}

				hpBar.transform.GetChild (1).GetChild (0).GetComponent<Text> ().text = eScript.cooldown.ToString ();
			}
		}

		//reduce cooldown
		if (!BoardManager.instance.isPlayerTurn) {
			for (int i = 0; i < monsterCooldown.Count; i++) {
				monsterCooldown [i]--;
				if (monsterCooldown [i] > 0) {
					cooldownTexts.GetChild (i).GetChild (0).GetComponent<Text> ().text = monsterCooldown [i].ToString ();
				} else {
					cooldownTexts.GetChild (i).GetChild (0).GetComponent<Text> ().text = "OK";
					cooldownTexts.GetChild (i).GetChild (0).GetComponent<ScaleUpAndDown> ().isActive = true;
					monsterThumbnails [i].GetComponent<ScaleUpAndDown> ().isActive = true;

					if (PlayerPrefs.GetInt ("skillTutorial", 0) == 0) {
						PlayerPrefs.SetInt("skillTutorial",1);
						skillTutorial.SetActive (true);
					}
				}
			}
		}

		if (!BoardManager.instance.CheckMatchPossible ()) {
			print ("no more match");
			BoardManager.instance.Reboard ();
		}

		//set player turn
		BoardManager.instance.isPlayerTurn = true;
		StartCoroutine (BoardManager.instance.GuideTimer ());
		monsterStatus [0].leaderSkillBehavior.TriggerAbility (monsterStatus [0]);
	
		//set monster thumbnails as active
		for (int i = 0; i < monsterThumbnails.Count; i++) {
			monsterThumbnails [i].interactable = true;
		}
	}

	void TargetEnemy(GameObject target){
		currentEnemy = target;
	}

	void ManuallyTargetEnemy(GameObject target){
		currentEnemy = target;

		targetIcon.SetActive (true);
		targetIcon.transform.position = new Vector3 (currentEnemy.transform.position.x, currentEnemy.transform.position.y - 0.5f, currentEnemy.transform.position.z);
	}

	IEnumerator EndWave(){
		yield return new WaitForSeconds (0.5f);
		Resources.UnloadUnusedAssets ();

		targetIcon.SetActive (false); //remove target icon if available
		floor.Move ();

		if (currentWave == enemiesData.Count) {
			floor.ShowWarning ();
		}
	}

	public void SearchNewEnemy(){
		List<Enemy> enemyList = new List<Enemy> ();

		foreach (GameObject e in enemies) {
			enemyList.Add (e.GetComponent<Enemy> ());
		}

		if (enemies.Count > 0) {
			Enemy target = enemyList [0];

			foreach (Enemy e in enemyList) {
				if (e.currentHp < target.currentHp) {
					target = e;
				}
			}

			TargetEnemy (target.gameObject);
		} else {
			if (currentWave < enemiesData.Count) {
				//End of the wave

				//set skill buttons inactive
				for (int i = 0; i < monstersInTeam; i++) {
					monsterThumbnails [i].interactable = false;
				}

				currentWave++;
				StartCoroutine (EndWave ());
			} else {
				//game complete

				//set skill buttons inactive
				for (int i = 0; i < monstersInTeam; i++) {
					monsterThumbnails [i].interactable = false;
				}

				gameOver = true;
				StartCoroutine (GameClear ());

				SoundManager.instance.PlayEffect (clearSound);
			}
		}

	}

	IEnumerator GameClear(){
		SoundManager.instance.MuteMusic ();

		clearText.SetActive (true);

		float time = 0.3f;
		//scale text down
		float t = 0;
		float start = 3f;
		float end = 1f;

		while (t < time) {
			float scale = Mathf.Lerp (start, end, t / time);
			clearText.transform.localScale = new Vector3 (scale, scale, 1f);
			t += Time.deltaTime;
			yield return null;
		}

		clearText.transform.localScale = new Vector3 (1f, 1f, 1f);

		yield return new WaitForSeconds (3f);

		if (PersistentData.instance.gameType == "dungeon") {
			Initiate.Fade ("reward", new Color (0.1f, 0.1f, 0.1f), 1.5f);
		} else if (PersistentData.instance.gameType == "arena") {
			PersistentData.instance.arenaResult = "win";
			Initiate.Fade ("arenaResult", new Color (0.1f, 0.1f, 0.1f), 1.5f);
		} else if (PersistentData.instance.gameType == "tower") {
			//Give reward to player
			string reward = PersistentData.instance.currentDungeon.reward;
			GameSparksManager.instance.playerData.tower += 1;
			ReceiveReward (reward);

			Initiate.Fade ("main", new Color (0.1f, 0.1f, 0.1f), 1.5f);
		}
	}

	void ReceiveReward(string shortCode){
		string[] s = shortCode.Split ('.');

		if (s [1] == "gems") {
			GameSparksManager.instance.playerData.gems += Int32.Parse (s [0]);
			AnalyticsManager.instance.ReceiveGems ("tower dungeon", Int32.Parse (s [0]));
		} else if (s [1] == "gold") {
			GameSparksManager.instance.playerData.gold += Int32.Parse (s [0]);
		} else if (s [1] == "energy") {
			GameSparksManager.instance.playerData.stamina += Int32.Parse (s [0]);
			//SaveLoad.Save ();
		} else if (s [1] == "spaces") {
			GameSparksManager.instance.playerData.maxInventory += Int32.Parse (s [0]);
		} else { //In case of monster
			GameSparksManager.instance.AddMonster(shortCode);
		}

		GameSparksManager.instance.SavePlayerData ();
	}

	public void SpawnDamageText(Vector3 position,int damage,string element){
		GameObject g = Instantiate (damageTextPrefab, position + new Vector3 (0f, 1.5f, 0f), Quaternion.identity);
		g.transform.SetParent (gameCanvas.transform, false);
		Text text = g.GetComponent<Text> ();
		text.color = BoardManager.instance.damageTextColor [element];
		text.text = damage.ToString ();
		g.GetComponent<DamageText> ().MoveUp ();
	}

	public IEnumerator PlayerAttack(){
		yield return new WaitForSeconds (0.5f); //slow it a bit

		//check if heal
		if (BoardManager.instance.elementDamages ["heart"] > 0) {
			Heal (Mathf.RoundToInt (BoardManager.instance.elementDamages ["heart"] * totalRCV / 100f));
		}

		for (int i = 0; i < monsterStatus.Count; i++) {
			if (BoardManager.instance.elementDamages [monsterStatus [i].element] <= 0) {
				continue;
			}

			if (enemies.Count == 0) {

				EndTurnTask ();

				yield break;
			}

			//Launch projectile toward enemy (s)
			if (BoardManager.instance.isAttackAll[monsterStatus[i].element] == false) { //target one enemy

				Vector3 pos = new Vector3 (projectilePosX [i], 1f, 0f);
				GameObject projectile = fireProjectile;

				if (monsterStatus [i].element == "fire") {
					projectile = fireProjectile;
				} else if (monsterStatus [i].element == "water") {
					projectile = waterProjectile;
				} else if (monsterStatus [i].element == "plant") {
					projectile = plantProjectile;
				} else if (monsterStatus [i].element == "light") {
					projectile = lightProjectile;
				} else if (monsterStatus [i].element == "dark") {
					projectile = darkProjectile;
				}

				projectile = Instantiate (projectile, new Vector3 (pos.x, pos.y, 0f), Quaternion.identity) as GameObject;
				Vector3 enemyPos = currentEnemy.transform.position;

				Vector3 target = Quaternion.Euler (-70f, 0f, 0f) * (enemyPos + new Vector3 (0f, 6f, 0f));
				int damage = Mathf.RoundToInt (BoardManager.instance.elementDamages [monsterStatus [i].element] * monsterStatus [i].attack.Value / 100f);

				int elementalAdvantage = ElementalBonus (monsterStatus [i], currentEnemy.GetComponent<MonsterController> ());
				if (elementalAdvantage == 1) { //check for elemental advantage
					damage = Mathf.RoundToInt (damage * 1.5f);
				} else if (elementalAdvantage == -1) { //check for elemental advantage
					damage = Mathf.RoundToInt (damage * 0.5f);
				}
				StartCoroutine (SmoothMovementProjectile (currentEnemy, projectile, monsterStatus [i].element, target, damage, 0.2f));

			} else { //Target all enemies

				for (int a = 0; a < enemies.Count; a++) {
					Vector3 pos = new Vector3 (projectilePosX [i], 1f, 0f);
					GameObject projectile = fireProjectile;

					if (monsterStatus [i].element == "fire") {
						projectile = fireProjectile;
					} else if (monsterStatus [i].element == "water") {
						projectile = waterProjectile;
					} else if (monsterStatus [i].element == "plant") {
						projectile = plantProjectile;
					} else if (monsterStatus [i].element == "light") {
						projectile = lightProjectile;
					} else if (monsterStatus [i].element == "dark") {
						projectile = darkProjectile;
					}

					projectile = Instantiate (projectile, new Vector3 (pos.x, pos.y, 0f), Quaternion.identity) as GameObject;
					Vector3 enemyPos = enemies[a].transform.position;

					Vector3 target = Quaternion.Euler (-70f, 0f, 0f) * (enemyPos + new Vector3 (0f, 6f, 0f));
					int damage = Mathf.RoundToInt (BoardManager.instance.elementDamages [monsterStatus [i].element] * monsterStatus [i].attack.Value / 100f);

					int elementalAdvantage = ElementalBonus (monsterStatus [i], enemies[a].GetComponent<MonsterController> ());
					if (elementalAdvantage == 1) { //check for elemental advantage
						damage = Mathf.RoundToInt (damage * 1.5f);
					} else if (elementalAdvantage == -1) { //check for elemental advantage
						damage = Mathf.RoundToInt (damage * 0.5f);
					}
					StartCoroutine (SmoothMovementProjectile (enemies[a], projectile, monsterStatus [i].element, target, damage, 0.2f));
				}

			}

			if (enemies.Count == 0) {

				EndTurnTask ();

				yield break;
			}

			yield return new WaitForSeconds (0.5f);
		}

		EndTurnTask ();

		//all attack is complete and there are enemies alive
		for (int i = 0; i < enemies.Count; i++) {
			if (enemies [i].GetComponent<Enemy> ().CheckIfAttack ()) {
				yield return new WaitForSeconds (0.5f); //wait while an enemy attacking
			}
		}

		if (gameOver == true) { //check if gameover
			yield break;
		}

		//reduce cooldown
		if (!BoardManager.instance.isPlayerTurn) {
			for (int i = 0; i < monsterCooldown.Count; i++) {
				monsterCooldown [i]--;
				if (monsterCooldown[i] > 0) {
					cooldownTexts.GetChild (i).GetChild (0).GetComponent<Text> ().text = monsterCooldown [i].ToString ();
				} else {
					cooldownTexts.GetChild (i).GetChild (0).GetComponent<Text> ().text = "OK";
					cooldownTexts.GetChild (i).GetChild (0).GetComponent<ScaleUpAndDown> ().isActive = true;
					monsterThumbnails [i].GetComponent<ScaleUpAndDown> ().isActive = true;

					if (PlayerPrefs.GetInt ("skillTutorial", 0) == 0) {
						PlayerPrefs.SetInt ("skillTutorial", 1);
						skillTutorial.SetActive (true);
					}
				}
			}
		}

		if (!BoardManager.instance.CheckMatchPossible ()) {
			print ("no more match");
			BoardManager.instance.Reboard ();
		}

		//Set turn to player
		BoardManager.instance.isPlayerTurn = true;
		StartCoroutine (BoardManager.instance.GuideTimer ());
		monsterStatus [0].leaderSkillBehavior.TriggerAbility (monsterStatus [0]);
	}

	void EndTurnTask(){
		//collect element keys
		List<string> keys = new List<string>();
		foreach (string key in BoardManager.instance.elementDamages.Keys) {
			keys.Add (key);
		}

		//reduce turnBuff
		List<int> indexesToRemove = new List<int>();
		for (int i = 0; i < BoardManager.instance.buffAttacks.Count; i++) {
			BoardManager.instance.buffAttacks [i].turnBuff--;

			if (BoardManager.instance.buffAttacks [i].elementBuff == "") {
				BoardManager.instance.buffAttacks [i].icon.transform.GetChild (0).GetComponent<Text> ().text = BoardManager.instance.buffAttacks [i].turnBuff.ToString ();
			} else {
				BoardManager.instance.buffAttacks [i].icon.transform.GetChild (2).GetComponent<Text> ().text = BoardManager.instance.buffAttacks [i].turnBuff.ToString ();
			}

			if (BoardManager.instance.buffAttacks[i].turnBuff <= 0) {
				for (int j = 0; j < keys.Count; j++) {
					if (BoardManager.instance.buffAttacks [i].elementBuff == keys [j] || BoardManager.instance.buffAttacks [i].elementBuff == "") {
						BoardManager.instance.buffAttacksTemporary [keys [j]] /= BoardManager.instance.buffAttacks [i].multiplier;
					}
				}

				Destroy (BoardManager.instance.buffAttacks [i].icon);

				indexesToRemove.Add (i);
			}
		}
		indexesToRemove.Sort ();
		for (int i = indexesToRemove.Count - 1; i >= 0; i--) {
			BoardManager.instance.buffAttacks.RemoveAt (indexesToRemove[i]);
		}

		indexesToRemove.Clear ();

		for (int i = 0; i < BoardManager.instance.buffDefenses.Count; i++) {
			BoardManager.instance.buffDefenses [i].turnBuff--;

			if (BoardManager.instance.buffDefenses [i].elementBuff == "") {
				BoardManager.instance.buffDefenses [i].icon.transform.GetChild (0).GetComponent<Text> ().text = BoardManager.instance.buffDefenses [i].turnBuff.ToString ();
			} else {
				BoardManager.instance.buffDefenses [i].icon.transform.GetChild (2).GetComponent<Text> ().text = BoardManager.instance.buffDefenses [i].turnBuff.ToString ();
			}

			if (BoardManager.instance.buffDefenses [i].turnBuff <= 0) {
				for (int j = 0; j < keys.Count; j++) {
					if (BoardManager.instance.buffDefenses [i].elementBuff == keys [j] || BoardManager.instance.buffDefenses [i].elementBuff == "") {
						BoardManager.instance.buffDefensesTemporary [keys [j]] /= (1f - (BoardManager.instance.buffDefenses[i].damageReduction / 100f));
					}
				}

				Destroy (BoardManager.instance.buffDefenses [i].icon);

				indexesToRemove.Add (i);
			}
		}
		indexesToRemove.Sort ();
		for (int i = indexesToRemove.Count - 1; i >= 0; i--) {
			BoardManager.instance.buffDefenses.RemoveAt (indexesToRemove[i]);
		}
			
		for (int j = 0; j < keys.Count; j++) {
			BoardManager.instance.elementDamages [keys [j]] = 0; //reset element damages
			BoardManager.instance.runeTypesCount [keys [j]] = 0; //reset rune count

			BoardManager.instance.isAttackAll [keys [j]] = false; //reset isAttackAll
		}

		//reset damage texts
		for (int j = 0; j < monsterStatus.Count; j++) {
			Text damageText = BoardManager.instance.damageTexts.GetChild(j).GetComponent<Text>();
			Color c = damageText.color;
			c.a = 0f;
			damageText.color = c;
			damageText.text = "0";
		}

		BoardManager.instance.healText.text = "0";
		BoardManager.instance.healText.gameObject.SetActive (false);

		StartCoroutine (PositionBuffIcon ());
	}

	IEnumerator PositionBuffIcon(){
		yield return new WaitForSeconds (0.1f);

		for (int i = 0; i < buffColumn.childCount; i++) {
			buffColumn.GetChild(i).localPosition = new Vector3 (-25f, 300f - ((i + 1) * 100f), 0f);
			print (300f - ((i + 1) * 100f));
		}
	}

	//SmoothMovement projectile for player
	IEnumerator SmoothMovementProjectile(GameObject enemyTarget,GameObject projectile,string element,Vector3 end,int damage,float time){
		SoundManager.instance.PlayEffect (projectileSound);

		float t = 0;
		Vector3 startPos = projectile.transform.localPosition;

		while (t < time) {
			t += Time.deltaTime;
			projectile.transform.localPosition = Vector3.Lerp (startPos, end, t/time);
			yield return null;
		}

		if (enemies.Count == 0) {
			yield break;
		}

		//projectile reach the target

		if (element == "fire") {
			Instantiate (BoardManager.instance.fireExplode, enemyTarget.transform.position + new Vector3 (0f, 1f, 0f), Quaternion.identity);
		}else if (element == "water") {
			Instantiate (BoardManager.instance.waterExplode, enemyTarget.transform.position + new Vector3 (0f, 1f, 0f), Quaternion.identity);
		}else if (element == "plant") {
			Instantiate (BoardManager.instance.plantExplode, enemyTarget.transform.position + new Vector3 (0f, 1f, 0f), Quaternion.identity);
		}else if (element == "light") {
			Instantiate (BoardManager.instance.lightExplode, enemyTarget.transform.position + new Vector3 (0f, 1f, 0f), Quaternion.identity);
		}else if (element == "dark") {
			Instantiate (BoardManager.instance.darkExplode, enemyTarget.transform.position + new Vector3 (0f, 1f, 0f), Quaternion.identity);
		}

		enemyTarget.GetComponent<Enemy> ().CalculateDamage (damage, element);

		//SpawnDamageText (enemyTarget.transform.position, damage, element);
	}

	public IEnumerator EnemyAttack(MonsterController m){

		Enemy e = m.gameObject.GetComponent<Enemy> ();

		int damage = m.attack.Value + e.buffAttack;
		int target = UnityEngine.Random.Range (0, monstersInTeam);

		//apply damage reduction
		damage = Mathf.RoundToInt(damage * BoardManager.instance.buffDefensePermanent[m.element] * BoardManager.instance.buffDefensesTemporary[m.element]);

		if (damage < 0) {
			damage = 0;
		}
			
		print ("Player was dealed " + damage.ToString () + " damage");

		//Play animation
		TestAnimation anim = m.gameObject.transform.GetChild(0).GetComponent<TestAnimation>();
		anim.Attack ();

		yield return new WaitForSeconds (0.5f);

		//Spawn projectile
		GameObject projectile = fireProjectile;
		if (m.element == "fire") {
			projectile = Instantiate (fireProjectile, m.transform.position, Quaternion.identity);
		} else if (m.element == "water") {
			projectile = Instantiate (waterProjectile, m.transform.position, Quaternion.identity);
		} else if (m.element == "plant") {
			projectile = Instantiate (plantProjectile, m.transform.position, Quaternion.identity);
		} else if (m.element == "light") {
			projectile = Instantiate (lightProjectile, m.transform.position, Quaternion.identity);
		} else if (m.element == "dark") {
			projectile = Instantiate (darkProjectile, m.transform.position, Quaternion.identity);
		}

		Vector3 pos = Quaternion.Euler (-70f, 0f, 0f) * (new Vector3 (projectilePosX [target], 1f, 0f));
		StartCoroutine (SmoothMovementProjectileEnemy (projectile, pos,damage,0.2f));

		yield break;
	}

	IEnumerator SmoothMovementProjectileEnemy(GameObject g,Vector3 end,int damage,float time){
		SoundManager.instance.PlayEffect (projectileSound);

		float t = 0;
		Vector3 startPos = g.transform.localPosition;

		while (t < time) {
			t += Time.deltaTime;
			g.transform.localPosition = Vector3.Lerp (startPos, end, t/time);
			yield return null;
		}

		currentHp -= damage;
		if (currentHp < 0) {
			currentHp = 0;

			//Check for notdie skill
			if (GameplayManager.instance.monsterStatus [0].leaderSkill != null) {
				if (monsterStatus [0].leaderSkill.GetString ("type") == "NotDieOneTime") {
					monsterStatus [0].leaderSkillBehavior.TriggerAbility (monsterStatus [0]);
				}
			}
		}

		float length = (float)currentHp / (float)totalHp;
		StartCoroutine (ChangeWidth (totalHpGauge, totalHpGauge.fillAmount, length, 0.3f));
		totalHpText.text = currentHp.ToString() + "/" + totalHp.ToString();

		if (currentHp > 0) {
			yield break;
		} else {
			yield return new WaitForSeconds (0.3f);
			gameOver = true;

			if (PersistentData.instance.gameType == "dungeon" || PersistentData.instance.gameType == "tower") {
				gameOverDialog.SetActive (true);
				emptyPanel.SetActive (true);
			} else if (PersistentData.instance.gameType == "worldBoss") {
				StartCoroutine (GameOver ());
			}
		}

	}

	IEnumerator GameOver(){

		SoundManager.instance.MuteMusic ();

		SoundManager.instance.PlayEffect (gameOverSound);
		gameOverPanel.gameObject.SetActive (true);
		gameOverText.gameObject.SetActive (true);

		float time = 2f;

		//fade in gameOver text;
		float t = 0;
		Color start = new Color (gameOverText.color.r, gameOverText.color.g, gameOverText.color.b, 0f);
		Color end = new Color (gameOverText.color.r, gameOverText.color.g, gameOverText.color.b, 1f);

		Color panelStart = new Color (gameOverPanel.color.r, gameOverPanel.color.g, gameOverPanel.color.b, 0f);
		Color panelEnd = new Color (gameOverPanel.color.r, gameOverPanel.color.g, gameOverPanel.color.b, 100f / 255f);

		while (t < time) {
			gameOverPanel.color = Color.Lerp (panelStart, panelEnd, t / time);
			gameOverText.color = Color.Lerp (start, end, t / time);
			t += Time.deltaTime;
			yield return null;
		}

		yield return new WaitForSeconds (3f);

		PersistentData.instance.playCount++;

		if (PersistentData.instance.gameType == "dungeon" || PersistentData.instance.gameType == "tower") {
			PersistentData.instance.latestResult = "lose";
			Initiate.Fade ("main", new Color (0.1f, 0.1f, 0.1f), 1.5f);
		} else if (PersistentData.instance.gameType == "arena") {
			PersistentData.instance.arenaResult = "lose";
			Initiate.Fade ("arenaResult", new Color (0.1f, 0.1f, 0.1f), 1.5f);
		} else if (PersistentData.instance.gameType == "worldBoss") {
			PersistentData.instance.worldBossScore = cumulativeDamage;
			Initiate.Fade ("worldBossResult", new Color (0.1f, 0.1f, 0.1f), 1.5f);
		}
	}

	IEnumerator ChangeWidth(Image img,float start,float target,float time){
		float t = 0;

		while (t < time) {
			t += Time.deltaTime;
			img.fillAmount = Mathf.Lerp (start, target, t / time);
			yield return null;
		}
	}

	int ElementalBonus(MonsterController self,MonsterController target){ //1 for advantage, 0 for normal, -1 for disadvantage
		if (self.element == "fire") {
			if (target.element == "plant") {
				return 1;
			} else if (target.element == "water") {
				return -1;
			}
		} else if (self.element == "water") {
			if (target.element == "fire") {
				return 1;
			} else if (target.element == "plant") {
				return -1;
			}
		} else if (self.element == "plant") {
			if (target.element == "water") {
				return 1;
			} else if (target.element == "fire") {
				return -1;
			}
		} else if (self.element == "light") {
			if (target.element == "dark") {
				return 1;
			}
		} else if (self.element == "dark") {
			if (target.element == "light") {
				return 1;
			}
		}

		return 0;
	}

	private bool IsPointerOverUIObject() {
		PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
		eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
		List<RaycastResult> results = new List<RaycastResult>();
		EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
		return results.Count > 0;
	}

	//enemy popup code
	void Update(){
		if (Input.GetMouseButtonDown (0)) {
			timeTemp = Time.time;

			if (enemyDetailPopup.activeSelf == true) {
				StartCoroutine (CloseEnemyPopup ());
			}
		}

		if (Input.GetMouseButtonUp (0) && Time.time - timeTemp < .5f) {
			//target enemy
			Vector2 inputPos = Camera.main.ScreenToWorldPoint (Input.mousePosition);
			RaycastHit2D[] touches = Physics2D.RaycastAll (inputPos, inputPos, 0.5f);
			if (touches.Length > 0) {
				var hit = touches [0];
				if (hit.transform != null) {
					if (hit.transform.gameObject.tag == "enemy") {
						if (!IsPointerOverUIObject()) {
							GameplayManager.instance.ManuallyTargetEnemy (hit.transform.gameObject);
						}
					}
				}
			}

			return;
		}

		if (Input.GetMouseButton(0) && Time.time - timeTemp >= .5f && enemyDetailPopup.gameObject.activeSelf == false) {
			Vector2 inputPos = Camera.main.ScreenToWorldPoint (Input.mousePosition);
			RaycastHit2D[] touches = Physics2D.RaycastAll (inputPos, inputPos, 0.5f);
			if (touches.Length > 0) {
				var hit = touches [0];
				if (hit.transform != null) {
					if (hit.transform.gameObject.tag == "enemy") {
						if (!IsPointerOverUIObject ()) {
							MonsterController m = hit.transform.gameObject.GetComponent<MonsterController> ();
							StartCoroutine (OpenEnemyPopup (m));
						}
					}
				}
			}

			timeTemp = 0;

			return;
		}
	}

	Sprite getElementSprite(string element){
		if (element == "fire") {
			return fireBadge;
		} else if (element == "water") {
			return waterBadge;
		} else if (element == "plant") {
			return plantBadge;
		} else if (element == "light") {
			return lightBadge;
		} else if (element == "dark") {
			return darkBadge;
		}

		return null;
	}

	IEnumerator OpenEnemyPopup(MonsterController m){

		//Find total buff attacks and defense;
		Enemy e = m.gameObject.GetComponent<Enemy>();

		//Show details
		Transform t = enemyDetailPopup.transform;
		t.GetChild (0).GetComponent<Text> ().text = m.thaiName;

		//Construct stat key
		string statKey = m.tribe + "\n";

		if (e.buffAttack < 0) {
			statKey = statKey + "<color=red>" + (m.attack + e.buffAttack).ToString () + "</color>" + "\n";
		} else {
			statKey = statKey + (m.attack + e.buffAttack).ToString () + "\n";
		}

		if (PersistentData.instance.gameType == "dungeon" || PersistentData.instance.gameType == "tower") {
			statKey = statKey + m.gameObject.GetComponent<Enemy> ().currentHp + " / " + m.hitPoint.ToString () + "\n";
		} else if (PersistentData.instance.gameType == "worldBoss") {
			statKey = statKey + "??????\n";
		}

		if (e.buffDefense < 0) {
			statKey = statKey + "<color=red>" + (m.defense + e.buffDefense).ToString () + "</color>";
		} else {
			statKey = statKey + (m.defense + e.buffDefense).ToString ();
		}

		t.GetChild (2).GetComponent<Text> ().text = statKey;

		t.GetChild (3).GetComponent<Image> ().sprite = getElementSprite (m.element);

		//assign skill description
		if (m.enemySkillBehavior != null) {
			t.GetChild (4).GetComponent<Text> ().text = m.enemySkillBehavior.aDescription;
		} else {
			t.GetChild (4).GetComponent<Text> ().text = "";
		}
			
		enemyDetailPopup.transform.localScale = new Vector3 (.3f, .3f, 1f);
		enemyDetailPopup.SetActive (true);

		//Scale up
		float start = 0.3f;
		float end = 1f;
		float time = 0.1f;
		float ti = 0f;
		while(ti<time){
			ti += Time.deltaTime;
			float scale = Mathf.Lerp (start, end, ti / time);
			enemyDetailPopup.transform.localScale = new Vector3 (scale, scale, 1f);
			yield return null;
		}
	}

	IEnumerator CloseEnemyPopup(){
		//Scale downx
		float start = 1f;
		float end = 0.3f;
		float time = 0.1f;
		float ti = 0f;
		while(ti<time){
			ti += Time.deltaTime;
			float scale = Mathf.Lerp (start, end, ti / time);
			enemyDetailPopup.transform.localScale = new Vector3 (scale, scale, 1f);
			yield return null;
		}

		enemyDetailPopup.SetActive (false);
	}

	//drop an item
	IEnumerator DropCoin(Vector3 position){
		if (PersistentData.instance.gameType == "worldBoss" || PersistentData.instance.gameType == "tower") {
			yield break;
		}

		yield return new WaitForSeconds (0.5f);
		Instantiate (coinSpin, position + new Vector3(0f,0.5f,0f), Quaternion.Euler(new Vector3(0f,0f,90f)));
	}

	public void DropItem(string type, Vector3 position){
		if (type == "coin") {
			StartCoroutine (DropCoin (position));
		} else if (type == "monster") {
			StartCoroutine (DropMonster (position));
		}
	}

	IEnumerator DropMonster(Vector3 position){
		yield return new WaitForSeconds (1f);
		Instantiate (eggPrefab, position + new Vector3 (0f, 0.5f, 0f), Quaternion.identity);
	}

	public void Revive(){

		if (GameSparksManager.instance.playerData.gems >= 30) {
			//subtract gems used
			GameSparksManager.instance.playerData.gems = GameSparksManager.instance.playerData.gems - 30;
			AnalyticsManager.instance.SpendGems ("Revive", 30);
			GameSparksManager.instance.SavePlayerData ();

			gameOver = false;

			//remove dialog
			gameOverDialog.SetActive (false);
			emptyPanel.SetActive (false);

			//check matches
			if (!BoardManager.instance.CheckMatchPossible ()) {
				print ("no more match");
				BoardManager.instance.Reboard ();
			}

			//revive
			Heal (totalHp);
			BoardManager.instance.isPlayerTurn = true;
			StartCoroutine (BoardManager.instance.GuideTimer ());
			monsterStatus [0].leaderSkillBehavior.TriggerAbility (monsterStatus [0]);

		} else {
			//Show buy gem dialog
			gameOverDialog.SetActive(false);
			notEnoughGemDialog.SetActive (true);
		}
	}

	public void NotRevive(){
		gameOverDialog.SetActive (false);
		emptyPanel.SetActive (false);
		notEnoughGemDialog.SetActive (false);

		//game over code
		StartCoroutine(GameOver());
	}

	//Menu section
	public void Menu(){
		menuDialog.SetActive (true);
		emptyPanel.SetActive (true);
	}

	public void Yes(){
		PersistentData.instance.playCount++;
		Initiate.Fade("main",new Color(0.1f,0.1f,0.1f),1.5f);
	}

	public void No(){
		menuDialog.SetActive (false);
		skillDescDialog.SetActive (false);

		emptyPanel.SetActive (false);
	}

	public void CloseDialog(){ //Use for tutorial popup only
		howtoplayTutorial.SetActive(false);
		skillTutorial.SetActive (false);
		targetTutorial.SetActive (false);
		enemyDetailTutorial.SetActive (false);
		elementTutorial.SetActive (false);
	}

	public void AddCoins(int amount){
		int coin = Int32.Parse (coinText.text);
		coin += amount;

		coinText.text = coin.ToString ();

		//Add gold to Persistent scene too
		PersistentData.instance.goldReceived += amount;
	}

	public void AddEgg(int amount = 1){
		int egg = Int32.Parse (eggText.text);
		egg += amount;

		eggText.text = egg.ToString ();
	}

	IEnumerator SmoothMovement(GameObject g,Vector3 end,float time){
		float t = 0;
		Vector3 startPos = g.transform.localPosition;

		while (t < time) {
			t += Time.deltaTime;
			g.transform.localPosition = Vector3.Lerp (startPos, end, t/time);
			yield return null;
		}
	}

	//Skill section
	public void Heal(int point){
		currentHp += point;
		if (currentHp > totalHp) {
			currentHp = totalHp;
		}
		//change width
		float length = (float)currentHp / (float)totalHp;
		StartCoroutine (ChangeWidth (totalHpGauge, totalHpGauge.fillAmount, length, 0.3f));

		//change text
		totalHpText.text = currentHp.ToString () + "/" + totalHp.ToString ();
	}

	public void DealDamageAbility(MonsterController m,string vfx,int damage,bool isTargetAll, bool ignoreDefense = false){
		StartCoroutine (DealDamageAbilityAnimate (m, vfx, damage, isTargetAll, ignoreDefense));
	}

	IEnumerator DealDamageAbilityAnimate(MonsterController m,string vfx,int damage,bool isTargetAll, bool ignoreDefense = false){
		//Shoot a projectile towards an enemy.
		Vector3 pos = new Vector3 (projectilePosX [m.orderInGame], 1, 0f);

		GameObject projectile = fireProjectile;
		if (m.element == "fire") {
			projectile = fireProjectile;
		} else if (m.element == "water") {
			projectile = waterProjectile;
		} else if (m.element == "plant") {
			projectile = plantProjectile;
		} else if (m.element == "light") {
			projectile = lightProjectile;
		} else if (m.element == "dark") {
			projectile = darkProjectile;
		}

		if (isTargetAll) {
			Vector3 positionToSpawn = Quaternion.Euler (-70f, 0f, 0f) * (new Vector3 (0f, 6f, 0f));

			projectile = Instantiate (projectile, new Vector3 (pos.x, pos.y, 0f), Quaternion.identity) as GameObject;

			StartCoroutine (SmoothMovement (projectile, positionToSpawn, 0.2f));
			yield return new WaitForSeconds (0.2f);

			Instantiate (Resources.Load("Skills/" + vfx,typeof(GameObject)), positionToSpawn, Quaternion.identity);
			yield return new WaitForSeconds (0.5f);

			int count = enemies.Count;
			for (int i = 0; i < count; i++) {
				int damageToCalculate = damage;

				int elementalAdvantage = ElementalBonus (m, enemies [0].GetComponent<MonsterController> ());
				if (elementalAdvantage == 1) { //check for elemental advantage
					damageToCalculate = Mathf.RoundToInt (damageToCalculate * 1.5f);
				}else if (elementalAdvantage == -1) { //check for elemental advantage
					damageToCalculate = Mathf.RoundToInt (damageToCalculate * 0.5f);
				}

				enemies [0].GetComponent<Enemy> ().CalculateDamage (damageToCalculate, m.element, ignoreDefense);
			}

			//yield return new WaitForSeconds (0.75f);

			//yield break;

		} else {
			Vector3 positionToSpawn = Quaternion.Euler (-70f, 0f, 0f) * (currentEnemy.transform.position + new Vector3 (0f, 6f, 0f));

			projectile = Instantiate (projectile, new Vector3 (pos.x, pos.y, 0f), Quaternion.identity) as GameObject;

			StartCoroutine (SmoothMovement (projectile, positionToSpawn, 0.2f));
			yield return new WaitForSeconds (0.2f);

			Instantiate (Resources.Load("Skills/" + vfx,typeof(GameObject)), positionToSpawn, Quaternion.identity);
			yield return new WaitForSeconds (0.5f);

			int elementalAdvantage = ElementalBonus (m, currentEnemy.GetComponent<MonsterController> ());
			if (elementalAdvantage == 1) { //check for elemental advantage
				damage = Mathf.RoundToInt (damage * 1.5f);
			}else if (elementalAdvantage == -1) { //check for elemental advantage
				damage = Mathf.RoundToInt (damage * 0.5f);
			}

			currentEnemy.GetComponent<Enemy> ().CalculateDamage (damage, m.element, ignoreDefense);
		}
	}
		
	public void DebuffTarget(string typeBuff,int percentDebuff,int turnBuff,string elementDebuff){ //only for active skill and target all enemies
		if (typeBuff == "atk") {
			for (int i = 0; i < enemies.Count; i++) {
				MonsterController m = enemies [i].GetComponent<MonsterController> ();
				if (m.element == elementDebuff || elementDebuff == "") {
					Enemy e = enemies [i].GetComponent<Enemy> ();

					e.buffAttack = Mathf.RoundToInt (-percentDebuff * m.attack.Value / 100f);
					e.turnBuffAttack = turnBuff;
				}
			}
		} else if (typeBuff == "def") {
			for (int i = 0; i < enemies.Count; i++) {
				MonsterController m = enemies [i].GetComponent<MonsterController> ();
				Enemy e = enemies [i].GetComponent<Enemy> ();

				e.buffDefense = Mathf.RoundToInt (-percentDebuff * m.defense.Value / 100f);
				e.turnBuffDefense = turnBuff;
			}
		}
	}

	public void BuffSelfTemporary(string monsterUsed,string typeBuff,string elementBuff = "",float multiplier = 0f,int turnBuff = 0,int damageReduction = 0){
		if (typeBuff == "atk") {

			if (multiplier >= 100) {
				print ("Error buff self atk");
				return;
			}

			GameObject icon = buffElementPrefab;

			if (elementBuff == "") { //for every element
				List<string> keys = new List<string> ();
				foreach(string key in BoardManager.instance.buffAttacksTemporary.Keys){
					keys.Add (key);
				}

				for (int i = 0; i < keys.Count; i++) {
					BoardManager.instance.buffAttacksTemporary [keys [i]] *= multiplier;
				}

				icon = Instantiate (buffTeamPrefab, buffColumn);
				icon.transform.localPosition = new Vector3 (-25f, 300f - (buffColumn.childCount * 100f), 0f);
				icon.transform.GetChild (0).GetComponent<Text> ().text = turnBuff.ToString ();

			} else {
				
				BoardManager.instance.buffAttacksTemporary [elementBuff] *= multiplier;

				icon = Instantiate (buffElementPrefab, buffColumn);
				icon.transform.localPosition = new Vector3 (-25f, 300f - (buffColumn.childCount * 100f), 0f);
				icon.GetComponent<Image> ().sprite = getElementSprite (elementBuff);
				icon.transform.GetChild (0).gameObject.SetActive (true);
				icon.transform.GetChild (2).GetComponent<Text> ().text = turnBuff.ToString ();
			}

			BuffAttack ba = new BuffAttack(monsterUsed,multiplier,turnBuff,elementBuff,icon);
			BoardManager.instance.buffAttacks.Add (ba);

		} else if (typeBuff == "def") {

			GameObject icon = buffElementPrefab;

			if (elementBuff == "") {
				List<string> keys = new List<string> ();
				foreach(string key in BoardManager.instance.buffDefensesTemporary.Keys){
					keys.Add (key);
				}

				for (int i = 0; i < keys.Count; i++) {
					BoardManager.instance.buffDefensesTemporary [keys [i]] *= (1f - (damageReduction / 100f));
				}

				icon = Instantiate (buffTeamPrefab, buffColumn);
				icon.GetComponent<Image> ().sprite = shieldSprite;
				icon.transform.localPosition = new Vector3 (-25f, 300f - (buffColumn.childCount * 100f), 0f);
				icon.transform.GetChild (0).GetComponent<Text> ().text = turnBuff.ToString ();

			} else {
				BoardManager.instance.buffDefensesTemporary [elementBuff] *= (1f - (damageReduction / 100f));

				icon = Instantiate (buffElementPrefab, buffColumn);
				icon.transform.localPosition = new Vector3 (-25f, 300f - (buffColumn.childCount * 100f), 0f);
				icon.GetComponent<Image> ().sprite = getElementSprite (elementBuff);
				icon.transform.GetChild (1).gameObject.SetActive (true);
				icon.transform.GetChild (2).GetComponent<Text> ().text = turnBuff.ToString ();
			}

			BuffDefense bd = new BuffDefense(monsterUsed,damageReduction,turnBuff,elementBuff,icon);
			BoardManager.instance.buffDefenses.Add (bd);

		}
	}

	//Leader skill section
	public void BuffSelfPermanent(string typeBuff,string elementBuff = "",float multiplier = 1f,int damageReduction = 0){

		if (typeBuff == "atk") {

			if (multiplier >= 100) {
				print ("Error buff self atk");
				return;
			}

			if (elementBuff == "") { //for every element
				List<string> keys = new List<string> ();
				foreach(string key in BoardManager.instance.buffAttacksPermanent.Keys){
					keys.Add (key);
				}

				for (int i = 0; i < keys.Count; i++) {
					if (keys[i] != "heart") {
						BoardManager.instance.buffAttacksPermanent [keys [i]] *= multiplier;
					}
				}

			} else {

				BoardManager.instance.buffAttacksPermanent [elementBuff] *= multiplier;

			}

		} else if (typeBuff == "def") {

			if (elementBuff == "") {
				List<string> keys = new List<string> ();
				foreach (string key in BoardManager.instance.buffDefensePermanent.Keys){
					keys.Add (key);
				}

				for (int i = 0; i < keys.Count; i++) {
					if (keys [i] != "heart") {
						BoardManager.instance.buffDefensePermanent [keys [i]] *= (1f - (damageReduction / 100f));
					}
				}

			} else {
				BoardManager.instance.buffDefensePermanent [elementBuff] *= (1f - (damageReduction / 100f));
			}
		}
	}

	public void BuffSelfTribe(string tribeBuff, float multiplier){
		for (int i = 0; i < monsterStatus.Count; i++) {
			if (monsterStatus [i].tribe == tribeBuff) {
				monsterStatus [i].attack = new SafeInt (Mathf.RoundToInt (monsterStatus [i].attack.Value * multiplier));
			}
		}
	}
}
