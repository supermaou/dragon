﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Board : MonoBehaviour {

	public int index;

	public Transform leftNeighbor;
	public Transform rightNeighbor;
	public Transform topNeighbor;
	public Transform bottomNeighbor;

	public Transform upLeftNeighbor;
	public Transform upRightNeighbor;
	public Transform downLeftNeighbor;
	public Transform downRightNeighbor;
}
