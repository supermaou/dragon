﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersistentData : MonoBehaviour {

	public static PersistentData instance = null;

	public int playCount = 0;

	//Game variables
	public string gameType;
	public string arenaResult;
	public int ratingWin;
	public int ratingLose;

	public DungeonData currentDungeon;
	public List<List<MonsterData>> enemiesData = new List<List<MonsterData>>();
	public int enemiesLoadCount = 0;
	public int turnUsedToComplete = 0;
	public List<string> monsterDropped = new List<string> ();
	public int goldReceived = 0;
	public string latestResult = "";

	public int worldBossScore;
	public int bestScore;

	//UI variables
	public bool loadedStamina = false;
	public bool loadedMonsterData = false;

	public int detailStack = 1;

	public string gachaElement = "";
	public string monsterGet = "";

	// Use this for initialization
	void Awake () {
		if (instance == null) {
			instance = this;
		} else {
			Destroy (gameObject);
		}
		DontDestroyOnLoad (gameObject);
	}
		
	public void ResetData(){
		currentDungeon = null;
		enemiesData.Clear ();
		monsterDropped.Clear ();
		enemiesLoadCount = 0;
		turnUsedToComplete = 0;
		goldReceived = 0;
	}

	//for adding quest progress
	public void AddQuestProgress(string type){
		for (int i = 0; i < GameSparksManager.instance.questData.Count; i++) {
			if (GameSparksManager.instance.questData [i].questType == type) {
				GameSparksManager.instance.questData [i].AddProgress ();
				break;
			}
		}
	}

	public void AddDailyQuestProgress(string shortCode){
		if (!GameSparksManager.instance.playerData.dailyQuestsToClaim.Contains (shortCode)) {
			GameSparksManager.instance.playerData.dailyQuestsToClaim.Add (shortCode);

			if (isClearAllDailyQuests ()) {
				AddDailyQuestProgress ("dailyQuestClear");
			}
		}
	}

	bool isClearAllDailyQuests(){
		int count = 0;
		List<string> shortCodes = new List<string> () {
			"normalDungeonPlays",
			"dailyDungeonPlays",
			"worldBossPlay",
			"gachaTable"
		};

		for (int i = 0; i < shortCodes.Count; i++) {
			if (GameSparksManager.instance.playerData.dailyQuestsComplete.Contains (shortCodes [i]) || GameSparksManager.instance.playerData.dailyQuestsToClaim.Contains (shortCodes [i])) {
				continue;
			} else {
				return false;
			}
		}

		return true;
	}
}
