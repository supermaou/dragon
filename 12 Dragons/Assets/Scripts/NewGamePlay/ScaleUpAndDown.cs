﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScaleUpAndDown : MonoBehaviour {

	public bool isActive = false;

	public float time = 0.5f;
	public float start = 1f;
	public float end = 1.1f;

	void Start(){
		StartCoroutine (Scaling ());
	}

	IEnumerator Scaling(){

		while (true) {

			if (isActive) {
				float t = 0;

				while (t < time) {
					float scale = Mathf.Lerp (start, end, t / time);
					transform.localScale = new Vector2 (scale, scale);
					t += Time.deltaTime;
					yield return null;
				}

				t = 0;

				while (t < time) {
					float scale = Mathf.Lerp (end, start, t / time);
					transform.localScale = new Vector2 (scale, scale);
					t += Time.deltaTime;
					yield return null;
				}
			} else {
				yield return null;
			}
		}
	}
}
