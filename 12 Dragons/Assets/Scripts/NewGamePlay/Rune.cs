﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Rune : MonoBehaviour {

	public string element;

	private Image img;

	void Awake(){
		img = GetComponent<Image> ();
	}

	private string[] possibleRuneToRandom = new string[]{
		"fire","water","plant","light","dark","heart"
	};

	public void SwitchRuneType(string toRune){
		StartCoroutine (SwitchRuneTypeAnimate (toRune));
	}

	IEnumerator SwitchRuneTypeAnimate(string toRune){
		if (toRune == "random") {
			element = possibleRuneToRandom [RandomElement ()];
		} else {
			element = toRune;
		}

		//scaling down
		float time = 0.25f;
		float t = 0;

		while (t < time) {
			float scale = Mathf.Lerp (1f, 0f, t / time);
			transform.localScale = new Vector2 (scale, scale);
			t += Time.deltaTime;
			yield return null;
		}
			
		if (element == "fire") {
			img.sprite = BoardManager.instance.fireRuneSprite;
		} else if (element == "water") {
			img.sprite = BoardManager.instance.waterRuneSprite;
		} else if (element == "plant") {
			img.sprite = BoardManager.instance.plantRuneSprite;
		} else if (element == "light") {
			img.sprite = BoardManager.instance.lightRuneSprite;
		} else if (element == "dark") {
			img.sprite = BoardManager.instance.darkRuneSprite;
		} else if (element == "heart") {
			img.sprite = BoardManager.instance.heartRuneSprite;
		}

		//scaling up
		t = 0;

		while (t < time) {
			float scale = Mathf.Lerp (0f, 1f, t / time);
			transform.localScale = new Vector2 (scale, scale);
			t += Time.deltaTime;
			yield return null;
		}

		transform.localScale = new Vector2 (1f, 1f);
	}

	int RandomElement(){
		return Random.Range (0, possibleRuneToRandom.Length);
	}
}
