﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;
using System;

public class BuffAttack{
	public string monsterUsed;
	public float multiplier;
	public int turnBuff;
	public string elementBuff;
	public GameObject icon;

	public BuffAttack(string monsterUsed,float multiplier,int turnBuff,string elementBuff,GameObject icon){
		string[] s = monsterUsed.Split ('.');

		this.monsterUsed = s [0];
		this.multiplier = multiplier;
		this.turnBuff = turnBuff;
		this.elementBuff = elementBuff;
		this.icon = icon;
	}
}

public class BuffDefense{
	public string monsterUsed;
	public int damageReduction;
	public int turnBuff;
	public string elementBuff;
	public GameObject icon;

	public BuffDefense(string monsterUsed,int damageReduction,int turnBuff,string elementBuff,GameObject icon){
		this.monsterUsed = monsterUsed;
		this.damageReduction = damageReduction;
		this.turnBuff = turnBuff;
		this.elementBuff = elementBuff;
		this.icon = icon;
	}
}

public class BoardManager : MonoBehaviour {

	public static BoardManager instance = null;

	public float moveTime = 5f;
	public int combo;

	public Transform board;
	public Transform damageTexts;
	public Text healText;

	public string[] runeTypes = new string[]{
		"fire","water","plant","light","dark","heart"
	};
	public string banRune;

	public GameObject fireRune;
	public GameObject waterRune;
	public GameObject plantRune;
	public GameObject lightRune;
	public GameObject darkRune;
	public GameObject heartRune;

	public GameObject selectedRune;

	//ref variables
	public Image timerGauge;
	public ComboText comboNumberText;
	public ComboText comboBonusText;

	//swipe variable
	Vector2 firstPressPos;
	Vector2 secondPressPos;
	Vector2 currentSwipe;

	//explosion effects
	public GameObject fireExplode;
	public GameObject waterExplode;
	public GameObject plantExplode;
	public GameObject lightExplode;
	public GameObject darkExplode;
	public GameObject heartExplode;

	//Sprite reference
	public Sprite fireRuneSprite;
	public Sprite waterRuneSprite;
	public Sprite plantRuneSprite;
	public Sprite lightRuneSprite;
	public Sprite darkRuneSprite;
	public Sprite heartRuneSprite;

	//game variables
	public bool isPlayerTurn = false;

	public Dictionary<string,bool> isAttackAll = new Dictionary<string,bool>(){
		{"fire",false},
		{"water",false},
		{"plant",false},
		{"light",false},
		{"dark",false},
		{"heart",false} //Not using it but use for easy coding
	};

	//damage variables
	public Dictionary<string, int> elementDamages = new Dictionary<string,int>(){ //the damage is percent base
		{"fire",0},
		{"water",0},
		{"plant",0},
		{"light",0},
		{"dark",0},
		{"heart",0}
	};

	public Dictionary<string, int> runeTypesCount = new Dictionary<string,int>(){
		{"fire",0},
		{"water",0},
		{"plant",0},
		{"light",0},
		{"dark",0},
		{"heart",0}
	};

	//buff variables multiplier for example, 2 or 1.5 or something
	public Dictionary<string,float> buffAttacksPermanent = new Dictionary<string,float>(){
		{"fire",1},
		{"water",1},
		{"plant",1},
		{"light",1},
		{"dark",1},
		{"heart",1}
	};

	public Dictionary<string,float> buffDefensePermanent = new Dictionary<string,float>(){
		{"fire",1},
		{"water",1},
		{"plant",1},
		{"light",1},
		{"dark",1},
		{"heart",1}
	};

	//buff variables for active skill
	public Dictionary<string,float> buffAttacksTemporary = new Dictionary<string,float>(){
		{"fire",1},
		{"water",1},
		{"plant",1},
		{"light",1},
		{"dark",1},
		{"heart",1}
	};
	public List<BuffAttack> buffAttacks = new List<BuffAttack>();

	public Dictionary<string,float> buffDefensesTemporary = new Dictionary<string,float>(){
		{"fire",1},
		{"water",1},
		{"plant",1},
		{"light",1},
		{"dark",1},
		{"heart",1}
	};
	public List<BuffDefense> buffDefenses = new List<BuffDefense> ();

	//color damage text data
	public Dictionary<string,Color> damageTextColor = new Dictionary<string,Color>(){
		{"fire",new Color(255f/255f,90f/255f,90f/255f,1f)},
		{"water",new Color (90f / 255f, 221f / 255f, 255f / 255f,1f)},
		{"plant",new Color (82f / 255f, 223f / 255f, 0f / 255f,1f)},
		{"light",new Color (255f / 255f, 248f / 255f, 0f / 255f,1f)},
		{"dark",new Color (194f / 255f, 126f / 255f, 255f / 255f,1f)}
	};

	//sound variables
	public AudioClip runeBurstSound;

	void Awake(){
		instance = this;

		SetupBoard ();
	}

	void Start(){
		SwipeManager.OnSwipeDetected += OnSwipeDetected;
	}

	void SetupBoard(){

		for(int i=0;i<board.childCount;i++) {
			string element = RandomElement ();

			GameObject g = fireRune;
			if (element == "fire") {
				g = Instantiate (fireRune, board.GetChild(i), false);
				g.transform.localPosition = new Vector3 (0f, 0f, 0f);
			} else if (element == "water") {
				g = Instantiate (waterRune, board.GetChild(i), false);
				g.transform.localPosition = new Vector3 (0f, 0f, 0f);
			} else if (element == "plant") {
				g = Instantiate (plantRune, board.GetChild(i), false);
				g.transform.localPosition = new Vector3 (0f, 0f, 0f);
			} else if (element == "light") {
				g = Instantiate (lightRune, board.GetChild(i), false);
				g.transform.localPosition = new Vector3 (0f, 0f, 0f);
			} else if (element == "dark") {
				g = Instantiate (darkRune, board.GetChild(i), false);
				g.transform.localPosition = new Vector3 (0f, 0f, 0f);
			} else if (element == "heart") {
				g = Instantiate (heartRune, board.GetChild(i), false);
				g.transform.localPosition = new Vector3 (0f, 0f, 0f);
			}

			Board b = board.GetChild (i).GetComponent<Board> ();

			if (i % 7 != 0) { //not most left
				b.leftNeighbor = board.GetChild(i-1);
				if (i >= 7) { //not most top
					b.upLeftNeighbor = board.GetChild(i-8);
				}

				if (i <= 34) { //not most buttom
					b.downLeftNeighbor = board.GetChild(i+6);
				}
			}

			if (i % 7 != 6) { //not most right
				b.rightNeighbor = board.GetChild(i+1);

				if (i >= 7) { //not most top
					b.upRightNeighbor = board.GetChild(i-6);
				}

				if (i <= 34) { //not most buttom
					b.downRightNeighbor = board.GetChild(i+8);
				}
			}

			if (i >= 7) { //not most top
				b.topNeighbor = board.GetChild(i-7);
			}

			if (i <= 34) { //not most bottom
				b.bottomNeighbor = board.GetChild (i + 7);
			}
		}
	}

	string RandomElement(){
		string rune = runeTypes [UnityEngine.Random.Range (0, 6)];

		while (rune == banRune) {
			rune = runeTypes [UnityEngine.Random.Range (0, 6)];
		}

		return rune;
	}

	void OnSwipeDetected (Swipe direction, Vector2 swipeVelocity)
	{
		if (GameplayManager.instance.gameOver) {
			return;
		}

		if (GameplayManager.instance.emptyPanel.activeSelf) {
			return;
		}

		if (selectedRune == null || !isPlayerTurn) {
			return;
		}

		Board currentBoard = selectedRune.transform.parent.GetComponent<Board>();

		// do something with direction or the velocity of the swipe
		if (direction == Swipe.Up) {
			if(currentBoard.topNeighbor != null){
				StartCoroutine(SwitchRune(selectedRune.transform.parent,currentBoard.topNeighbor));
			}
		} else if (direction == Swipe.Down) {
			if(currentBoard.bottomNeighbor != null){
				StartCoroutine(SwitchRune(selectedRune.transform.parent,currentBoard.bottomNeighbor));
			}
		} else if (direction == Swipe.Left) {
			if(currentBoard.leftNeighbor != null){
				StartCoroutine(SwitchRune(selectedRune.transform.parent,currentBoard.leftNeighbor));
			}
		} else if (direction == Swipe.Right) {
			if(currentBoard.rightNeighbor != null){
				StartCoroutine(SwitchRune(selectedRune.transform.parent,currentBoard.rightNeighbor));
			}
		} else if (direction == Swipe.UpRight) {
			if (currentBoard.upRightNeighbor != null) {
				StartCoroutine (SwitchRune (selectedRune.transform.parent, currentBoard.upRightNeighbor));
			}
		} else if (direction == Swipe.DownRight) {
			if (currentBoard.downRightNeighbor != null) {
				StartCoroutine (SwitchRune (selectedRune.transform.parent, currentBoard.downRightNeighbor));
			}
		} else if (direction == Swipe.UpLeft) {
			if (currentBoard.upLeftNeighbor != null) {
				StartCoroutine (SwitchRune (selectedRune.transform.parent, currentBoard.upLeftNeighbor));
			}
		} else if (direction == Swipe.DownLeft) {
			if (currentBoard.downLeftNeighbor != null) {
				StartCoroutine (SwitchRune (selectedRune.transform.parent, currentBoard.downLeftNeighbor));
			}
		}

		selectedRune = null;
	}
	/*
	void Update(){
		if (GameplayManager.instance.gameOver) {
			return;
		}

		if (GameplayManager.instance.emptyPanel.activeSelf) {
			return;
		}

		#if UNITY_EDITOR
		if(Input.GetMouseButtonDown(0) && isPlayerTurn)
		{
			//save began touch 2d point
			firstPressPos = new Vector2(Input.mousePosition.x,Input.mousePosition.y);

			PointerEventData pointerData = new PointerEventData(EventSystem.current);

			pointerData.position = Input.mousePosition;

			List<RaycastResult> results = new List<RaycastResult>();
			EventSystem.current.RaycastAll(pointerData, results);

			if (results.Count > 0) {
				selectedRune = results[results.Count-1].gameObject;
			}
		}

		if(Input.GetMouseButtonUp(0) && selectedRune != null && isPlayerTurn)
		{
			//save ended touch 2d point
			secondPressPos = new Vector2(Input.mousePosition.x,Input.mousePosition.y);

			//create vector from the two points
			currentSwipe = new Vector2(secondPressPos.x - firstPressPos.x, secondPressPos.y - firstPressPos.y);

			//normalize the 2d vector
			currentSwipe.Normalize();

			if(selectedRune == null){
				return;
			}

			Board currentBoard = selectedRune.transform.parent.GetComponent<Board>();

			//swipe upwards
			if(currentSwipe.y > 0 && currentSwipe.x > -0.5f && currentSwipe.x < 0.5f)
			{
				//Debug.Log("up swipe");
				if(currentBoard.topNeighbor != null){
					StartCoroutine(SwitchRune(selectedRune.transform.parent,currentBoard.topNeighbor));
				}
			}
			//swipe down
			if(currentSwipe.y < 0 &&  currentSwipe.x > -0.5f &&  currentSwipe.x < 0.5f)
			{
				//Debug.Log("down swipe");
				if(currentBoard.bottomNeighbor != null){
					StartCoroutine(SwitchRune(selectedRune.transform.parent,currentBoard.bottomNeighbor));
				}
			}
			//swipe left
			if(currentSwipe.x < 0 &&  currentSwipe.y > -0.5f &&  currentSwipe.y < 0.5f)
			{
				//Debug.Log("left swipe");
				if(currentBoard.leftNeighbor != null){
					StartCoroutine(SwitchRune(selectedRune.transform.parent,currentBoard.leftNeighbor));
				}
			}
			//swipe right
			if(currentSwipe.x > 0 &&  currentSwipe.y > -0.5f &&  currentSwipe.y < 0.5f)
			{
				//Debug.Log("right swipe");
				if(currentBoard.rightNeighbor != null){
					StartCoroutine(SwitchRune(selectedRune.transform.parent,currentBoard.rightNeighbor));
				}
			}

			selectedRune = null;
		}
		#elif UNITY_ANDROID || UNITY_IOS

		if(Input.touches.Length > 0){
			Touch t = Input.GetTouch(0);

			if(t.phase == TouchPhase.Began && isPlayerTurn)
			{
				//save began touch 2d point
				firstPressPos = new Vector2(t.position.x,t.position.y);

				PointerEventData pointerData = new PointerEventData(EventSystem.current);

				pointerData.position = t.position;

				List<RaycastResult> results = new List<RaycastResult>();
				EventSystem.current.RaycastAll(pointerData, results);

				if (results.Count > 0) {
					selectedRune = results[results.Count-1].gameObject;
				}
			}

			if(t.phase == TouchPhase.Ended && selectedRune != null && isPlayerTurn)
			{

				//save ended touch 2d point
				secondPressPos = new Vector2(t.position.x,t.position.y);

				//create vector from the two points
				currentSwipe = new Vector2(secondPressPos.x - firstPressPos.x, secondPressPos.y - firstPressPos.y);

				//normalize the 2d vector
				currentSwipe.Normalize();

				Board currentBoard = selectedRune.transform.parent.GetComponent<Board>();

				//swipe upwards
				if(currentSwipe.y > 0 && currentSwipe.x > -0.5f && currentSwipe.x < 0.5f)
				{
					//Debug.Log("up swipe");
					if(currentBoard.topNeighbor != null){
						StartCoroutine(SwitchRune(selectedRune.transform.parent,currentBoard.topNeighbor));
					}
				}
				//swipe down
				if(currentSwipe.y < 0 &&  currentSwipe.x > -0.5f &&  currentSwipe.x < 0.5f)
				{
					//Debug.Log("down swipe");
					if(currentBoard.bottomNeighbor != null){
						StartCoroutine(SwitchRune(selectedRune.transform.parent,currentBoard.bottomNeighbor));
					}
				}
				//swipe left
				if(currentSwipe.x < 0 &&  currentSwipe.y > -0.5f &&  currentSwipe.y < 0.5f)
				{
					//Debug.Log("left swipe");
					if(currentBoard.leftNeighbor != null){
						StartCoroutine(SwitchRune(selectedRune.transform.parent,currentBoard.leftNeighbor));
					}
				}
				//swipe right
				if(currentSwipe.x > 0 &&  currentSwipe.y > -0.5f &&  currentSwipe.y < 0.5f)
				{
					//Debug.Log("right swipe");
					if(currentBoard.rightNeighbor != null){
						StartCoroutine(SwitchRune(selectedRune.transform.parent,currentBoard.rightNeighbor));
					}
				}

				selectedRune = null;
			}
		}

		#endif
	}*/

	public IEnumerator GuideTimer(){
		float time = 4f;
		float t = 0;

		while (selectedRune == null && t < time) {
			t += Time.deltaTime;
			yield return null;
		}

		if (t >= time) {
			//Guide it
			List<int> pair = GuideMatch();
			if (pair == null) {
				print ("no guide possible");
				yield break;
			}

			Transform guideTarget = board.GetChild (pair [0]).GetChild (0);

			if (pair [0] == pair [1] - 7) { //Vertically move
				DOTween.Kill("guide");
				guideTarget.DOLocalMove (new Vector3 (0f, -20f, 0f),0.3f).SetLoops (-1, LoopType.Yoyo).SetId("guide");
			} else if (pair [0] == pair [1] - 1) { //Horizontally move
				DOTween.Kill("guide");
				guideTarget.DOLocalMove (new Vector3 (20f, 0f, 0f),0.3f).SetLoops (-1, LoopType.Yoyo).SetId("guide");
			}
		}
	}

	IEnumerator TurnTimer(){
		float time = moveTime;
		float t = moveTime;

		while (t >= 0) {
			t -= Time.deltaTime;
			timerGauge.fillAmount = t / time;
			yield return null;
		}

		timerGauge.gameObject.SetActive (false);

		isPlayerTurn = false;

		//Add turn
		PersistentData.instance.turnUsedToComplete++;

		StartCoroutine (DestroyMatches ());

	}

	IEnumerator SwitchRune(Transform board1, Transform board2){

		//Remove tutorial dialog
		if (GameplayManager.instance.howtoplayTutorial.activeSelf == true) {
			GameplayManager.instance.howtoplayTutorial.SetActive (false);
		}

		if (timerGauge.gameObject.activeSelf == false) {
			timerGauge.gameObject.SetActive (true);

			StartCoroutine (TurnTimer ());
		}

		DOTween.Kill ("guide");
		SetToDefaultPosition ();

		Transform rune1 = board1.GetChild (0);
		Transform rune2 = board2.GetChild (0);

		rune1.SetParent (board2);
		rune2.SetParent (board1);

		rune1.DOLocalMove (new Vector3 (0f, 0f, 0f), 0.1f).WaitForCompletion ();
		rune2.DOLocalMove (new Vector3 (0f, 0f, 0f), 0.1f).WaitForCompletion ();

		List<Transform> matches = FindMatch ();

		if (matches.Contains (rune1) || matches.Contains (rune2)) {
			yield break;

		} else { //switch it back

			yield return new WaitForSeconds (0.1f);

			rune1.SetParent (board1);
			rune2.SetParent (board2);

			rune1.DOLocalMove (new Vector3 (0f, 0f, 0f), 0.1f).WaitForCompletion ();
			rune2.DOLocalMove (new Vector3 (0f, 0f, 0f), 0.1f).WaitForCompletion ();
		}

	}

	void SetToDefaultPosition(){
		foreach (Transform t in board) {
			t.GetChild (0).localPosition = new Vector3 (0f, 0f, 0f);
		}
	}

	IEnumerator DestroyMatches(){

		List<Transform> matches = FindMatch ();

		combo = 0;

		while (matches.Count > 0) { //for automatically falling rune that explode
			yield return new WaitForSeconds (0.5f);

			List<List<Transform>> groupedMatches = FindGroupMatch ();

			int startGroupCound = groupedMatches.Count;
			//print ("total " + startGroupCound.ToString () + " group");

			while (groupedMatches.Count > 0) { //for every group match
				//for every combo

				//play rune burst sound
				SoundManager.instance.PlayEffect(runeBurstSound);

				if (groupedMatches [0].Count <= 0) {
					groupedMatches.RemoveAt (0);
					continue;
				}

				string element = groupedMatches [0] [0].GetComponent<Rune> ().element;

				elementDamages [element] += 100 + (groupedMatches [0].Count - 3) * 25; //Add damage
				runeTypesCount[element] += groupedMatches[0].Count;

				if (groupedMatches [0].Count >= 5) {
					isAttackAll [groupedMatches[0][0].GetComponent<Rune> ().element] = true;
				}

				//Burst effect
				for (int i = 0; i < groupedMatches [0].Count; i++) {
					if (groupedMatches [0] [i] == null) {
						print ("null at index: " + i.ToString () + " and element " + element);
						print ("start group count: " + startGroupCound.ToString ());
					}
					ExplodeRune (groupedMatches [0] [i].gameObject);
				}

				groupedMatches.RemoveAt (0);

				//Increase Attack number at thumbnails
				IncreaseAttack(element);

				//add every combo
				combo++;
				comboNumberText.GetComponent<TextMeshProUGUI>().text = combo.ToString () + " Combo!";
				comboNumberText.gameObject.SetActive (true);

				comboBonusText.GetComponent<TextMeshProUGUI>().text = "+" + ((combo - 1) * 25).ToString() + "%";
				comboBonusText.gameObject.SetActive (true);

				comboNumberText.t = 0;
				comboBonusText.t = 0;

				yield return new WaitForSeconds (0.2f);
			}

			yield return new WaitForSeconds (0.5f);

			FillEmptyDown ();

			matches = FindMatch ();
		}

		//collect key for each element
		List<string> keys = new List<string>();
		foreach (string key in BoardManager.instance.elementDamages.Keys) {
			keys.Add (key);
		}

		//Increase damage for combo
		for (int j = 0; j < keys.Count; j++) {
			if (elementDamages [keys [j]] > 0) {
				elementDamages [keys [j]] = elementDamages [keys [j]] + 25 * (combo - 1);
				IncreaseAttack (keys [j]);
			}
		}

		//Increase damage from all buffs
		for (int i = 0; i < keys.Count; i++) {
			elementDamages [keys [i]] = Mathf.RoundToInt (elementDamages [keys [i]] * buffAttacksPermanent [keys [i]] * buffAttacksTemporary [keys [i]]);
		}

		//Trigger skill about rune burst
		if (GameplayManager.instance.monsterStatus [0].leaderSkill != null) {
			if (GameplayManager.instance.monsterStatus [0].leaderSkill.GetString ("type") == "BurstRuneTypesBuff") {
				GameplayManager.instance.monsterStatus [0].leaderSkillBehavior.TriggerAbility (GameplayManager.instance.monsterStatus [0]);
			} else if (GameplayManager.instance.monsterStatus [0].leaderSkill.GetString ("type") == "DamageBuffOnRuneCount") {
				GameplayManager.instance.monsterStatus [0].leaderSkillBehavior.TriggerAbility (GameplayManager.instance.monsterStatus [0]);
			}
		}

		StartCoroutine (GameplayManager.instance.PlayerAttack ());
	}

	void ExplodeRune(GameObject rune){
		string element = rune.GetComponent<Rune> ().element;

		rune.SetActive (false);

		GameObject g = fireExplode;
		if (element == "fire") {
			g = Instantiate (fireExplode, rune.transform.parent, false);
			g.transform.localPosition = new Vector3 (0f, 0f, 0f);
		} else if (element == "water") {
			g = Instantiate (waterExplode, rune.transform.parent, false);
			g.transform.localPosition = new Vector3 (0f, 0f, 0f);
		} else if (element == "plant") {
			g = Instantiate (plantExplode, rune.transform.parent, false);
			g.transform.localPosition = new Vector3 (0f, 0f, 0f);
		} else if (element == "light") {
			g = Instantiate (lightExplode, rune.transform.parent, false);
			g.transform.localPosition = new Vector3 (0f, 0f, 0f);
		} else if (element == "dark") {
			g = Instantiate (darkExplode, rune.transform.parent, false);
			g.transform.localPosition = new Vector3 (0f, 0f, 0f);
		} else if (element == "heart") {
			g = Instantiate (heartExplode, rune.transform.parent, false);
			g.transform.localPosition = new Vector3 (0f, 0f, 0f);
		}

		if (rune != null) {
			Destroy (rune);
		}
	}
		
	private List<List<Transform>> GroupMatch(){ //grouping the rune that connect into chunk
		List<List<Transform>> groupedMatches = new List<List<Transform>> ();

		List<List<Rune>> groupingMatch = new List<List<Rune>> ();

		List<Transform> matches = FindMatch ();

		while (matches.Count > 0) {

			Transform thisMatchCell = matches [0].parent;
			Rune thisRune = matches [0].GetComponent<Rune> ();

			bool isFoundGroup = false;
			for (int i = 0; i < groupingMatch.Count; i++) {
				for (int j = 0; j < groupingMatch [i].Count; j++) {
					if (thisRune.element != groupingMatch [i] [j].element) {
						continue;
					}

					Board cell = groupingMatch [i] [j].transform.parent.GetComponent<Board> ();
					if (thisMatchCell == cell.topNeighbor || thisMatchCell == cell.bottomNeighbor || thisMatchCell == cell.leftNeighbor || thisMatchCell == cell.rightNeighbor) {
						groupingMatch [i].Add (thisRune);
						isFoundGroup = true;
						if (matches.Count > 0) {
							matches.Remove (matches [0]);
						}
						break;
					}
				}
			}

			if (!isFoundGroup) {
				groupingMatch.Add (new List<Rune> ());
				groupingMatch [groupingMatch.Count - 1].Add (thisRune);
				matches.Remove (matches [0]);
			}
		}

		for (int i = 0; i < groupingMatch.Count; i++) {
			groupedMatches.Add (new List<Transform> ());
			for (int j = 0; j < groupingMatch [i].Count; j++) {
				groupedMatches [i].Add (groupingMatch [i] [j].transform);
			}
		}

		return groupedMatches;
	}

	public List<int> GuideMatch(){
		List<List<int>> pairs = new List<List<int>> ();

		List<List<Transform>> runeBoard = new List<List<Transform>> ();

		//construct rune board
		for (int i = 0; i < board.childCount; i++) {
			if (i % 7 == 0) {
				runeBoard.Add (new List<Transform> ());
			}

			if (board.GetChild (i).GetChild (0) != null) {
				runeBoard [runeBoard.Count - 1].Add (board.GetChild (i).GetChild (0));
			} else {
				runeBoard [runeBoard.Count - 1].Add (null);
			}
		}

		//try switching
		for (int a = 0; a < board.childCount; a++) {
			int i = a / 7;
			int j = a % 7;

			List<Transform> match = FindMatch (runeBoard);

			if (i < 5) { //switching vertically
				Transform tmp = runeBoard[i][j];
				runeBoard [i] [j] = runeBoard [i + 1] [j];
				runeBoard [i + 1] [j] = tmp;

				string element1 = runeBoard [i] [j].GetComponent<Rune> ().element;
				string element2 = runeBoard [i + 1] [j].GetComponent<Rune> ().element;

				if (element1 != element2) {
					List<Transform> match1 = FindMatch (runeBoard);
					if ((!match.Contains (runeBoard [i] [j]) && match1.Contains (runeBoard [i + 1] [j])) || (!match.Contains (runeBoard [i + 1] [j]) && match1.Contains (runeBoard [i] [j]))) {
						//Found one pair
						pairs.Add (new List<int> ());
						pairs [pairs.Count - 1].Add (a);
						pairs [pairs.Count - 1].Add (a + 7);
					}
				}

				//Switch back
				tmp = runeBoard[i][j];
				runeBoard [i] [j] = runeBoard [i + 1] [j];
				runeBoard [i + 1] [j] = tmp;
			}

			if (j < 6) { //switching horizontally
				Transform tmp = runeBoard[i][j];
				runeBoard [i] [j] = runeBoard [i] [j + 1];
				runeBoard [i] [j + 1] = tmp;

				string element1 = runeBoard [i] [j].GetComponent<Rune> ().element;
				string element2 = runeBoard [i] [j + 1].GetComponent<Rune> ().element;

				if (element1 != element2) {
					List<Transform> match1 = FindMatch (runeBoard);
					if ((!match.Contains (runeBoard [i] [j]) && match1.Contains (runeBoard [i] [j + 1])) || (!match.Contains (runeBoard [i] [j + 1]) && match1.Contains (runeBoard [i] [j]))) {
						//Found one pair
						pairs.Add (new List<int> ());
						pairs [pairs.Count - 1].Add (a);
						pairs [pairs.Count - 1].Add (a + 1);
					}
				}

				//Switch back
				tmp = runeBoard[i][j];
				runeBoard [i] [j] = runeBoard [i] [j + 1];
				runeBoard [i] [j + 1] = tmp;
			}
		}

		if (pairs.Count > 0) {
			int rand = UnityEngine.Random.Range (0, pairs.Count);

			return pairs [rand];
		} else {
			return null;
		}
	}

	public bool CheckMatchPossible(){

		List<List<Transform>> runeBoard = new List<List<Transform>> ();

		//construct rune board
		for (int i = 0; i < board.childCount; i++) {
			if (i % 7 == 0) {
				runeBoard.Add (new List<Transform> ());
			}

			if (board.GetChild (i).GetChild (0) != null) {
				runeBoard [runeBoard.Count - 1].Add (board.GetChild (i).GetChild (0));
			} else {
				runeBoard [runeBoard.Count - 1].Add (null);
			}
		}

		//try switching
		for (int a = 0; a < board.childCount; a++) {
			int i = a / 7;
			int j = a % 7;

			if (i < 5) { //switching vertically
				Transform tmp = runeBoard[i][j];
				runeBoard [i] [j] = runeBoard [i + 1] [j];
				runeBoard [i + 1] [j] = tmp;

				List<Transform> match1 = FindMatch (runeBoard);
				if (match1.Count > 0) {
					return true;

				} else { //switch back
					tmp = runeBoard[i][j];
					runeBoard [i] [j] = runeBoard [i + 1] [j];
					runeBoard [i + 1] [j] = tmp;
				}
			}

			if (j < 6) { //switching horizontally
				Transform tmp = runeBoard[i][j];
				runeBoard [i] [j] = runeBoard [i] [j + 1];
				runeBoard [i] [j + 1] = tmp;

				List<Transform> match1 = FindMatch (runeBoard);
				if (match1.Count > 0) {
					return true;

				} else { //switch back
					tmp = runeBoard[i][j];
					runeBoard [i] [j] = runeBoard [i] [j + 1];
					runeBoard [i] [j + 1] = tmp;
				}
			}
		}

		return false;
	}

	private List<Transform> FindMatch(List<List<Transform>> listToCheck = default (List<List<Transform>>)){
		List<Transform> matchTiles = new List<Transform> ();

		List<List<Transform>> runeBoard = new List<List<Transform>> ();


		if (listToCheck == null) {
			//construct rune board
			for (int i = 0; i < board.childCount; i++) {
				if (i % 7 == 0) {
					runeBoard.Add (new List<Transform> ());
				}

				if (board.GetChild (i).GetChild (0) != null) {
					runeBoard [runeBoard.Count - 1].Add (board.GetChild (i).GetChild (0));
				} else {
					runeBoard [runeBoard.Count - 1].Add (null);
				}
			}
		} else {
			runeBoard = listToCheck;
		}

		//checking the vertical tiles
		for (int i = 0; i < runeBoard.Count; i++) {
			for (int j = 0; j < runeBoard [i].Count; j++) {
				if (runeBoard [i] [j] == null) {
					continue;
				}

				int matchCount = 0;
				int i2 = runeBoard.Count;
				int i1;
				for (i1 = i + 1; i1 < i2; i1++) {
					if (runeBoard [i1] [j] == null) {
						break;
					}

					if (runeBoard [i1] [j].GetComponent<Rune>().element != runeBoard [i] [j].GetComponent<Rune>().element) {
						break;
					}

					matchCount++;
				}

				if (matchCount >= 2) {
					for (int i3 = i; i3 < i1; i3++) {
						if (!matchTiles.Contains (runeBoard [i3] [j])) {
							matchTiles.Add (runeBoard [i3] [j]);
						}
					}
				}
			}
		}

		//checking the horizontal tiles
		for (int i = 0; i < runeBoard.Count; i++) {
			for (int j = 0; j < runeBoard [i].Count; j++) {
				if (runeBoard [i] [j] == null) {
					continue;
				}

				int matchCount = 0;
				int j2 = runeBoard [i].Count;
				int j1;
				for (j1 = j + 1; j1 < j2; j1++) {
					if (runeBoard [i] [j1] == null) {
						break;
					}

					if (runeBoard [i] [j1].GetComponent<Rune>().element != runeBoard [i] [j].GetComponent<Rune>().element) {
						break;
					}

					matchCount++;
				}

				if (matchCount >= 2) {
					for (int j3 = j; j3 < j1; j3++) {
						if (!matchTiles.Contains (runeBoard [i] [j3])) {
							matchTiles.Add (runeBoard [i] [j3]);
						}
					}
				}
			}
		}

		return matchTiles;
	}

	private List<List<Transform>> FindGroupMatch(){
		List<Transform> matchTiles = new List<Transform> ();

		List<List<Transform>> groupMatchTiles = new List<List<Transform>> ();

		List<List<Transform>> runeBoard = new List<List<Transform>> ();

		//construct rune board
		for (int i = 0; i < board.childCount; i++) {
			if (i % 7 == 0) {
				runeBoard.Add (new List<Transform> ());
			}

			if (board.GetChild (i).GetChild (0) != null) {
				runeBoard [runeBoard.Count - 1].Add (board.GetChild (i).GetChild (0));
			} else {
				runeBoard [runeBoard.Count - 1].Add (null);
			}
		}

		//checking the vertical tiles
		for (int i = 0; i < runeBoard.Count; i++) {
			for (int j = 0; j < runeBoard [i].Count; j++) {
				if (runeBoard [i] [j] == null) {
					continue;
				}

				int matchCount = 0;
				int i2 = runeBoard.Count;
				int i1;
				for (i1 = i + 1; i1 < i2; i1++) {
					if (runeBoard [i1] [j] == null) {
						break;
					}

					if (runeBoard [i1] [j].GetComponent<Rune>().element != runeBoard [i] [j].GetComponent<Rune>().element) {
						break;
					}

					matchCount++;
				}

				if (matchCount >= 2) {

					List<bool> isDuplicate = new List<bool> ();
					//add in matchTiles
					for (int i3 = i; i3 < i1; i3++) {
						isDuplicate.Add (matchTiles.Contains (runeBoard [i3] [j]));

						if (!matchTiles.Contains (runeBoard [i3] [j])) {
							matchTiles.Add (runeBoard [i3] [j]);
						}
					}

					//add in group
					if (!isDuplicate.Contains (true)) {
						groupMatchTiles.Add (new List<Transform> ());
						for (int i3 = i; i3 < i1; i3++) {
							groupMatchTiles [groupMatchTiles.Count - 1].Add (runeBoard [i3] [j]);
						}
					}
				}
					

			}
		}

		//checking the horizontal tiles
		for (int i = 0; i < runeBoard.Count; i++) {
			for (int j = 0; j < runeBoard [i].Count; j++) {
				if (runeBoard [i] [j] == null) {
					continue;
				}

				int matchCount = 0;
				int j2 = runeBoard [i].Count;
				int j1;
				for (j1 = j + 1; j1 < j2; j1++) {
					if (runeBoard [i] [j1] == null) {
						break;
					}

					if (runeBoard [i] [j1].GetComponent<Rune>().element != runeBoard [i] [j].GetComponent<Rune>().element) {
						break;
					}

					matchCount++;
				}

				if (matchCount >= 2) {

					//add in matchTiles
					List<bool> isDuplicate = new List<bool>();
					for (int j3 = j; j3 < j1; j3++) {
						isDuplicate.Add (matchTiles.Contains (runeBoard [i] [j3]));

						if (!matchTiles.Contains (runeBoard [i] [j3])) {
							matchTiles.Add (runeBoard [i] [j3]);
						}
					}

					if (!isDuplicate.Contains (true)) {
						//add in group if not duplicate
						groupMatchTiles.Add(new List<Transform>());
						for (int j3 = j; j3 < j1; j3++) {
							groupMatchTiles[groupMatchTiles.Count - 1].Add (runeBoard [i] [j3]);
						}

					}else if (isDuplicate.Contains(false) && isDuplicate.Contains(true)) {
						//find the group that have the duplicate in, and then add it to the same group
						int count = 0; //count for the group that found duplicate value in
						int originalGroupIndex = 0; //In case of there are more than 1 group that contain duplicate
						for (int a = 0; a < groupMatchTiles.Count; a++) { //loop in all group matches
							for (int j3 = j; j3 < j1; j3++) { //loop in current match
								if (groupMatchTiles [a].Contains (runeBoard [i] [j3])) { //if there is even a part of this match in a group
									count++;

									if (count == 1) {
										//add value to group
										for (int j4 = j; j4 < j1; j4++) {
											if (!groupMatchTiles [a].Contains (runeBoard [i] [j4])) {
												groupMatchTiles [a].Add (runeBoard [i] [j4]);
											}
										}

										originalGroupIndex = a;

									} else { //in case of |_| shape of rune
										//add all tile in 'a' group to group the original group that have 'a'
										for (int b = 0; b < groupMatchTiles [a].Count; b++) {
											if (!groupMatchTiles [originalGroupIndex].Contains (groupMatchTiles [a] [b])) {
												groupMatchTiles [originalGroupIndex].Add (groupMatchTiles [a] [b]);
											}
										}

										//clear group match tile
										groupMatchTiles [a].Clear ();
									}

									//already add current match to a group and break it
									break;
								}
							}
						}

					}
				}


			}
		}

		//remove empty groupmatch
		List<int> indexToRemove = new List<int>();
		for (int i = 0; i < groupMatchTiles.Count; i++) {
			if (groupMatchTiles [i].Count == 0) {
				indexToRemove.Add (i);
			}
		}

		indexToRemove.Sort ();

		for (int i = indexToRemove.Count - 1; i >= 0; i--) {
			//if (groupMatchTiles.Count > indexToRemove [i]) {
				groupMatchTiles.RemoveAt (indexToRemove [i]);
			//}
		}

		return groupMatchTiles;
	}

	private void FillEmptyDown(){

		List<List<Transform>> runeBoard = new List<List<Transform>> ();

		//construct rune board
		for (int i = 0; i < board.childCount; i++) {
			if (i % 7 == 0) {
				runeBoard.Add (new List<Transform> ());
			}

			if (board.GetChild (i).childCount > 0) {
				runeBoard [runeBoard.Count - 1].Add (board.GetChild (i).GetChild (0));
			} else {
				runeBoard [runeBoard.Count - 1].Add (null);
			}
		}

		//replace the empty tiles with the one above
		for (int i = runeBoard.Count - 1; i >= 0; i--) {
			for (int j = 0; j < runeBoard [i].Count; j++) {
				if (runeBoard [i] [j] == null) {
					for (int i2 = i; i2 >= 0; i2--) {
						if (runeBoard [i2] [j] != null) {
							runeBoard [i] [j] = runeBoard [i2] [j];
							runeBoard [i2] [j] = null;

							runeBoard [i] [j].SetParent (board.GetChild (i * 7 + j));
							runeBoard[i][j].localPosition = new Vector3 (0f, 100f, 0f);
							break;
						}
					}
				}
			}
		}

		//Instantiate the new tiles
		for (int a = 0; a < board.childCount; a++) {
			int i = a / 7;
			int j = a % 7;

			if (runeBoard [i] [j] == null) {
				string element = RandomElement ();

				GameObject g = fireRune;
				if (element == "fire") {
					g = Instantiate (fireRune, board.GetChild(a), false);
					g.transform.localPosition = new Vector3 (0f, 100f, 0f);
				} else if (element == "water") {
					g = Instantiate (waterRune, board.GetChild(a), false);
					g.transform.localPosition = new Vector3 (0f, 100f, 0f);
				} else if (element == "plant") {
					g = Instantiate (plantRune, board.GetChild(a), false);
					g.transform.localPosition = new Vector3 (0f, 100f, 0f);
				} else if (element == "light") {
					g = Instantiate (lightRune, board.GetChild(a), false);
					g.transform.localPosition = new Vector3 (0f, 100f, 0f);
				} else if (element == "dark") {
					g = Instantiate (darkRune, board.GetChild(a), false);
					g.transform.localPosition = new Vector3 (0f, 100f, 0f);
				} else if (element == "heart") {
					g = Instantiate (heartRune, board.GetChild(a), false);
					g.transform.localPosition = new Vector3 (0f, 100f, 0f);
				}

				runeBoard [i] [j] = g.transform; 
			}
		}

		for (int i = 0; i < runeBoard.Count; i++) {
			for (int j = 0; j < runeBoard [i].Count; j++) {
				runeBoard [i] [j].DOLocalMove (new Vector3 (0f, 0f, 0f), 0.5f).WaitForCompletion ();
			}
		}
	}

	public void Reboard(){
		for (int i = 0; i < board.childCount; i++) {
			Rune thisRune = board.GetChild (i).GetChild (0).GetComponent<Rune> ();
			thisRune.SwitchRuneType (RandomElement ());
		}
	}

	void IncreaseAttack(string element){

		for (int i = 0; i < GameplayManager.instance.monsterStatus.Count; i++) {
			if (GameplayManager.instance.monsterStatus [i].element == element) {
				damageTexts.GetChild (i).GetComponent<Text> ().color = damageTextColor [element];
				StartCoroutine (ChangeScale (damageTexts.GetChild (i), 1.5f, 0.2f));
				StartCoroutine (CountTo (damageTexts.GetChild (i).GetComponent<Text> (), Mathf.RoundToInt(elementDamages [element] * GameplayManager.instance.monsterStatus[i].attack.Value / 100f) , 1f));
			}
		}

		//other special rune
		if (element == "heart") {
			healText.gameObject.SetActive (true);
			StartCoroutine (ChangeScale (healText.transform, 1.5f, 0.2f));
			StartCoroutine (CountTo (healText, Mathf.RoundToInt(elementDamages [element] * GameplayManager.instance.totalRCV / 100f) , 1f));
		}

	}

	IEnumerator CountTo(Text uiText,int target,float time){
		float t = 0;

		int current = Int32.Parse (uiText.text);

		while (t < time) {
			uiText.text = Mathf.RoundToInt (Mathf.Lerp (current, target, t / time)).ToString ();
			t += Time.deltaTime;
			yield return null;
		}

		StartCoroutine (ChangeScale (uiText.transform, 1f, 0.2f));
	}

	IEnumerator ChangeScale(Transform ui,float target,float time){
		float t = 0;

		float startScale = ui.localScale.x;

		while (t < time) {
			float scale = Mathf.Lerp (startScale, target, t / time);
			ui.localScale = new Vector2 (scale, scale);
			t += Time.deltaTime;
			yield return null;
		}
	}
}
