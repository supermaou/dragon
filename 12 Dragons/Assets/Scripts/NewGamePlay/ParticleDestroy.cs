﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleDestroy : MonoBehaviour {

	// Use this for initialization
	void OnEnable () {
		StartCoroutine (DestroySelf ());
	}
	
	IEnumerator DestroySelf(){
		yield return new WaitForSeconds (0.5f);

		Destroy (gameObject);
	}
}
