﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MovingFloor : MonoBehaviour {

	public TextMeshProUGUI waveText;
	public GameObject warning;
	Vector3 startPos;

	void Awake(){
		startPos = transform.position;
	}

	public void Move(){

		waveText.text = "Wave " + GameplayManager.instance.currentWave.ToString () + "/" + GameplayManager.instance.enemiesData.Count.ToString ();

		if (GameplayManager.instance.currentWave < GameplayManager.instance.enemiesData.Count && (PersistentData.instance.gameType == "dungeon" || PersistentData.instance.gameType == "tower")) {
			waveText.gameObject.SetActive (true);
		}
		Vector3 target = Quaternion.Euler (70f, 0f, 0f) * new Vector3 (0f, -10f, 0f);

		StartCoroutine (SmoothMovement (transform.position + target, 1f));
	}

	IEnumerator SmoothMovement(Vector3 end,float time){

		float t = 0;
		Vector3 startPos = transform.position;

		while (t < time) {
			t += Time.deltaTime;
			transform.position = Vector3.Lerp (startPos, end, t);
			yield return null;
		}
			
		StartCoroutine (GameplayManager.instance.SpawnEnemies());
		waveText.gameObject.SetActive (false);

		if (warning.activeSelf == true) {
			SoundManager.instance.MuteEffectLoop ();
			warning.SetActive (false);
		}

		//Display Tutorial things
		if (GameplayManager.instance.currentWave == 1 && PlayerPrefs.GetInt ("tutorial", 0) == 0) {
			yield return new WaitForSeconds (1f);
			PlayerPrefs.SetInt ("tutorial", 1);
			GameplayManager.instance.howtoplayTutorial.SetActive (true);
		} else if (GameplayManager.instance.currentWave == 2 && PlayerPrefs.GetInt ("enemyDetailTutorial", 0) == 0) { //Last wave
			yield return new WaitForSeconds (1f);
			PlayerPrefs.SetInt ("enemyDetailTutorial", 1);
			GameplayManager.instance.enemyDetailTutorial.SetActive (true);
		} else if (GameplayManager.instance.currentWave == 2 && PlayerPrefs.GetInt ("elementTutorial", 0) == 0) {
			yield return new WaitForSeconds (1f);
			PlayerPrefs.SetInt ("elementTutorial", 1);
			GameplayManager.instance.elementTutorial.SetActive (true);
		} else if (GameplayManager.instance.currentWave == 2 && PlayerPrefs.GetInt ("targetTutorial", 0) == 0) {
			yield return new WaitForSeconds (1f);
			PlayerPrefs.SetInt ("targetTutorial", 1);
			GameplayManager.instance.targetTutorial.SetActive (true);
		}
	}

	public void ShowWarning(){

		if (PersistentData.instance.gameType == "dungeon" || PersistentData.instance.gameType == "tower") {
			warning.SetActive (true);
		}

		SoundManager.instance.PlayEffectLoop (GameplayManager.instance.warningSound);
	}
}
