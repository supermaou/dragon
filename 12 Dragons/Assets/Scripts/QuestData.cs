﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSparks.Core;
using UnityEngine.UI;

public class QuestData {

	public string questType;
	public List<int> targetValue;
	public List<string> reward;

	public int currentTargetIndex = 0;
	public int progress = 0;

	public void AddProgress(){
		if (currentTargetIndex >= targetValue.Count) {
			return;
		}

		string type = questType;

		if (type == "login") {
			GameSparksManager.instance.playerData.login++;
			progress = GameSparksManager.instance.playerData.login.Value;
		} else if (type == "clear") {
			GameSparksManager.instance.playerData.clear++;
			progress = GameSparksManager.instance.playerData.clear.Value;
		} else if (type == "gacha") {
			GameSparksManager.instance.playerData.gacha++;
			progress = GameSparksManager.instance.playerData.gacha.Value;
		} else if (type == "evolve") {
			GameSparksManager.instance.playerData.evolve++;
			progress = GameSparksManager.instance.playerData.evolve.Value;
		} else if (type == "fuse") {
			GameSparksManager.instance.playerData.fuse++;
			progress = GameSparksManager.instance.playerData.fuse.Value;
		} else if (type == "beat") {
			GameSparksManager.instance.playerData.beat++;
			progress = GameSparksManager.instance.playerData.beat.Value;
		} else if (type == "cleardun") {
			GameSparksManager.instance.playerData.dungeon++;
			progress = GameSparksManager.instance.playerData.dungeon.Value;
		} else if (type == "dailydun") {
			GameSparksManager.instance.playerData.dailydun++;
			progress = GameSparksManager.instance.playerData.dailydun.Value;
		} else if (type == "bossraid") {
			GameSparksManager.instance.playerData.bossraid++;
			progress = GameSparksManager.instance.playerData.bossraid.Value;
		}


		if (progress >= targetValue [currentTargetIndex]) {
			QuestSuccess ();
		}

		GameSparksManager.instance.SavePlayerData ();
	}

	void QuestSuccess(){ //When quest success, just call quest success. the current target value will be achieved
		if (currentTargetIndex >= targetValue.Count) {
			return;
		}

		string s = targetValue [currentTargetIndex].ToString () + ";" + questType + ";" + reward [currentTargetIndex].ToString ();
		GameSparksManager.instance.playerData.unclaimAchievement.Add (s);
		//the data will be saved in addprogress method

		currentTargetIndex++;
	}
}
