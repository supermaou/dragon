﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSparks.Core;

public class MonsterData {

	public string objectId = "";

	public string shortCode = "";

	public int isLocked = 0;

	public SafeInt level = new SafeInt (1);
	public SafeInt exp = new SafeInt(0);

	public List<int> team = new List<int> ();
	public List<int> order = new List<int> ();

	public string monsterName = "";
	public string thaiName = "";

	public List<string> evolveMaterials = new List<string> ();

	public int stars = 0;
	public string element = "";
	public string tribe = "";

	public SafeInt minAttack = new SafeInt (0);
	public SafeInt minHitPoint = new SafeInt (0);
	public SafeInt minRecovery = new SafeInt (0);

	public SafeInt maxAttack = new SafeInt (0);
	public SafeInt maxHitPoint = new SafeInt (0);
	public SafeInt maxRecovery = new SafeInt (0);

	public GSData leaderSkill;
	public GSData activeSkill;
	public SafeInt cooldown = new SafeInt(200);

	public SafeInt startExp = new SafeInt (2000);
	public SafeInt maxExp = new SafeInt (1000000);

	public float maxLevel;

	//growth factor
	public float gfAttack = 0f;
	public float gfHitPoint = 0f;
	public float gfRecovery = 0f;

	//Calculated variable
	public SafeInt hitPoint = new SafeInt(0);
	public SafeInt attack = new SafeInt (0);
	public SafeInt recovery = new SafeInt (0);

	public Ability leaderSkillBehavior;
	public Ability activeSkillBehavior;
	public SafeInt skillLevel = new SafeInt(1);
	public int maxSkillLevel;

	//Variable only assign in enemy
	public GSData enemySkill;
	public Ability enemySkillBehavior;
	public SafeInt defense;
	public SafeInt lootChance = new SafeInt(0);

	public void AssignAllSkills(){ //Use for evolve scene
		AssignLeaderSkill ();
		AssignActiveSkill ();
	}

	void AssignLeaderSkill(){ //for monster details page
		if (leaderSkill == null) {
			leaderSkillBehavior = new NoSkill ();
			leaderSkillBehavior.Initialize (leaderSkill);

			return;
		}

		if (leaderSkill.GetString ("type") == "BuffSelf") {
			leaderSkillBehavior = new BuffSelf ();
		} else if (leaderSkill.GetString ("type") == "IncreaseMoveTime") {
			leaderSkillBehavior = new IncreaseMoveTime ();
		} else if (leaderSkill.GetString ("type") == "AttackIncreaseWhenFullHp") {
			leaderSkillBehavior = new AttackIncreaseWhenFullHp ();
		} else if (leaderSkill.GetString ("type") == "AttackIncreaseWhenHpBelow") {
			leaderSkillBehavior = new AttackIncreaseWhenHpBelow ();
		} else if (leaderSkill.GetString ("type") == "BonusGold") {
			leaderSkillBehavior = new BonusGold ();
		} else if (leaderSkill.GetString ("type") == "IncreaseDropChance") {
			leaderSkillBehavior = new IncreaseDropChance ();
		} else if (leaderSkill.GetString ("type") == "BurstRuneTypesBuff") {
			leaderSkillBehavior = new BurstRuneTypesBuff ();
		} else if (leaderSkill.GetString ("type") == "NotDieOneTime") {
			leaderSkillBehavior = new NotDieOneTime ();
		} else if (leaderSkill.GetString ("type") == "TeamAllElements") {
			leaderSkillBehavior = new TeamAllElements ();
		} else if (leaderSkill.GetString ("type") == "TribeBuff") {
			leaderSkillBehavior = new TribeBuff ();
		} else if (leaderSkill.GetString ("type") == "BuffHitPoint") {
			leaderSkillBehavior = new BuffHitPoint ();
		} else if (leaderSkill.GetString ("type") == "DamageBuffOnRuneCount") {
			leaderSkillBehavior = new DamageBuffOnRuneCount ();
		}

		if (leaderSkillBehavior != null) {
			leaderSkillBehavior.Initialize (leaderSkill);
		}
	}

	void AssignActiveSkill(){ //for monster details page
		if (activeSkill == null) {
			activeSkillBehavior = new NoSkill ();
			activeSkillBehavior.Initialize (activeSkill);

			return;
		}

		if (activeSkill.GetString ("type") == "DealDamage") {
			activeSkillBehavior = new DealDamage ();
		} else if (activeSkill.GetString ("type") == "DealDamagePercent") {
			activeSkillBehavior = new DealDamagePercent ();
		} else if (activeSkill.GetString ("type") == "Heal") {
			activeSkillBehavior = new Heal ();
		} else if (activeSkill.GetString ("type") == "EnemyDebuff") {
			activeSkillBehavior = new EnemyDebuff ();
		} else if (activeSkill.GetString ("type") == "IncreaseCooldown") {
			activeSkillBehavior = new IncreaseCooldown ();
		} else if (activeSkill.GetString ("type") == "SwitchRune") {
			activeSkillBehavior = new SwitchRune ();
		} else if (activeSkill.GetString ("type") == "TemporaryBuff") {
			activeSkillBehavior = new TemporaryBuff ();
		}

		if (activeSkillBehavior != null) {
			activeSkillBehavior.Initialize (activeSkill);
		}
	}

	void AssignEnemySkill(){
		if (enemySkill.GetString ("type") == "ChangeRune") {
			enemySkillBehavior = new ChangeRune ();
		} else if (enemySkill.GetString ("type") == "EnemyHeal") {
			enemySkillBehavior = new EnemyHeal ();
		} else if (enemySkill.GetString ("type") == "IncreaseAttack") {
			enemySkillBehavior = new IncreaseAttack ();
		} else if (enemySkill.GetString ("type") == "MorethanCombo") {
			enemySkillBehavior = new MorethanCombo ();
		} else if (enemySkill.GetString ("type") == "MorethanTypes") {
			enemySkillBehavior = new MorethanTypes ();
		} else if (enemySkill.GetString ("type") == "RuneCountRequire") {
			enemySkillBehavior = new RuneCountRequire ();
		} else if (enemySkill.GetString ("type") == "BanRune") {
			enemySkillBehavior = new BanRune ();
		}

		enemySkillBehavior.Initialize (enemySkill);
	}

	public void SetStatToLevel(int level){
		this.level = new SafeInt (level);

		//set other stat here
		this.attack = new SafeInt(CurrentStat(minAttack.Value,maxAttack.Value,level,gfAttack));
		this.hitPoint = new SafeInt (CurrentStat (minHitPoint.Value, maxHitPoint.Value, level, gfHitPoint));
		this.recovery = new SafeInt (CurrentStat (minRecovery.Value, maxRecovery.Value, level, gfRecovery));
	}

	private int CurrentStat(int baseStat,int maxStat,int level, float gf){

		float stat = baseStat + (maxStat - baseStat) * Mathf.Pow ((level - 1f) / (maxLevel - 1f), gf);

		return Mathf.RoundToInt (stat);
	}

	public int StatDifference(int levelA, int levelB, int baseStat,int maxStat,float gf){
		//levelA higher than levelB

		float statA = baseStat + (maxStat - baseStat) * Mathf.Pow ((levelA - 1f) / (maxLevel - 1f), gf);
		float statB = baseStat + (maxStat - baseStat) * Mathf.Pow ((levelB - 1f) / (maxLevel - 1f), gf);

		return Mathf.RoundToInt (statA - statB);
	}

	//return level according to exp
	public int CalculateLevel(int exp){
		for (int i = 1; i < 100; i++) {
			int level = 1;
			int cumulativeExp = CumulativeExp (i);

			if (exp < cumulativeExp) {
				level = i - 1;
				if (level > maxLevel) {
					return (int)maxLevel;
				} else {
					return level;
				}
			}
		}

		return 1;
	}

	//return cumulative exp upto the level
	private int CumulativeExp(int level){
		float currentLevelExp = maxExp.Value * Mathf.Pow ((level - 1f) / 98f, 2.5f);
		return Mathf.RoundToInt (currentLevelExp);
	}

	//return exp left require for next level
	public int ExpRequireForNextLevel(){
		if (level.Value >= maxLevel) {
			return 0;
		}

		float nextLevelExp = maxExp.Value * Mathf.Pow ((level.Value) / 98f, 2.5f);

		return Mathf.RoundToInt (nextLevelExp - exp.Value);
	}

	public int TotalExpForMaxLevel(){
		float maxLevelExp = maxExp.Value * Mathf.Pow ((maxLevel - 1f) / 98f, 2.5f);
		return Mathf.FloorToInt (maxLevelExp);
	}

	//return all exp require for next level
	public int TotalExpRequireForNextLevel(){
		float currentLevelExp = maxExp.Value * Mathf.Pow ((level.Value - 1f) / 98f, 2.5f);
		float nextLevelExp = maxExp.Value * Mathf.Pow ((level.Value) / 98f, 2.5f);

		return Mathf.RoundToInt (nextLevelExp - currentLevelExp);
	}
}
