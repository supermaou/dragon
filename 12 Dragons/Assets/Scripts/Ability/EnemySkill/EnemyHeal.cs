﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSparks.Core;

public class EnemyHeal : Ability {

	int percentHeal; //10 or 20 or others
	int cooldown;

	int cooldownLeft;

	public override void Initialize (GSData abilityData){
		percentHeal = abilityData.GetInt ("percentHeal") ?? 0;
		cooldown = abilityData.GetInt ("cooldown") ?? 6;

		cooldownLeft = cooldown;

		aDescription = "ฟื้นฟูพลังชีวิต " + percentHeal.ToString() + "% ของพลังชีวิตสูงสุดทุกๆ " + cooldown.ToString() + " เทิร์น";

		cooldownLeft++;
	}

	public override void TriggerAbility(MonsterController m){
		cooldownLeft--;

		if (cooldownLeft <= 0) {
			cooldownLeft = cooldown;

			//Heal itself
			m.GetComponent<Enemy>().Heal( Mathf.RoundToInt(percentHeal * m.hitPoint.Value / 100f) );
		}
	}
}
