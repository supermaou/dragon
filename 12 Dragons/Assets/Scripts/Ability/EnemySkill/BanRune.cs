﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSparks.Core;

public class BanRune : Ability {

	string banRune;

	public override void Initialize (GSData abilityData){
		banRune = abilityData.GetString ("banRune");

		aDescription = "ทำให้ไม่มีรูน";

		if (banRune == "fire") {
			aDescription = aDescription + "ไฟ";
		} else if (banRune == "water") {
			aDescription = aDescription + "น้ำ";
		} else if (banRune == "plant") {
			aDescription = aDescription + "ดิน";
		} else if (banRune == "light") {
			aDescription = aDescription + "แสง";
		} else if (banRune == "dark") {
			aDescription = aDescription + "ความมืด";
		} else if (banRune == "heart") {
			aDescription = aDescription + "หัวใจ";
		}

		aDescription = aDescription + "ตกลงมา";
	}

	public override void TriggerAbility(MonsterController m){
		BoardManager.instance.banRune = banRune;
	}
}
