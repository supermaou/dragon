﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSparks.Core;

public class ChangeRune : Ability {

	string toRune;
	int count;

	int cooldown;

	int cooldownLeft;

	public override void Initialize (GSData abilityData){
		toRune = abilityData.GetString ("toRune");
		count = abilityData.GetInt ("count") ?? 1;
		cooldown = abilityData.GetInt ("cooldown") ?? 6;

		cooldownLeft = cooldown;

		aDescription = "เปลี่ยนรูนจำนวน " + count.ToString() + " รูน เป็นรูน";
		if (toRune == "fire") {
			aDescription = aDescription + "ไฟ";
		} else if (toRune == "water") {
			aDescription = aDescription + "น้ำ";
		} else if (toRune == "plant") {
			aDescription = aDescription + "ดิน";
		} else if (toRune == "light") {
			aDescription = aDescription + "แสง";
		} else if (toRune == "dark") {
			aDescription = aDescription + "ความมืด";
		} else if (toRune == "heart") {
			aDescription = aDescription + "หัวใจ";
		}

		aDescription = aDescription + "แบบสุ่ม ทุกๆ " + cooldown.ToString() + " เทิร์น";

		cooldownLeft++; //to compensate when call trigger ability at the start of enemy instance
	}

	public override void TriggerAbility(MonsterController m){
		cooldownLeft--;

		if (cooldownLeft <= 0) {
			cooldownLeft = cooldown;

			List<int> randomIndexes = new List<int> ();
			for (int i = 0; i < count; i++) {
				
				int rand = Random.Range (0, BoardManager.instance.board.childCount);

				while (randomIndexes.Contains (rand)) {
					rand = Random.Range (0, BoardManager.instance.board.childCount);
				}

				randomIndexes.Add (rand);
			}

			for (int i = 0; i < randomIndexes.Count; i++) {
				Rune thisRune = BoardManager.instance.board.GetChild (randomIndexes [i]).GetChild(0).GetComponent<Rune> ();
				thisRune.SwitchRuneType (toRune);
			}
		}
	}
}
