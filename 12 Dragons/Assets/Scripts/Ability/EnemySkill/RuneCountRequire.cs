﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSparks.Core;

public class RuneCountRequire : Ability {

	string requiredRune;
	int count;

	private bool isActivate = false;

	public override void Initialize (GSData abilityData){
		requiredRune = abilityData.GetString ("requiredRune");
		count = abilityData.GetInt ("count") ?? 0;

		aDescription = "ได้รับความเสียหายเมื่อสลายรูน";
		if (requiredRune == "fire") {
			aDescription = aDescription + "ไฟ";
		} else if (requiredRune == "water") {
			aDescription = aDescription + "น้ำ";
		} else if (requiredRune == "plant") {
			aDescription = aDescription + "ดิน";
		} else if (requiredRune == "light") {
			aDescription = aDescription + "แสง";
		} else if (requiredRune == "dark") {
			aDescription = aDescription + "ความมืด";
		} else if (requiredRune == "heart") {
			aDescription = aDescription + "หัวใจ";
		}

		aDescription = aDescription + " " + count.ToString () + " รูนขึ้นไป";
	}

	public override void TriggerAbility(MonsterController m){
		if (isActivate) {
			return;
		}

		Enemy e = m.GetComponent<Enemy> ();
		e.requiredRune = requiredRune;
		e.requiredRuneCount = count;

		isActivate = true;
	}
}
