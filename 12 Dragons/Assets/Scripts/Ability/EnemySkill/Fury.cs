﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSparks.Core;

public class Fury : Ability {

	int attackIncrease; //Percent base such as 100% or 200%

	bool isActivate = false;

	public override void Initialize (GSData abilityData){
		attackIncrease = abilityData.GetInt ("attackIncrease") ?? 0;

		aDescription = "เมื่อพลังชีวิตเหลือน้อยกว่าพลังชีวิตสูงสุดจะเพิ่มพลังโจมตีเป็น " + attackIncrease.ToString() + "%";
	}

	public override void TriggerAbility(MonsterController m){
		if (isActivate) {
			return;
		}

		int currentHp = (int) m.GetComponent<Enemy>().currentHp.Value;
		int totalHp = m.hitPoint.Value;

		if (currentHp < totalHp) {
			m.attack = new SafeInt (Mathf.RoundToInt (attackIncrease * m.attack.Value / 100f));
			isActivate = true;
		}
	}
}
