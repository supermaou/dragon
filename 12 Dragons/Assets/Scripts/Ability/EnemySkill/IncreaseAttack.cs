﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSparks.Core;

public class IncreaseAttack : Ability {

	int percentIncrease; //10 or 20 or sth
	int cooldown;

	int cooldownLeft;

	public override void Initialize (GSData abilityData){
		percentIncrease = abilityData.GetInt ("percentIncrease") ?? 0;
		cooldown = abilityData.GetInt ("cooldown") ?? 6;

		cooldownLeft = cooldown;

		aDescription = "เพิ่มพลังโจมตี " + percentIncrease.ToString() + "% ทุกๆ " + cooldown.ToString() + " เทิร์น";

		cooldownLeft++;
	}

	public override void TriggerAbility(MonsterController m){
		cooldownLeft--;

		if (cooldownLeft <= 0) {
			cooldownLeft = cooldown;

			m.attack += Mathf.RoundToInt (percentIncrease * m.attack.Value / 100f);
		}
	}
}
