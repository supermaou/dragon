﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSparks.Core;

public class MorethanCombo : Ability {

	int comboNeed;

	public override void Initialize (GSData abilityData){
		comboNeed = abilityData.GetInt ("comboNeed") ?? 0;

		aDescription = "รับความเสียหารจากการโจมตี " + comboNeed.ToString() + " ตอมโบขึ้นไปเท่านั้น";
	}

	public override void TriggerAbility(MonsterController m){
		m.GetComponent<Enemy> ().requiredCombo = comboNeed;
	}
}
