﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSparks.Core;

public class MorethanTypes : Ability {

	List<string> types;

	bool isActivate = false;

	public override void Initialize (GSData abilityData){
		types = abilityData.GetStringList ("types");

		aDescription = "รับความเสียหายเมื่อสลายรูน";

		for (int i = 0; i < types.Count; i++) {
			if (types [i] == "fire") {
				aDescription = aDescription + "ไฟ";
			} else if (types [i] == "water") {
				aDescription = aDescription + "น้ำ";
			} else if (types [i] == "plant") {
				aDescription = aDescription + "ดิน";
			} else if (types [i] == "light") {
				aDescription = aDescription + "แสง";
			} else if (types [i] == "dark") {
				aDescription = aDescription + "ความมืด";
			}

			if (i < types.Count - 1) {
				aDescription = aDescription + ", ";
			}
		}

		aDescription = aDescription + " ในเทิร์นเดียวกันเท่านั้น";
	}

	public override void TriggerAbility(MonsterController m){
		if (isActivate) {
			return;
		}

		m.GetComponent<Enemy> ().fixTypes = types;
		isActivate = true;
	}
}
