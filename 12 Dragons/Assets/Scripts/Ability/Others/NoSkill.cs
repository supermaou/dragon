﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSparks.Core;

public class NoSkill : Ability {

	public override void Initialize (GSData abilityData){
		aDescription = "ไม่มี";
	}

	public override void TriggerAbility(MonsterController m){
		return;
	}
}
