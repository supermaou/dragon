﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSparks.Core;

public class TemporaryBuff : Ability {

	string typeBuff;

	string elementBuff;
	float multiplier; //such as 1.5

	int damageReduction; // percent such as 50% or 30%

	int turnBuff;

	string fromRune;
	string toRune;
	int runeCount;

	public override void Initialize (GSData abilityData){
		typeBuff = abilityData.GetString ("typeBuff");

		elementBuff = abilityData.GetString ("elementBuff");
		multiplier = abilityData.GetFloat ("multiplier") ?? 1;

		damageReduction = abilityData.GetInt ("damageReduction") ?? 0;

		turnBuff = abilityData.GetInt ("turnBuff") ?? 1;

		fromRune = abilityData.GetString ("fromRune") ?? "";
		toRune = abilityData.GetString ("toRune") ?? "";
		runeCount = abilityData.GetInt ("runeCount") ?? 0;

		aDescription = "";
		if (typeBuff == "atk") {
			aDescription = "เพิ่มพลังโจมตี";
		} else if (typeBuff == "def") {
			aDescription = "ลดความเสียหาย";
		}

		if (elementBuff == "fire") {
			aDescription = aDescription + "ธาตุไฟ";
		} else if (elementBuff == "water") {
			aDescription = aDescription + "ธาตุน้ำ";
		} else if (elementBuff == "plant") {
			aDescription = aDescription + "ธาตุดิน";
		} else if (elementBuff == "light") {
			aDescription = aDescription + "ธาตุแสง";
		} else if (elementBuff == "dark") {
			aDescription = aDescription + "ธาตุความมืด";
		}

		if (typeBuff == "atk") {
			aDescription = aDescription + " " + multiplier.ToString() + " เท่า";
		} else if (typeBuff == "def") {
			aDescription = aDescription + " " + damageReduction.ToString () + "%";
		}

		aDescription = aDescription + " เป็นเวลา " + turnBuff.ToString() + " เทิร์น";

		if (fromRune != "" && toRune != "") {
			aDescription = aDescription + " และเปลี่ยนรูน";

			if (fromRune == "fire") {
				aDescription = aDescription + "ไฟ";
			} else if (fromRune == "water") {
				aDescription = aDescription + "น้ำ";
			} else if (fromRune == "plant") {
				aDescription = aDescription + "ดิน";
			} else if (fromRune == "light") {
				aDescription = aDescription + "แสง";
			} else if (fromRune == "dark") {
				aDescription = aDescription + "ความมืด";
			} else if (fromRune == "heart") {
				aDescription = aDescription + "หัวใจ";
			}

			if (runeCount > 0) {
				aDescription = aDescription + "สูงสุด " + runeCount.ToString () + " รูน ";
			}

			aDescription = aDescription + "เป็นรูน";

			if (toRune == "fire") {
				aDescription = aDescription + "ไฟ";
			} else if (toRune == "water") {
				aDescription = aDescription + "น้ำ";
			} else if (toRune == "plant") {
				aDescription = aDescription + "ดิน";
			} else if (toRune == "light") {
				aDescription = aDescription + "แสง";
			} else if (toRune == "dark") {
				aDescription = aDescription + "ความมืด";
			} else if (toRune == "heart") {
				aDescription = aDescription + "หัวใจ";
			}
		}
	}

	public override void TriggerAbility(MonsterController m){
		//Check condition
		for (int i = 0; i < BoardManager.instance.buffAttacks.Count; i++) { //the same monster cannot use skill at the same time
			if (BoardManager.instance.buffAttacks [i].monsterUsed == m.shortCode.Split('.')[0]) {
				GameplayManager.instance.OnSkillError ();
				return;
			}
		}

		for (int i = 0; i < BoardManager.instance.buffDefenses.Count; i++) { //the same monster cannot use skill at the same time
			if (BoardManager.instance.buffDefenses [i].monsterUsed == m.shortCode.Split('.')[0]) {
				GameplayManager.instance.OnSkillError ();
				return;
			}
		}

		//Use skill for real
		if (typeBuff == "atk") {
			GameplayManager.instance.BuffSelfTemporary (m.shortCode,typeBuff, elementBuff, multiplier, turnBuff);
		} else if (typeBuff == "def") {
			GameplayManager.instance.BuffSelfTemporary (m.shortCode,typeBuff, "", 0f, turnBuff, damageReduction);
		}

		if (fromRune != "") {
			if (runeCount == 0) {
				for (int i = 0; i < BoardManager.instance.board.childCount; i++) {
					Rune thisRune = BoardManager.instance.board.GetChild (i).GetChild (0).GetComponent<Rune> ();
					if (thisRune.element == fromRune) {
						thisRune.SwitchRuneType (toRune);
					}
				}
			} else {
				List<Rune> runeList = new List<Rune> ();

				for (int i = 0; i < BoardManager.instance.board.childCount; i++) {
					Rune thisRune = BoardManager.instance.board.GetChild (i).GetChild (0).GetComponent<Rune> ();
					if (thisRune.element == fromRune) {
						runeList.Add (thisRune);
					}
				}

				for (int i = 0; i < runeCount; i++) {
					if (runeList.Count <= 0) {
						break;
					}

					int rand = Random.Range (0, runeList.Count);
					runeList [rand].SwitchRuneType (toRune);
					runeList.RemoveAt (rand);
				}
			}
		}

		GameplayManager.instance.ResetCooldown (m);
	}
}
