﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSparks.Core;

public class DealDamage : Ability {

	string vfx;
	int fixDamage;
	int attackMultiply;
	bool isTargetAll;

	string elementBuff;

	//multiplier base such as 1.5 or 2 or something
	float atkSelfBuff;

	//percent base such as 50% 30% or so
	float atkEnemyDebuff;
	float defEnemyDebuff;

	int turnBuff;

	string fromRune;
	string toRune;

	public override void Initialize (GSData abilityData){
		vfx = abilityData.GetString ("vfx");

		fixDamage = abilityData.GetInt ("fixDamage") ?? 0;
		attackMultiply = abilityData.GetInt ("attackMultiply") ?? 0;

		isTargetAll = abilityData.GetBoolean ("isTargetAll") ?? false;

		elementBuff = abilityData.GetString ("elementBuff") ?? null;
		atkSelfBuff = abilityData.GetFloat ("atkSelfBuff") ?? 0;

		atkEnemyDebuff = abilityData.GetFloat ("atkEnemyDebuff") ?? 0;
		defEnemyDebuff = abilityData.GetFloat ("defEnemyDebuff") ?? 0;

		turnBuff = abilityData.GetInt ("turnBuff") ?? 0;

		fromRune = abilityData.GetString ("fromRune") ?? "";
		toRune = abilityData.GetString ("toRune") ?? "";

		//set description
		aDescription = "โจมตีด้วยพลังโจมตี";

		if (fixDamage > 0) {
			aDescription = aDescription + " " + fixDamage.ToString () + " แก่ศัตรู";
		} else if (attackMultiply > 0) {
			aDescription = aDescription + " " + attackMultiply.ToString() + " เท่าของพลังโจมตีแก่ศัตรู";
		}

		if (isTargetAll) {
			aDescription = aDescription + "ทุกตัว";
		} else {
			aDescription = aDescription + " 1 ตัว";
		}

		if(atkSelfBuff > 0){
			aDescription = aDescription + "และเพิ่มพลังโจมตีธาตุ";

			if (elementBuff == "fire") {
				aDescription = aDescription + "ไฟ";
			} else if (elementBuff == "water") {
				aDescription = aDescription + "น้ำ";
			} else if (elementBuff == "plant") {
				aDescription = aDescription + "ดิน";
			} else if (elementBuff == "light") {
				aDescription = aDescription + "แสง";
			} else if (elementBuff == "dark") {
				aDescription = aDescription + "ความมืด";
			}

			aDescription = aDescription + " " + atkSelfBuff.ToString () + " เท่า เป็นเวลา " + turnBuff.ToString () + " เทิร์น";
		}

		if (atkEnemyDebuff > 0) {
			aDescription = aDescription + "และลดพลังโจมตีของศัตรู " + atkEnemyDebuff.ToString() + "% เป็นเวลา " + turnBuff.ToString() + " เทิร์น";
		}

		if (defEnemyDebuff > 0) {
			aDescription = aDescription + "และลดพลังป้องกันของศัตรู " + defEnemyDebuff.ToString() + "% เป็นเวลา " + turnBuff.ToString() + " เทิร์น";
		}

		if (toRune != "" && fromRune != "") {
			aDescription = aDescription + " และเปลี่ยนรูน";

			if (fromRune == "fire") {
				aDescription = aDescription + "ไฟ";
			} else if (fromRune == "water") {
				aDescription = aDescription + "น้ำ";
			} else if (fromRune == "plant") {
				aDescription = aDescription + "ดิน";
			} else if (fromRune == "light") {
				aDescription = aDescription + "แสง";
			} else if (fromRune == "dark") {
				aDescription = aDescription + "ความมืด";
			} else if (fromRune == "heart") {
				aDescription = aDescription + "หัวใจ";
			}

			aDescription = aDescription + "เป็นรูน";

			if (toRune == "fire") {
				aDescription = aDescription + "ไฟ";
			} else if (toRune == "water") {
				aDescription = aDescription + "น้ำ";
			} else if (toRune == "plant") {
				aDescription = aDescription + "ดิน";
			} else if (toRune == "light") {
				aDescription = aDescription + "แสง";
			} else if (toRune == "dark") {
				aDescription = aDescription + "ความมืด";
			} else if (toRune == "heart") {
				aDescription = aDescription + "หัวใจ";
			}
		}
	}

	public override void TriggerAbility(MonsterController m){

		if (attackMultiply > 0) {
			int damage = Mathf.RoundToInt (m.attack.Value * attackMultiply);
			GameplayManager.instance.DealDamageAbility (m, vfx, damage, isTargetAll);
		}

		if (fixDamage > 0) {
			GameplayManager.instance.DealDamageAbility (m, vfx, fixDamage, isTargetAll);
		}

		if (atkSelfBuff > 0) {
			GameplayManager.instance.BuffSelfPermanent ("atk", elementBuff, atkSelfBuff);
		}

		GameplayManager.instance.ResetCooldown (m);
	}

}
