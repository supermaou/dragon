﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSparks.Core;

public class Heal : Ability {

	int percentHeal; //50 or 30 or 100

	public override void Initialize (GSData abilityData){
		percentHeal = abilityData.GetInt ("percentHeal") ?? 0;

		aDescription = "ฟื้นฟูพลังชีวิต " + percentHeal.ToString () + "% ของพลังชีวิตสูงสุด";
	}

	public override void TriggerAbility(MonsterController m){
		if (GameplayManager.instance.currentHp == GameplayManager.instance.totalHp) {
			GameplayManager.instance.OnSkillError ();
			return;
		}

		if (percentHeal > 0) {
			int amount = Mathf.RoundToInt (percentHeal * GameplayManager.instance.totalHp / 100f);
			GameplayManager.instance.Heal (amount);
		}

		GameplayManager.instance.ResetCooldown (m);
	}
}
