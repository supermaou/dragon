﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSparks.Core;

public class DealDamagePercent : Ability {

	string vfx;
	int percentDamage;
	bool isTargetAll;

	public override void Initialize (GSData abilityData){
		vfx = SkillVfxList.instance.vfxs [abilityData.GetInt ("vfx") ?? 0].skillName;

		percentDamage = abilityData.GetInt ("percentDamage") ?? 0;
		isTargetAll = abilityData.GetBoolean ("isTargetAll") ?? false;

		aDescription = "ลดพลังชีวิตของศัตรู";

		if (isTargetAll) {
			aDescription = aDescription + "ทุกตัว";
		} else {
			aDescription = aDescription + " 1 ตัว";
		}

		aDescription = aDescription + " จำนวน " + percentDamage.ToString() + "% ของพลังชีวิตที่เหลืออยู่";
	}

	public override void TriggerAbility(MonsterController m){
		if (!isTargetAll) {
			Enemy e = GameplayManager.instance.currentEnemy.GetComponent<Enemy> ();

			int damage =  (int) e.currentHp.Value * percentDamage;

			if (PersistentData.instance.gameType == "worldBoss") { //Effect only 1000 for world boss
				damage = 1000;
			}

			GameplayManager.instance.DealDamageAbility (m, vfx, damage, false, true);
		} else {
			for (int i = 0; i < GameplayManager.instance.enemies.Count; i++) {
				Enemy e = GameplayManager.instance.enemies [i].GetComponent<Enemy> ();

				int damage = (int) e.currentHp.Value * percentDamage;

				if (PersistentData.instance.gameType == "worldBoss") { //Effect only 1000 for world boss
					damage = 1000;
				}

				GameplayManager.instance.DealDamageAbility (m, vfx, damage, false, true);
			}	
		}

		GameplayManager.instance.ResetCooldown (m);
	}
}
