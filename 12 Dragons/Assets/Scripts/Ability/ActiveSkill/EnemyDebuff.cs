﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSparks.Core;

public class EnemyDebuff : Ability {

	string typeBuff;

	int percentDebuff;
	int turnBuff;

	string elementDebuff;

	string fromRune;
	string toRune;

	//target all enemies by default (always)

	public override void Initialize (GSData abilityData){
		typeBuff = abilityData.GetString ("typeBuff");
		percentDebuff = abilityData.GetInt ("percentDebuff") ?? 0;

		turnBuff = abilityData.GetInt ("turnBuff") ?? 0;

		elementDebuff = abilityData.GetString ("elementDebuff") ?? "";

		fromRune = abilityData.GetString ("fromRune") ?? "";
		toRune = abilityData.GetString ("toRune") ?? "";

		aDescription = "ลดพลัง";
		if (typeBuff == "atk") {
			aDescription = aDescription + "โจมตีแก่ศัตรู";

			if (elementDebuff != "") {
				aDescription = aDescription + "ธาตุ" + GetElementText (elementDebuff);
			} else {
				aDescription = aDescription + "ทุกตัว";
			}

		} else if (typeBuff == "def") {
			aDescription = aDescription + "ป้องกันของศัตรูทุกตัว";
		}

		aDescription = aDescription + " " + percentDebuff.ToString() + "% เป็นเวลา " + turnBuff.ToString() + " เทิร์น";

		if (fromRune != "" && toRune != "") {
			aDescription = aDescription + " และเปลี่ยนรูน";

			aDescription = aDescription + GetElementText(fromRune) + "เป็นรูน" + GetElementText (toRune);

		}
	}

	string GetElementText(string rune){
		if (rune == "fire") {
			return "ไฟ";
		} else if (rune == "water") {
			return "น้ำ";
		} else if (rune == "plant") {
			return "ดิน";
		} else if (rune == "light") {
			return "แสง";
		} else if (rune == "dark") {
			return "ความมืด";
		} else if (rune == "heart") {
			return "หัวใจ";
		}

		return "";
	}

	public override void TriggerAbility(MonsterController m){
		if (typeBuff == "atk") {
			for (int i = 0; i < GameplayManager.instance.enemies.Count; i++) { //Can not debuff enemy if the other debuff still in effect
				Enemy e = GameplayManager.instance.enemies [i].GetComponent<Enemy> ();
				if (e.turnBuffAttack > 0) {
					GameplayManager.instance.OnSkillError ();
					return;
				}
			}

			if (elementDebuff != "") { //Check if buff attack at specific element have any enemy with that element, if not, cannot be used.
				bool isValid = false;
				for (int i = 0; i < GameplayManager.instance.enemies.Count; i++) {
					MonsterController mm = GameplayManager.instance.enemies [i].GetComponent<MonsterController> ();
					if (mm.element == elementDebuff) {
						isValid = true;
						break;
					}
				}

				if (!isValid) {
					GameplayManager.instance.OnSkillError ();
					return;
				}
			}

		} else if (typeBuff == "def") {
			for (int i = 0; i < GameplayManager.instance.enemies.Count; i++) { //Can not debuff enemy if the other debuff still in effect
				Enemy e = GameplayManager.instance.enemies [i].GetComponent<Enemy> ();
				if (e.turnBuffDefense > 0) {
					GameplayManager.instance.OnSkillError ();
					return;
				}
			}
		}

		GameplayManager.instance.DebuffTarget (typeBuff, percentDebuff, turnBuff,elementDebuff);

		if (fromRune != "") {
			for (int i = 0; i < BoardManager.instance.board.childCount; i++) {
				Rune thisRune = BoardManager.instance.board.GetChild (i).GetChild (0).GetComponent<Rune> ();
				if (thisRune.element == fromRune) {
					thisRune.SwitchRuneType (toRune);
				}
			}
		}

		GameplayManager.instance.ResetCooldown (m);
	}
}
