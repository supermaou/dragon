﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSparks.Core;

public class IncreaseCooldown : Ability {

	int increaseCooldown;
	bool isTargetAll;

	public override void Initialize (GSData abilityData){
		increaseCooldown = abilityData.GetInt ("increaseCooldown") ?? 0;
		isTargetAll = abilityData.GetBoolean ("isTargetAll") ?? false;

		aDescription = "เพิ่มคูลดาวน์การโจมตีของศัตรู";

		if (isTargetAll) {
			aDescription = aDescription + "ทุกตัว";
		} else {
			aDescription = aDescription + " 1 ตัว";
		}

		aDescription = aDescription + " เป็นเวลา " + increaseCooldown.ToString() + " เทิร์น";
	}

	public override void TriggerAbility(MonsterController m){
		if (!isTargetAll) {
			Enemy e = GameplayManager.instance.currentEnemy.GetComponent<Enemy> ();
			e.cooldown += increaseCooldown;
		} else {
			for (int i = 0; i < GameplayManager.instance.enemies.Count; i++) {
				Enemy e = GameplayManager.instance.enemies [i].GetComponent<Enemy> ();
				e.cooldown += increaseCooldown;
			}
		}

		GameplayManager.instance.ResetCooldown (m);
	}
}
