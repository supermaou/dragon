﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSparks.Core;

public class SwitchRune : Ability {

	string fromRune;
	string toRune; //fire, water, or random

	int count = 0;

	public override void Initialize (GSData abilityData){
		fromRune = abilityData.GetString ("fromRune") ?? "";
		toRune = abilityData.GetString ("toRune") ?? "";
		count = abilityData.GetInt ("count") ?? 0;

		aDescription = aDescription + "เปลี่ยนรูน";

		if (fromRune == "fire") {
			aDescription = aDescription + "ไฟ";
		} else if (fromRune == "water") {
			aDescription = aDescription + "น้ำ";
		} else if (fromRune == "plant") {
			aDescription = aDescription + "ดิน";
		} else if (fromRune == "light") {
			aDescription = aDescription + "แสง";
		} else if (fromRune == "dark") {
			aDescription = aDescription + "ความมืด";
		} else if (fromRune == "heart") {
			aDescription = aDescription + "หัวใจ";
		} else if (fromRune == "" && count > 0) {
			aDescription = aDescription + " " + count.ToString() + " รูนแบบสุ่ม ";
		}

		aDescription = aDescription + "เป็นรูน";

		if (toRune == "fire") {
			aDescription = aDescription + "ไฟ";
		} else if (toRune == "water") {
			aDescription = aDescription + "น้ำ";
		} else if (toRune == "plant") {
			aDescription = aDescription + "ดิน";
		} else if (toRune == "light") {
			aDescription = aDescription + "แสง";
		} else if (toRune == "dark") {
			aDescription = aDescription + "ความมืด";
		} else if (toRune == "heart") {
			aDescription = aDescription + "หัวใจ";
		}
	}

	public override void TriggerAbility(MonsterController m){
		if (fromRune != "") {
			for (int i = 0; i < BoardManager.instance.board.childCount; i++) {
				//search for 'fromRune' element rune
				Rune thisRune = BoardManager.instance.board.GetChild (i).GetChild (0).GetComponent<Rune> ();
				if (thisRune.element == fromRune) {
					thisRune.SwitchRuneType (toRune);
				}
			}
		} else {
			//Find runes to change from
			List<int> indexesToChange = new List<int> ();
			for (int i = 0; i < count; i++) {
				bool ok = false;
				int c = 0;
				while (!ok) {
					int rand = Random.Range (0, BoardManager.instance.board.childCount);
					Rune thisRune = BoardManager.instance.board.GetChild (rand).GetChild (0).GetComponent<Rune> ();
					if (thisRune.element != toRune && !indexesToChange.Contains (rand)) {
						indexesToChange.Add (rand);
						ok = true;
					}
					c++;
					if (c > 10) {
						ok = true;
					}
				}
			}

			//Change runes
			for (int i = 0; i < indexesToChange.Count; i++) {
				Rune thisRune = BoardManager.instance.board.GetChild (indexesToChange[i]).GetChild (0).GetComponent<Rune> ();
				thisRune.SwitchRuneType (toRune);
			}
		}

		GameplayManager.instance.ResetCooldown (m);
	}
}
