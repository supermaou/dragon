﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSparks.Core;

public class IncreaseDropChance : Ability {

	int increaseDropChance;

	public override void Initialize (GSData abilityData){
		increaseDropChance = abilityData.GetInt ("increaseDropChance") ?? 0;

		aDescription = "เพิ่มอัตราการดรอปมอนสเตอร์ทุกชนิด " + increaseDropChance.ToString() + "%";
	}

	public override void TriggerAbility(MonsterController m){
		GameplayManager.instance.dropChanceIncrease = increaseDropChance;
	}
}
