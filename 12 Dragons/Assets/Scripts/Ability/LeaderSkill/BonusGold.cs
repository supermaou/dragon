﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSparks.Core;

public class BonusGold : Ability {

	int goldBonusPercent;

	public override void Initialize (GSData abilityData){
		goldBonusPercent = abilityData.GetInt ("goldBonusPercent") ?? 0;

		aDescription = "เพิ่มโบนัสเงินที่ได้จากด่าน " + goldBonusPercent.ToString() + "%";
	}

	public override void TriggerAbility(MonsterController m){
		GameplayManager.instance.goldBonusPercent = goldBonusPercent;
	}
}
