﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSparks.Core;

public class AttackIncreaseWhenHpBelow : Ability {

	float multiplier; //multiplier base
	int hpBelow; //Percent base

	List<string> elementKeys = new List<string>(); //element keys for setting the damage
	bool isAdded = false;
	//Test Complete
	public override void Initialize (GSData abilityData){
		multiplier = abilityData.GetFloat ("multiplier") ?? 1;
		hpBelow = abilityData.GetInt ("hpBelow") ?? 0;

		if (BoardManager.instance != null) {
			//collect key for each element
			foreach (string key in BoardManager.instance.elementDamages.Keys) {
				if (key != "heart") {
					elementKeys.Add (key);
				}
			}
		}

		aDescription = "เมื่อพลังชีวิตต่ำกว่า " + hpBelow.ToString () + "% จะเพิ่มพลังโจมตีเป็น " + multiplier.ToString () + " เท่า";
	}

	public override void TriggerAbility(MonsterController m){
		if (GameplayManager.instance.currentHp <= Mathf.RoundToInt (GameplayManager.instance.totalHp * hpBelow / 100f) && isAdded == false) {
			isAdded = true;
			for (int i = 0; i < elementKeys.Count; i++) {
				if (elementKeys [i] != "heart") {
					BoardManager.instance.buffAttacksPermanent [elementKeys [i]] *= multiplier;
				}
			}
		}

		if (GameplayManager.instance.currentHp > Mathf.RoundToInt (GameplayManager.instance.totalHp * hpBelow / 100f) && isAdded == true) {
			isAdded = false;
			for (int i = 0; i < elementKeys.Count; i++) {
				if (elementKeys [i] != "heart") {
					BoardManager.instance.buffAttacksPermanent [elementKeys [i]] /= multiplier;
				}
			}
		}
	}
}
