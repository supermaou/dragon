﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSparks.Core;

public class AttackIncreaseWhenFullHp : Ability {

	float multiplier; //multiplier base

	List<string> elementKeys = new List<string>(); //element keys for setting the damage
	bool isAdded = false;

	//Test Complete
	public override void Initialize (GSData abilityData){
		multiplier = abilityData.GetFloat ("multiplier") ?? 1f;

		if (BoardManager.instance != null) {
			//collect key for each element
			foreach (string key in BoardManager.instance.elementDamages.Keys) {
				if (key != "heart") {
					elementKeys.Add (key);
				}
			}
		}

		aDescription = "พลังโจมตีเพิ่มเป็น " + multiplier.ToString() + " เท่า เมื่อพลังชีวิตเต็ม";
	}

	public override void TriggerAbility(MonsterController m){
		if (GameplayManager.instance.currentHp == GameplayManager.instance.totalHp && isAdded == false) {
			isAdded = true;
			for (int i = 0; i < elementKeys.Count; i++) {
				if (elementKeys [i] != "heart") {
					BoardManager.instance.buffAttacksPermanent [elementKeys [i]] *= multiplier;
				}
			}
		}

		if (GameplayManager.instance.currentHp != GameplayManager.instance.totalHp && isAdded == true) {
			isAdded = false;
			for (int i = 0; i < elementKeys.Count; i++) {
				if (elementKeys [i] != "heart") {
					BoardManager.instance.buffAttacksPermanent [elementKeys [i]] /= multiplier;
				}
			}
		}
	}
}
