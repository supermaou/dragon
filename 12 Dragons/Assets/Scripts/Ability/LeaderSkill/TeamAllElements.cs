﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSparks.Core;

public class TeamAllElements : Ability {

	float multiplier;

	List<string> elementKeys = new List<string> ();
	bool isActive = false; //Call once

	public override void Initialize (GSData abilityData){
		multiplier = abilityData.GetFloat ("multiplier") ?? 1f;

		if (BoardManager.instance != null) {
			//collect key for each element
			foreach (string key in BoardManager.instance.elementDamages.Keys) {
				if (key != "heart") {
					elementKeys.Add (key);
				}
			}
		}

		aDescription = "เมื่อในทีมมีมอนสเตอร์ธาตุไฟ น้ำ ดิน แสง และความมืด จะเพิ่มพลังโจมตีเป็น " + multiplier.ToString() + " เท่า";
	}

	public override void TriggerAbility(MonsterController m){

		if (isActive == true) {
			return;
		}

		List<string> allElements = new List<string> ();

		for (int i = 0; i < GameplayManager.instance.monsterStatus.Count; i++) {
			allElements.Add (GameplayManager.instance.monsterStatus [i].element);
		}

		if (allElements.Contains ("fire") && allElements.Contains ("water") && allElements.Contains ("plant") && allElements.Contains ("light") && allElements.Contains ("dark")) {
			for (int i = 0; i < elementKeys.Count; i++) {
				if (elementKeys [i] != "heart") {
					BoardManager.instance.buffAttacksPermanent [elementKeys [i]] *= multiplier;
				}
			}
		}

		isActive = true;
	}
}
