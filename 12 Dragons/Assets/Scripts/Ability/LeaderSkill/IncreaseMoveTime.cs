﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSparks.Core;

public class IncreaseMoveTime : Ability {

	int moveTime;

	bool isActive = false; //Call only once

	public override void Initialize (GSData abilityData){
		moveTime = abilityData.GetInt ("moveTime").Value;

		aDescription = "เพิ่มเวลาการจับคู่รูนอีก " + moveTime.ToString() + " วินาที";
	}

	public override void TriggerAbility(MonsterController m){
		if (isActive == true) {
			return;
		}

		BoardManager.instance.moveTime += (float) moveTime;
		isActive = true;
	}
}
