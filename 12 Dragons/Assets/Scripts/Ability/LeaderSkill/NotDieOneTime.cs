﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSparks.Core;

public class NotDieOneTime : Ability {

	bool isUsed = false;

	public override void Initialize (GSData abilityData){
		aDescription = "เมื่อโดนโจมตีจนพลังชีวิตเหลือ 0 จะไม่ตาย และทำให้พลังชีวิตเหลือ 1 ได้ 1 ครั้ง"; 
	}

	public override void TriggerAbility(MonsterController m){
		if (GameplayManager.instance.currentHp <= 0 && isUsed == false) {
			GameplayManager.instance.currentHp = 1;
			isUsed = true;
		}
	}
}
