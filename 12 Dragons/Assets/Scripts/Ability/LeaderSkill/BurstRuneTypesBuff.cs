﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSparks.Core;

public class BurstRuneTypesBuff : Ability {

	float multiplier;
	List<string> types;

	List<string> elementKeys = new List<string>();

	//Test Complete
	public override void Initialize (GSData abilityData){
		multiplier = abilityData.GetFloat ("multiplier") ?? 1f;
		types = abilityData.GetStringList ("types");

		if (BoardManager.instance != null) {
			//collect key for each element
			foreach (string key in BoardManager.instance.elementDamages.Keys) {
				elementKeys.Add (key);
			}
		}
			
		aDescription = "เมื่อสลายรูน";
		for (int i = 0; i < types.Count; i++) {
			if (types [i] == "fire") {
				aDescription = aDescription + "ไฟ";
			}else if(types [i] == "water") {
				aDescription = aDescription + "น้ำ";
			}else if(types [i] == "plant") {
				aDescription = aDescription + "ดิน";
			}else if(types [i] == "light") {
				aDescription = aDescription + "แสง";
			}else if(types [i] == "dark") {
				aDescription = aDescription + "มืด";
			}

			aDescription = aDescription + " ";
		}

		if (types.Count > 1) {
			aDescription = aDescription + "ในเทิร์นเดียวกัน พลังโจมตีเพิ่มขึ้นเป็น " + multiplier.ToString() + " เท่า";
		}
	}

	public override void TriggerAbility(MonsterController m){

		if (ContainTypes()) {
			for (int i = 0; i < elementKeys.Count; i++) {
				if (BoardManager.instance.elementDamages [elementKeys [i]] > 0 && elementKeys[i] != "heart") {
					BoardManager.instance.elementDamages [elementKeys [i]] = Mathf.RoundToInt (BoardManager.instance.elementDamages [elementKeys [i]] * multiplier);
				}
			}
		}
	}

	bool ContainTypes(){
		for (int i = 0; i < types.Count; i++) {
			if (BoardManager.instance.elementDamages [types [i]] <= 0) {
				return false;
			}
		}

		return true;
	}
}
