﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSparks.Core;

public class BuffSelf : Ability {

	string typeBuff; // atk or def or both

	string elementBuff;
	string tribeBuff; //support only attack buff

	float multiplier; //for example, 1.5 or 2

	int damageReduction; //for example, 50 % or 30%

	bool isActive = false; //Call only once

	public override void Initialize (GSData abilityData){
		typeBuff = abilityData.GetString ("typeBuff");

		elementBuff = abilityData.GetString ("elementBuff") ?? "";
		tribeBuff = abilityData.GetString ("tribeBuff") ?? "";

		multiplier = abilityData.GetFloat ("multiplier") ?? 1;

		damageReduction = abilityData.GetInt ("damageReduction") ?? 0;

		if (typeBuff == "atk") {
			aDescription = "เพิ่มพลังโจมตี";

			if (elementBuff == "fire") {
				aDescription = aDescription + "ธาตุไฟ";
			} else if (elementBuff == "water") {
				aDescription = aDescription + "ธาตุน้ำ";
			} else if (elementBuff == "plant") {
				aDescription = aDescription + "ธาตุดิน";
			} else if (elementBuff == "light") {
				aDescription = aDescription + "ธาตุแสง";
			} else if (elementBuff == "dark") {
				aDescription = aDescription + "ธาตุความมืด";
			} else if (elementBuff == "heart") {
				aDescription = aDescription + "รูนหัวใจ";
			}

			if (tribeBuff != "") {
				aDescription = aDescription + "แก่เผ่า" + tribeBuff + " ";
			}

			aDescription = aDescription + "เป็น " + multiplier.ToString() + " เท่า";

		} else if (typeBuff == "def") {
			aDescription = "ลดความเสียหาย";

			if (elementBuff == "fire") {
				aDescription = aDescription + "ต่อธาตุไฟ";
			} else if (elementBuff == "water") {
				aDescription = aDescription + "ต่อธาตุน้ำ";
			} else if (elementBuff == "plant") {
				aDescription = aDescription + "ต่อธาตุดิน";
			} else if (elementBuff == "light") {
				aDescription = aDescription + "ต่อธาตุแสง";
			} else if (elementBuff == "dark") {
				aDescription = aDescription + "ต่อธาตุความมืด";
			}

			aDescription = aDescription + " " + damageReduction.ToString() + "%";
		}
			
		if (tribeBuff != "") {
			aDescription = "เพิ่มพลังโจมตีให้มอนสเตอร์เผ่า" + tribeBuff + " เป็น " + multiplier.ToString() + " เท่า";
		}
	}

	public override void TriggerAbility(MonsterController m){

		if (isActive == true) {
			return;
		}

		if (tribeBuff == "") {
			if (typeBuff == "atk") {
				GameplayManager.instance.BuffSelfPermanent (typeBuff, elementBuff, multiplier);
			}

			if (typeBuff == "def") {
				GameplayManager.instance.BuffSelfPermanent (typeBuff, elementBuff, 0f, damageReduction);
			}
		}

		if (tribeBuff != "") {
			GameplayManager.instance.BuffSelfTribe (tribeBuff, multiplier);
		}

		isActive = true;
	}

}
