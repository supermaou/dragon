﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSparks.Core;

public class BuffHitPoint : Ability {

	bool isActive = false;

	float multiplier;

	public override void Initialize (GSData abilityData){
		multiplier = abilityData.GetFloat ("multiplier") ?? 1f;

		aDescription = "เพิ่มพลังชีวิตเป็น " + multiplier.ToString() + " เท่า";
	}
	
	public override void TriggerAbility(MonsterController m){
		if (isActive) {
			return;
		}

		GameplayManager.instance.totalHp = Mathf.RoundToInt (GameplayManager.instance.totalHp * multiplier);
		GameplayManager.instance.currentHp = GameplayManager.instance.totalHp;
		GameplayManager.instance.totalHpText.text = GameplayManager.instance.currentHp.ToString () + " / " + GameplayManager.instance.totalHp.ToString ();

		isActive = true;
	}
}
