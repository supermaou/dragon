﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSparks.Core;

public class TribeBuff : Ability {

	bool isActive = false;

	string tribe;
	float multiplier; //for attack
	float hpMultiplier; //for hit point

	public override void Initialize (GSData abilityData){
		tribe = abilityData.GetString ("tribe") ?? "";
		multiplier = abilityData.GetFloat ("multiplier") ?? 1f;
		hpMultiplier = abilityData.GetFloat ("hpMultiplier") ?? 1f;

		aDescription = "เมื่อมอนสเตอร์ในทีมทุกตัวเป็น" + tribe;
		if (multiplier > 1f && hpMultiplier > 1f) {
			aDescription = aDescription + " เพิ่มพลังโจมตีเป็น " + multiplier.ToString () + " เท่า และเพิ่มพลังชีวิตเป็น " + hpMultiplier.ToString () + " เท่า";
		} else if (multiplier > 1f) {
			aDescription = aDescription + " เพิ่มพลังโจมตีเป็น " + multiplier.ToString () + " เท่า";
		} else if (hpMultiplier > 1f) {
			aDescription = aDescription + " เพิ่มพลังชีวิตเป็น " + hpMultiplier.ToString () + " เท่า";
		}

	}

	public override void TriggerAbility(MonsterController m){
		if (isActive == true) {
			return;
		}

		int count = 0;
		for (int i = 0; i < GameplayManager.instance.monsterStatus.Count; i++) {
			if (GameplayManager.instance.monsterStatus [i].tribe == tribe) {
				count++;
			}
		}

		if (count == GameplayManager.instance.monsterStatus.Count) {
			if (multiplier > 1f) {
				GameplayManager.instance.BuffSelfPermanent ("atk", "", multiplier);
			}

			if (hpMultiplier > 1f) {
				GameplayManager.instance.totalHp = Mathf.RoundToInt (GameplayManager.instance.totalHp * hpMultiplier);
				GameplayManager.instance.currentHp = GameplayManager.instance.totalHp;
				GameplayManager.instance.totalHpText.text = GameplayManager.instance.currentHp.ToString () + " / " + GameplayManager.instance.totalHp.ToString ();
			}
		}

		isActive = true;
	}
}
