﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSparks.Core;

public class DamageBuffOnRuneCount : Ability {

	string runeType;
	int runeCount;
	float multiplier;

	public override void Initialize (GSData abilityData){
		runeType = abilityData.GetString ("runeType");
		runeCount = abilityData.GetInt ("runeCount") ?? 10;
		multiplier = abilityData.GetFloat ("multiplier") ?? 1;

		aDescription = "เมื่อสลายรูน";
		if (runeType == "fire") {
			aDescription = aDescription + "ไฟ";
		}else if(runeType == "water") {
			aDescription = aDescription + "น้ำ";
		}else if(runeType == "plant") {
			aDescription = aDescription + "ดิน";
		}else if(runeType == "light") {
			aDescription = aDescription + "แสง";
		}else if(runeType == "dark") {
			aDescription = aDescription + "มืด";
		}

		aDescription = aDescription + " " + runeCount.ToString() + " รูนหรือมากกว่า จะเพิ่มพลังโจมตีธาตุ";
		if (runeType == "fire") {
			aDescription = aDescription + "ไฟ";
		}else if(runeType == "water") {
			aDescription = aDescription + "น้ำ";
		}else if(runeType == "plant") {
			aDescription = aDescription + "ดิน";
		}else if(runeType == "light") {
			aDescription = aDescription + "แสง";
		}else if(runeType == "dark") {
			aDescription = aDescription + "มืด";
		}

		aDescription = aDescription + " เป็น " + multiplier.ToString() + " เท่า";
	}

	public override void TriggerAbility(MonsterController m){
		if (BoardManager.instance.runeTypesCount [runeType] >= runeCount) {
			BoardManager.instance.elementDamages [runeType] = Mathf.RoundToInt (BoardManager.instance.elementDamages [runeType] * multiplier);
		}
	}
}
