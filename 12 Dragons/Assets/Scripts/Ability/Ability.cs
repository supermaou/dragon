﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSparks.Core;

public abstract class Ability {

	public string aName;
	public string aDescription;
	public AudioClip aSound;
	public int cooldown;

	public abstract void Initialize (GSData abilityData);
	public abstract void TriggerAbility(MonsterController m);
}
