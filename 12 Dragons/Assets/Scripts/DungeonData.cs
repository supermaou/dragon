﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSparks.Core;

public class DungeonData {

	public string type = "normal";
	public string dungeonName;
	public string thaiName;

	public int order;
	public List<string> onlyDays; //only for daily dungeons 
	public int floor; //only in tower dungeons
	public string reward; //only in tower dungeons

	public int background;
	public int music;

	public int dropRateNormal;
	public int dropRateBoss;

	public string bossShortCode;

	public int stamina;
	//public string element;
	public int gold;
	public int exp;
	public List<int> wave = new List<int> ();
	public List<EnemyData> enemies = new List<EnemyData> ();

	public int stars = 0;

}

public class EnemyData{
	public string enemyShortCode;
	public int turn;
	public int hitPoint;
	public int attack;
	public int defense;
	public int lootChance;
	public GSData enemySkill;
}
