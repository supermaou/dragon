﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

using Assets.SimpleAndroidNotifications;

public class GameNotification : MonoBehaviour {

	void OnApplicationPause(bool pauseStatus){
		if (!pauseStatus) { //resume
			
		} else { //pause
			NotificationManager.CancelAll();

			//Energy notification
			if (GameSparksManager.instance.playerData.maxStamina > GameSparksManager.instance.playerData.stamina) {
				int targetTime = (GameSparksManager.instance.playerData.maxStamina.Value - GameSparksManager.instance.playerData.stamina.Value) * 60;

				if (targetTime < 300) {
					return;
				}

				var notificationParams = new NotificationParams
				{
					Id = UnityEngine.Random.Range(0, int.MaxValue),
					Delay = TimeSpan.FromSeconds((double) targetTime),
					Title = "พลังงานเต็มแล้ว!",
					Message = "มารวบรวมมอนสเตอร์กันเถอะ",
					Ticker = "พลังงานเต็มแล้ว! มาผจญภัยกันเถอะ",
					Sound = true,
					Vibrate = true,
					Light = true,
					SmallIcon = NotificationIcon.Heart,
					SmallIconColor = new Color(0, 0.5f, 0),
					LargeIcon = "app_icon"
				};

				NotificationManager.SendCustom(notificationParams);
			}


		}
	}
}
