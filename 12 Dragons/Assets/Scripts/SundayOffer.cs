﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class SundayOffer : MonoBehaviour {

	public GameObject offer;

	void OnEnable(){
		if (DateTime.Now.DayOfWeek != DayOfWeek.Sunday) {
			offer.SetActive (false);
			return;
		} else {
			offer.SetActive (true);
		}

		string today = DateTime.Now.Date.ToString ("d");
		if (SaveLoad.userData.sundayGemOffer.Contains (today) && offer.tag == "gemOffer") {
			offer.SetActive (false);
		}

		if (SaveLoad.userData.sundayMaterialOffer.Contains (today) && offer.tag == "materialOffer") {
			offer.SetActive (false);
		}
	}
}
