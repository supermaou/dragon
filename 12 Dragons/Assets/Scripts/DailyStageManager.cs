﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DailyStageManager : MonoBehaviour {

	public static int[] activeDungeons = new int[]{1,2,3};

	public static DailyStage GetStage(int stage){
		return new DailyStage (stage);
	}
}

public class DailyStage{
	public string stageName;
	public int stamina;
	public string element;
	public int wave;
	public List<List<EnemyData>> enemiesData = new List<List<EnemyData>> ();

	public DailyStage(int stage){
		if (stage == 1) {
			stageName = "First daily quest";
			stamina = 10;
			element = "fire";
			wave = 3;
			for (int i = 0; i < wave; i++) {
				enemiesData.Add (new List<EnemyData> ());
			}
		} else if (stage == 2) {
			stageName = "Second daily quest";
			stamina = 10;
			element = "fire";
			wave = 3;
			for (int i = 0; i < wave; i++) {
				enemiesData.Add (new List<EnemyData> ());
			}

		} else if (stage == 3) {
			stageName = "Third daily quest";
			stamina = 10;
			element = "fire";
			wave = 3;
			for (int i = 0; i < wave; i++) {
				enemiesData.Add (new List<EnemyData> ());
			}

		} 
	}
}
