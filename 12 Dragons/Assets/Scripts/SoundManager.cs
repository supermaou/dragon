﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {

	public static SoundManager instance = null;
	public AudioSource musicSource;
	public AudioSource efxSourceLoop;
	public AudioSource efxSource;

	public bool musicEnable = true;
	public bool sfxEnable = true;

	void Awake(){
		if (instance == null) {
			instance = this;
		} else {
			Destroy (gameObject);
		}
		DontDestroyOnLoad (gameObject);

		if (PlayerPrefs.GetInt ("music", 1) == 1) {
			SoundManager.instance.musicEnable = true;
		} else {
			SoundManager.instance.musicEnable = false;
			MuteMusic ();
		}

		if (PlayerPrefs.GetInt ("sfx", 1) == 1) {
			SoundManager.instance.sfxEnable = true;
		} else {
			SoundManager.instance.sfxEnable = false;
		}
	}

	public void PlayMusic(AudioClip clip){
		if (!musicEnable) {
			return;
		}

		musicSource.clip = clip;
		musicSource.Play ();
	}

	public void MuteMusic(){
		if (musicSource.clip != null) {
			musicSource.Stop ();
			musicSource.clip = null;
		}
	}

	public void PlayEffect(AudioClip clip){
		if (!sfxEnable) {
			return;
		}

		efxSource.clip = clip;
		efxSource.Play ();
	}

	public void PlayEffectLoop(AudioClip clip){
		if (!sfxEnable) {
			return;
		}

		efxSourceLoop.clip = clip;
		efxSourceLoop.Play ();
	}

	public void MuteEffectLoop(){
		efxSourceLoop.Stop ();
		efxSourceLoop.clip = null;
	}
}
