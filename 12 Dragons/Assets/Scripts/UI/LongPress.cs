﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class LongPress : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerExitHandler, IDragHandler, IBeginDragHandler, IEndDragHandler {

	private float holdTime = 0.8f;
	private bool held = false;

	public UnityEvent onLongPress = new UnityEvent();
	public UnityEvent onClick = new UnityEvent();

	public bool interactable = true;

	private bool notTriggerClick = false;
	private ScrollRect scrollRectParent;

	void Awake(){
		scrollRectParent = GetComponentInParent<ScrollRect> ();
	}

	public void OnPointerDown(PointerEventData eventData)
	{
		held = false;
		notTriggerClick = false;
		Invoke ("OnLongPress", holdTime);
	}

	public void OnPointerUp(PointerEventData eventData)
	{
		CancelInvoke ("OnLongPress");

		if (!held && interactable && !notTriggerClick) {
			onClick.Invoke ();
		}
	}

	public void OnPointerExit(PointerEventData eventData)
	{
		CancelInvoke("OnLongPress");
	}

	public void OnDrag(PointerEventData eventData)
	{
		notTriggerClick = true;
		if(scrollRectParent != null)
			scrollRectParent.OnDrag(eventData);
	}
	public void OnEndDrag(PointerEventData eventData)
	{
		if(scrollRectParent != null)
			scrollRectParent.OnEndDrag(eventData);
	}
	public void OnBeginDrag(PointerEventData eventData)
	{
		if(scrollRectParent != null)
			scrollRectParent.OnBeginDrag(eventData);
	}

	void OnLongPress()
	{
		held = true;
		onLongPress.Invoke();
	}
}
