﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSparks.Core;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class FriendManager : MonoBehaviour {

	public Transform content;

	public GameObject friendPrefab;

	public Text inputField;

	public Button searchBtn;
	public GameObject searchIcon;
	public GameObject xIcon;

	public Sprite normalBtn;

	public Text friendTitleText;

	void SetSpriteToOriginalSize(Image img, float target){
		float width = img.sprite.rect.width;
		float height = img.sprite.rect.height;

		if (height > width) {
			float multiplier = target / height;

			img.rectTransform.sizeDelta = new Vector2 (width * multiplier, target);
		} else {
			float multiplier = target / width;

			img.rectTransform.sizeDelta = new Vector2 (target, height * multiplier);
		}
	}

	void OnEnable(){
		friendTitleText.text = "เพื่อน " + GameSparksManager.instance.playerData.friendList.Count.ToString () + " / " + GameSparksManager.instance.playerData.maxFriends.ToString ();

		StartCoroutine (StartDelay ());
	}

	IEnumerator StartDelay(){
		GameSparksManager.instance.LoadPlayerData ();

		yield return new WaitForSeconds (0.05f);

		ClearSearch ();
	}

	void ShowFriends(List<string> playerIds, string type = "friends"){
		new GameSparks.Api.Requests.LogEventRequest ().SetEventKey ("GetFriendData").SetEventAttribute("playerIds", playerIds).Send ((response) => {
			if(!response.HasErrors){

				GSData data = response.ScriptData.GetGSData("friendData");

				List<string> displayNames = data.GetStringList("displayNames");
				List<string> monsterData = data.GetStringList("monsterData");

				for (int i = 0; i < displayNames.Count; i++) {
					GameObject g = Instantiate (friendPrefab);
					g.transform.SetParent (content, false);

					g.transform.GetChild(0).GetComponent<Image>().sprite = MonsterList.instance.GetThumbnail(monsterData[i]);
					SetSpriteToOriginalSize(g.transform.GetChild(0).GetComponent<Image>(),96f);
					g.transform.GetChild(1).GetComponent<Text>().text = displayNames[i];

					int a = i;
					if(type == "friends"){
						g.transform.GetChild (3).gameObject.SetActive (true);
						g.transform.GetChild (4).gameObject.SetActive (false);
						g.transform.GetChild (5).gameObject.SetActive (false);

						if(GameSparksManager.instance.playerData.friendEnergy[a] > 0){
							g.transform.GetChild(3).GetComponent<Button>().onClick.AddListener(() => ReceiveEnergy(playerIds[a]));
							g.transform.GetChild(3).GetChild(1).GetComponent<Text>().text = "รับ";
						}else if(GameSparksManager.instance.playerData.friendDailySend[a] == 0){
							g.transform.GetChild(3).GetComponent<Button>().onClick.AddListener(() => SendEnergy(playerIds[a]));
							g.transform.GetChild(3).GetChild(1).GetComponent<Text>().text = "ส่ง";
						}else if(GameSparksManager.instance.playerData.friendDailySend[a] == 1){
							g.transform.GetChild(3).GetComponent<Button>().interactable = false;
							g.transform.GetChild(3).GetChild(1).GetComponent<Text>().text = "ส่งแล้ว";
						}
					}else{
						g.transform.GetChild (3).gameObject.SetActive (false);
						g.transform.GetChild (4).gameObject.SetActive (true);
						g.transform.GetChild (5).gameObject.SetActive (true);

						g.transform.GetChild(4).GetComponent<Button>().onClick.AddListener(() => AcceptFriendRequest(playerIds[a]));
						g.transform.GetChild(5).GetComponent<Button>().onClick.AddListener(() => DeclineFriendRequest(playerIds[a]));
					}
				}

			}else{
				print("Error friend display");
				print(response.Errors.JSON);
				GameSparksManager.instance.OnError();
			}
		});
	}

	void AcceptFriendRequest(string id){
		if (GameSparksManager.instance.playerData.friendList.Count >= 5) {
			return;
		}

		GameSparksManager.instance.playerData.friendList.Add (id);
		GameSparksManager.instance.playerData.friendEnergy.Add (0);
		GameSparksManager.instance.playerData.friendDailySend.Add (0);

		GameSparksManager.instance.playerData.friendRequests.Remove (id);
		GameSparksManager.instance.SavePlayerData ();

		GameObject g = EventSystem.current.currentSelectedGameObject.transform.parent.gameObject;
		g.transform.GetChild (3).gameObject.SetActive (true);
		g.transform.GetChild (4).gameObject.SetActive (false);
		g.transform.GetChild (5).gameObject.SetActive (false);

		g.transform.GetChild (3).GetComponent<Button> ().onClick.AddListener (() => SendEnergy (id));
		friendTitleText.text = "เพื่อน " + GameSparksManager.instance.playerData.friendList.Count.ToString () + " / " + GameSparksManager.instance.playerData.maxFriends.ToString ();
	}

	void DeclineFriendRequest(string id){
		GameSparksManager.instance.playerData.friendRequests.Remove (id);
		GameSparksManager.instance.SavePlayerData ();

		Destroy (EventSystem.current.currentSelectedGameObject.transform.parent.gameObject);
	}

	void SendEnergy(string id){

		GameObject g = EventSystem.current.currentSelectedGameObject;

		g.GetComponent<Button> ().interactable = false;
		g.transform.GetChild(1).GetComponent<Text>().text = "ส่งแล้ว";

		for (int i = 0; i < GameSparksManager.instance.playerData.friendList.Count; i++) {
			if (GameSparksManager.instance.playerData.friendList [i] == id) {
				if (GameSparksManager.instance.playerData.friendDailySend [i] == 1) {
					return;
				}

				GameSparksManager.instance.playerData.friendDailySend [i] = 1;
				GameSparksManager.instance.SavePlayerData ();

				break;
			}
		}

		new GameSparks.Api.Requests.LogEventRequest ().SetEventKey ("SendEnergy").SetEventAttribute ("player_id", id).Send ((response) => {
			if(!response.HasErrors){
				print("Send success");
			}else{
				print("Error");
				print(response.Errors.JSON);
				GameSparksManager.instance.OnError();
			}
		});
	}

	void ReceiveEnergy(string id){
		for (int i = 0; i < GameSparksManager.instance.playerData.friendList.Count; i++) {
			if (GameSparksManager.instance.playerData.friendList [i] == id) {
				GameSparksManager.instance.playerData.stamina += GameSparksManager.instance.playerData.friendEnergy [i];
				//SaveLoad.Save ();

				GameSparksManager.instance.playerData.friendEnergy [i] = 0;
				GameSparksManager.instance.SavePlayerData ();

				UIManager.instance.UpdateStaminaText ();

				//Switch button back to normal
				Button b = EventSystem.current.currentSelectedGameObject.GetComponent<Button> ();
				b.onClick.RemoveAllListeners ();
				b.gameObject.GetComponent<Image> ().sprite = normalBtn;
				Text t = b.gameObject.transform.GetChild (1).GetComponent<Text> ();
				t.color = Color.white;

				if (GameSparksManager.instance.playerData.friendDailySend [i] == 0) {
					t.text = "ส่ง";
					b.onClick.AddListener(() => SendEnergy(id));
				} else {
					t.text = "รับแล้ว";
					b.onClick.RemoveAllListeners ();
				}
				break;
			}
		}
	}

	void Search(){
		if (inputField.text != "") {
			FindFriend (inputField.text);

			searchBtn.onClick.RemoveAllListeners ();
			searchBtn.onClick.AddListener (() => ClearSearch ());

			searchIcon.SetActive (false);
			xIcon.SetActive (true);
		}
	}

	void FindFriend(string displayName){
		new GameSparks.Api.Requests.LogEventRequest ().SetEventKey ("findPlayer").SetEventAttribute("query",displayName).Send ((response) => {
			if(!response.HasErrors){

				foreach (Transform t in content) {
					Destroy (t.gameObject);
				}
				
				GSData playerList = response.ScriptData.GetGSData("playerList");
				List<string> playerIds = playerList.GetStringList("playerIds");
				List<string> monsterData = playerList.GetStringList("monsterData");

				if(playerIds.Count > 0){
					for(int i=0;i<playerIds.Count;i++){
						GameObject g = Instantiate(friendPrefab);
						g.transform.SetParent(content,false);

						Image img = g.transform.GetChild(0).GetComponent<Image>();
						img.sprite = MonsterList.instance.GetThumbnail(monsterData[i]);
						SetSpriteToOriginalSize(img,96f);

						g.transform.GetChild(1).GetComponent<Text>().text = displayName;
						g.transform.GetChild(2).GetComponent<Text>().text = "";
						g.transform.GetChild(6).gameObject.SetActive(true);

						int a = i;
						g.transform.GetChild(6).GetComponent<Button>().onClick.AddListener(() => SendFriendRequest(playerIds[a]));
					}
				}else{
					//Show nothing
				}

			}else{
				GameSparksManager.instance.OnError();
			}
		});
	}

	void SendFriendRequest(string playerId){
		EventSystem.current.currentSelectedGameObject.GetComponent<Button> ().interactable = false;

		new GameSparks.Api.Requests.LogEventRequest ().SetEventKey ("sendFriendRequest").SetEventAttribute ("player_id", playerId).Send ((response) => {
			if(!response.HasErrors){
				print("Send success");
			}else{
				print("Error");
				print(response.Errors.JSON);
				GameSparksManager.instance.OnError();
			}
		});
	}

	void ClearSearch(){
		inputField.text = "";

		foreach (Transform t in content) {
			Destroy (t.gameObject);
		}

		ShowFriends (GameSparksManager.instance.playerData.friendRequests, "requests");
		ShowFriends (GameSparksManager.instance.playerData.friendList);

		searchBtn.onClick.RemoveAllListeners ();
		searchBtn.onClick.AddListener (() => Search ());

		searchIcon.SetActive (true);
		xIcon.SetActive (false);
	}
}
