﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {

	public static UIManager instance;

	//other scenes reference
	public GameObject header;
	public GameObject bottomNav;
	public GameObject home; // home = team
	public GameObject quest;
	public GameObject inventory;
	public GameObject dungeonTypes;
	public GameObject normalDungeons;
	public GameObject dailyDungeons;
	public GameObject towerDungeons;
	public GameObject shop;
	public GameObject buyGems;
	public GameObject buyInventory;
	public GameObject editTeam;
	public GameObject editMonster;
	public GameObject switchMonster;
	public GameObject monsterDetails;
	public GameObject evolve;
	public GameObject fuse;
	public GameObject selectFuseMonster;
	public GameObject bulkSelling;
	public GameObject gachaList;
	public GameObject gachaTable;
	public GameObject getMonster;
	public GameObject monsterBook;
	public GameObject worldBoss;
	public GameObject friends;
	public GameObject dailyReward;
	public Image wholeScreenPanel;

	//variables
	public GameObject currentScene;
	public GameObject previousScene;

	public Sprite btnGlow;
	public Sprite btnNormal;

	public Sprite fire;
	public Sprite water;
	public Sprite plant;
	public Sprite light;
	public Sprite dark;

	public int stageSelected;

	//Mons to switch
	public MonsterData monA;
	public MonsterData monB;

	//public MonsterController evolveForm;
	public string evolveShortCode;

	//nav reference
	public Image[] navMenu;

	public Text displayNameText;
	public Text rankText;
	public Text staminaText;
	//public Image staminaBar;
	public Text gemText;
	public Text coinText;
	public Text xpText;
	public Image xpBar;

	//Monsters to fuse
	public List<MonsterController> mons2fuse = new List<MonsterController>();

	//reset sign
	public int resetInventory = 1;
	public int resetEditMonster = 1;
	public int resetSelectMonster = 1;
	public int resetDungeons = 1;

	//Store dungeon data for game scene
	public DungeonData currentDungeon;

	//dialog
	public GameObject fullInventoryDialog;
	public GameObject staminaRefillDialog;
	public GameObject notEnoughGemsDialog;
	public GameObject reviewDialog;
	public GameObject monsterDetailDialog;
	public GameObject gachaConfirmDialog;
	public GameObject starterPackDialog;
	public GameObject messageDialog;

	public GameObject transparentPanel;
	public GameObject unclickableTransparentPanel;

	public AudioClip backgroundMusic;
	public GameObject soundSettingDialog;

	void Awake(){
		instance = this;

		displayNameText.text = GameSparksManager.instance.playerData.displayName;

		if (PersistentData.instance.playCount % 5 == 0 && !SaveLoad.userData.reviewVersions.Contains("review" + Application.version) && PersistentData.instance.playCount > 0) {
			reviewDialog.SetActive (true);
		}

		//show noti upon start the game
		//quest noti
		if (GameSparksManager.instance.playerData.unclaimAchievement.Count > 0) {
			UIManager.instance.navMenu [0].transform.GetChild (2).gameObject.SetActive (true);
			UIManager.instance.navMenu [0].transform.GetChild (2).GetChild (0).GetComponent<Text> ().text = (GameSparksManager.instance.playerData.unclaimAchievement.Count + GameSparksManager.instance.playerData.dailyQuestsToClaim.Count).ToString ();
		}

		if (GameSparksManager.instance.playerData.gems >= 10) {
			UIManager.instance.navMenu [2].transform.GetChild (2).gameObject.SetActive (true);
			UIManager.instance.navMenu [2].transform.GetChild (2).GetChild (0).GetComponent<Text> ().text = Mathf.Floor (GameSparksManager.instance.playerData.gems.Value / 10f).ToString();
		}

		//Show starter pack promotion dialog
		if (PersistentData.instance.latestResult == "lose") {
			SaveLoad.userData.loseCount++;
			SaveLoad.Save ();
			PersistentData.instance.latestResult = "";

			if (SaveLoad.userData.loseCount % 3 == 1 && GameSparksManager.instance.playerData.isBuyStarterPack.Value == 0) {
				starterPackDialog.SetActive (true);
			}
		}

		//Show inventory noti
		if (SaveLoad.userData.inventoryNoti > 0) {
			UIManager.instance.navMenu [1].transform.GetChild (2).gameObject.SetActive (true);
			UIManager.instance.navMenu [1].transform.GetChild (2).GetChild (0).GetComponent<Text> ().text = SaveLoad.userData.inventoryNoti.ToString ();
		}

	}

	IEnumerator Start(){ //check if come back to see getmonster scene

		//StartCoroutine (MonsterList.instance.CheckEvolveMaterials ());

		if (PersistentData.instance.monsterGet != "") { //Show monster get case
			MonsterData m = new MonsterData ();
			m.shortCode = PersistentData.instance.monsterGet;
			//load data from server
			GameSparksManager.instance.GetMonsterData (m);

			while (!PersistentData.instance.loadedMonsterData || m.stars == 0) {
				yield return null;
			}

			ShowGetMonster (m);
			PersistentData.instance.monsterGet = "";

		} else if (GameSparksManager.instance.playerData.dailyLogin.Value == 0) { //Show daily login case
			currentScene = dailyReward;
			currentScene.SetActive (true);

			StartCoroutine(Move(header,new Vector3(header.transform.localPosition.x,810f,0f),0.25f));
			StartCoroutine(Move(bottomNav,new Vector3(0f,-800f,0f),0.25f));
		} else { //normal case
			currentScene = home;
			currentScene.SetActive (true);

			StartCoroutine(Move(header,new Vector3(header.transform.localPosition.x,810f,0f),0.25f));
			StartCoroutine(Move(bottomNav,new Vector3(0f,-800f,0f),0.25f));

			if (PersistentData.instance.currentDungeon != null) { //tower case
				string reward = PersistentData.instance.currentDungeon.reward;

				messageDialog.transform.GetChild (0).GetComponent<Text> ().text = "ได้รับ " + GetRewardText (reward);
				messageDialog.SetActive (true);

				PersistentData.instance.ResetData ();
			}
		}
	}

	string GetRewardText(string shortCode){
		string[] s = shortCode.Split ('.');

		if (s [1] == "gems") {
			return s [0] + " เพชร";
		} else if (s [1] == "gold") {
			return s [0] + " ทอง";
		} else if (s [1] == "energy") {
			return s [0] + " พลังงาน";
		} else if (s [1] == "spaces") {
			return s [0] + " ช่องเก็บของ";
		} else { //mon case
			if (s [0] == "fireegg") {
				return "วัตถุดิบเพิ่มเลเวลธาตุไฟ";
			} else if (s [0] == "wateregg") {
				return "วัตถุดิบเพิ่มเลเวลธาตุน้ำ";
			} else if (s [0] == "woodegg") {
				return "วัตถุดิบเพิ่มเลเวลธาตุดิน";
			} else if (s [0] == "lightegg") {
				return "วัตถุดิบเพิ่มเลเวลธาตุแสง";
			} else if (s [0] == "darkegg") {
				return "วัตถุดิบเพิ่มเลเวลธาตุความมืด";
			}
		}

		return "";
	}

	void OnDisable(){
		Resources.UnloadUnusedAssets ();
	}

	void OnEnable(){
		Resources.UnloadUnusedAssets ();

		rankText.text = GameSparksManager.instance.playerData.rank.ToString ();
		UpdateCurrencyTexts ();

		//set xp text
		xpText.text = (GameSparksManager.instance.playerData.TotalExpRequireForNextRank () - GameSparksManager.instance.playerData.ExpRequireForNextRank()).ToString () + " / "
		+ GameSparksManager.instance.playerData.TotalExpRequireForNextRank ().ToString ();

		//set xp bar
		xpBar.fillAmount = (float)(GameSparksManager.instance.playerData.TotalExpRequireForNextRank () - GameSparksManager.instance.playerData.ExpRequireForNextRank())
		/ (float)GameSparksManager.instance.playerData.TotalExpRequireForNextRank ();

		SoundManager.instance.PlayMusic (backgroundMusic);

	}

	public void UpdateCurrencyTexts(){
		gemText.text = GameSparksManager.instance.playerData.gems.ToString ();
		coinText.text = GameSparksManager.instance.playerData.gold.ToString ();
	}

	public void UpdateStaminaText(){
		//Set stamina text
		staminaText.text = GameSparksManager.instance.playerData.stamina.ToString() + " / " + GameSparksManager.instance.playerData.maxStamina.ToString();
		//staminaBar.fillAmount = (float) SaveLoad.userData.stamina / (float)GameSparksManager.instance.playerData.maxStamina;
	}

	public void BackBtn(){
		UpdateCurrencyTexts ();

		if (currentScene == monsterDetails && previousScene == inventory) {
			StartCoroutine (FadeOutAndFadeIn (monsterDetails, inventory));
		} else if (currentScene == monsterDetails && previousScene == home) {
			StartCoroutine (FadeOutAndFadeIn (monsterDetails, home));
		} else if (currentScene == fuse && previousScene == home) {

			if (PersistentData.instance.detailStack != 1) {
				PersistentData.instance.detailStack = 1;
			}

			mons2fuse.Clear ();

			StartCoroutine (FadeOutAndFadeIn (fuse, home));
		} else if (currentScene == evolve && previousScene == home) {
			StartCoroutine (FadeOutAndFadeIn (evolve, home));
		} else if (currentScene == evolve) {
			StartCoroutine (FadeOutAndFadeIn (evolve, inventory));
		} else if (currentScene == monsterDetails && previousScene == getMonster) {
			StartCoroutine (FadeOutAndFadeIn (monsterDetails, shop));
		} else if (currentScene == monsterDetails && previousScene == selectFuseMonster) {
			StartCoroutine (FadeOutAndFadeIn (monsterDetails, selectFuseMonster));
		} else if (currentScene == monsterDetails && previousScene == editMonster) {
			StartCoroutine (FadeOutAndFadeIn (monsterDetails, editMonster));
		} else if (currentScene == monsterDetails && previousScene == bulkSelling) {
			StartCoroutine (FadeOutAndFadeIn (monsterDetails, bulkSelling));
		} else if (currentScene == monsterDetails && previousScene == worldBoss) {
			StartCoroutine (FadeOutAndFadeIn (monsterDetails, worldBoss));
		} else if (currentScene == monsterDetails) {
			StartCoroutine (FadeOutAndFadeIn (monsterDetails, inventory));
		} else if (currentScene == switchMonster) {
			StartCoroutine (FadeOutAndFadeIn (switchMonster, editMonster));
		} else if (currentScene == editMonster) {
			StartCoroutine (FadeOutAndFadeIn (editMonster, home));
		} else if (currentScene == editTeam) {
			StartCoroutine (FadeOutAndFadeIn (editTeam, home));
		} else if (currentScene == fuse) {

			if (PersistentData.instance.detailStack != 1) {
				PersistentData.instance.detailStack = 1;
			}

			mons2fuse.Clear ();
			StartCoroutine (FadeOutAndFadeIn (fuse, inventory));
		} else if (currentScene == selectFuseMonster) {
			mons2fuse.Clear ();
			StartCoroutine (FadeOutAndFadeIn (currentScene, fuse));
		} else if (currentScene == buyGems) {
			StartCoroutine (FadeOutAndFadeIn (currentScene, shop));
		} else if (currentScene == gachaTable) {
			StartCoroutine (FadeOutAndFadeIn (currentScene, shop));
		} else if (currentScene == getMonster) {
			StartCoroutine (FadeOutAndFadeIn (currentScene, shop));
		} else if (currentScene == monsterBook) {
			StartCoroutine (FadeOutAndFadeIn (currentScene, home));
		} else if (currentScene == friends) {
			StartCoroutine (FadeOutAndFadeIn (currentScene, home));
		} else if (currentScene == bulkSelling) {
			StartCoroutine (FadeOutAndFadeIn (currentScene, inventory));
		}
	}

	public void ShowHome(){
		for (int i = 0; i < navMenu.Length; i++) {
			if (i == 3) {
				navMenu [i].sprite = btnGlow;
			} else {
				navMenu [i].sprite = btnNormal;
			}
		}

		if (currentScene != home) {
			StartCoroutine (FadeOutAndFadeIn (currentScene, home));
		}
	}

	public void ResetHome(){
		StartCoroutine (FadeOutAndFadeIn (currentScene, home));
	}

	public void EditTeam(){
		StartCoroutine (FadeOutAndFadeIn (currentScene, editTeam));
	}

	public void EditMonster(MonsterData m){
		monA = m;
		StartCoroutine (FadeOutAndFadeIn (currentScene, editMonster));
	}

	public void ShowQuest(){
		for (int i = 0; i < navMenu.Length; i++) {
			if (i == 0) {
				navMenu [i].sprite = btnGlow;
			} else {
				navMenu [i].sprite = btnNormal;
			}
		}

		StartCoroutine (FadeOutAndFadeIn (currentScene, quest));
	}

	public void ShowInventory(){ //in nav menu
		fullInventoryDialog.SetActive(false);

		for (int i = 0; i < navMenu.Length; i++) {
			if (i == 1) {
				navMenu [i].sprite = btnGlow;
			} else {
				navMenu [i].sprite = btnNormal;
			}
		}

		if (currentScene != inventory) {
			StartCoroutine (FadeOutAndFadeIn (currentScene, inventory));
		}
	}

	public void ShowShop(){ //in nav menu
		for (int i = 0; i < navMenu.Length; i++) {
			if (i == 2) {
				navMenu [i].sprite = btnGlow;
			} else {
				navMenu [i].sprite = btnNormal;
			}
		}

		if (currentScene != shop) {
			StartCoroutine (FadeOutAndFadeIn (currentScene, shop));
		}
	}

	public void ShowBuyGems(){ //in nav menu
		notEnoughGemsDialog.SetActive(false);
		StartCoroutine (FadeOutAndFadeIn (currentScene, buyGems));
	}

	public void ShowDungeons(){ //in nav menu
		for (int i = 0; i < navMenu.Length; i++) {
			navMenu [i].sprite = btnNormal;
		}

		if (currentScene != dungeonTypes) {
			StartCoroutine (FadeOutAndFadeIn (currentScene, dungeonTypes));
		}
	}

	public void ShowNormalDungeons(){
		StartCoroutine (FadeOutAndFadeIn (currentScene, normalDungeons));
	}

	public void ShowDailyDungeons(){
		if (GameSparksManager.instance.playerData.rank.Value < 10) {
			messageDialog.transform.GetChild(0).GetComponent<Text>().text = "ต้องการเลเวล 10 ขึ้นไป";
			messageDialog.SetActive (true);
			return;
		}

		StartCoroutine (FadeOutAndFadeIn (currentScene, dailyDungeons));
	}

	public void ShowTowerDungeons(){
		StartCoroutine (FadeOutAndFadeIn (currentScene, towerDungeons));
	}

	public void ShowEvolve(){
		monsterDetailDialog.SetActive (false);

		StartCoroutine (FadeOutAndFadeIn (currentScene, evolve));
	}

	public void ShowMonsterBook(){
		StartCoroutine (FadeOutAndFadeIn (currentScene, monsterBook));
	}

	public void ShowWorldBoss(){
		if (GameSparksManager.instance.playerData.rank.Value < 20) {
			messageDialog.transform.GetChild(0).GetComponent<Text>().text = "ต้องการเลเวล 20 ขึ้นไป";
			messageDialog.SetActive (true);
			return;
		}

		StartCoroutine (FadeOutAndFadeIn (currentScene, worldBoss));
	}

	public void ShowFriends(){
		StartCoroutine (FadeOutAndFadeIn (currentScene, friends));
	}

	public void ShowFuse(){
		monsterDetailDialog.SetActive (false);

		StartCoroutine (FadeOutAndFadeIn (currentScene, fuse));
	}

	public void ShowEvolveAnimate(){
		StartCoroutine (SmoothFadeOut (currentScene));
	}

	public void HideEvolveAnimate(){
		currentScene = monsterDetails;
		StartCoroutine (SmoothFadeIn (currentScene));
	}

	public void ShowFuseAnimate(){
		StartCoroutine (SmoothFadeOut (currentScene));
	}

	public void HideFuseAnimate(){
		StartCoroutine (SmoothFadeIn(currentScene));
	}

	public void ShowSelectFuseMonster(){
		StartCoroutine (FadeOutAndFadeIn (currentScene, selectFuseMonster));
	}

	public void ShowGachaList(){
		StartCoroutine (FadeOutAndFadeIn (currentScene, gachaList));
	}

	public void ShowGachaTable(){
		StartCoroutine (FadeOutAndFadeIn (currentScene, gachaTable));
	}

	public void ShowBulkSelling(){
		StartCoroutine (FadeOutAndFadeIn (currentScene, bulkSelling));
	}

	public void ShowBuyInventory(){
		fullInventoryDialog.SetActive (false);

		StartCoroutine (FadeOutAndFadeIn (currentScene, buyInventory));
	}

	public void ShowGetMonster(MonsterData m){
		monA = m;

		StartCoroutine(Move(header,new Vector3(header.transform.localPosition.x,1100f,0f),0.25f));
		StartCoroutine(Move(bottomNav,new Vector3(0f,-1100f,0f),0.25f));
		if (currentScene != null) {
			currentScene.SetActive (false);
		}
		StartCoroutine (SmoothFadeIn (getMonster));
		StartCoroutine (FadeOut (0.8f));

		currentScene = getMonster;
	}

	public void ShowSwitchMonster(GameObject g){
		monB = g.GetComponent<MonsterController> ().monsterData;
		StartCoroutine (FadeOutAndFadeIn (currentScene, switchMonster));
	}

	public void ShowMonsterDetails(MonsterData m, int stack = 1){
		monsterDetailDialog.SetActive (false);

		PersistentData.instance.detailStack = stack;

		if (stack == 1) {
			monA = m;
		} else {
			monB = m;
		}

		StartCoroutine (FadeOutAndFadeIn (currentScene, monsterDetails));
	}

	IEnumerator FadeOutAndFadeIn(GameObject source, GameObject target){

		//defining previous scene
		if (currentScene == home || currentScene == inventory || currentScene == getMonster || currentScene == selectFuseMonster || currentScene == editMonster || currentScene == bulkSelling || currentScene == worldBoss) {
			previousScene = currentScene;
		} else {
			if (currentScene == fuse) {
				print ("Curent scene is fuse!");
			}
			previousScene = null;
		}

		//Start fading out
		if (target == monsterDetails || target == fuse || target == evolve) {
			//StartCoroutine (SmoothFadeOut (menu));
			StartCoroutine(Move(header,new Vector3(header.transform.localPosition.x,1100f,0f),0.25f));
			StartCoroutine(Move(bottomNav,new Vector3(0f,-1100f,0f),0.25f));
		}

		StartCoroutine (SmoothFadeOut (source));
		yield return new WaitForSeconds (0.5f);

		if ((currentScene == getMonster || previousScene == getMonster) && target == shop) {
			StartCoroutine(Move(header,new Vector3(header.transform.localPosition.x,810f,0f),0.25f));
			StartCoroutine(Move(bottomNav,new Vector3(0f,-800f,0f),0.25f));
		}

		/*
		if (currentScene == monsterDetails && (target != fuse && target != evolve && target != selectFuseMonster)) {
			StartCoroutine(Move(header,new Vector3(header.transform.localPosition.x,810f,0f),0.25f));
			StartCoroutine(Move(bottomNav,new Vector3(0f,-800f,0f),0.25f));
		}*/

		if ((currentScene == monsterDetails || currentScene == fuse || currentScene == evolve) && (target == inventory || target == home || target == editMonster || target == bulkSelling || target == worldBoss)) {
			StartCoroutine(Move(header,new Vector3(header.transform.localPosition.x,810f,0f),0.25f));
			StartCoroutine(Move(bottomNav,new Vector3(0f,-800f,0f),0.25f));
		}

		StartCoroutine (SmoothFadeIn (target));

		currentScene = target;

	}

	IEnumerator SmoothFadeOut(GameObject parent){
		UIManager.instance.unclickableTransparentPanel.SetActive (true);

		Transform[] childTransform;
		List<Image> images = new List<Image> ();
		List<Text> texts = new List<Text> ();

		childTransform = parent.GetComponentsInChildren<Transform> ();
		foreach (Transform t in childTransform) {
			Text text = t.gameObject.GetComponent<Text> ();
			Image img = t.gameObject.GetComponent<Image> ();

			if (img) {
				images.Add (img);
			} else if (text) {
				texts.Add (text);
			}
		}

		float alpha = 1f;

		while (alpha > 0f) {
			alpha -= Time.deltaTime*2f;
			foreach (Image i in images) {
				i.color = new Color (i.color.r, i.color.g, i.color.b, alpha);
			}

			foreach (Text txt in texts) {
				txt.color = new Color (txt.color.r, txt.color.g, txt.color.b, alpha);
			}

			yield return null;
		}

		parent.SetActive (false);
	}

	IEnumerator SmoothFadeIn(GameObject parent){
		parent.SetActive (true);

		Transform[] childTransform;
		List<Image> images = new List<Image> ();
		List<Text> texts = new List<Text> ();

		childTransform = parent.GetComponentsInChildren<Transform> ();
		foreach (Transform t in childTransform) {
			Text text = t.gameObject.GetComponent<Text> ();
			Image img = t.gameObject.GetComponent<Image> ();

			if (img) {
				images.Add (img);
			} else if (text) {
				texts.Add (text);
			}
		}

		float alpha = 0f;

		while (alpha < 1f) {
			alpha += Time.deltaTime*2f;
			foreach (Image i in images) {
				if (i != null) {
					i.color = new Color (i.color.r, i.color.g, i.color.b, alpha);
				}
			}

			foreach (Text txt in texts) {
				if (txt != null) {
					txt.color = new Color (txt.color.r, txt.color.g, txt.color.b, alpha);
				}
			}

			yield return null;
		}

		UIManager.instance.unclickableTransparentPanel.SetActive (false);
	}

	IEnumerator FadeOutSprite(GameObject g){
		g.SetActive (false);
		yield return null;
			
	}

	IEnumerator FadeInSprite(GameObject g){
		g.SetActive (true);
		yield return null;
	}

	IEnumerator Move(GameObject g,Vector3 end,float time){
		float t = 0f;
		Vector3 start = g.transform.localPosition;

		while (t < time) {
			t += Time.deltaTime;
			g.transform.localPosition = Vector3.Lerp (start, end, t / time);
			yield return null;
		}
	}
		
	public IEnumerator FadeIn(float time){

		wholeScreenPanel.gameObject.SetActive (true);

		float t = 0;
		Color start = new Color (wholeScreenPanel.color.r, wholeScreenPanel.color.g, wholeScreenPanel.color.b, 0f);
		Color end = new Color (wholeScreenPanel.color.r, wholeScreenPanel.color.g, wholeScreenPanel.color.b, 1f);
		while (t < time) {
			t += Time.deltaTime;

			wholeScreenPanel.color = Color.Lerp (start, end, t / time);

			yield return null;
		}
	}

	public IEnumerator FadeOut(float time){

		wholeScreenPanel.gameObject.SetActive (false);

		float t = 0;
		Color start = new Color (wholeScreenPanel.color.r, wholeScreenPanel.color.g, wholeScreenPanel.color.b, 1f);
		Color end = new Color (wholeScreenPanel.color.r, wholeScreenPanel.color.g, wholeScreenPanel.color.b, 0f);
		while (t < time) {
			t += Time.deltaTime;

			wholeScreenPanel.color = Color.Lerp (start, end, t / time);

			yield return null;
		}
	}

	public void CloseDialog(){
		fullInventoryDialog.SetActive (false);
		staminaRefillDialog.SetActive (false);
		notEnoughGemsDialog.SetActive (false);
		reviewDialog.SetActive (false);
	}

	public void RateApp(){
		if (!SaveLoad.userData.reviewVersions.Contains("review" + Application.version)) {
			reviewDialog.SetActive (false);

			SaveLoad.userData.reviewVersions.Add ("review" + Application.version);
			SaveLoad.Save ();

			GameSparksManager.instance.playerData.gems += 5;
			GameSparksManager.instance.SavePlayerData ();

			AnalyticsManager.instance.ReceiveGems ("review", 5);

			UpdateCurrencyTexts ();
		}

		#if UNITY_ANDROID
		Application.OpenURL("market://details?id=" + Application.bundleIdentifier);
		#endif
	}

	public void Share(){
		GetComponent<NativeShare> ().ShareScreenshotWithText ("เล่นเกมส์ Fairy War ได้แล้ววันนี้", "https://play.google.com/store/apps/details?id=" + Application.bundleIdentifier);
	}

	public void ShowSoundSetting(){
		soundSettingDialog.SetActive (true);
	}

	public void RefillStamina(){
		if (GameSparksManager.instance.playerData.gems >= 5) {
			//update to the server
			GameSparksManager.instance.playerData.gems = GameSparksManager.instance.playerData.gems - 5;
			GameSparksManager.instance.SavePlayerData ();

			AnalyticsManager.instance.SpendGems ("stamina", 5);

			//save stamina
			GameSparksManager.instance.playerData.stamina += GameSparksManager.instance.playerData.maxStamina.Value;
			GameSparksManager.instance.SavePlayerData ();
			//SaveLoad.Save ();

			staminaRefillDialog.SetActive (false);

			UpdateCurrencyTexts ();
			UpdateStaminaText ();
		} else {
			//not enough gems
			CloseDialog();

			notEnoughGemsDialog.SetActive(true);
		}
	}

	public void OpenGacha(string element){

		if (GameSparksManager.instance.monsterData.Count >= GameSparksManager.instance.playerData.maxInventory.Value) {
			fullInventoryDialog.SetActive (true);
			return;
		}

		if (element != "" && GameSparksManager.instance.playerData.specialGacha.Value != 0) {
			messageDialog.transform.GetChild(0).GetComponent<Text>().text = "เปิดได้วันละครั้ง";
			messageDialog.SetActive (true);
			return;
		}

		Button b = gachaConfirmDialog.transform.GetChild (1).GetComponent<Button> ();
		b.onClick.RemoveAllListeners ();
		b.onClick.AddListener (() => ConfirmToOpen (element)); 

		gachaConfirmDialog.SetActive (true);
			
	}

	void ConfirmToOpen(string element){
		gachaConfirmDialog.SetActive (false);

		SafeInt cost = new SafeInt (0);
		if (element == "") {
			cost.Value = 10;
		} else {
			cost.Value = 20;
		}

		if (GameSparksManager.instance.playerData.gems >= cost) {
			//Go to gacha scene
			PersistentData.instance.gachaElement = element;
			Initiate.Fade("gacha",new Color(1f,1f,1f),1.5f);
		} else {
			CloseDialog ();
			notEnoughGemsDialog.SetActive (true);
		}
	}
}
