﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;
using UnityEngine.Purchasing.Security;
using System;
using UnityEngine.UI;

public class Purchaser : MonoBehaviour, IStoreListener {

	private static IStoreController m_StoreController;
	private static IExtensionProvider m_StoreExtensionProvider;

	public GameObject messageDialog;
	public GameObject confirmSpacesPurchaseDialog;

	public GameObject starterPackDialog;
	public GameObject buyDailyGemsDialog;

	public static string[] productIdGems = new string[] {
		"com.supermom.fairy.150gems",
		"com.supermom.fairy.10gems",
		"com.supermom.fairy.36gems",
		"com.supermom.fairy.70gems",
		"com.supermom.fairy.110gems",
		"com.supermom.fairy.500gems",
		"com.supermom.fairy.10materials",
		"com.supermom.fairy.starterpack",
		"com.supermom.fairy.dailygems"
	};

	private Dictionary<string,int> spacesCost = new Dictionary<string,int>(){
		{"5spaces", 10},
		{"20spaces", 35},
		{"50spaces", 80}
	};
		
	void Start(){
		if (m_StoreController == null) {
			InitializePurchasing ();
		}
	}
		
	public void InitializePurchasing(){
		if (IsInitialized ()) {
			return;
		}

		var builder = ConfigurationBuilder.Instance (StandardPurchasingModule.Instance ());

		for (int i = 0; i < productIdGems.Length; i++) {
			builder.AddProduct (productIdGems[i], ProductType.Consumable);
		}

		UnityPurchasing.Initialize(this, builder);
	}

	private bool IsInitialized(){
		return m_StoreController != null && m_StoreExtensionProvider != null;
	}

	public void BuyProductId(string productId){
		if (IsInitialized ()) {
			Product product = m_StoreController.products.WithID(productId);

			if (product != null && product.availableToPurchase)
			{
				Debug.Log(string.Format("Purchasing product asychronously: '{0}'", product.definition.id));
				// ... buy the product. Expect a response either through ProcessPurchase or OnPurchaseFailed 
				// asynchronously.
				m_StoreController.InitiatePurchase(product);
			}
			// Otherwise ...
			else
			{
				// ... report the product look-up failure situation  
				Debug.Log("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
			}
		}else
		{
			// ... report the fact Purchasing has not succeeded initializing yet. Consider waiting longer or 
			// retrying initiailization.
			Debug.Log("BuyProductID FAIL. Not initialized.");
		}
	}

	public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
	{
		// Purchasing has succeeded initializing. Collect our Purchasing references.
		Debug.Log("OnInitialized: PASS");

		// Overall Purchasing system, configured with products for this application.
		m_StoreController = controller;
		// Store specific subsystem, for accessing device-specific store features.
		m_StoreExtensionProvider = extensions;
	}

	public void OnInitializeFailed(InitializationFailureReason error)
	{
		// Purchasing set-up has not succeeded. Check error for reason. Consider sharing this reason with the user.
		Debug.Log("OnInitializeFailed InitializationFailureReason:" + error);
	}

	public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs e) 
	{

		bool validPurchase = true; // Presume valid for platforms with no R.V.

		// Unity IAP's validation logic is only included on these platforms.
		#if UNITY_ANDROID || UNITY_IOS || UNITY_STANDALONE_OSX
		// Prepare the validator with the secrets we prepared in the Editor
		// obfuscation window.
		var validator = new CrossPlatformValidator(GooglePlayTangle.Data(),
			AppleTangle.Data(), Application.bundleIdentifier);

		try {
			// On Google Play, result has a single product ID.
			// On Apple stores, receipts contain multiple products.
			var result = validator.Validate(e.purchasedProduct.receipt);
			// For informational purposes, we list the receipt(s)
			Debug.Log("Receipt is valid. Contents:");
			foreach (IPurchaseReceipt productReceipt in result) {
				Debug.Log(productReceipt.productID);
				Debug.Log(productReceipt.purchaseDate);
				Debug.Log(productReceipt.transactionID);
			}
		} catch (IAPSecurityException) {
			Debug.Log("Invalid receipt, not unlocking content");
			validPurchase = false;
		}
		#endif

		if (validPurchase) {

			// Unlock the appropriate content here.
			if (e.purchasedProduct.definition.id == "com.supermom.fairy.10gems") {
				GameSparksManager.instance.playerData.gems.Value += 10;
			} else if (e.purchasedProduct.definition.id == "com.supermom.fairy.36gems") {
				GameSparksManager.instance.playerData.gems.Value += 36;
			} else if (e.purchasedProduct.definition.id == "com.supermom.fairy.70gems") {
				GameSparksManager.instance.playerData.gems.Value += 70;
			} else if (e.purchasedProduct.definition.id == "com.supermom.fairy.110gems") {
				GameSparksManager.instance.playerData.gems.Value += 110;
			} else if (e.purchasedProduct.definition.id == "com.supermom.fairy.150gems") {
				GameSparksManager.instance.playerData.gems.Value += 150;
				SaveLoad.userData.sundayGemOffer.Add (DateTime.Now.Date.ToString ("d"));
			} else if (e.purchasedProduct.definition.id == "com.supermom.fairy.500gems") {
				GameSparksManager.instance.playerData.gems += 500;
			} else if (e.purchasedProduct.definition.id == "com.supermom.fairy.10materials") {

				//Add materials
				GameSparksManager.instance.AddMonster ("wateregg.1");
				GameSparksManager.instance.AddMonster ("wateregg.1");
				GameSparksManager.instance.AddMonster ("fireegg.1");
				GameSparksManager.instance.AddMonster ("fireegg.1");
				GameSparksManager.instance.AddMonster ("woodegg.1");
				GameSparksManager.instance.AddMonster ("woodegg.1");
				GameSparksManager.instance.AddMonster ("lightegg.1");
				GameSparksManager.instance.AddMonster ("lightegg.1");
				GameSparksManager.instance.AddMonster ("darkegg.1");
				GameSparksManager.instance.AddMonster ("darkegg.1");

				SaveLoad.userData.sundayMaterialOffer.Add (DateTime.Now.Date.ToString ("d"));

			} else if (e.purchasedProduct.definition.id == "com.supermom.fairy.starterpack") {
				GameSparksManager.instance.playerData.gems += 50;

				GameSparksManager.instance.AddMonster ("galahad.2");

				GameSparksManager.instance.playerData.isBuyStarterPack = new SafeInt (1);

				starterPackDialog.SetActive (false);
			} else if (e.purchasedProduct.definition.id == "com.supermom.fairy.dailygems") {
				GameSparksManager.instance.playerData.gems.Value += 1;
				GameSparksManager.instance.playerData.dailyGems.Value = 1;
				GameSparksManager.instance.playerData.isReceived.Value = 1;

				buyDailyGemsDialog.SetActive (false);
			}

			GameSparksManager.instance.SavePlayerData ();
			UIManager.instance.UpdateCurrencyTexts ();
			SaveLoad.Save ();

			//Show message dialog
			messageDialog.transform.GetChild (0).GetComponent<Text> ().text = "การซื้อสำเร็จ";
			messageDialog.SetActive (true);
		} else {
			messageDialog.transform.GetChild (0).GetComponent<Text> ().text = "การซื้อล้มเหลว";
			messageDialog.SetActive (true);
		}

		return PurchaseProcessingResult.Complete;
	}

	public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
	{
		// A product purchase attempt did not succeed. Check failureReason for more detail. Consider sharing 
		// this reason with the user to guide their troubleshooting actions.
		Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));

		//Show message dialog
		messageDialog.transform.GetChild(0).GetComponent<Text>().text = "การซื้อล้มเหลว";
		messageDialog.SetActive (true);
	}
	/*
	void VerifyPurchaseGooglePlay(string dataJson,string dataSignature){
		new GameSparks.Api.Requests.GooglePlayBuyGoodsRequest ().SetSignedData (dataJson).SetSignature (dataSignature).Send ((response) => {
			//update the gems receipt according to response data
			//print(response.BoughtItems.GetEnumerator().Current.BaseData.JSON);

			if (!response.HasErrors) {
				string shortCode = response.BoughtItems.GetEnumerator().Current.BaseData.GetString("shortCode");

				if(shortCode == "150_gems")
				{
					GameSparksManager.instance.playerData.gems += 150;
					SaveLoad.userData.sundayGemOffer.Add(DateTime.Now.Date.ToString("d"));
				}
				else if(shortCode == "10_gems")
				{
					GameSparksManager.instance.playerData.gems += 10;
				}
				else if(shortCode == "36_gems")
				{
					GameSparksManager.instance.playerData.gems += 36;
				}
				else if(shortCode == "70_gems")
				{
					GameSparksManager.instance.playerData.gems += 70;
				}
				else if(shortCode == "110_gems")
				{
					GameSparksManager.instance.playerData.gems += 110;
				}
				else if(shortCode == "500_gems")
				{
					GameSparksManager.instance.playerData.gems += 500;
				}
				else if(shortCode == "10_materials")
				{
					//Add materials
					GameSparksManager.instance.AddMonster("wateregg.1");
					GameSparksManager.instance.AddMonster("wateregg.1");
					GameSparksManager.instance.AddMonster("fireegg.1");
					GameSparksManager.instance.AddMonster("fireegg.1");
					GameSparksManager.instance.AddMonster("woodegg.1");
					GameSparksManager.instance.AddMonster("woodegg.1");
					GameSparksManager.instance.AddMonster("lightegg.1");
					GameSparksManager.instance.AddMonster("lightegg.1");
					GameSparksManager.instance.AddMonster("darkegg.1");
					GameSparksManager.instance.AddMonster("darkegg.1");

					SaveLoad.userData.sundayMaterialOffer.Add(DateTime.Now.Date.ToString("d"));
				}
				else if(shortCode == "starter_pack"){
					GameSparksManager.instance.playerData.gems += 50;

					GameSparksManager.instance.AddMonster("galahad.2");

					GameSparksManager.instance.playerData.isBuyStarterPack = new SafeInt(1);

					starterPackDialog.SetActive(false);
				}
				else if(shortCode == "daily_gems"){
					GameSparksManager.instance.playerData.gems.Value += 1;
					GameSparksManager.instance.playerData.dailyGems.Value = 1;
					GameSparksManager.instance.playerData.isReceived.Value = 1;

					buyDailyGemsDialog.SetActive(false);
				}

				UIManager.instance.UpdateCurrencyTexts();
				GameSparksManager.instance.SavePlayerData();
				SaveLoad.Save();

				//Show message dialog
				messageDialog.transform.GetChild(0).GetComponent<Text>().text = "การซื้อสำเร็จ";
				messageDialog.SetActive (true);

				
				//refresh scene
				if(UIManager.instance != null){
					UIManager.instance.ShowShop();
				}

			}else{

				Debug.Log("Error checking with Google Play");
				//messageDialog.transform.GetChild(0).
			}
		});
	}*/

	public void ConfirmSpacePurchase(string shortCode){
		string message = "ต้องการจะเพิ่มช่องเก็บของ ";

		if (shortCode == "5spaces") {
			message = message + " 5 ช่อง";
		} else if (shortCode == "20spaces") {
			message = message + " 20 ช่อง";
		} else if (shortCode == "50spaces") {
			message = message + " 50 ช่อง";
		}

		message = message + " ด้วย " + spacesCost[shortCode].ToString() + " เพชร หรือไม่";

		confirmSpacesPurchaseDialog.transform.GetChild (0).GetComponent<Text> ().text = message;

		Button b = confirmSpacesPurchaseDialog.transform.GetChild (1).GetComponent<Button> ();
		b.onClick.RemoveAllListeners ();
		b.onClick.AddListener (() => BuyMoreInventorySpaces (shortCode));

		confirmSpacesPurchaseDialog.SetActive (true);
	}

	void BuyMoreInventorySpaces(string shortCode){
		confirmSpacesPurchaseDialog.SetActive (false);

		if (GameSparksManager.instance.playerData.gems.Value >= spacesCost [shortCode]) {

			GameSparksManager.instance.playerData.gems.Value -= spacesCost [shortCode];
			AnalyticsManager.instance.SpendGems (shortCode, spacesCost [shortCode]);

			SafeInt space = new SafeInt (0);

			if (shortCode == "5spaces") {
				space.Value = 5;
			} else if (shortCode == "20spaces") {
				space.Value = 20;
			} else if (shortCode == "50spaces") {
				space.Value = 50;
			}

			if (GameSparksManager.instance.playerData.maxInventory + space.Value <= 300) { //less than maximum value
				GameSparksManager.instance.playerData.maxInventory.Value += space.Value;

				GameSparksManager.instance.SavePlayerData ();
				UIManager.instance.UpdateCurrencyTexts ();

				//Show message dialog
				messageDialog.transform.GetChild (0).GetComponent<Text> ().text = "การซื้อสำเร็จ";
				messageDialog.SetActive (true);
			} else {
				messageDialog.transform.GetChild (0).GetComponent<Text> ().text = "จำนวนช่องที่จะซื้อเกินจำนวนช่องสูงสุดที่สามารถมีได้";
				messageDialog.SetActive (true);
			}

		}
	}
}
