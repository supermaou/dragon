﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSparks.Core;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class GachaScene : MonoBehaviour {

	public Image crystal;

	public Sprite normalCrystal;
	public Sprite fireCrystal;
	public Sprite waterCrystal;
	public Sprite plantCrystal;
	public Sprite lightCrystal;
	public Sprite darkCrystal;

	void Start(){
		if (PersistentData.instance.gachaElement == "fire") {
			crystal.sprite = fireCrystal;
		} else if (PersistentData.instance.gachaElement == "water") {
			crystal.sprite = waterCrystal;
		} else if (PersistentData.instance.gachaElement == "plant") {
			crystal.sprite = plantCrystal;
		} else if (PersistentData.instance.gachaElement == "light") {
			crystal.sprite = lightCrystal;
		} else if (PersistentData.instance.gachaElement == "dark") {
			crystal.sprite = darkCrystal;
		} else {
			crystal.sprite = normalCrystal;
		}
	}

	public void OpenGacha(){
		EventSystem.current.currentSelectedGameObject.GetComponent<Button> ().interactable = false;

		new GameSparks.Api.Requests.LogEventRequest ().SetEventKey ("OpenGacha").SetEventAttribute ("element", PersistentData.instance.gachaElement).Send ((response) => {
			if(!response.HasErrors){
				string monShortCode = response.ScriptData.GetString("mon");

				GameSparksManager.instance.AddMonster(monShortCode);

				if(PersistentData.instance.gachaElement == ""){
					GameSparksManager.instance.playerData.gems.Value -= 10;
					GameSparksManager.instance.SavePlayerData();
					AnalyticsManager.instance.SpendGems("gacha",10);
					AnalyticsManager.instance.OpenGacha("normalGacha");
				}else{
					GameSparksManager.instance.playerData.gems.Value -= 20;
					GameSparksManager.instance.playerData.specialGacha.Value = 1;
					GameSparksManager.instance.SavePlayerData();
					AnalyticsManager.instance.SpendGems("gacha",20);
					AnalyticsManager.instance.OpenGacha("specialGacha");
				}

				PersistentData.instance.monsterGet = monShortCode;

				//Add quest progress
				PersistentData.instance.AddQuestProgress("gacha");

				PersistentData.instance.gachaElement = "";

				Initiate.Fade("main",new Color(1f,1f,1f),1.5f);
			}else{
				GameSparksManager.instance.OnError ();
			}
		});
	}
}
