﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSparks.Core;
using UnityEngine.UI;
using System;

public class ArenaUI : MonoBehaviour {
	/*
	public Transform content;
	public Text scoreText;
	public Image medal;

	public GameObject rankItemPrefab;

	public GameObject staminaRefillDialog;
	public GameObject rewardListDialog;

	public Sprite[] medalList;
	int[] medalScores = new int[]{
		5000,4500,4000,3500,2500,1500,0
	};

	int staminaUsed = 20;

	void Awake(){
		PostScore (1000);
	}

	void OnEnable(){

		LoadLeaderboard ();

		scoreText.text = GameSparksManager.instance.playerData.arenaScore.ToString ();

		for (int i = 0; i < medalScores.Length; i++) {
			if (GameSparksManager.instance.playerData.arenaScore >= medalScores [i]) {
				medal.sprite = medalList [i];
				break;
			}
		}
			
	}

	IEnumerator Init(){
		yield return null;

		LoadLeaderboard ();

		scoreText.text = GameSparksManager.instance.playerData.arenaScore.ToString ();

		for (int i = 0; i < medalScores.Length; i++) {
			if (GameSparksManager.instance.playerData.arenaScore >= medalScores [i]) {
				medal.sprite = medalList [i];
				break;
			}
		}
	}

	void PostScore(int score){
		new GameSparks.Api.Requests.LogEventRequest ().SetEventKey ("Score_Event").SetEventAttribute ("Score_Attribute", score).Send ((response) => {
			if(!response.HasErrors){
				print("Post Score Success");
			}else{
				print("Post Score Failed");
				GameSparksManager.instance.OnError();
			}
		});
	}

	void LoadLeaderboard (){
		new GameSparks.Api.Requests.LeaderboardDataRequest ().SetLeaderboardShortCode("Arena_Leaderboard").SetEntryCount(10).Send ((response) => {
			if(!response.HasErrors){

				foreach(GameSparks.Api.Responses.LeaderboardDataResponse._LeaderboardData d in response.Data){
					GameObject g = Instantiate(rankItemPrefab);
					g.transform.SetParent(content,false);

					g.transform.GetChild(0).GetComponent<Text>().text = d.Rank.Value.ToString() + ".";

					for (int i = 0; i < medalScores.Length; i++) {
						if ( Int32.Parse((d.JSONData["Score_Attribute"] ?? 1000).ToString()) >= medalScores [i]) {
							g.transform.GetChild(1).GetComponent<Image>().sprite = medalList[i];
							break;
						}
					}

					g.transform.GetChild(2).GetComponent<Text>().text = d.UserName;
					g.transform.GetChild(3).GetComponent<Text>().text = "คะแนน " + (d.JSONData["Score_Attribute"] ?? 1000).ToString();
				}
			}else{
				print("Error getting leaderboard");
				GameSparksManager.instance.OnError();
			}
		});
	}

	public void StartGame(){
		if(!GS.Authenticated){
			GameSparksManager.instance.OnError();

			return;
		}

		if(SaveLoad.userData.stamina < staminaUsed){
			staminaRefillDialog.SetActive(true);
			return;
		}

		if(GameSparksManager.instance.teamData [GameSparksManager.instance.playerData.currentTeam].Count <= 0){
			return;
		}

		new GameSparks.Api.Requests.LogEventRequest ().SetEventKey ("MatchMaking").Send ((response) => {
			if (!response.HasErrors) {
				GSData data = response.ScriptData.GetGSData("arenaData");

				List<GSData> monsterData = data.GetGSDataList("monsterData");

				PersistentData.instance.ratingWin = data.GetInt("ratingWin").Value;
				PersistentData.instance.ratingLose = data.GetInt("ratingLose").Value;

				PersistentData.instance.enemiesLoadCount = 0;

				for(int i=0;i<monsterData.Count;i++){
					GSData currentMonster = monsterData[i].GetGSData("monster");

					PersistentData.instance.enemiesData.Add(new List<MonsterData>());

					MonsterData m = new MonsterData();
					m.shortCode = currentMonster.GetString("shortCode");
					m.exp = currentMonster.GetInt("exp").Value;
					m.cooldown = 1;
					GameSparksManager.instance.GetMonsterData(m,"enemy");

					PersistentData.instance.enemiesData[i].Add(m);
				}

				StartCoroutine(EnterArena(monsterData.Count));

			} else {
				GameSparksManager.instance.OnError();
			}
		});
	}

	IEnumerator EnterArena(int enemyTeamCount){
		while (!PersistentData.instance.loadedMonsterData) {
			yield return null;
		}

		while (PersistentData.instance.enemiesLoadCount < enemyTeamCount) {
			yield return null;
		}

		//set enemies monster stat
		for (int i = 0; i < PersistentData.instance.enemiesData.Count; i++) {
			for (int j = 0; j < PersistentData.instance.enemiesData [i].Count; j++) {
				PersistentData.instance.enemiesData [i] [j].SetStatToLevel (PersistentData.instance.enemiesData [i] [j].exp);
			}
		}

		PersistentData.instance.gameType = "arena";

		SaveLoad.userData.stamina -= staminaUsed;
		if (SaveLoad.userData.stamina < 0) {
			SaveLoad.userData.stamina = 0;
		}
		SaveLoad.Save ();

		Initiate.Fade("gameplay",new Color(0.1f,0.1f,0.1f),1.5f);
	}

	void CalculateTimeLeft(){
		
	}

	void OnDisable(){
		rewardListDialog.SetActive (false);

		foreach (Transform t in content) {
			Destroy (t.gameObject);
		}
	}

	public void ShowRewardList(){
		rewardListDialog.SetActive (true);
	}

	public void CloseDialog(){
		rewardListDialog.SetActive (false);
	}*/
}
