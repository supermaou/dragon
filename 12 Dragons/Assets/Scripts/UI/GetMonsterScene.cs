﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GetMonsterScene : MonoBehaviour {

	private GameObject mon;

	public TextMeshProUGUI rareText;
	public GameObject[] stars;
	public Button dataButton;

	void OnEnable(){

		if (UIManager.instance.monA.stars == 5) {
			rareText.text = "CHAMPION";
		} else if (UIManager.instance.monA.stars == 4) {
			rareText.text = "LEGEND";
		} else if (UIManager.instance.monA.stars == 3) {
			rareText.text = "RARE";
		} else {
			print ("Stars: " + UIManager.instance.monA.stars.ToString());
			rareText.text = "GREAT";
		}

		rareText.gameObject.SetActive (true);

		mon = Instantiate (MonsterList.instance.GetPrefab (UIManager.instance.monA.shortCode), new Vector3 (0f, -1.5f, 0f), Quaternion.identity);

		dataButton.onClick.RemoveAllListeners ();
		dataButton.onClick.AddListener (() => UIManager.instance.ShowMonsterDetails (UIManager.instance.monA));

		for (int i = 0; i < stars.Length; i++) {
			if (i < UIManager.instance.monA.stars) {
				stars [i].SetActive (true);
				stars [i].transform.localPosition = new Vector3 (((float)i - (float)UIManager.instance.monA.stars / 2f + 0.5f) * 80f, stars [i].transform.localPosition.y, 0f);
			} else {
				stars [i].SetActive (false);
			}
		}
	}

	void OnDisable(){
		if (mon != null) {
			Destroy (mon);
		}

		rareText.gameObject.SetActive (false);

		//notification
		if (SaveLoad.userData.inventoryNoti > 0) {
			UIManager.instance.navMenu [1].transform.GetChild (2).gameObject.SetActive (true);
			UIManager.instance.navMenu [1].transform.GetChild (2).GetChild (0).GetComponent<Text> ().text = SaveLoad.userData.inventoryNoti.ToString ();
		}
	}
}
