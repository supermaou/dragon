﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SwitchMonster : MonoBehaviour {

	public Image mon1;
	public Text name1;
	public Image element1;
	public Text valueMon1;
	public Image[] mon1stars;

	public Image mon2;
	public Text name2;
	public Image element2;
	public Text valueMon2;
	public Image[] mon2stars;

	public Sprite empty;

	void SetSpriteToOriginalSize(Image img, float target){
		float width = img.sprite.rect.width;
		float height = img.sprite.rect.height;

		if (height > width) {
			float multiplier = target / height;

			img.rectTransform.sizeDelta = new Vector2 (width * multiplier, target);
		} else {
			float multiplier = target / width;

			img.rectTransform.sizeDelta = new Vector2 (target, height * multiplier);
		}
	}

	void OnEnable(){
		if (UIManager.instance.monA == null) {
			UIManager.instance.monA = new MonsterData ();
		}
			
		if (UIManager.instance.monA.monsterName == "" || UIManager.instance.monA.objectId == "") {
			mon1.sprite = empty;
			name1.text = "";
			valueMon1.text = "";
			element1.gameObject.SetActive (false);

			mon2.sprite = MonsterList.instance.GetThumbnail (UIManager.instance.monB.shortCode);
			if (UIManager.instance.monB.level.Value < UIManager.instance.monB.maxLevel) {
				valueMon2.text = UIManager.instance.monB.level.ToString () + "\n" + UIManager.instance.monB.attack.ToString () + "\n" + UIManager.instance.monB.hitPoint.ToString () + "\n" + UIManager.instance.monB.recovery.ToString();
			} else {
				valueMon2.text = "MAX\n" + UIManager.instance.monB.attack.ToString () + "\n" + UIManager.instance.monB.hitPoint.ToString () + "\n" + UIManager.instance.monB.recovery.ToString();
			}
			name2.text = UIManager.instance.monB.thaiName;

			if (UIManager.instance.monB.element == "fire") {
				element2.sprite = UIManager.instance.fire;
			} else if (UIManager.instance.monB.element == "water") {
				element2.sprite = UIManager.instance.water;
			} else if (UIManager.instance.monB.element == "plant") {
				element2.sprite = UIManager.instance.plant;
			} else if (UIManager.instance.monB.element == "light") {
				element2.sprite = UIManager.instance.light;
			} else if (UIManager.instance.monB.element == "dark") {
				element2.sprite = UIManager.instance.dark;
			}

			SetSpriteToOriginalSize (mon1, 100f);
			SetSpriteToOriginalSize (mon2, 100f);

			for (int i = 0; i < 6; i++) {
				mon1stars [i].gameObject.SetActive (false);

				if (i < UIManager.instance.monB.stars) {
					mon2stars [i].gameObject.SetActive (true);
				} else {
					mon2stars [i].gameObject.SetActive (false);
				}
			}

			float mid = (((float)UIManager.instance.monB.stars) / 2f) - 0.5f;
			for (int i = 0; i < UIManager.instance.monB.stars; i++) {
				mon2stars [i].transform.localPosition = new Vector3 (((float)i - mid) * 30f, 0f, 0f);
			}

			return;
		}

		//Set up scene
		mon1.sprite = MonsterList.instance.GetThumbnail(UIManager.instance.monA.shortCode);
		mon2.sprite = MonsterList.instance.GetThumbnail (UIManager.instance.monB.shortCode);

		SetSpriteToOriginalSize (mon1, 100f);
		SetSpriteToOriginalSize (mon2, 100f);

		if (UIManager.instance.monA.level.Value < UIManager.instance.monA.maxLevel) {
			valueMon1.text = UIManager.instance.monA.level.ToString () + "\n" + UIManager.instance.monA.attack.ToString () + "\n" + UIManager.instance.monA.hitPoint.ToString () + "\n" + UIManager.instance.monA.recovery.ToString();
		} else {
			valueMon1.text = "MAX\n" + UIManager.instance.monA.attack.ToString () + "\n" + UIManager.instance.monA.hitPoint.ToString () + "\n" + UIManager.instance.monA.recovery.ToString();
		}

		name1.text = UIManager.instance.monA.thaiName;
		element1.gameObject.SetActive (true);

		if (UIManager.instance.monA.element == "fire") {
			element1.sprite = UIManager.instance.fire;
		}
		else if (UIManager.instance.monA.element == "water") {
			element1.sprite = UIManager.instance.water;
		}
		else if (UIManager.instance.monA.element == "plant") {
			element1.sprite = UIManager.instance.plant;
		}
		else if (UIManager.instance.monA.element == "light") {
			element1.sprite = UIManager.instance.light;
		}
		else if (UIManager.instance.monA.element == "dark") {
			element1.sprite = UIManager.instance.dark;
		}

		if (UIManager.instance.monB.level.Value < UIManager.instance.monB.maxLevel) {
			valueMon2.text = UIManager.instance.monB.level.ToString () + "\n" + UIManager.instance.monB.attack.ToString () + "\n" + UIManager.instance.monB.hitPoint.ToString () + "\n" + UIManager.instance.monB.recovery.ToString();
		} else {
			valueMon2.text = "MAX\n" + UIManager.instance.monB.attack.ToString () + "\n" + UIManager.instance.monB.hitPoint.ToString () + "\n" + UIManager.instance.monB.recovery.ToString();
		}

		name2.text = UIManager.instance.monB.thaiName;

		if (UIManager.instance.monB.element == "fire") {
			element2.sprite = UIManager.instance.fire;
		}
		else if (UIManager.instance.monB.element == "water") {
			element2.sprite = UIManager.instance.water;
		}
		else if (UIManager.instance.monB.element == "plant") {
			element2.sprite = UIManager.instance.plant;
		}
		else if (UIManager.instance.monB.element == "light") {
			element2.sprite = UIManager.instance.light;
		}
		else if (UIManager.instance.monB.element == "dark") {
			element2.sprite = UIManager.instance.dark;
		}

		for (int i = 0; i < 6; i++) {
			if (i < UIManager.instance.monA.stars) {
				mon1stars [i].gameObject.SetActive (true);
			} else {
				mon1stars [i].gameObject.SetActive (false);
			}

			if (i < UIManager.instance.monB.stars) {
				mon2stars [i].gameObject.SetActive (true);
			} else {
				mon2stars [i].gameObject.SetActive (false);
			}
		}

		float midA = (((float)UIManager.instance.monA.stars) / 2f) - 0.5f;
		for (int i = 0; i < UIManager.instance.monA.stars; i++) {
			mon1stars [i].transform.localPosition = new Vector3 (((float)i - midA) * 30f, 0f, 0f);
		}

		float midB = (((float)UIManager.instance.monB.stars) / 2f) - 0.5f;
		for (int i = 0; i < UIManager.instance.monB.stars; i++) {
			mon2stars [i].transform.localPosition = new Vector3 (((float)i - midB) * 30f, 0f, 0f);
		}
	}

	public void Confirm(){
		if (UIManager.instance.monA.monsterName == "" || UIManager.instance.monA.objectId == "") {
			if(GameSparksManager.instance.teamData[GameSparksManager.instance.playerData.currentTeam].Count < 6){
				GameSparksManager.instance.teamData [GameSparksManager.instance.playerData.currentTeam].Add (UIManager.instance.monB);
				//send update to server
				int count = GameSparksManager.instance.teamData [GameSparksManager.instance.playerData.currentTeam].Count - 1;

				GameSparksManager.instance.ChangeMonsterTeam (UIManager.instance.monB.objectId, GameSparksManager.instance.playerData.currentTeam, count);

				//Update the team data locally
				foreach (MonsterData m in GameSparksManager.instance.teamData [GameSparksManager.instance.playerData.currentTeam]) {
					if (m == UIManager.instance.monB) {
						m.team [GameSparksManager.instance.playerData.currentTeam] = 1;
						m.order [GameSparksManager.instance.playerData.currentTeam] = GameSparksManager.instance.teamData [GameSparksManager.instance.playerData.currentTeam].Count - 1;
					}
				}

				foreach (MonsterData m in GameSparksManager.instance.monsterData) {
					if (m == UIManager.instance.monB) {
						m.team [GameSparksManager.instance.playerData.currentTeam] = 1;
						m.order [GameSparksManager.instance.playerData.currentTeam] = GameSparksManager.instance.teamData [GameSparksManager.instance.playerData.currentTeam].Count - 1;
					}
				}
			}

			UIManager.instance.ShowHome ();
		} else {
			SwitchTeamMember (UIManager.instance.monA, UIManager.instance.monB);
		}
	}

	void SwitchTeamMember(MonsterData monA, MonsterData monB){

		for (int i = 0; i < GameSparksManager.instance.teamData [GameSparksManager.instance.playerData.currentTeam].Count; i++) {
			if (GameSparksManager.instance.teamData [GameSparksManager.instance.playerData.currentTeam] [i].objectId == monA.objectId) {
				//update team data
				GameSparksManager.instance.teamData [GameSparksManager.instance.playerData.currentTeam] [i] = monB;

				string oidToAdd = monB.objectId;
				string oidToRemove = monA.objectId;
				int team = GameSparksManager.instance.playerData.currentTeam;

				//send update to server
				GameSparksManager.instance.ChangeMonsterTeam (oidToAdd, team,i, oidToRemove);

				//Update the team data locally
				foreach (MonsterData m in GameSparksManager.instance.teamData [GameSparksManager.instance.playerData.currentTeam]) {
					if (m == UIManager.instance.monB) {
						m.team [GameSparksManager.instance.playerData.currentTeam] = 1;
						m.order [GameSparksManager.instance.playerData.currentTeam] = i;
					} else if (m == UIManager.instance.monA) {
						m.team [GameSparksManager.instance.playerData.currentTeam] = 0;
						m.order [GameSparksManager.instance.playerData.currentTeam] = 0;
					}
				}

				foreach (MonsterData m in GameSparksManager.instance.monsterData) {
					if (m == UIManager.instance.monB) {
						m.team [GameSparksManager.instance.playerData.currentTeam] = 1;
						m.order [GameSparksManager.instance.playerData.currentTeam] = i;
					} else if (m == UIManager.instance.monA) {
						m.team [GameSparksManager.instance.playerData.currentTeam] = 0;
						m.order [GameSparksManager.instance.playerData.currentTeam] = 0;
					}
				}
			}
		}

		UIManager.instance.ShowHome ();
	}
		
}
