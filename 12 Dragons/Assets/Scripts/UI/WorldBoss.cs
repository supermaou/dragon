﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GameSparks.Core;
using System;

public class WorldBoss : MonoBehaviour {

	public Transform content;
	public Text scoreText;
	public Text timeLeftText;
	public Image medal;
	public Image bossThumbnail;
	public Text bossName;

	public Text rankText;

	public GameObject rewardDialog;
	public GameObject receiveRewardDialog;
	public GameObject rewardBadge;
	public GameObject messageDialog;
	public GameObject othersTeamDialog;

	public Text message;
	public Text resultText;
	public GameObject rankItemPrefab;

	public Sprite[] medalList;

	void SetSpriteToOriginalSize(Image img, float target){
		float width = img.sprite.rect.width;
		float height = img.sprite.rect.height;

		if (height > width) {
			float multiplier = target / height;

			img.rectTransform.sizeDelta = new Vector2 (width * multiplier, target);
		} else {
			float multiplier = target / width;

			img.rectTransform.sizeDelta = new Vector2 (target, height * multiplier);
		}
	}

	void OnEnable(){
		LoadLeaderboard ();

		rankText.text = "-";
		scoreText.text = "0";
		medal.sprite = medalList [5];
		SetSpriteToOriginalSize (medal, 80f);

		if (GameSparksManager.instance.playerData.worldBoss_reward != "") {
			rewardBadge.SetActive (true);
		} else {
			rewardBadge.SetActive (false);
		}

		CalculateTimeLeft ();
		SetBossThumbnail ();

		LoadPlayerData ();
	}

	void SetBossThumbnail(){
		new GameSparks.Api.Requests.LogEventRequest ().SetEventKey ("getBossName").Send ((response) => {
			if(!response.HasErrors){
				string shortCode = response.ScriptData.GetGSData("currentBoss").GetString("shortCode");
				bossThumbnail.sprite = MonsterList.instance.GetThumbnail(shortCode);

				if(shortCode == "kingarthur.3"){
					bossName.text = "ผู้พิชิตดาบศักดิ์สิทธิ์ อาเธอร์";
				}else if(shortCode == "lancelot.3"){
					bossName.text = "โล่ที่แกร่งที่สุด แลนสลอต";
				}else if(shortCode == "merlin.3"){
					bossName.text = "สารานุกรมเดินได้ เมอร์ลิน";
				}else if(shortCode == "galahad.3"){
					bossName.text = "หอกเทวดา ริชชี่";
				}else if(shortCode == "mordred.3"){
					bossName.text = "เจ้าแห่งศาสตร์มืด มอเดรด";
				}

				SetSpriteToOriginalSize(bossThumbnail,100f);
			}
		});
	}

	void CalculateTimeLeft(){
		new GameSparks.Api.Requests.LogEventRequest ().SetEventKey ("getLastBossResetTime").Send ((response) => {
			if(!response.HasErrors){
				long time = response.ScriptData.GetGSData("time").GetLong("time").Value;

				DateTime date = new DateTime(1970,1,1) + new TimeSpan(time * 10000);
				date = date.AddDays(7);

				int daysLeft = (int) (date - DateTime.UtcNow).TotalDays;
				int hourLeft = (int) (date - DateTime.UtcNow).TotalHours % 24;

				timeLeftText.text = "เหลือเวลาอีก " + daysLeft.ToString() + " วัน " + hourLeft.ToString() + " ชั่วโมง";

			}else{
				GameSparksManager.instance.OnError();
			}
		});
	}

	void LoadPlayerData(){

		new GameSparks.Api.Requests.GetLeaderboardEntriesRequest ().SetLeaderboards (new List<string>{ "WorldBoss_Leaderboard" }).Send ((response) => {
			if(!response.HasErrors){
				string jsonData = response.JSONString;

				string[] data = jsonData.Split(new[]{"\"WorldBoss_Leaderboard\":"},StringSplitOptions.None);

				if(data[1] != "{}}"){
					rankText.text = GetRank(jsonData);
					scoreText.text = GetScore(jsonData);

					PersistentData.instance.bestScore = Int32.Parse(scoreText.text);

					int rank = Int32.Parse(rankText.text);
					if(rank == 1){
						medal.sprite = medalList[0];
					}else if(rank <= 10){
						medal.sprite = medalList[1];
					}else if(rank <= 50){
						medal.sprite = medalList[2];
					}else if(rank <= 200){
						medal.sprite = medalList[3];
					}else if(rank <= 500){
						medal.sprite = medalList[4];
					}else{
						medal.sprite = medalList[5];
					}
				}

			}else{
				GameSparksManager.instance.OnError();
			}
		});
	}

	string GetRank(string jsonData){
		string[] tokens = jsonData.Split(new[]{"rank\":"},StringSplitOptions.None);
		string[] tokens2 = tokens[1].Split('}');

		return tokens2 [0];
	}

	string GetScore(string jsonData){
		string[] tokens = jsonData.Split(new[]{"\"Score_Attribute\":"},StringSplitOptions.None);
		string[] tokens2 = tokens [1].Split (new[]{ ",\"when\"" }, StringSplitOptions.None);

		return tokens2 [0];
	}

	void LoadLeaderboard (){
		foreach (Transform t in content) {
			Destroy (t.gameObject);
		}

		new GameSparks.Api.Requests.LeaderboardDataRequest ().SetLeaderboardShortCode("WorldBoss_Leaderboard").SetEntryCount(25).Send ((response) => {
			if(!response.HasErrors){

				foreach(GameSparks.Api.Responses.LeaderboardDataResponse._LeaderboardData d in response.Data){
					GameObject g = Instantiate(rankItemPrefab);
					g.transform.SetParent(content,false);

					g.transform.GetChild(0).GetComponent<Text>().text = d.Rank.Value.ToString() + ".";

					g.GetComponent<LeaderboardItem>().LoadTeamData(d.UserId);

					g.transform.GetChild(2).GetComponent<Text>().text = d.UserName;
					g.transform.GetChild(3).GetComponent<Text>().text = "คะแนน " + (d.JSONData["Score_Attribute"] ?? 0).ToString();
					g.transform.GetChild(4).GetComponent<Button>().onClick.AddListener(() => ShowOtherPeopleTeam(g,d.UserId));
				}
			}else{
				GameSparksManager.instance.OnError();
			}
		});
	}

	void ShowOtherPeopleTeam(GameObject g,string userId){

		LeaderboardItem l = g.GetComponent<LeaderboardItem> ();
		List<string> team = l.team;
		List<int> levels = l.levels;
		List<int> skillLevels = l.skillLevels;

		for (int i = 0; i < 6; i++) {
			if(i < team.Count){
				Image img = othersTeamDialog.transform.GetChild(i).GetChild(0).GetComponent<Image>();
				img.sprite = MonsterList.instance.GetThumbnail(team[i]);
				SetSpriteToOriginalSize(img,110f);

				Text txt = othersTeamDialog.transform.GetChild(i).GetChild(1).GetComponent<Text>();
				txt.text = "Lv. " + levels[i].ToString();

				txt.gameObject.SetActive(true);

				Button b = othersTeamDialog.transform.GetChild(i).GetChild(0).GetComponent<Button>();
				b.onClick.RemoveAllListeners();

				MonsterData m = new MonsterData();
				m.shortCode = team[i];
				m.level.Value = levels[i];
				m.skillLevel.Value = skillLevels[i];

				GameSparksManager.instance.GetMonsterData(m,"",false);

				b.onClick.AddListener(() => UIManager.instance.ShowMonsterDetails(m));

			}else{
				Image img = othersTeamDialog.transform.GetChild(i).GetChild(0).GetComponent<Image>();
				img.sprite = UIManager.instance.btnNormal;
				SetSpriteToOriginalSize(img,90f);

				othersTeamDialog.transform.GetChild(i).GetChild(1).gameObject.SetActive(false);

				Button b = othersTeamDialog.transform.GetChild(i).GetChild(0).GetComponent<Button>();
				b.onClick.RemoveAllListeners();
				b.interactable = false;
			}
		}

		othersTeamDialog.SetActive(true);
			
	}

	public void StartGame(){
		if(!GS.Authenticated){
			GameSparksManager.instance.OnError();

			return;
		}

		if (GameSparksManager.instance.playerData.dailyRaid.Value == 1) {
			//Show popup
			message.text = "ล่าบอสได้วันละครั้ง";
			messageDialog.SetActive(true);
			return;
		}

		if(GameSparksManager.instance.teamData [GameSparksManager.instance.playerData.currentTeam].Count <= 0){
			return;
		}

		new GameSparks.Api.Requests.LogEventRequest ().SetEventKey ("boss_fight").Send ((response) => {
			if (!response.HasErrors) {
				GSData data = response.ScriptData.GetGSData("currentBoss");

				DungeonData d = new DungeonData();

				d.background = data.GetInt("background") ?? 0;
				d.music = data.GetInt("music") ?? 0;
				d.dropRateNormal = 0;
				d.dropRateBoss = 0;
				//d.element = data.GetString("element");

				d.gold = 0;
				d.exp = 0;
				d.wave = data.GetIntList("wave");

				List<GSData> enemyList = data.GetGSDataList("enemies");

				d.enemies.Clear();

				for(int j=0;j<enemyList.Count;j++){
					EnemyData e = new EnemyData();
					e.enemyShortCode = enemyList[j].GetString("name");
					e.turn = enemyList[j].GetInt("turn").Value;
					e.hitPoint = enemyList[j].GetInt("hitPoint").Value;
					e.attack = enemyList[j].GetInt("attack").Value;
					e.defense = enemyList[j].GetInt("defense").Value;
					e.lootChance = 0;
					e.enemySkill = enemyList[j].GetGSData("enemySkill");
					d.enemies.Add(e);
				}

				PersistentData.instance.gameType = "worldBoss";
				PersistentData.instance.ResetData();
				PersistentData.instance.currentDungeon = d;

				StartCoroutine (LoadEnemiesData ()); //cache enemies data to Persistence data gameobject;

			}else{
				GameSparksManager.instance.OnError();
			}
		});
	}

	IEnumerator LoadEnemiesData(){
		PersistentData.instance.enemiesData.Clear ();
		PersistentData.instance.enemiesLoadCount = 0;

		//extract data from EnemyData class and put it in MonsterData class and also get the data according to its shortCode
		int count = 0;
		for (int i = 0; i < PersistentData.instance.currentDungeon.wave.Count; i++) {
			PersistentData.instance.enemiesData.Add (new List<MonsterData> ());
			for (int j = 0; j < PersistentData.instance.currentDungeon.wave [i]; j++) {
				MonsterData m = new MonsterData ();
				m.shortCode = PersistentData.instance.currentDungeon.enemies [count].enemyShortCode;
				m.cooldown = new SafeInt (PersistentData.instance.currentDungeon.enemies [count].turn);
				m.hitPoint = new SafeInt (PersistentData.instance.currentDungeon.enemies [count].hitPoint);
				m.attack = new SafeInt (PersistentData.instance.currentDungeon.enemies [count].attack);
				m.defense = new SafeInt (PersistentData.instance.currentDungeon.enemies [count].defense);
				m.lootChance = new SafeInt (PersistentData.instance.currentDungeon.enemies [count].lootChance);
				m.enemySkill = PersistentData.instance.currentDungeon.enemies [count].enemySkill;
				GameSparksManager.instance.GetMonsterData (m, "enemy");
				PersistentData.instance.enemiesData [i].Add (m);
				count++;
			}
		}

		while (!PersistentData.instance.loadedMonsterData) {
			yield return null;
		}

		while (PersistentData.instance.enemiesLoadCount < PersistentData.instance.currentDungeon.enemies.Count) {
			yield return null;
		}

		Initiate.Fade("gameplay",new Color(0.1f,0.1f,0.1f),1.5f);
	}

	public void ShowRewardDialog(){
		rewardDialog.SetActive (true);
	}

	public void ClosePopup(){
		rewardDialog.SetActive (false);
		receiveRewardDialog.SetActive (false);
	}

	public void ReceiveReward(){
		if (GameSparksManager.instance.playerData.worldBoss_reward == "") {

			resultText.text = "สามารถรับรางวัลได้หลังสิ้นสุดซีซั่น (ทุกวันจันทร์)";
			receiveRewardDialog.SetActive(true);

			return;
		}

		new GameSparks.Api.Requests.LogEventRequest ().SetEventKey ("boss_reward").Send ((response) => {
			if(!response.HasErrors){
				GameSparksManager.instance.playerData.worldBoss_reward = "";

				GSData data = response.ScriptData.GetGSData("reward");

				//string monster = data.GetString("monster");
				int gold = data.GetInt("gold") ?? 0;
				int gems = data.GetInt("gems") ?? 0;

				string txt = "ได้รับ " + gold.ToString() + " ทอง และ " + gems.ToString() + " เพชร";

				GameSparksManager.instance.playerData.gold += gold;
				GameSparksManager.instance.playerData.gems += gems;
				AnalyticsManager.instance.ReceiveGems("WorldBoss Reward",gems);
				GameSparksManager.instance.SavePlayerData();
				UIManager.instance.UpdateCurrencyTexts();

				/*
				if(monster != null){
					txt = txt + " + มอนสเตอร์บอส ";

					string[] s = monster.Split('.');
					if(s[1] == "1"){
						txt = txt + " 4 ดาว";
					}else if(s[1] == "2"){
						txt = txt + " 5 ดาว";
					}else if(s[1] == "3"){
						txt = txt + " 6 ดาว";
					}

					GameSparksManager.instance.AddMonster(monster);
				}*/

				resultText.text = txt;

				receiveRewardDialog.SetActive(true);
				rewardBadge.SetActive(false);

			}else{
				print(response.Errors.JSON);
				GameSparksManager.instance.OnError();
			}
		});
	}
}
