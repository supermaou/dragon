﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FuseScene : MonoBehaviour {

	public GameObject fuseObj;
	public GameObject monSprite;
	public Text monName;
	public Text monStat;
	public Image element;
	public Image[] materials;
	//public GameObject levelupText;
	public Text expNeeded;
	public Text expGainText;
	public Image expBar;
	public Text statIncrease;
	public Text moneyToUse;
	public GameObject moneyNotEnoughText;

	private int moneyToUseInt;
	private Color defaultColor = new Color (47f / 255f, 47f / 255f, 47f / 255f);

	void SetSpriteToOriginalSize(Image img, float target){
		float width = img.sprite.rect.width;
		float height = img.sprite.rect.height;

		if (height > width) {
			float multiplier = target / height;

			img.rectTransform.sizeDelta = new Vector2 (width * multiplier, target);
		} else {
			float multiplier = target / width;

			img.rectTransform.sizeDelta = new Vector2 (target, height * multiplier);
		}
	}

	void OnEnable(){
		//Calculate remaining exp
		int expRequire = UIManager.instance.monA.ExpRequireForNextLevel();
		int expGain = TotalExpGain ();

		//check if already at max exp
		if (UIManager.instance.monA.exp >= UIManager.instance.monA.maxExp) {
			expGain = 0;
		}

		//set text for exp required
		if (expRequire > expGain) {
			expNeeded.text = (UIManager.instance.monA.TotalExpRequireForNextLevel() - expRequire + expGain).ToString () + " / " + UIManager.instance.monA.TotalExpRequireForNextLevel().ToString ();
			expBar.fillAmount = (float) (UIManager.instance.monA.TotalExpRequireForNextLevel() - expRequire + expGain) / (float) UIManager.instance.monA.TotalExpRequireForNextLevel ();
		} else {
			if (expRequire == 0) {
				expNeeded.text = "เลเวลสูงสุด"; 
			} else {
				expNeeded.text = "เลเวลอัพ"; 
			}
			expBar.fillAmount = 1f;

			int levelIncrease = UIManager.instance.monA.CalculateLevel (UIManager.instance.monA.exp.Value + expGain) - UIManager.instance.monA.level.Value;

			MonsterData ma = UIManager.instance.monA;
			int atkDiff = ma.StatDifference (ma.CalculateLevel (ma.exp.Value + expGain), ma.level.Value, ma.minAttack.Value, ma.maxAttack.Value, ma.gfAttack);
			int hpDiff = ma.StatDifference (ma.CalculateLevel (ma.exp.Value + expGain), ma.level.Value, ma.minHitPoint.Value, ma.maxHitPoint.Value, ma.gfHitPoint);
			int defDiff = ma.StatDifference (ma.CalculateLevel (ma.exp.Value + expGain), ma.level.Value, ma.minRecovery.Value, ma.maxRecovery.Value, ma.gfRecovery);

			if (ma.exp + expGain >= ma.TotalExpForMaxLevel ()) {
				statIncrease.text = "MAX" + "\n+" + atkDiff.ToString () + "\n+" + hpDiff.ToString () + "\n+" + defDiff.ToString ();
			} else {
				statIncrease.text = "+" + levelIncrease.ToString () + "\n+" + atkDiff.ToString () + "\n+" + hpDiff.ToString () + "\n+" + defDiff.ToString ();
			}
		}

		if (expGain <= 0) {
			statIncrease.text = "+0\n+0\n+0\n+0";
			expGainText.gameObject.SetActive (false);
		} else {
			expGainText.text = "+ " + expGain.ToString() + " exp";
			expGainText.gameObject.SetActive (true);
		}

		//Set UI
		monSprite = Instantiate(MonsterList.instance.GetPrefab(UIManager.instance.monA.shortCode),new Vector3(0f,0.5f,0f),Quaternion.identity) as GameObject;

		monName.text = UIManager.instance.monA.thaiName;
		if (UIManager.instance.monA.level.Value < UIManager.instance.monA.maxLevel) {
			monStat.text = UIManager.instance.monA.level.ToString () + "\n" + UIManager.instance.monA.attack.ToString () + "\n" + UIManager.instance.monA.hitPoint.ToString ()
				+ "\n" + UIManager.instance.monA.recovery.ToString ();
		} else {
			monStat.text = "MAX\n" + UIManager.instance.monA.attack.ToString () + "\n" + UIManager.instance.monA.hitPoint.ToString ()
				+ "\n" + UIManager.instance.monA.recovery.ToString ();
		}

		//set plus icon to on and off
		for (int i = 0; i < 5; i++) {
			if (i < UIManager.instance.mons2fuse.Count) {
				materials [i].sprite = MonsterList.instance.GetThumbnail (UIManager.instance.mons2fuse [i].shortCode);
				materials [i].color = Color.white;
				materials [i].transform.GetChild (0).gameObject.SetActive (false);
				SetSpriteToOriginalSize (materials [i], 100f);
			} else {
				materials [i].sprite = null;
				materials [i].color = defaultColor;
				materials [i].rectTransform.sizeDelta = new Vector2 (100f, 100f);
				materials [i].transform.GetChild (0).gameObject.SetActive (true);
			}
		}

		if (UIManager.instance.monA.element == "fire") {
			element.sprite = UIManager.instance.fire;
		} else if (UIManager.instance.monA.element == "water") {
			element.sprite = UIManager.instance.water;
		} else if (UIManager.instance.monA.element == "plant") {
			element.sprite = UIManager.instance.plant;
		} else if (UIManager.instance.monA.element == "light") {
			element.sprite = UIManager.instance.light;
		} else if (UIManager.instance.monA.element == "dark") {
			element.sprite = UIManager.instance.dark;
		}

		//Set money used to fuse
		moneyToUseInt = (UIManager.instance.mons2fuse.Count * 200 * UIManager.instance.monA.level.Value);
		moneyToUse.text = (UIManager.instance.mons2fuse.Count * 200 * UIManager.instance.monA.level.Value).ToString ();

		if (GameSparksManager.instance.playerData.gold < moneyToUseInt) {
			moneyNotEnoughText.SetActive (true);
		} else {
			moneyNotEnoughText.SetActive (false);
		}
	}

	void OnDisable(){
		if (monSprite) {
			Destroy (monSprite);
		}
	}

	public void Fuse(){
		//check if there is any materials added
		if (UIManager.instance.mons2fuse.Count < 1) {
			return;
		}

		if (Application.internetReachability == NetworkReachability.NotReachable) {
			return;
		}

		//check if money is enough
		if (GameSparksManager.instance.playerData.gold < moneyToUseInt) {
			return;
		}

		GameSparksManager.instance.playerData.gold -= moneyToUseInt;
		AnalyticsManager.instance.SpendGold ("fuse", moneyToUseInt);
		GameSparksManager.instance.SavePlayerData ();

		fuseObj.SetActive (true);
		//monSprite.gameObject.SetActive (false);
		UIManager.instance.ShowFuseAnimate ();

		//StartCoroutine (Delay (0.5f));
	}
		
	int TotalExpGain(){
		SafeInt exp = new SafeInt (0);

		for (int i = 0; i < UIManager.instance.mons2fuse.Count; i++) {
			if (UIManager.instance.mons2fuse [i].element == UIManager.instance.monA.element) {
				exp = new SafeInt(exp.Value + Mathf.RoundToInt (UIManager.instance.mons2fuse [i].exp.Value * 0.5f * 1.5f));
				exp = new SafeInt(exp.Value + Mathf.RoundToInt (UIManager.instance.mons2fuse [i].startExp.Value * 0.5f * 1.5f));
			} else {
				exp = new SafeInt(exp.Value + Mathf.RoundToInt (UIManager.instance.mons2fuse [i].exp.Value * 0.5f));
				exp = new SafeInt(exp.Value + Mathf.RoundToInt (UIManager.instance.mons2fuse [i].startExp.Value * 0.5f));
			}
		}

		return exp.Value;
	}
}
