﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.EventSystems;

public class Inventory : MonoBehaviour {

	public GameObject starPrefab;
	public GameObject levelText;

	public Text capacityText;

	public GameObject detailDialog;

	public Sprite lockSprite;
	public Sprite unlockSprite;

	public GameObject SortOptionDialog;

	public List<string> criteriaElements = new List<string> {
		"fire",
		"water",
		"plant",
		"light",
		"dark"
	};

	public List<string> criteriaStars = new List<string>{
		"1",
		"2",
		"3",
		"4",
		"5",
		"6"
	};

	private int startTapCriteriaElements = 0;
	private int startTapCriteriaStars = 0;

	void OnEnable(){
		if (SaveLoad.userData.inventoryNoti > 0) {
			SaveLoad.userData.inventoryNoti = 0;
			SaveLoad.Save ();
			UIManager.instance.navMenu [1].transform.GetChild (2).gameObject.SetActive (false);
		}

		/*
		if (UIManager.instance.resetInventory == 1) {
			UIManager.instance.resetInventory = 0;
			ShowInventory ();
		}*/

		criteriaElements = new List<string> {
			"fire",
			"water",
			"plant",
			"light",
			"dark"
		};

		criteriaStars = new List<string>{
			"1",
			"2",
			"3",
			"4",
			"5",
			"6"
		};

		startTapCriteriaElements = 0;
		startTapCriteriaStars = 0;

		for (int i = 0; i < 11; i++) {
			SortOptionDialog.transform.GetChild (i).GetComponent<Image> ().color = Color.white;
			foreach (Transform t in SortOptionDialog.transform.GetChild (i)) {
				t.GetComponent<Text> ().color = Color.white;
			}
		}

		ShowInventory ();
	}

	void SetSpriteToOriginalSize(Image img, float target){
		float width = img.sprite.rect.width;
		float height = img.sprite.rect.height;

		if (height > width) {
			float multiplier = target / height;

			img.rectTransform.sizeDelta = new Vector2 (width * multiplier, target);
		} else {
			float multiplier = target / width;

			img.rectTransform.sizeDelta = new Vector2 (target, height * multiplier);
		}
	}

	void OnDisable(){
		foreach (Transform t in transform.GetChild(0)) {
			Destroy (t.gameObject);
		}
	}

	void ShowInventory(){
		capacityText.text = GameSparksManager.instance.monsterData.Count.ToString () + " / " + GameSparksManager.instance.playerData.maxInventory.ToString ();

		foreach (Transform t in transform.GetChild(0)) {
			Destroy (t.gameObject);
		}


		for (int i = 0; i < GameSparksManager.instance.monsterData.Count; i++) {
			//Rule out according to criteria
			if (!criteriaElements.Contains (GameSparksManager.instance.monsterData [i].element) || !criteriaStars.Contains (GameSparksManager.instance.monsterData [i].stars.ToString ())) {
				continue;
			}

			GameObject g = new GameObject ();
			g.transform.SetParent (transform.GetChild (0), false);

			Image im = g.AddComponent<Image> ();
			im.raycastTarget = false;

			g.AddComponent<Mask> ().showMaskGraphic = false;

			GameObject mon = new GameObject ();
			mon.transform.SetParent (g.transform, false);
			mon.transform.localPosition = new Vector3 (0f, 0f, 0f);

			Image img = mon.AddComponent<Image> ();
			img.sprite = MonsterList.instance.GetThumbnail (GameSparksManager.instance.monsterData [i].shortCode);
			SetSpriteToOriginalSize (img, 120f);

			MonsterController m = mon.AddComponent<MonsterController> ();
			m.CloneMonsterData (GameSparksManager.instance.monsterData [i]);

			Button b = mon.AddComponent<Button> ();
			b.onClick.AddListener (() => ShowDetailDialog(m.monsterData));

			GameObject levelTxt = Instantiate (levelText, g.transform, false);
			levelTxt.transform.localPosition = new Vector3 (0f, -80f, 0f);
			levelTxt.GetComponent<Text> ().text = "Level " + m.level.ToString ();

			/*
			for (int x = 0; x < m.stars; x++) {
				GameObject s = Instantiate (starPrefab, g.transform, false);
				s.transform.localPosition = new Vector3 (((float)x - (float)m.stars / 2f + 0.5f) * 20f, -80f, 0f);
			}*/
		}
	}

	void ShowDetailDialog(MonsterData m){
		UIManager.instance.monA = m;

		Image img = detailDialog.transform.GetChild (0).GetComponent<Image> ();
		img.sprite = MonsterList.instance.GetThumbnail (m.shortCode);
		SetSpriteToOriginalSize (img, 150f);

		detailDialog.transform.GetChild (1).GetComponent<Text> ().text = m.thaiName;
		detailDialog.transform.GetChild (2).GetComponent<Text> ().text = "Level " + m.level.ToString () + " / " + m.maxLevel.ToString ();

		if (m.element == "fire") {
			detailDialog.transform.GetChild (3).GetComponent<Image> ().sprite = UIManager.instance.fire;
		}else if (m.element == "water") {
			detailDialog.transform.GetChild (3).GetComponent<Image> ().sprite = UIManager.instance.water;
		}else if (m.element == "plant") {
			detailDialog.transform.GetChild (3).GetComponent<Image> ().sprite = UIManager.instance.plant;
		}else if (m.element == "light") {
			detailDialog.transform.GetChild (3).GetComponent<Image> ().sprite = UIManager.instance.light;
		}else if (m.element == "dark") {
			detailDialog.transform.GetChild (3).GetComponent<Image> ().sprite = UIManager.instance.dark;
		}

		Transform stars = detailDialog.transform.GetChild (4);
		for (int i = 0; i < 6; i++) {
			if (i < m.stars) {
				stars.GetChild (i).gameObject.SetActive (true);
				stars.GetChild(i).localPosition = new Vector3 (((float)i - (float)m.stars / 2f + 0.5f) * 50f, 0f, 0f);
			} else {
				stars.GetChild (i).gameObject.SetActive (false);
			}
		}

		//data btn
		detailDialog.transform.GetChild (5).GetComponent<Button> ().onClick.AddListener (() => UIManager.instance.ShowMonsterDetails (m));

		//evolve btn
		detailDialog.transform.GetChild(6).GetComponent<Button>().onClick.AddListener (() => UIManager.instance.ShowEvolve());

		//fuse btn
		detailDialog.transform.GetChild(7).GetComponent<Button>().onClick.AddListener(() => UIManager.instance.ShowFuse());

		//Check if there is evolve form
		detailDialog.transform.GetChild(6).gameObject.SetActive (false);

		for (int i = 0; i < MonsterList.instance.monsterModels.Length; i++) {
			string monsterShortCode = MonsterList.instance.monsterModels [i].shortCode;
			string[] model = monsterShortCode.Split ('.');
			string[] thisMon = m.shortCode.Split ('.');

			if (thisMon [0] == model [0] && Int32.Parse(thisMon [1]) == Int32.Parse(model [1]) - 1) {
				//There is an evolve form
				detailDialog.transform.GetChild(6).gameObject.SetActive (true);
				UIManager.instance.evolveShortCode = monsterShortCode;
			}
		}

		//Set lock icon
		detailDialog.transform.GetChild (8).GetComponent<Button> ().onClick.AddListener (() => ToggleLock (m));

		if (m.isLocked == 1) {
			Image im = detailDialog.transform.GetChild (8).GetComponent<Image> ();
			im.sprite = lockSprite;
			SetSpriteToOriginalSize (im, 70f);
		} else {
			Image im = detailDialog.transform.GetChild (8).GetComponent<Image> ();
			im.sprite = unlockSprite;
			SetSpriteToOriginalSize (im, 70f);
		}

		detailDialog.SetActive (true);
	}

	void ToggleLock(MonsterData m){
		if (m.isLocked == 1) {
			m.isLocked = 0;

			Image img = EventSystem.current.currentSelectedGameObject.GetComponent<Image> ();
			img.sprite = unlockSprite;
			SetSpriteToOriginalSize (img, 70f);
		} else {
			m.isLocked = 1;

			Image img = EventSystem.current.currentSelectedGameObject.GetComponent<Image> ();
			img.sprite = lockSprite;
			SetSpriteToOriginalSize (img, 70f);
		}

		GameSparksManager.instance.UpdateMonster (m.objectId, m.exp.Value, m.skillLevel.Value, m.isLocked);
	}

	void HideInventory(){
		foreach(Transform t in transform.GetChild(0)){
			Destroy(t.gameObject);
		}
	}


	public void ShowSortOption(){
		SortOptionDialog.SetActive (true);
	}

	public void ChooseCriteria(string command){
		if (command == "fire" || command == "water" || command == "plant" || command == "light" || command == "dark") {
			if (startTapCriteriaElements == 0) {
				startTapCriteriaElements = 1;
				criteriaElements.Clear ();
			}

			if (!criteriaElements.Contains (command)) {
				criteriaElements.Add (command);

				Transform self = EventSystem.current.currentSelectedGameObject.transform;
				self.GetComponent<Image> ().color = new Color (128f / 255f, 128f / 255f, 128f / 255f);
				foreach (Transform t in self) {
					t.GetComponent<Text>().color = new Color (128f / 255f, 128f / 255f, 128f / 255f);
				}

			} else {
				criteriaElements.Remove (command);

				Transform self = EventSystem.current.currentSelectedGameObject.transform;
				self.GetComponent<Image> ().color = Color.white;
				foreach (Transform t in self) {
					t.GetComponent<Text> ().color = Color.white;
				}
			}

		} else {
			if (startTapCriteriaStars == 0) {
				startTapCriteriaStars = 1;
				criteriaStars.Clear ();
			}

			if (!criteriaStars.Contains (command)) {
				criteriaStars.Add (command);

				Transform self = EventSystem.current.currentSelectedGameObject.transform;
				self.GetComponent<Image> ().color = new Color (128f / 255f, 128f / 255f, 128f / 255f);
				foreach (Transform t in self) {
					t.GetComponent<Text>().color = new Color (128f / 255f, 128f / 255f, 128f / 255f);
				}
			} else {
				criteriaStars.Remove (command);

				Transform self = EventSystem.current.currentSelectedGameObject.transform;
				self.GetComponent<Image> ().color = Color.white;
				foreach (Transform t in self) {
					t.GetComponent<Text> ().color = Color.white;
				}
			}
		}
	}

	public void ConfirmCriteria(){
		if (criteriaElements.Count <= 0) {
			criteriaElements = new List<string> {
				"fire",
				"water",
				"plant",
				"light",
				"dark"
			};
		}

		if (criteriaStars.Count <= 0) {
			criteriaStars = new List<string>{
				"1",
				"2",
				"3",
				"4",
				"5",
				"6"
			};
		}

		SortOptionDialog.SetActive (false);

		ShowInventory ();
	}

	string ChildNumberToCriteria(int childNumber){
		if (childNumber == 0) {
			return "fire";
		} else if (childNumber == 1) {
			return "water";
		} else if (childNumber == 2) {
			return "plant";
		} else if (childNumber == 3) {
			return "light";
		} else if (childNumber == 4) {
			return "dark";
		} else if (childNumber == 5) {
			return "1";
		} else if (childNumber == 6) {
			return "2";
		} else if (childNumber == 7) {
			return "3";
		} else if (childNumber == 8) {
			return "4";
		} else if (childNumber == 9) {
			return "5";
		} else if (childNumber == 10) {
			return "6";
		}

		return "";
	}
}
