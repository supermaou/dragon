﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GameSparks.Core;
using System;
using UnityEngine.EventSystems;

public class Home : MonoBehaviour {

	public Transform[] stars;

	public GameObject[] monsterSprites;
	public GameObject[] monsterStatPanels;

	public Image[] teamThumbnails;
	public Text[] nameTexts;
	public Text[] levelTexts;
	public Image[] elements;
	public Image[] dots;
	public Button[] changeButtons;
	public Button[] dataButtons;

	public GameObject resetTeamConfirmDialog;

	public Text teamText;

	//Element sprite
	public Sprite fire;
	public Sprite water;
	public Sprite plant;
	public Sprite light;
	public Sprite dark;

	public Sprite lockSprite;
	public Sprite unlockSprite;

	//other sprites
	public Sprite emptySprite;
	public Sprite emptyBadge;

	bool canMove = true;

	int currentPage = 0;

	private Vector2 touchOrigin = new Vector2(-100f,-100f);

	public GameObject detailDialog;
	public GameObject teamDetailPopup;
		
	void OnEnable(){
		SetupHomeScene ();
	}

	void OnDisable(){

		//Reset the gameobject that was instantiated
		for (int i = 0; i < monsterSprites.Length; i++) {
			if (monsterSprites [i] != null) {
				Destroy (monsterSprites [i]);
				monsterSprites [i] = null;
			}
		}

		Resources.UnloadUnusedAssets ();
	}

	IEnumerator Start(){
		yield return new WaitForSeconds (0.05f);
		SetupHomeScene ();
	}

	void SetSpriteToOriginalSize(Image img, float target){
		if (img.sprite == null) {
			return;
		}

		float width = img.sprite.rect.width;
		float height = img.sprite.rect.height;

		if (height > width) {
			float multiplier = target / height;

			img.rectTransform.sizeDelta = new Vector2 (width * multiplier, target);
		} else {
			float multiplier = target / width;

			img.rectTransform.sizeDelta = new Vector2 (target, height * multiplier);
		}
	}

	void SetupHomeScene(){

		teamText.text = (GameSparksManager.instance.playerData.currentTeam + 1).ToString() + "/5";

		for (int i = 0; i < GameSparksManager.instance.teamData[GameSparksManager.instance.playerData.currentTeam].Count; i++) {
			MonsterData mon = GameSparksManager.instance.teamData[GameSparksManager.instance.playerData.currentTeam][i];

			teamThumbnails[i].sprite = MonsterList.instance.GetThumbnail(mon.shortCode);
			SetSpriteToOriginalSize (teamThumbnails [i], 90f);
			if (mon.level.Value < mon.maxLevel) {
				levelTexts [i].text = "Level " + mon.level.ToString ();
			} else {
				levelTexts [i].text = "Level MAX";
			}

			if (i != currentPage) {
				teamThumbnails [i].color = new Color (178f / 255f, 178f / 255f, 178f / 255f);
			} else {
				teamThumbnails [i].color = new Color(1f,1f,1f);
			}
				
			if (monsterSprites [i] == null) {
				monsterSprites [i] = Instantiate (MonsterList.instance.GetPrefab (mon.shortCode), new Vector3 (6f * (i - currentPage), -2f, 0f), Quaternion.identity) as GameObject; 
			}

			MonsterController mc = teamThumbnails [i].gameObject.GetComponent<MonsterController> ();
			mc.CloneMonsterData (mon);
			//mc.sprite = monsterSprites [i].GetComponent<SpriteRenderer> ().sprite;
			//mc.thumbnail = teamThumbnails [i].sprite;

			changeButtons [i].onClick.RemoveAllListeners ();
			changeButtons [i].onClick.AddListener (() => UIManager.instance.EditMonster (mc.monsterData));

			dataButtons [i].onClick.RemoveAllListeners ();
			dataButtons [i].onClick.AddListener (() => ShowDetailDialog (mc.monsterData));

			nameTexts [i].text = mon.thaiName;

			//Set element
			if (mon.element == "fire") {
				elements [i].sprite = fire;
			} else if (mon.element == "water") {
				elements [i].sprite = water;
			} else if (mon.element == "plant") {
				elements [i].sprite = plant;
			} else if (mon.element == "light") {
				elements [i].sprite = light;
			} else if (mon.element == "dark") {
				elements [i].sprite = dark;
			}
			SetSpriteToOriginalSize (elements [i], 100f);

			//Set stars
			for (int j = 0; j < 6; j++) {
				if (j < mon.stars) {
					stars [i].GetChild (j).gameObject.SetActive (true);
				} else {
					stars [i].GetChild (j).gameObject.SetActive (false);
				}
			}
		}
			
		//Set empty team monsters
		for (int i = GameSparksManager.instance.teamData[GameSparksManager.instance.playerData.currentTeam].Count; i < 6; i++) {
			teamThumbnails [i].sprite = emptySprite;
			SetSpriteToOriginalSize (teamThumbnails [i], 80f);
			levelTexts [i].text = "";

			if (i != currentPage) {
				teamThumbnails [i].color = new Color (178f / 255f, 178f / 255f, 178f / 255f);
			} else {
				teamThumbnails [i].color = new Color(1f,1f,1f);
			}

			nameTexts[i].text = "";
			elements [i].sprite = emptyBadge;

			MonsterController mc = teamThumbnails [i].gameObject.GetComponent<MonsterController> ();
			mc.monsterData = new MonsterData ();

			changeButtons [i].onClick.RemoveAllListeners ();
			changeButtons [i].onClick.AddListener (() => UIManager.instance.EditMonster (mc.monsterData));

			dataButtons [i].onClick.RemoveAllListeners ();

			for (int j = 0; j < 6; j++) {
				stars [i].GetChild (j).gameObject.SetActive (false);
			}
		}
			
	}

	void Update(){
		int horizontal = 0;

		#if UNITY_EDITOR

		horizontal = (int) (Input.GetAxisRaw ("Horizontal"));

		#elif UNITY_IOS || UNITY_ANDROID
		if (Input.touchCount > 0) {
			Touch t = Input.touches [0];

			if (t.phase == TouchPhase.Began) {
				touchOrigin = t.position;
			} else if (t.phase == TouchPhase.Ended && touchOrigin.x > -100f) {
				Vector2 touchEnd = t.position;

				float x = touchEnd.x - touchOrigin.x;
				float y = touchEnd.y - touchOrigin.y;

				touchOrigin.x = -100;

				if (Mathf.Abs (x) > Mathf.Abs (y)) {
					horizontal = x > 0 ? -1 : 1;
				}
			}
		}
		#endif


		if (horizontal != 0 && UIManager.instance.currentScene == gameObject) {
			for (int i = 0; i < dots.Length; i++) {
				if (dots [i].color.r == 1f && dots[i].color.g == 1f && dots[i].color.b == 1f) {
					if (canMove == false) {
						break;
					}

					string move = "";

					if (horizontal == 1 && i < dots.Length - 1) {
						dots [i].color = new Color (103f / 255f, 103f / 255f, 103f / 255f);
						dots [i + 1].color = new Color (1f, 1f, 1f);;
						move = "right";
						currentPage += 1;
						canMove = false;
					} else if (horizontal == -1 && i > 0) {
						dots [i].color = new Color (103f / 255f, 103f / 255f, 103f / 255f);
						dots [i - 1].color = new Color (1f, 1f, 1f);;
						move = "left";
						currentPage -= 1;
						canMove = false;
					}

					if (move == "right") {
						foreach (GameObject g in monsterSprites) {
							if (g != null) {
								StartCoroutine (SmoothMovement (g, g.transform.position + new Vector3 (-6f, 0f, 0f), 0.5f));
							}
						}
						foreach (GameObject g in monsterStatPanels) {
							if (g != null) {
								StartCoroutine (SmoothMovement (g, g.GetComponent<RectTransform> ().localPosition + new Vector3 (-900f, 0f, 0f), 0.5f));
							}
						}
					} else if (move == "left") {
						foreach (GameObject g in monsterSprites) {
							if (g != null) {
								StartCoroutine (SmoothMovement (g, g.transform.position + new Vector3 (6f, 0f, 0f), 0.5f));
							}
						}
						foreach (GameObject g in monsterStatPanels) {
							if (g != null) {
								StartCoroutine (SmoothMovement (g, g.GetComponent<RectTransform> ().localPosition + new Vector3 (900f, 0f, 0f), 0.5f));
							}
						}
					}

					//Update thumbnails
					for (int a = 0; a < teamThumbnails.Length; a++) {
						if (a != currentPage) {
							teamThumbnails [a].color = new Color (178f / 255f, 178f / 255f, 178f / 255f);
						} else {
							teamThumbnails [a].color = new Color (1f, 1f, 1f);
						}
					}

					break;
				}
			}
		}
	}

	IEnumerator SmoothMovement(GameObject g,Vector3 end,float time){
		float currentTime = 0f;
		RectTransform t = g.GetComponent<RectTransform> ();
		Vector3 startPos = g.transform.position;

		if (t) {
			startPos = t.localPosition;
		}

		while (currentTime < time) {
			currentTime += Time.deltaTime;
			if (t) {
				t.localPosition = Vector3.Lerp (startPos, end, currentTime / time);
			} else {
				g.transform.position = Vector3.Lerp (startPos, end, currentTime / time);
			}
			yield return null;
		}

		canMove = true;
	}

	public void ShowTeamDetails(){
		int totalHp = 0;
		Dictionary<string,int> damageByElements = new Dictionary<string,int> ();
		damageByElements.Add ("fire", 0);
		damageByElements.Add ("water", 0);
		damageByElements.Add ("plant", 0);
		damageByElements.Add ("light", 0);
		damageByElements.Add ("dark", 0);
		damageByElements.Add ("heart", 0);

		for (int i = 0; i < GameSparksManager.instance.teamData [GameSparksManager.instance.playerData.currentTeam].Count; i++) {
			MonsterData m = GameSparksManager.instance.teamData [GameSparksManager.instance.playerData.currentTeam] [i];
			totalHp += m.hitPoint.Value;
			damageByElements [m.element] += m.attack.Value;
			damageByElements ["heart"] += m.recovery.Value;
		}

		teamDetailPopup.transform.GetChild (1).GetChild (0).GetChild (0).GetComponent<Text> ().text = totalHp.ToString () + " / " + totalHp.ToString ();

		teamDetailPopup.transform.GetChild (3).GetChild (1).GetComponent<Text> ().text = damageByElements ["fire"].ToString ();
		teamDetailPopup.transform.GetChild (4).GetChild (1).GetComponent<Text> ().text = damageByElements ["water"].ToString ();
		teamDetailPopup.transform.GetChild (5).GetChild (1).GetComponent<Text> ().text = damageByElements ["plant"].ToString ();
		teamDetailPopup.transform.GetChild (6).GetChild (1).GetComponent<Text> ().text = damageByElements ["light"].ToString ();
		teamDetailPopup.transform.GetChild (7).GetChild (1).GetComponent<Text> ().text = damageByElements ["dark"].ToString ();
		teamDetailPopup.transform.GetChild (8).GetChild (1).GetComponent<Text> ().text = damageByElements ["heart"].ToString ();

		teamDetailPopup.SetActive (true);
	}

	public void ShowResetTeamDialog(){
		resetTeamConfirmDialog.SetActive (true);
	}

	public void ResetTeam(){
		resetTeamConfirmDialog.SetActive (false);

		if (GameSparksManager.instance.teamData [GameSparksManager.instance.playerData.currentTeam].Count <= 0) {
			return;
		}

		//update the data locally
		foreach (MonsterData m in GameSparksManager.instance.teamData [GameSparksManager.instance.playerData.currentTeam]) {
			m.team [GameSparksManager.instance.playerData.currentTeam] = 0;
			m.order [GameSparksManager.instance.playerData.currentTeam] = 0;
		}

		foreach (MonsterData m in GameSparksManager.instance.monsterData) {
			if (m.team [GameSparksManager.instance.playerData.currentTeam] == 1) {
				m.team [GameSparksManager.instance.playerData.currentTeam] = 0;
				m.order [GameSparksManager.instance.playerData.currentTeam] = 0;
			}
		}

		//clear data locally
		GameSparksManager.instance.teamData [GameSparksManager.instance.playerData.currentTeam].Clear ();	

		//update on the server
		new GameSparks.Api.Requests.LogEventRequest ().SetEventKey ("resetTeam").SetEventAttribute ("team", GameSparksManager.instance.playerData.currentTeam)
		.SetDurable (true).Send ((response) => {
				
				if(!response.HasErrors){
					print("reset team success");
				}else{
					print(response.Errors.JSON);
				}
		});

		UIManager.instance.ResetHome ();
	}

	void ShowDetailDialog(MonsterData m){
		UIManager.instance.monA = m;

		Image img = detailDialog.transform.GetChild (0).GetComponent<Image> ();
		img.sprite = MonsterList.instance.GetThumbnail (m.shortCode);
		SetSpriteToOriginalSize (img, 150f);

		detailDialog.transform.GetChild (1).GetComponent<Text> ().text = m.thaiName;
		detailDialog.transform.GetChild (2).GetComponent<Text> ().text = "Level " + m.level.ToString () + " / " + m.maxLevel.ToString ();

		if (m.element == "fire") {
			detailDialog.transform.GetChild (3).GetComponent<Image> ().sprite = UIManager.instance.fire;
		}else if (m.element == "water") {
			detailDialog.transform.GetChild (3).GetComponent<Image> ().sprite = UIManager.instance.water;
		}else if (m.element == "plant") {
			detailDialog.transform.GetChild (3).GetComponent<Image> ().sprite = UIManager.instance.plant;
		}else if (m.element == "light") {
			detailDialog.transform.GetChild (3).GetComponent<Image> ().sprite = UIManager.instance.light;
		}else if (m.element == "dark") {
			detailDialog.transform.GetChild (3).GetComponent<Image> ().sprite = UIManager.instance.dark;
		}

		Transform stars = detailDialog.transform.GetChild (4);
		for (int i = 0; i < 6; i++) {
			if (i < m.stars) {
				stars.GetChild (i).gameObject.SetActive (true);
				stars.GetChild(i).localPosition = new Vector3 (((float)i - (float)m.stars / 2f + 0.5f) * 50f, 0f, 0f);
			} else {
				stars.GetChild (i).gameObject.SetActive (false);
			}
		}

		//data btn
		detailDialog.transform.GetChild (5).GetComponent<Button> ().onClick.AddListener (() => UIManager.instance.ShowMonsterDetails (m));

		//evolve btn
		detailDialog.transform.GetChild(6).GetComponent<Button>().onClick.AddListener (() => UIManager.instance.ShowEvolve());

		//fuse btn
		detailDialog.transform.GetChild(7).GetComponent<Button>().onClick.AddListener(() => UIManager.instance.ShowFuse());

		//Check if there is evolve form
		detailDialog.transform.GetChild(6).gameObject.SetActive (false);

		for (int i = 0; i < MonsterList.instance.monsterModels.Length; i++) {
			string monsterShortCode = MonsterList.instance.monsterModels [i].shortCode;
			string[] model = monsterShortCode.Split ('.');
			string[] thisMon = m.shortCode.Split ('.');

			if (thisMon [0] == model [0] && Int32.Parse(thisMon [1]) == Int32.Parse(model [1]) - 1) {
				//There is an evolve form
				detailDialog.transform.GetChild(6).gameObject.SetActive (true);
				UIManager.instance.evolveShortCode = monsterShortCode;
			}
		}

		//Set lock icon
		detailDialog.transform.GetChild (8).GetComponent<Button> ().onClick.AddListener (() => ToggleLock (m));

		if (m.isLocked == 1) {
			Image im = detailDialog.transform.GetChild (8).GetComponent<Image> ();
			im.sprite = lockSprite;
			SetSpriteToOriginalSize (im, 70f);
		} else {
			Image im = detailDialog.transform.GetChild (8).GetComponent<Image> ();
			im.sprite = unlockSprite;
			SetSpriteToOriginalSize (im, 70f);
		}

		detailDialog.SetActive (true);
	}

	void ToggleLock(MonsterData m){
		if (m.isLocked == 1) {
			m.isLocked = 0;

			Image img = EventSystem.current.currentSelectedGameObject.GetComponent<Image> ();
			img.sprite = unlockSprite;
			SetSpriteToOriginalSize (img, 70f);
		} else {
			m.isLocked = 1;

			Image img = EventSystem.current.currentSelectedGameObject.GetComponent<Image> ();
			img.sprite = lockSprite;
			SetSpriteToOriginalSize (img, 70f);
		}

		GameSparksManager.instance.UpdateMonster (m.objectId, m.exp.Value, m.skillLevel.Value, m.isLocked);
	}
}
