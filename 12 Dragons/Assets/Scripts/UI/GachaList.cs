﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSparks.Core;
using UnityEngine.UI;

public class GachaList : MonoBehaviour {

	public Transform content;

	public GameObject normalGacha;
	public GameObject fireGacha;
	public GameObject waterGacha;
	public GameObject plantGacha;
	public GameObject lightGacha;
	public GameObject darkGacha;

	void OnEnable(){
		CheckDay ();
	}

	void OnDisable(){
		foreach (Transform t in content) {
			Destroy (t.gameObject);
		}
	}

	void CheckDay(){
		new GameSparks.Api.Requests.LogEventRequest ().SetEventKey ("getDay").Send ((response) => {
			if(!response.HasErrors){
				string currentDay = response.ScriptData.GetString("day");

				GameObject g1 = Instantiate(normalGacha,content);
				g1.GetComponent<Button>().onClick.AddListener(() => UIManager.instance.OpenGacha(""));

				if(currentDay == "monday"){
					GameObject g2 = Instantiate(lightGacha,content);
					g2.GetComponent<Button>().onClick.AddListener(() => UIManager.instance.OpenGacha("light"));
				}else if(currentDay == "wednesday"){
					GameObject g2 = Instantiate(plantGacha,content);
					g2.GetComponent<Button>().onClick.AddListener(() => UIManager.instance.OpenGacha("plant"));
				}else if(currentDay == "friday"){
					GameObject g2 = Instantiate(waterGacha,content);
					g2.GetComponent<Button>().onClick.AddListener(() => UIManager.instance.OpenGacha("water"));
				}else if(currentDay == "saturday"){
					GameObject g2 = Instantiate(darkGacha,content);
					g2.GetComponent<Button>().onClick.AddListener(() => UIManager.instance.OpenGacha("dark"));
				}else if(currentDay == "sunday"){
					GameObject g2 = Instantiate(fireGacha,content);
					g2.GetComponent<Button>().onClick.AddListener(() => UIManager.instance.OpenGacha("fire"));
				}

			}else{
				GameSparksManager.instance.OnError();
			}
		});
	}
}
