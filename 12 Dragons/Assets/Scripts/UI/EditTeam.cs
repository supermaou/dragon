﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class EditTeam : MonoBehaviour {

	public Image[] buttons;
	public Transform[] teams;

	public Sprite normalBtn;
	public Sprite highlightedBtn;

	void OnEnable(){
		SetupEditTeam ();
	}

	void SetSpriteToOriginalSize(Image img, float target){
		float width = img.sprite.rect.width;
		float height = img.sprite.rect.height;

		if (height > width) {
			float multiplier = target / height;

			img.rectTransform.sizeDelta = new Vector2 (width * multiplier, target);
		} else {
			float multiplier = target / width;

			img.rectTransform.sizeDelta = new Vector2 (target, height * multiplier);
		}
	}

	void SetupEditTeam(){
		for (int i = 0; i < GameSparksManager.instance.teamData.Count; i++) {
			for (int j = 0; j < GameSparksManager.instance.teamData [i].Count; j++) {
				Image img = teams [i].GetChild (j).GetComponent<Image> ();
				img.sprite = MonsterList.instance.GetThumbnail (GameSparksManager.instance.teamData [i] [j].shortCode);
				SetSpriteToOriginalSize (img, 90f);
			}
		}

		for (int i = 0; i < buttons.Length; i++) {
			if (i == GameSparksManager.instance.playerData.currentTeam) {
				buttons [i].sprite = highlightedBtn;
				buttons [i].transform.GetChild (0).gameObject.SetActive (false);
				buttons [i].transform.GetChild (1).gameObject.SetActive (true);
			} else {
				buttons [i].sprite = normalBtn;
				buttons [i].transform.GetChild (0).gameObject.SetActive (true);
				buttons [i].transform.GetChild (1).gameObject.SetActive (false);
			}
		}
	}

	public void ChooseTeam(int teamNumber){
		foreach (Image i in buttons) {
			i.sprite = normalBtn;
			i.gameObject.transform.GetChild (0).gameObject.SetActive (true);
			i.gameObject.transform.GetChild (1).gameObject.SetActive (false);
		}

		GameObject g = EventSystem.current.currentSelectedGameObject;
		g.GetComponent<Image> ().sprite = highlightedBtn;
		g.transform.GetChild (0).gameObject.SetActive (false);
		g.transform.GetChild (1).gameObject.SetActive (true);

		GameSparksManager.instance.playerData.currentTeam = teamNumber;
		GameSparksManager.instance.SavePlayerData ();
	}
}
