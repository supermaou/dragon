﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GameSparks.Core;
using System;

public class DailyDungeonsManager : MonoBehaviour {

	public GameObject dungeonPrefab;
	public Transform content;

	public GameObject fullInventoryDialog;
	public GameObject staminaRefillDialog;
	public GameObject messageDialog;

	List<Button> dailyDungeonsButtons = new List<Button>();

	void SetSpriteToOriginalSize(Image img, float target){
		float width = img.sprite.rect.width;
		float height = img.sprite.rect.height;

		if (height > width) {
			float multiplier = target / height;

			img.rectTransform.sizeDelta = new Vector2 (width * multiplier, target);
		} else {
			float multiplier = target / width;

			img.rectTransform.sizeDelta = new Vector2 (target, height * multiplier);
		}
	}

	void OnEnable(){

		StartCoroutine (Init ());
	
	}

	IEnumerator Init(){
		GameSparksManager.instance.LoadDailyDungeons ();

		while (!GameSparksManager.instance.loadDailyDungeons) {
			yield return null;
		}

		ShowDungeons ();
	}

	void ShowDungeons(){
		foreach (Transform t in content) {
			Destroy (t.gameObject);
		}

		dailyDungeonsButtons.Clear ();
			
		for (int i = 0; i < GameSparksManager.instance.dailyDungeons.Count; i++) {

			GameObject g = Instantiate (dungeonPrefab) as GameObject;
			g.transform.SetParent (content, false);

			string boss = GameSparksManager.instance.dailyDungeons[i].enemies[GameSparksManager.instance.dailyDungeons[i].enemies.Count - 1].enemyShortCode;
			Image img = g.transform.GetChild (0).GetComponent<Image> ();
			img.sprite = MonsterList.instance.GetThumbnail (boss);
			SetSpriteToOriginalSize (img, 100f);

			g.transform.GetChild (1).GetComponent<Text> ().text = GameSparksManager.instance.dailyDungeons [i].thaiName;
			g.transform.GetChild (2).GetComponent<Text> ().text = "พลังงานที่ใช้: " + GameSparksManager.instance.dailyDungeons [i].stamina;
			g.transform.GetChild (3).gameObject.SetActive (false);
			g.transform.GetChild (5).gameObject.SetActive (true);

			Button b = g.GetComponent<Button> ();
			dailyDungeonsButtons.Add (b);

			int a = i;

			b.onClick.AddListener (() => StartGame (GameSparksManager.instance.dailyDungeons [a]));

			//Set as inactive if it already completed
			if (GameSparksManager.instance.playerData.dailyDungeonCompleted.Contains (GameSparksManager.instance.dailyDungeons [i].dungeonName)) { //Check if the dungeon is already completed
				dailyDungeonsButtons [i].interactable = false;
				dailyDungeonsButtons [i].transform.GetChild (5).GetComponent<Text> ().text = "ครั้งที่ผ่าน 1 / 1"; 
			}
		}
	}

	void StartGame(DungeonData d){
		if (!GS.Authenticated) {
			GameSparksManager.instance.OnError ();
			return;
		}

		if (GameSparksManager.instance.playerData.stamina < d.stamina) {
			staminaRefillDialog.SetActive (true);
			return;
			//the subtraction will occur in Load enemies data method below
		}

		//check if inventory is full
		if (GameSparksManager.instance.monsterData.Count >= GameSparksManager.instance.playerData.maxInventory.Value) {
			fullInventoryDialog.SetActive (true);
			return;
		}

		if (GameSparksManager.instance.teamData [GameSparksManager.instance.playerData.currentTeam].Count <= 0) {
			//no monster in team
			messageDialog.transform.GetChild(0).GetComponent<Text>().text = "ไม่มีมอนสเตอร์ในทีม";
			messageDialog.SetActive (true);

			return;
		}

		UIManager.instance.unclickableTransparentPanel.SetActive (true);

		PersistentData.instance.gameType = "dungeon";
		PersistentData.instance.ResetData();
		PersistentData.instance.currentDungeon = d;
		StartCoroutine (LoadEnemiesData ());
	}

	IEnumerator LoadEnemiesData(){
		PersistentData.instance.enemiesData.Clear ();
		PersistentData.instance.enemiesLoadCount = 0;

		//extract data from EnemyData class and put it in MonsterData class and also get the data according to its shortCode
		int count = 0;
		for (int i = 0; i < PersistentData.instance.currentDungeon.wave.Count; i++) {
			PersistentData.instance.enemiesData.Add (new List<MonsterData> ());
			for (int j = 0; j < PersistentData.instance.currentDungeon.wave [i]; j++) {
				MonsterData m = new MonsterData ();
				m.shortCode = PersistentData.instance.currentDungeon.enemies [count].enemyShortCode;
				m.cooldown = new SafeInt (PersistentData.instance.currentDungeon.enemies [count].turn);
				m.hitPoint = new SafeInt (PersistentData.instance.currentDungeon.enemies [count].hitPoint);
				m.attack = new SafeInt (PersistentData.instance.currentDungeon.enemies [count].attack);
				m.defense = new SafeInt (PersistentData.instance.currentDungeon.enemies [count].defense);
				m.lootChance = new SafeInt (PersistentData.instance.currentDungeon.enemies [count].lootChance);
				m.enemySkill = PersistentData.instance.currentDungeon.enemies [count].enemySkill;
				GameSparksManager.instance.GetMonsterData (m, "enemy");
				PersistentData.instance.enemiesData [i].Add (m);
				count++;
			}
		}

		while (!PersistentData.instance.loadedMonsterData) {
			yield return null;
		}

		while (PersistentData.instance.enemiesLoadCount < PersistentData.instance.currentDungeon.enemies.Count) {
			yield return null;
		}

		//subtract stamina
		if (GameSparksManager.instance.playerData.stamina >= PersistentData.instance.currentDungeon.stamina) {
			GameSparksManager.instance.playerData.stamina -= PersistentData.instance.currentDungeon.stamina;
			GameSparksManager.instance.SavePlayerData ();
			/*
			if (SaveLoad.userData.stamina < 0) {
				SaveLoad.userData.stamina = 0;
			}
			SaveLoad.Save ();*/

			Initiate.Fade ("gameplay", new Color (0.1f, 0.1f, 0.1f), 1.5f);
		}
	}

	void OnDisable(){

		foreach (Transform t in content) {
			if (t != null) {
				Destroy (t.gameObject);
			}
		}

	}
}
