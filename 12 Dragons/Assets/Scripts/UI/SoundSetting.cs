﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundSetting : MonoBehaviour {

	public Toggle music;
	public Toggle sfx;

	void Awake(){
		if (PlayerPrefs.GetInt ("music", 1) == 1) {
			music.isOn = true;
			SoundManager.instance.musicEnable = true;
		} else {
			music.isOn = false;
			SoundManager.instance.musicEnable = false;
		}

		if (PlayerPrefs.GetInt ("sfx", 1) == 1) {
			sfx.isOn = true;
			SoundManager.instance.sfxEnable = true;
		} else {
			sfx.isOn = false;
			SoundManager.instance.sfxEnable = false;
		}
	}

	public void SetMusic(){
		if (music.isOn) {
			SoundManager.instance.musicEnable = true;
			PlayerPrefs.SetInt ("music", 1);
			SoundManager.instance.PlayMusic (UIManager.instance.backgroundMusic);
		} else {
			SoundManager.instance.musicEnable = false;
			PlayerPrefs.SetInt ("music", 0);
			SoundManager.instance.MuteMusic ();
		}
	}

	public void SetSfx(){
		if (sfx.isOn) {
			SoundManager.instance.sfxEnable = true;
			PlayerPrefs.SetInt ("sfx", 1);
		} else {
			SoundManager.instance.sfxEnable = false;
			PlayerPrefs.SetInt ("sfx", 0);
		}
	}
}
