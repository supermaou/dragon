﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using GameSparks.Core;

public class MonsterBook : MonoBehaviour {

	List<Image> monsterThumbnails = new List<Image>();

	public Text progressText;
	public Transform groupButtonsContent;
	public Text rewardText;

	string currentTab = "etc";

	public Sprite highlightedButton;
	public Sprite normalButton;

	Dictionary<string,string[]> monsterTabGroup = new Dictionary<string,string[]>(){
		{"etc" , new string[]{"flametiger.1","flametiger.2","flametiger.3","flyingfish.1","flyingfish.2","flyingfish.3","leafdino.1","leafdino.2",
			"leafdino.3","salamander.1","salamander.2","salamander.3","turtledragon.1","turtledragon.2","turtledragon.3","monkpanda.1",
			"monkpanda.2","monkpanda.3"}},
		{"boar", new string[]{"waterboar.1","waterboar.2","waterboar.3","fireboar.1","fireboar.2","fireboar.3","woodboar.1","woodboar.2","woodboar.3",
			"lightboar.1","lightboar.2","lightboar.3","darkboar.1","darkboar.2","darkboar.3"}},
		{"sten", new string[]{"watersten.1","watersten.2","watersten.3","firesten.1","firesten.2","firesten.3","woodsten.1","woodsten.2","woodsten.3",
			"lightsten.1","lightsten.2","lightsten.3","darksten.1","darksten.2","darksten.3"}},
		{"shank", new string[]{"watershank.1","watershank.2","watershank.3","fireshank.1","fireshank.2","fireshank.3","woodshank.1","woodshank.2","woodshank.3"}},
		{"ninja", new string[]{"waterninja.1","waterninja.2","waterninja.3","fireninja.1","fireninja.2","fireninja.3","woodninja.1","woodninja.2","woodninja.3",
			"lightninja.1","lightninja.2","lightninja.3","darkninja.1","darkninja.2","darkninja.3"}},
		{"golem", new string[]{"watergolem.1","watergolem.2","watergolem.3","firegolem.1","firegolem.2","firegolem.3","woodgolem.1","woodgolem.2","woodgolem.3"}},
		{"fairy", new string[]{"lightfairy.1","lightfairy.2","lightfairy.3","darkfairy.1","darkfairy.2","darkfairy.3"}},
		{"elemental", new string[]{"waterelemental.1","waterelemental.2","waterelemental.3","fireelemental.1","fireelemental.2","fireelemental.3",
			"woodelemental.1","woodelemental.2","woodelemental.3","lightelemental.1","lightelemental.2","lightelemental.3","darkelemental.1","darkelemental.2",
			"darkelemental.3"}},
		{"7knights", new string[]{"morgan.1","morgan.2","morgan.3","gawain.1","gawain.2","gawain.3","vivian.1","vivian.2","vivian.3",
			"guinevere.1","guinevere.2","guinevere.3","igraine.1","igraine.2","igraine.3","ganieda.1","ganieda.2","ganieda.3",
			"ragnel.1","ragnel.2","ragnel.3"}},
		{"5emperors", new string[]{"kingarthur.1","kingarthur.2","kingarthur.3","lancelot.1","lancelot.2","lancelot.3","merlin.1","merlin.2","merlin.3",
			"galahad.1","galahad.2","galahad.3","mordred.1","mordred.2","mordred.3",}},
		{"kentauros", new string[]{"waterkentauros.1","waterkentauros.2","waterkentauros.3","firekentauros.1","firekentauros.2","firekentauros.3",
			"woodkentauros.1","woodkentauros.2","woodkentauros.3","lightkentauros.1","lightkentauros.2","lightkentauros.3",
			"darkkentauros.1","darkkentauros.2","darkkentauros.3",}},
		{"magicians", new string[]{"watermagician.1","watermagician.2","watermagician.3","firemagician.1","firemagician.2","firemagician.3",
			"woodmagician.1","woodmagician.2","woodmagician.3","lightmagician.1","lightmagician.2","lightmagician.3",
			"darkmagician.1","darkmagician.2","darkmagician.3"}}
	};

	Dictionary<string,int> rewards = new Dictionary<string,int>(){
		{"etc",30},
		{"boar",20},
		{"sten",20},
		{"shank",20},
		{"ninja",50},
		{"golem",20},
		{"fairy",50},
		{"elemental",50},
		{"7knights",80},
		{"5emperors",80},
		{"kentauros",50},
		{"magicians",30}
	};

	private List<string> monsterFoundedList = new List<string>();

	void Awake(){
		rewardText.text = rewards [currentTab].ToString ();

		if (GameSparksManager.instance.achievementList.Contains (currentTab + "_achievement")) {
			progressText.text = "รับรางวัลแล้ว";
		}

		//Spawn the monster thumbnails
		for (int i = 0; i < monsterTabGroup [currentTab].Length; i++) {
			GameObject g = new GameObject ();
			g.transform.SetParent (transform.GetChild (0), false);

			Image im = g.AddComponent<Image> ();
			im.raycastTarget = false;

			g.AddComponent<Mask> ().showMaskGraphic = false;

			GameObject mon = new GameObject ();
			mon.transform.SetParent (g.transform, false);
			mon.transform.localPosition = new Vector3 (0f, 0f, 0f);

			Image img = mon.AddComponent<Image> ();
			img.sprite = MonsterList.instance.GetThumbnail (monsterTabGroup [currentTab] [i]);
			SetSpriteToOriginalSize (img, 120f);

			img.color = Color.black;

			monsterThumbnails.Add (img);
		}

		GetMonsterFoundedList ();

	}

	public void SwitchTab(string tab){

		//set buttons
		foreach (Transform t in groupButtonsContent) {
			t.gameObject.GetComponent<Image> ().sprite = normalButton;
			t.GetChild (0).GetComponent<Text> ().color = Color.white;
		}
		EventSystem.current.currentSelectedGameObject.GetComponent<Image> ().sprite = highlightedButton;
		EventSystem.current.currentSelectedGameObject.transform.GetChild (0).GetComponent<Text> ().color = new Color (50f / 255f, 50f / 255f, 50f / 255f);

		monsterThumbnails.Clear ();

		//Destroy old data
		foreach (Transform t in transform.GetChild(0)) {
			Destroy (t.gameObject);
		}

		currentTab = tab;
		rewardText.text = rewards [currentTab].ToString ();

		if (GameSparksManager.instance.achievementList.Contains (currentTab + "_achievement")) {
			progressText.text = "รับรางวัลแล้ว";
		}

		//Spawn the monster thumbnails
		for (int i = 0; i < monsterTabGroup [currentTab].Length; i++) {
			GameObject g = new GameObject ();
			g.transform.SetParent (transform.GetChild (0), false);

			Image im = g.AddComponent<Image> ();
			im.raycastTarget = false;

			g.AddComponent<Mask> ().showMaskGraphic = false;

			GameObject mon = new GameObject ();
			mon.transform.SetParent (g.transform, false);
			mon.transform.localPosition = new Vector3 (0f, 0f, 0f);

			Image img = mon.AddComponent<Image> ();
			img.sprite = MonsterList.instance.GetThumbnail (monsterTabGroup [currentTab] [i]);
			SetSpriteToOriginalSize (img, 120f);

			img.color = Color.black;

			monsterThumbnails.Add (img);
		}

		//set alpha to 1 for the monster that player have founded
		for (int i = 0; i < monsterFoundedList.Count; i++) {
			for (int j = 0; j < monsterTabGroup [currentTab].Length; j++) {
				if (monsterFoundedList[i] == monsterTabGroup [currentTab] [j]) {
					monsterThumbnails [j].color = Color.white;
					break;
				}
			}
		}

		int count = 0;
		for (int i = 0; i < monsterTabGroup[currentTab].Length; i++) {
			if (monsterThumbnails [i].color == Color.white) {
				count++;
			}
		}

		if (count < monsterTabGroup [currentTab].Length) {
			progressText.text = count.ToString () + "/" + monsterTabGroup [currentTab].Length;
		} else {
			progressText.text = "รับรางวัล";
		}
	}

	void GetMonsterFoundedList(){
		new GameSparks.Api.Requests.LogEventRequest ().SetEventKey ("getMonsterFounded").Send ((response) => {
			if(!response.HasErrors){
				monsterFoundedList = response.ScriptData.GetStringList("list");

				//set alpha to 1 for the monster that player have founded
				for (int i = 0; i < monsterFoundedList.Count; i++) {
					for (int j = 0; j < monsterTabGroup [currentTab].Length; j++) {
						if (monsterFoundedList[i] == monsterTabGroup [currentTab] [j]) {
							monsterThumbnails [j].color = Color.white;
							break;
						}
					}
				}

				int count = 0;
				for (int i = 0; i < monsterTabGroup[currentTab].Length; i++) {
					if (monsterThumbnails [i].color == Color.white) {
						count++;
					}
				}

				progressText.text = count.ToString () + "/" + monsterTabGroup [currentTab].Length;
			}else{
				print(response.Errors.JSON);
				GameSparksManager.instance.OnError();
			}
		});
	}

	public void GetReward(){
		int count = 0;
		for (int i = 0; i < monsterTabGroup[currentTab].Length; i++) {
			if (monsterThumbnails [i].color == Color.white) {
				count++;
			}
		}

		if(count >= monsterTabGroup[currentTab].Length){
			//quest complete
			//get reward

			string command = currentTab + "_achievement";

			if (GameSparksManager.instance.achievementList.Contains (command)) {
				return;
			}

			new GameSparks.Api.Requests.LogEventRequest ().SetEventKey ("Award_Achievement").SetEventAttribute ("shortCode", command).Send ((response) => {
				if(!response.HasErrors){
					progressText.text = "รับรางวัลแล้ว";

					if (!GameSparksManager.instance.achievementList.Contains (command)) {
						GameSparksManager.instance.achievementList.Add(command);
						GameSparksManager.instance.playerData.gems += rewards[command.Split('_')[0]];
						AnalyticsManager.instance.ReceiveGems("Monster Book",rewards[command.Split('_')[0]]);
						GameSparksManager.instance.SavePlayerData();
					}
				}else{
					print("Add achievement error");
					GameSparksManager.instance.OnError();
				}
			});
		}
	}

	void SetSpriteToOriginalSize(Image img, float target){
		float width = img.sprite.rect.width;
		float height = img.sprite.rect.height;

		if (height > width) {
			float multiplier = target / height;

			img.rectTransform.sizeDelta = new Vector2 (width * multiplier, target);
		} else {
			float multiplier = target / width;

			img.rectTransform.sizeDelta = new Vector2 (target, height * multiplier);
		}
	}
}
