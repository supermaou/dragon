﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSparks.Core;
using UnityEngine.UI;

public class LeaderboardItem : MonoBehaviour {

	public List<string> team;
	public List<int> levels;
	public List<int> skillLevels;

	public void LoadTeamData(string userId){
		new GameSparks.Api.Requests.LogEventRequest ().SetEventKey ("getOtherTeamData").SetEventAttribute ("userID", userId).Send ((response) => {
			if (!response.HasErrors) {
				team = response.ScriptData.GetGSData("data").GetStringList("playerTeam");
				levels = response.ScriptData.GetGSData("data").GetIntList("levels");
				skillLevels = response.ScriptData.GetGSData("data").GetIntList("skillLevels");

				Image img = gameObject.transform.GetChild(1).GetComponent<Image>();
				img.sprite = MonsterList.instance.GetThumbnail(team[0]);
				SetSpriteToOriginalSize(img,125f);
			}else{
				GameSparksManager.instance.OnError();
			}
		});
	}

	void SetSpriteToOriginalSize(Image img, float target){
		float width = img.sprite.rect.width;
		float height = img.sprite.rect.height;

		if (height > width) {
			float multiplier = target / height;

			img.rectTransform.sizeDelta = new Vector2 (width * multiplier, target);
		} else {
			float multiplier = target / width;

			img.rectTransform.sizeDelta = new Vector2 (target, height * multiplier);
		}
	}
}
