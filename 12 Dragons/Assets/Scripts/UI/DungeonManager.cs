﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using GameSparks.Core;

public class DungeonManager : MonoBehaviour {

	public GameObject dungeonPrefab;
	public Transform content;

	public GameObject fullInventoryDialog;
	public GameObject staminaRefillDialog;

	public GameObject messageDialog;

	void SetSpriteToOriginalSize(Image img, float target){
		float width = img.sprite.rect.width;
		float height = img.sprite.rect.height;

		if (height > width) {
			float multiplier = target / height;

			img.rectTransform.sizeDelta = new Vector2 (width * multiplier, target);
		} else {
			float multiplier = target / width;

			img.rectTransform.sizeDelta = new Vector2 (target, height * multiplier);
		}
	}

	void OnEnable(){
		if (UIManager.instance.resetDungeons == 1 && gameObject.name == "NormalDungeons") {
			UIManager.instance.resetDungeons = 0;
			ShowDungeons ();
		}

		//StartCoroutine (TestValidity ());
	}

	void ShowDungeons(){
		foreach (Transform t in content) {
			t.gameObject.SetActive (false);
		}

		for (int i = GameSparksManager.instance.playerData.dungeon.Value; i >= 0; i--) { //loop through dungeon data in GameSparks
			if (i >= GameSparksManager.instance.dungeonData.Count) {
				continue;
			}

			GameObject g = Instantiate (dungeonPrefab) as GameObject;
			g.transform.SetParent (content, false);

			//set boss thumbnail
			string boss = GameSparksManager.instance.dungeonData[i].bossShortCode;
			Image img = g.transform.GetChild (0).GetComponent<Image> ();
			img.sprite = MonsterList.instance.GetThumbnail (boss);
			SetSpriteToOriginalSize (img, 100f);

			g.transform.GetChild (1).GetComponent<Text> ().text = GameSparksManager.instance.dungeonData [i].thaiName;
			g.transform.GetChild (2).GetComponent<Text> ().text = "พลังงานที่ใช้: " + GameSparksManager.instance.dungeonData [i].stamina;

			if (GameSparksManager.instance.dungeonData[i].stars == 0) { //In case of new dungeons
				GameObject badge = g.transform.GetChild (3).gameObject;
				badge.SetActive (true);

				g.transform.GetChild (4).gameObject.SetActive (false);
			} else { //In case of old dungeons
				g.transform.GetChild (3).gameObject.SetActive (false);
				g.transform.GetChild (4).gameObject.SetActive (true);

				//check if there is any star
				for (int x = 0; x < 3; x++) {
					if (x < GameSparksManager.instance.dungeonData [i].stars) {
						g.transform.GetChild (4).GetChild (x).GetComponent<Image> ().color = Color.white; //new Color (1f, 174f / 255f, 0f);
					} else {
						g.transform.GetChild (4).GetChild (x).GetComponent<Image> ().color = new Color (0.1f, 0.1f, 0.1f);
					}
				}
			}

			Button b = g.GetComponent<Button> ();

			int a = i;
			b.onClick.AddListener (() => StartGame (GameSparksManager.instance.dungeonData [a]));
		}
	}

	void StartGame(DungeonData d){
		
		if (!GS.Authenticated) {
			GameSparksManager.instance.OnError ();
			return;
		}

		if (GameSparksManager.instance.playerData.stamina < d.stamina) {
			staminaRefillDialog.SetActive (true);
			return;
			//the subtraction will occur in Load enemies data method below
		}

		if (GameSparksManager.instance.monsterData.Count >= GameSparksManager.instance.playerData.maxInventory.Value) {
			fullInventoryDialog.SetActive (true);
			return;
		}

		if (GameSparksManager.instance.teamData [GameSparksManager.instance.playerData.currentTeam].Count <= 0) {
			//no monster in team
			messageDialog.transform.GetChild(0).GetComponent<Text>().text = "ไม่มีมอนสเตอร์ในทีม";
			messageDialog.SetActive (true);

			return;
		}
			
		UIManager.instance.unclickableTransparentPanel.SetActive (true);

		new GameSparks.Api.Requests.LogEventRequest ().SetEventKey ("GetDungeonData").SetEventAttribute("name", d.dungeonName).Send ((response) => {
			if(!response.HasErrors){
				GSData data = response.ScriptData.GetGSData("dungeonData");

				d.background = data.GetInt("background") ?? 0;
				d.music = data.GetInt("music") ?? 0;
				d.dropRateNormal = data.GetInt("dropRateNormal") ?? 50;
				d.dropRateBoss = data.GetInt("dropRateBoss") ?? 0;
				//d.element = data.GetString("element");

				int gold = data.GetInt("gold") ?? 0;
				int maxGold = Mathf.RoundToInt(gold * 1.2f);
				int minGold = Mathf.RoundToInt(gold * 0.8f);

				d.gold = Random.Range(minGold,maxGold);
				d.exp = data.GetInt("exp") ?? 0;
				d.wave = data.GetIntList("wave");

				List<GSData> enemyList = data.GetGSDataList("enemies");

				d.enemies.Clear();

				for(int j=0;j<enemyList.Count;j++){
					EnemyData e = new EnemyData();
					e.enemyShortCode = enemyList[j].GetString("name");
					e.turn = enemyList[j].GetInt("turn").Value;
					e.hitPoint = enemyList[j].GetInt("hitPoint").Value;
					e.attack = enemyList[j].GetInt("attack").Value;
					e.defense = enemyList[j].GetInt("defense").Value;
					e.lootChance = enemyList[j].GetInt("lootChance") ?? 0;
					e.enemySkill = enemyList[j].GetGSData("enemySkill");
					d.enemies.Add(e);
				}

				//Set dungeon in Persistence data
				PersistentData.instance.gameType = "dungeon";
				PersistentData.instance.ResetData();
				PersistentData.instance.currentDungeon = d;

				StartCoroutine (LoadEnemiesData ()); //cache enemies data to Persistence data gameobject;

			}else{
				GameSparksManager.instance.OnError();
			}
		});
	}

	IEnumerator LoadEnemiesData(){

		PersistentData.instance.enemiesData.Clear ();
		PersistentData.instance.enemiesLoadCount = 0;

		//extract data from EnemyData class and put it in MonsterData class and also get the data according to its shortCode
		int count = 0;
		for (int i = 0; i < PersistentData.instance.currentDungeon.wave.Count; i++) {
			PersistentData.instance.enemiesData.Add (new List<MonsterData> ());
			for (int j = 0; j < PersistentData.instance.currentDungeon.wave [i]; j++) {
				MonsterData m = new MonsterData ();
				m.shortCode = PersistentData.instance.currentDungeon.enemies [count].enemyShortCode;
				m.cooldown = new SafeInt (PersistentData.instance.currentDungeon.enemies [count].turn);
				m.hitPoint = new SafeInt(PersistentData.instance.currentDungeon.enemies [count].hitPoint);
				m.attack= new SafeInt(PersistentData.instance.currentDungeon.enemies [count].attack);
				m.defense= new SafeInt(PersistentData.instance.currentDungeon.enemies [count].defense);
				m.lootChance = new SafeInt(PersistentData.instance.currentDungeon.enemies [count].lootChance);
				m.enemySkill = PersistentData.instance.currentDungeon.enemies [count].enemySkill;
				GameSparksManager.instance.GetMonsterData (m, "enemy");
				PersistentData.instance.enemiesData [i].Add (m);
				count++;
			}
		}
			
		while (!PersistentData.instance.loadedMonsterData) {
			yield return null;
		}
			
		while (PersistentData.instance.enemiesLoadCount < PersistentData.instance.currentDungeon.enemies.Count) {
			yield return null;
		}
				
		//subtract stamina
		if (GameSparksManager.instance.playerData.stamina >= PersistentData.instance.currentDungeon.stamina) {
			GameSparksManager.instance.playerData.stamina -= PersistentData.instance.currentDungeon.stamina;
			GameSparksManager.instance.SavePlayerData ();
			/*
			if (SaveLoad.userData.stamina < 0) {
				SaveLoad.userData.stamina = 0;
			}
			SaveLoad.Save ();*/

			Initiate.Fade ("gameplay", new Color (0.1f, 0.1f, 0.1f), 1.5f);
		}
	}

	public IEnumerator TestValidity(){

		print ("start checking");

		for (int i = 0; i < GameSparksManager.instance.dungeonData.Count; i++) {
			//print ("progrss: " + (((float)i / (float)GameSparksManager.instance.dungeonData.Count) * 100).ToString () + "%");
			bool isCompleteChecking = false;

			new GameSparks.Api.Requests.LogEventRequest ().SetEventKey ("GetDungeonData").SetEventAttribute ("name", GameSparksManager.instance.dungeonData[i].dungeonName).Send ((response) => {
				if (!response.HasErrors) {
					GSData data = response.ScriptData.GetGSData("dungeonData");

					List<GSData> enemyList = data.GetGSDataList("enemies");

					for(int j=0;j<enemyList.Count;j++){
						EnemyData e = new EnemyData();
						e.enemyShortCode = enemyList[j].GetString("name");
						if(!MonsterList.instance.IsShortCodeValid(e.enemyShortCode)){
							print("Invalid enemy shortcode at " + GameSparksManager.instance.dungeonData[i].dungeonName + " at position " + j.ToString());
						}
					}

					List<int> wave = data.GetIntList("wave");
					int allCount = 0;
					for(int j=0;j<wave.Count;j++){
						allCount += wave[j];
					}

					if(enemyList.Count != allCount){
						print("Enemy count is not associate with wave data at " + GameSparksManager.instance.dungeonData[i].dungeonName);
					}

					isCompleteChecking = true;

				}else{
					print("test error");
				}
			});

			while (!isCompleteChecking) {
				yield return null;
			}
		}

		print ("complete checking");
	}

}
