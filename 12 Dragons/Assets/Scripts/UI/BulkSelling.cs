﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BulkSelling : MonoBehaviour {

	public GameObject framePrefab;
	public GameObject starPrefab;
	public GameObject levelText;

	public Transform content;

	List<MonsterController> monsters2Sell = new List<MonsterController>();

	public Text amountText;
	public Text moneyGotText;

	public Text sellMessage;
	public GameObject confirmDialog;
	public GameObject warningDialog;

	private SafeInt totalMoney2Get;

	private Color inactiveColor = new Color(150f/255f,150f/255f,150f/255f);

	void SetSpriteToOriginalSize(Image img, float target){
		float width = img.sprite.rect.width;
		float height = img.sprite.rect.height;

		if (height > width) {
			float multiplier = target / height;

			img.rectTransform.sizeDelta = new Vector2 (width * multiplier, target);
		} else {
			float multiplier = target / width;

			img.rectTransform.sizeDelta = new Vector2 (target, height * multiplier);
		}
	}

	void OnEnable(){
		monsters2Sell.Clear ();
		totalMoney2Get.Value = 0;

		//Set texts
		amountText.text = "มอนสเตอร์ที่ชาย: " + monsters2Sell.Count.ToString() + " / 20";
		moneyGotText.text = "เงินที่ได้รับ: " + totalMoney2Get.ToString ();

		ShowInventory ();
	}

	void OnDisable(){
		foreach (Transform t in content) {
			Destroy (t.gameObject);
		}
	}

	void ShowInventory(){
		foreach (Transform t in content) {
			Destroy (t.gameObject);
		}

		for (int i = 0; i < GameSparksManager.instance.monsterData.Count; i++) {
			if (GameSparksManager.instance.monsterData [i].isLocked == 1) {
				continue;
			}

			GameObject g = new GameObject ();
			g.transform.SetParent (content, false);

			Image im = g.AddComponent<Image> ();
			im.raycastTarget = false;

			g.AddComponent<Mask> ().showMaskGraphic = false;

			GameObject mon = new GameObject ();
			mon.transform.SetParent (g.transform, false);
			mon.transform.localPosition = new Vector3 (0f, 0f, 0f);

			Image img = mon.AddComponent<Image> ();
			img.sprite = MonsterList.instance.GetThumbnail (GameSparksManager.instance.monsterData [i].shortCode);
			SetSpriteToOriginalSize (img, 120f);

			//Instantiate frmae prefab
			GameObject frame = Instantiate(framePrefab) as GameObject;
			frame.transform.SetParent (g.transform, false);
			frame.transform.localPosition = new Vector3 (0f, 0f, 0f);
			frame.GetComponent<Image> ().raycastTarget = false;
			frame.SetActive (false);

			//Clone data
			MonsterController m = mon.AddComponent<MonsterController> ();
			m.CloneMonsterData (GameSparksManager.instance.monsterData [i]);

			LongPress lp = mon.AddComponent<LongPress> ();
			lp.onClick.AddListener (() => AddMonster2Sell (g));
			lp.onLongPress.AddListener (() => UIManager.instance.ShowMonsterDetails (m.monsterData));
			lp.interactable = true;

			if (!IsNotInAnyTeam (GameSparksManager.instance.monsterData [i])) {
				lp.interactable = false;

				//change color
				lp.gameObject.GetComponent<Image>().color = inactiveColor;
			}

			GameObject levelTxt = Instantiate (levelText, g.transform, false);
			levelTxt.transform.localPosition = new Vector3 (0f, -80f, 0f);
			levelTxt.GetComponent<Text> ().text = "Level " + m.level.ToString ();

			/*
			//instantiate stars
			for (int x = 0; x < m.stars; x++) {
				GameObject s = Instantiate (starPrefab, g.transform, false);
				s.transform.localPosition = new Vector3 (((float)x - (float)m.stars / 2f + 0.5f) * 20f, -80f, 0f);
			}*/

		}
	}

	void AddMonster2Sell(GameObject g){
		if (monsters2Sell.Count < 20) {
			g.transform.GetChild (1).gameObject.SetActive (true);

			MonsterController m = g.transform.GetChild(0).GetComponent<MonsterController> ();
			monsters2Sell.Add (m);

			LongPress lp = g.transform.GetChild(0).GetComponent<LongPress> ();
			lp.onClick.RemoveAllListeners ();
			lp.onClick.AddListener (() => RemoveMonster2Sell (g));

			totalMoney2Get = totalMoney2Get + MoneyGotFromMon (m);

			//Set texts
			amountText.text = "มอนสเตอร์ที่ชาย: " + monsters2Sell.Count.ToString() + " / 20";
			moneyGotText.text = "เงินที่ได้รับ: " + totalMoney2Get.ToString ();
		}
	}

	void RemoveMonster2Sell(GameObject g){
		g.transform.GetChild (1).gameObject.SetActive (false);

		LongPress lp = g.transform.GetChild(0).GetComponent<LongPress> ();
		lp.onClick.RemoveAllListeners ();
		lp.onClick.AddListener (() => AddMonster2Sell (g));

		MonsterController m = g.transform.GetChild(0).GetComponent<MonsterController> ();
		monsters2Sell.Remove (m);

		totalMoney2Get = totalMoney2Get - MoneyGotFromMon (m);

		//Set texts
		amountText.text = "มอนสเตอร์ที่ชาย: " + monsters2Sell.Count.ToString() + " / 20";
		moneyGotText.text = "เงินที่ได้รับ: " + totalMoney2Get.ToString ();
	}

	public void Confirm(){
		bool isContainRare = false;
		for (int i = 0; i < monsters2Sell.Count; i++) {
			if (monsters2Sell [i].stars >= 4) {
				isContainRare = true;
				break;
			}
		}

		if (!isContainRare) {
			sellMessage.text = "ต้องการขายมอนสเตอร์ " + monsters2Sell.Count.ToString () + " ตัว ในราคา " + totalMoney2Get.ToString () + " หรือไม่";
			confirmDialog.SetActive (true);
		} else {
			warningDialog.SetActive (true);
		}
	}

	public void WarningConfirm(){
		warningDialog.SetActive (false);

		sellMessage.text = "ต้องการขายมอนสเตอร์ " + monsters2Sell.Count.ToString () + " ตัว ในราคา " + totalMoney2Get.ToString () + " หรือไม่";
		confirmDialog.SetActive (true);
	}

	int MoneyGotFromMon(MonsterController m){
		if (m.stars == 6) {
			return 8000;
		} else if (m.stars == 5) {
			return 4000;
		} else if (m.stars == 4) {
			return 2000;
		} else if (m.stars == 3) {
			return 1000;
		} else if (m.stars == 2) {
			return 500;
		} else {
			return 250;
		}
	}

	public void Sell(){
		confirmDialog.SetActive (false);

		UIManager.instance.resetInventory = 1;
		UIManager.instance.resetEditMonster = 1;
		UIManager.instance.resetSelectMonster = 1;

		for (int i = 0; i < monsters2Sell.Count; i++) {
			GameSparksManager.instance.monsterData.Remove (monsters2Sell [i].monsterData);
			GameSparksManager.instance.RemoveMonster (monsters2Sell [i].objectId);
		}

		GameSparksManager.instance.playerData.gold += totalMoney2Get.Value;
		GameSparksManager.instance.SavePlayerData ();

		UIManager.instance.UpdateCurrencyTexts ();
		UIManager.instance.ShowInventory ();
	}

	public void ClosePopup(){
		confirmDialog.SetActive (false);
	}

	public bool IsNotInAnyTeam(MonsterData m){
		List<List<MonsterData>> data = GameSparksManager.instance.teamData;

		for (int i = 0; i < 5; i++) {
			for (int j = 0; j < data [i].Count; j++) {
				if (data [i] [j].objectId == m.objectId) {
					return false;
				}
			}
		}

		return true;
	}
}
