﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Popup : MonoBehaviour {

	void OnEnable(){
		if (UIManager.instance != null) {
			UIManager.instance.transparentPanel.SetActive (true);
			Button b = UIManager.instance.transparentPanel.GetComponent<Button> ();
			b.onClick.RemoveAllListeners ();
			b.onClick.AddListener (() => CloseSelf ());
		}
		StartCoroutine (Scaling ());
	}

	void OnDisable(){
		if (UIManager.instance != null) {
			UIManager.instance.transparentPanel.SetActive (false);
		}
	}

	IEnumerator Scaling(){
		float start = 0.3f;
		float end = 1f;
		float time = 0.1f;
		float ti = 0f;
		while(ti<time){
			ti += Time.deltaTime;
			float scale = Mathf.Lerp (start, end, ti / time);
			transform.localScale = new Vector3 (scale, scale, 1f);
			yield return null;
		}
	}

	public void CloseSelf(){
		gameObject.SetActive (false);
	}
}
