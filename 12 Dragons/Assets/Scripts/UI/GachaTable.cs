﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSparks.Core;
using UnityEngine.UI;
using System;
using UnityEngine.EventSystems;

public class GachaTable : MonoBehaviour {

	public GameObject fullInventoryDialog; //dialog when full inventory reach
	public GameObject notEnoughGemsDialog; //dialog when not enough gems
	public GameObject messageDialog;
	public GameObject confirmPurchaseDialog; //confirm purchase dialog

	public Transform table;
	public Text timeLeftText;
	public GameObject resetButton;

	public Sprite plate;

	public Image featureMon;

	private GameObject selectedTile;
	private bool isOpening = false;

	private int cost;

	void SetSpriteToOriginalSize(Image img, float target){
		float width = img.sprite.rect.width;
		float height = img.sprite.rect.height;

		if (height > width) {
			float multiplier = target / height;

			img.rectTransform.sizeDelta = new Vector2 (width * multiplier, target);
		} else {
			float multiplier = target / width;

			img.rectTransform.sizeDelta = new Vector2 (target, height * multiplier);
		}
	}

	public void LoadGachaTable(){
		new GameSparks.Api.Requests.LogEventRequest ().SetEventKey ("getGacha").Send ((response) => {
			if (!response.HasErrors) {
				SaveLoad.userData.gachaList = response.ScriptData.GetStringList("gacha");

				SaveLoad.userData.featureMon = SaveLoad.userData.gachaList[0];

				featureMon.sprite = MonsterList.instance.GetThumbnail(SaveLoad.userData.featureMon);
				SetSpriteToOriginalSize(featureMon,120f);

				foreach (Transform t in table) {
					GachaTile gt = t.GetComponent<GachaTile> ();
					int row = gt.row;
					int col = gt.col;

					SaveLoad.userData.gachaOpenData[row][col] = "0";

					t.GetComponent<Image> ().sprite = plate;
					t.GetComponent<RectTransform>().sizeDelta = new Vector2(120f,120f);
					t.GetComponent<Button> ().interactable = true;
					gt.SetToOriginalColor ();
				}

				SaveLoad.userData.gachaStartTime = (int) (DateTime.UtcNow - new DateTime (1970, 1, 1)).TotalSeconds;
				SaveLoad.Save ();

				resetButton.SetActive(false);

				InitTable();

			}else{
				GameSparksManager.instance.OnError();
			}
		});
	}

	void InitTable(){
		int timePass = (int) (DateTime.UtcNow - new DateTime (1970, 1, 1)).TotalSeconds - SaveLoad.userData.gachaStartTime;

		int timeLeftDays = Mathf.FloorToInt ((float)(604800 - timePass) / 86400);
		int timeLeftHours = Mathf.FloorToInt (((float)(604800 - timePass) % 86400f) / 3600);

		timeLeftText.text = "เหลือเวลาอีก " + timeLeftDays.ToString () + " วัน " + timeLeftHours.ToString () + " ชั่วโมง";

		SetThumbnails ();

		featureMon.sprite = MonsterList.instance.GetThumbnail(SaveLoad.userData.featureMon);
		SetSpriteToOriginalSize (featureMon, 120f);
	}

	void OnEnable(){
		//time detection
		int timePass = (int) (DateTime.UtcNow - new DateTime (1970, 1, 1)).TotalSeconds - SaveLoad.userData.gachaStartTime;

		if (timePass < 604800) {
			InitTable ();
		} 
		else 
		{
			SaveLoad.userData.gachaList.Clear ();
			SaveLoad.Save ();
			LoadGachaTable ();
		}

		if (SaveLoad.userData.gachaList.Count < 36) {
			resetButton.SetActive (true);
		}

		//opening parameter
		isOpening = false;

		//notification
		if (SaveLoad.userData.inventoryNoti > 0) {
			UIManager.instance.navMenu [1].transform.GetChild (2).gameObject.SetActive (true);
			UIManager.instance.navMenu [1].transform.GetChild (2).GetChild (0).GetComponent<Text> ().text = SaveLoad.userData.inventoryNoti.ToString ();
		}
	}

	void SetThumbnails(){
		foreach (Transform t in table) {
			GachaTile gt = t.GetComponent<GachaTile> ();

			if (SaveLoad.userData.gachaOpenData [gt.row] [gt.col] != "0") {
				Image img = t.GetComponent<Image> ();
				img.sprite = MonsterList.instance.GetThumbnail (SaveLoad.userData.gachaOpenData [gt.row] [gt.col]);
				img.color = Color.white;
				SetSpriteToOriginalSize (img, 120f);
				t.GetComponent<Button> ().interactable = false;
			}
		}
	}

	public void OpenGacha(){

		if (isOpening) {
			return;
		}

		if (GameSparksManager.instance.playerData.dailyGacha.Value > 0) {
			messageDialog.transform.GetChild(0).GetComponent<Text>().text = "เปิดกระดานได้วันละครั้ง";
			messageDialog.SetActive (true);
			return;
		}

		if (GameSparksManager.instance.monsterData.Count >= GameSparksManager.instance.playerData.maxInventory.Value) {
			fullInventoryDialog.SetActive (true);
			return;
		}

		selectedTile = EventSystem.current.currentSelectedGameObject;
		confirmPurchaseDialog.SetActive (true);
		confirmPurchaseDialog.transform.GetChild (0).GetComponent<Text> ().text = "เปิดแผ่นป้ายเวทย์มนต์ 1 ครั้ง\nโดยใช้ 1000 ทอง";
		cost = 1000;
	}

	public void Yes(){
		if (GameSparksManager.instance.playerData.gold >= cost) {
			GameSparksManager.instance.playerData.gold.Value -= cost;
			GameSparksManager.instance.playerData.dailyGacha.Value += 1;
			GameSparksManager.instance.SavePlayerData ();
			UIManager.instance.UpdateCurrencyTexts ();

			AnalyticsManager.instance.SpendGold ("TableGacha", cost);
		} else {
			//not enough gems
			confirmPurchaseDialog.SetActive (false);
			notEnoughGemsDialog.SetActive (true);

			return;
		}

		confirmPurchaseDialog.SetActive (false);
		UIManager.instance.unclickableTransparentPanel.SetActive (true);

		StartCoroutine (GachaAnimate (selectedTile));
	}

	IEnumerator GachaAnimate(GameObject g){
		PersistentData.instance.AddDailyQuestProgress ("gachaTable");

		isOpening = true;

		//Change color
		float time = 1f;
		float t = 0;
		Image img = g.GetComponent<Image>();
		Color start = img.color;
		Color end = Color.white;

		while (t < time) {
			t += Time.deltaTime;
			img.color = Color.Lerp (start, end, t / time);
			yield return null;
		}

		StartCoroutine (UIManager.instance.FadeIn (0.8f));

		//Random monster
		int rand = UnityEngine.Random.Range(0,SaveLoad.userData.gachaList.Count);

		MonsterData m = new MonsterData ();
		m.shortCode = SaveLoad.userData.gachaList [rand];
		//load data from server
		GameSparksManager.instance.GetMonsterData(m);

		while (!PersistentData.instance.loadedMonsterData) {
			yield return null;
		}

		//Set image in board
		g.GetComponent<Image>().sprite = MonsterList.instance.GetThumbnail(SaveLoad.userData.gachaList[rand]);
		g.GetComponent<Button> ().interactable = false; //remove the listener

		//Adding monster for real
		GameSparksManager.instance.AddMonster (SaveLoad.userData.gachaList [rand]);

		//Add quest progress
		PersistentData.instance.AddQuestProgress("gacha");

		//Shop noti
		if (GameSparksManager.instance.playerData.gems >= 10) {
			UIManager.instance.navMenu [2].transform.GetChild (2).gameObject.SetActive (true);
			UIManager.instance.navMenu [2].transform.GetChild (2).GetChild (0).GetComponent<Text> ().text = Mathf.Floor (GameSparksManager.instance.playerData.gems.Value / 10f).ToString();
		} else {
			UIManager.instance.navMenu [2].transform.GetChild (2).gameObject.SetActive (false);
		}

		//set data in SaveLoad
		int row = g.GetComponent<GachaTile>().row;
		int col = g.GetComponent<GachaTile> ().col;

		SaveLoad.userData.gachaOpenData [row] [col] = SaveLoad.userData.gachaList [rand];

		//Remove monster from pool
		if (SaveLoad.userData.featurePool.Contains (SaveLoad.userData.gachaList [rand])) {
			SaveLoad.userData.featurePool.Remove (SaveLoad.userData.gachaList [rand]);
		}
		SaveLoad.userData.gachaList.RemoveAt(rand);

		SaveLoad.Save ();

		resetButton.SetActive (true);

		UIManager.instance.ShowGetMonster (m);
		UIManager.instance.unclickableTransparentPanel.SetActive (false);
	}
}
