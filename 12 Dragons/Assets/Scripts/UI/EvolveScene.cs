﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EvolveScene : MonoBehaviour {

	public GameObject monSprite;
	public Text monName;

	public GameObject evolveObj;
	public Image[] materials;
	public Text levelNeed;
	public Text moneyToUseText;
	public GameObject moneyNotEnoughText;

	private SafeInt moneyToUse;
	private SafeInt levelTarget;

	void SetSpriteToOriginalSize(Image img, float target){
		float width = img.sprite.rect.width;
		float height = img.sprite.rect.height;

		if (height > width) {
			float multiplier = target / height;

			img.rectTransform.sizeDelta = new Vector2 (width * multiplier, target);
		} else {
			float multiplier = target / width;

			img.rectTransform.sizeDelta = new Vector2 (target, height * multiplier);
		}
	}

	void OnEnable(){

		//set texts in scene
		monSprite = Instantiate(MonsterList.instance.GetPrefab(UIManager.instance.monA.shortCode),new Vector3(0f,0.5f,0f),Quaternion.identity) as GameObject;
		monName.text = UIManager.instance.monA.thaiName;

		levelNeed.text = "ต้องการเลเวล " + UIManager.instance.monA.maxLevel.ToString ();
		levelTarget.Value = (int)UIManager.instance.monA.maxLevel;

		//Set sprites for materials
		for (int i = 0; i < 5; i++) {
			if (i < UIManager.instance.monA.evolveMaterials.Count) {
				materials [i].gameObject.SetActive (true);
				materials [i].sprite = MonsterList.instance.GetThumbnail (UIManager.instance.monA.evolveMaterials [i]);
				SetSpriteToOriginalSize (materials [i], 100f);

				//Check if player have materials and set alpha according to it
				if (IsMaterialAvailable (UIManager.instance.monA.evolveMaterials [i])) {
					materials[i].GetComponent<Button>().interactable = true;
				} else {
					materials[i].GetComponent<Button>().interactable = false;
				}
			} else {
				materials [i].gameObject.SetActive (false);
			}
		}

		//string[] thisMon = UIManager.instance.monA.shortCode.Split ('.');

		if (UIManager.instance.monA.stars == 5) {
			moneyToUse.Value = 300000;
		} else if (UIManager.instance.monA.stars == 4) {
			moneyToUse.Value = 100000;
		} else if (UIManager.instance.monA.stars == 3) {
			moneyToUse.Value = 30000;
		} else if (UIManager.instance.monA.stars == 2) {
			moneyToUse.Value = 10000;
		} else if (UIManager.instance.monA.stars == 1) {
			moneyToUse.Value = 3000;
		}

		/*
		if (thisMon[1] == "1") {
			moneyToUse = 100000;
		} else if (thisMon[1] == "2") {
			moneyToUse = 300000;
		}*/

		if (GameSparksManager.instance.playerData.gold < moneyToUse) {
			moneyNotEnoughText.SetActive (true);
		} else {
			moneyNotEnoughText.SetActive (false);
		}

		moneyToUseText.text = moneyToUse.ToString ();
	}

	public void Evolve(){

		if (Application.internetReachability == NetworkReachability.NotReachable) {
			return;
		}

		//Check if player have all materials needed
		int materialHave = 0;
		for (int i = 0; i < UIManager.instance.monA.evolveMaterials.Count; i++) {
			if (IsMaterialAvailable (UIManager.instance.monA.evolveMaterials [i])) {
				materialHave++;
			}
		}

		//if player have materials and enough level
		if (materialHave == UIManager.instance.monA.evolveMaterials.Count && UIManager.instance.monA.level >= levelTarget && GameSparksManager.instance.playerData.gold >= moneyToUse)
		{
			GameSparksManager.instance.playerData.gold -= moneyToUse;
			AnalyticsManager.instance.SpendGold ("evolve", moneyToUse.Value);
			GameSparksManager.instance.SavePlayerData ();
			evolveObj.SetActive (true);
			//monSprite.gameObject.SetActive (false);
			UIManager.instance.ShowEvolveAnimate ();
		}
	}

	void OnDisable(){
		if (monSprite) {
			Destroy (monSprite);
		}
	}
		
	bool IsMaterialAvailable(string shortCode){
		for (int i = 0; i < GameSparksManager.instance.monsterData.Count; i++) {
			if (GameSparksManager.instance.monsterData [i].objectId != UIManager.instance.monA.objectId) {
				if (GameSparksManager.instance.monsterData [i].shortCode == shortCode) {
					return true;
				}
			}
		}

		return false;
	}
}
