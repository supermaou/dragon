﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using GameSparks.Core;

public class GachaManager : MonoBehaviour {

	public Image[] featureMons;

	public GameObject fullInventoryDialog; //dialog when full inventory reach
	public GameObject notEnoughGemsDialog; //dialog when not enough gems

	public GameObject dialog; //dialog to confirm purchase

	public Transform table; //gacha table
	public Text timeLeftText;
	public GameObject resetButton;

	public Sprite plate;
	public Sprite defaultSprite;

	private GameObject selectedTile;
	private bool isOpening = false;
	private bool openAll = false;
	private int cost;

	void SetSpriteToOriginalSize(Image img, float target){
		float width = img.sprite.rect.width;
		float height = img.sprite.rect.height;

		if (height > width) {
			float multiplier = target / height;

			img.rectTransform.sizeDelta = new Vector2 (width * multiplier, target);
		} else {
			float multiplier = target / width;

			img.rectTransform.sizeDelta = new Vector2 (target, height * multiplier);
		}
	}

	void LoadGacha(){
			
		new GameSparks.Api.Requests.LogEventRequest ().SetEventKey ("getGacha").Send ((response) => {
			if(!response.HasErrors){
				SaveLoad.userData.gachaList = response.ScriptData.GetStringList("gacha");

				SaveLoad.userData.featureList.Clear();
				SaveLoad.userData.featurePool.Clear();
				for(int i=0;i<3;i++){
					SaveLoad.userData.featureList.Add(SaveLoad.userData.gachaList[i]);
					SaveLoad.userData.featurePool.Add(SaveLoad.userData.gachaList[i]);
				}

				SetFeatureMons();

				foreach (Transform t in table) {
					GachaTile gt = t.GetComponent<GachaTile> ();
					int row = gt.row;
					int col = gt.col;

					SaveLoad.userData.gachaOpenData[row][col] = "0";

					t.GetComponent<Image> ().sprite = plate;
					t.GetComponent<RectTransform>().sizeDelta = new Vector2(120f,120f);
					t.GetComponent<Button> ().interactable = true;
					gt.SetToOriginalColor ();
				}

				resetButton.SetActive (false);

				SaveLoad.userData.gachaStartTime = (int) (DateTime.UtcNow - new DateTime (1970, 1, 1)).TotalSeconds;
				SaveLoad.Save ();

				//UIManager.instance.ShowGacha();

				print("load gacha complete");
			}else{
				Debug.Log("Error Loading Gacha...");
				GameSparksManager.instance.OnError();
			}
		});
	}

	void SetFeatureMons(){
		//load feature image
		for(int i=0;i<SaveLoad.userData.featureList.Count;i++){
			if (!SaveLoad.userData.featurePool.Contains (SaveLoad.userData.featureList [i])) {
				featureMons [i].sprite = MonsterList.instance.GetThumbnail (SaveLoad.userData.featureList [i]);
				featureMons [i].transform.GetChild (0).gameObject.SetActive (false);
				SetSpriteToOriginalSize (featureMons [i], 120f);
			} else {
				featureMons [i].sprite = defaultSprite;
				featureMons [i].transform.GetChild (0).gameObject.SetActive (true);
				SetSpriteToOriginalSize (featureMons [i], 120f);
			}
		}
	}

	void OnEnable(){

		//time detection
		int timePass = (int) (DateTime.UtcNow - new DateTime (1970, 1, 1)).TotalSeconds - SaveLoad.userData.gachaStartTime;

		if (timePass < 604800) {
			int timeLeftDays = Mathf.FloorToInt ((float)(604800 - timePass) / 86400);
			int timeLeftHours = Mathf.FloorToInt (((float)(604800 - timePass) % 86400f) / 3600);

			timeLeftText.text = "เหลือเวลาอีก " + timeLeftDays.ToString () + " วัน " + timeLeftHours.ToString () + " ชั่วโมงก่อนที่จะถูกรีเซ็ต";

			SetThumbnails ();
			SetFeatureMons ();

			if (CheckIfOpenAllBoardAlready () || SaveLoad.userData.featurePool.Count <= 0) {
				resetButton.SetActive (true);
			}
		} 
		else 
		{
			SaveLoad.userData.featureList.Clear();
			SaveLoad.userData.gachaList.Clear ();
			SaveLoad.Save ();
			LoadGacha ();
		}

		//opening parameter
		isOpening = false;

	}
		
	void SetThumbnails(){
		foreach (Transform t in table) {
			GachaTile gt = t.GetComponent<GachaTile> ();

			if (SaveLoad.userData.gachaOpenData [gt.row] [gt.col] != "0") {
				Image img = t.GetComponent<Image> ();
				img.sprite = MonsterList.instance.GetThumbnail (SaveLoad.userData.gachaOpenData [gt.row] [gt.col]);
				img.color = Color.white;
				SetSpriteToOriginalSize (img, 120f);
				t.GetComponent<Button> ().interactable = false;
			}
		}
	}

	public void Yes(){
		if (GameSparksManager.instance.playerData.gems >= cost) {
			GameSparksManager.instance.playerData.gems -= cost;
			GameSparksManager.instance.SavePlayerData ();
			UIManager.instance.UpdateCurrencyTexts ();

		} else {
			//not enough gems
			dialog.SetActive (false);
			notEnoughGemsDialog.SetActive (true);

			return;
		}

		dialog.SetActive (false);
		UIManager.instance.unclickableTransparentPanel.SetActive (true);

		if (!openAll) {
			StartCoroutine (GachaAnimate (selectedTile));
		} else {
			StartCoroutine (GachaOpenAll ());
		}
	}

	public void No(){
		dialog.SetActive (false);
	}

	public void OpenGacha(){

		if (isOpening) {
			return;
		}

		openAll = false;

		if (GameSparksManager.instance.monsterData.Count >= GameSparksManager.instance.playerData.maxInventory.Value) {
			fullInventoryDialog.SetActive (true);
			return;
		}

		selectedTile = EventSystem.current.currentSelectedGameObject;
		dialog.SetActive (true);
		dialog.transform.GetChild (0).GetComponent<Text> ().text = "เปิดแผ่นป้ายเวทย์มนต์ 1 ครั้ง\nโดยใช้เพชร 10 เพชร";
		cost = 10;

	}
		
	public void OpenAllGachaBtn(){
		if (isOpening) {
			return;
		}

		openAll = true;

		if (GameSparksManager.instance.monsterData.Count >= GameSparksManager.instance.playerData.maxInventory.Value) {
			fullInventoryDialog.SetActive (true);
			return;
		}

		dialog.SetActive (true);

		//count unopen magic board
		int count = 0;
		for (int i = 0; i < 6; i++) {
			for (int j = 0; j < 6; j++) {
				if (SaveLoad.userData.gachaOpenData [i] [j] == "0") {
					count++;
				}
			}
		}

		dialog.transform.GetChild (0).GetComponent<Text> ().text = "เปิดแผ่นป้ายเวทย์มนต์ " + count.ToString() + " ครั้ง\nโดยใช้เพชร " + (count*10).ToString() + " เพชร";
		cost = count * 10;

	}

	bool CheckIfOpenAllBoardAlready(){
		for (int i = 0; i < 6; i++) {
			for (int j = 0; j < 6; j++) {
				if (SaveLoad.userData.gachaOpenData [i] [j] == "0") {
					return false;
				}
			}
		}

		return true;
	}

	IEnumerator GachaAnimate(GameObject g){
		isOpening = true;

		//Change color
		float time = 1f;
		float t = 0;
		Image img = g.GetComponent<Image>();
		Color start = img.color;
		Color end = Color.white;

		while (t < time) {
			t += Time.deltaTime;
			img.color = Color.Lerp (start, end, t / time);
			yield return null;
		}

		if (!openAll) {
			StartCoroutine (UIManager.instance.FadeIn (0.8f));
		}

		//Random monster
		int rand = UnityEngine.Random.Range(0,SaveLoad.userData.gachaList.Count);

		MonsterData m = new MonsterData ();
		m.shortCode = SaveLoad.userData.gachaList [rand];
		//load data from server
		GameSparksManager.instance.GetMonsterData(m);

		while (!PersistentData.instance.loadedMonsterData) {
			yield return null;
		}
			
		//UIManager.instance.monA = m;

		//Set image in board
		g.GetComponent<Image>().sprite = MonsterList.instance.GetThumbnail(SaveLoad.userData.gachaList[rand]);
		g.GetComponent<Button> ().interactable = false; //remove the listener

		//Adding monster for real
		GameSparksManager.instance.AddMonster (SaveLoad.userData.gachaList [rand]);

		//Add quest progress
		PersistentData.instance.AddQuestProgress("gacha");

		//SaveLoad.userData.inventoryNoti++; //Set for notification on nav bar

		//Shop noti
		if (GameSparksManager.instance.playerData.gems >= 10) {
			UIManager.instance.navMenu [2].transform.GetChild (2).gameObject.SetActive (true);
			UIManager.instance.navMenu [2].transform.GetChild (2).GetChild (0).GetComponent<Text> ().text = Mathf.Floor (GameSparksManager.instance.playerData.gems.Value / 10f).ToString();
		} else {
			UIManager.instance.navMenu [2].transform.GetChild (2).gameObject.SetActive (false);
		}

		//set data in SaveLoad
		int row = g.GetComponent<GachaTile>().row;
		int col = g.GetComponent<GachaTile> ().col;

		SaveLoad.userData.gachaOpenData [row] [col] = SaveLoad.userData.gachaList [rand];

		//Remove monster from pool
		if (SaveLoad.userData.featurePool.Contains (SaveLoad.userData.gachaList [rand])) {
			SaveLoad.userData.featurePool.Remove (SaveLoad.userData.gachaList [rand]);
		}
		SaveLoad.userData.gachaList.RemoveAt(rand);

		SaveLoad.Save ();

		if (CheckIfOpenAllBoardAlready () || SaveLoad.userData.featurePool.Count == 0) {
			resetButton.SetActive (true);
		}

		if (!openAll) {
			UIManager.instance.ShowGetMonster (m);
			UIManager.instance.unclickableTransparentPanel.SetActive (false);
		}
	}

	IEnumerator GachaOpenAll(){
		foreach (Transform t in table) {
			Image img = t.gameObject.GetComponent<Image> ();
			if (img.color != Color.white) {
				//still not open
				StartCoroutine(GachaAnimate(t.gameObject));
				yield return new WaitForSeconds (.1f);
			}
		}

		UIManager.instance.unclickableTransparentPanel.SetActive (false);
	}

	void SetAlphaToOne(GameObject parent){
		Transform[] childTransform = parent.GetComponentsInChildren<Transform> ();

		foreach (Transform t in childTransform) {
			Text text = t.gameObject.GetComponent<Text> ();
			Image img = t.gameObject.GetComponent<Image> ();

			if (img) {
				img.color = new Color (img.color.r, img.color.g, img.color.b, 1f);
			} else if (text) {
				text.color = new Color (text.color.r, text.color.g, text.color.b, 1f);
			}
		}
	}
}
