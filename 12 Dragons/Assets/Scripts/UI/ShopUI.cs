﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSparks.Core;
using UnityEngine.UI;
using System;

public class ShopUI : MonoBehaviour {

	public GameObject buyDailyGemsDialog;

	public GameObject messageDialog;
	public Text result;

	public GameObject couponPopup;
	public GameObject referPopup;

	public Text referCodeText;

	public InputField inputText;
	public InputField inputText2;

	public void ShowCouponPopup(){
		couponPopup.SetActive (true);
	}

	public void ShowReferPopup(){
		GetReferCode ();
		referPopup.SetActive (true);
	}

	void GetReferCode(){
		new GameSparks.Api.Requests.LogEventRequest ().SetEventKey ("getReferCode").Send ((response) => {
			if(!response.HasErrors){
				referCodeText.text = "ชวนเพือนใหม่มาเล่น และให้เขากรอกโค้ด " + response.ScriptData.GetString("code") + " สิ ทั้งคุณและเพื่อนจะได้รับ 10 เพชร";
			}else{
				GameSparksManager.instance.OnError();
			}
		});
	}

	public void ClaimCoupon(){
		if (inputText.text != "") {
			string code = inputText.text;
			inputText.text = "";

			couponPopup.SetActive (false);

			new GameSparks.Api.Requests.LogEventRequest ().SetEventKey ("Check_Coupon").SetEventAttribute ("code", code).Send ((response) => {
				if(!response.HasErrors){
					string reward = response.ScriptData.GetString("reward");

					if(reward == null || reward == ""){
						couponPopup.SetActive(false);
						result.text = "โค้ดไม่ถูกต้อง";
						messageDialog.SetActive(true);
					}else{
						couponPopup.SetActive(false);

						GiveReward(reward,"coupon");

					}

				}else{
					GameSparksManager.instance.OnError();
				}
			});
		}
	}

	public void ClaimReferCode(){
		if (inputText2.text != "") {
			string code = inputText2.text;
			inputText2.text = "";

			referPopup.SetActive (false);

			if (GameSparksManager.instance.playerData.referReward.Value != 0) {
				messageDialog.transform.GetChild(0).GetComponent<Text>().text = "คุณเคยใส่โค้ดไปแล้ว";
				messageDialog.SetActive (true);
				return;
			}

			new GameSparks.Api.Requests.LogEventRequest ().SetEventKey ("Check_ReferCode").SetEventAttribute ("code", code).Send ((response) => {
				if(!response.HasErrors){
					string reward = response.ScriptData.GetString("reward");

					referPopup.SetActive(false);
					if(reward == null || reward == ""){
						result.text = "โค้ดไม่ถูกต้อง";
						messageDialog.SetActive(true);
					}else{
						GameSparksManager.instance.playerData.referReward.Value = 1;
						GiveReward(reward,"referral code");
					}

				}else{
					GameSparksManager.instance.OnError();
				}
			});
		}
	}

	void GiveReward(string reward,string tag){
		string[] s = reward.Split ('.');

		if (s [1] == "gold" || s [1] == "coins" || s [1] == "coin") {
			GameSparksManager.instance.playerData.gold += Int32.Parse (s [0]);
			GameSparksManager.instance.SavePlayerData ();
			UIManager.instance.UpdateCurrencyTexts ();

			result.text = "ได้รับ " + s[0] + " ทอง";
		} else if (s [1] == "gems") {
			GameSparksManager.instance.playerData.gems += Int32.Parse (s [0]);
			GameSparksManager.instance.SavePlayerData ();
			UIManager.instance.UpdateCurrencyTexts ();

			AnalyticsManager.instance.ReceiveGems (tag, Int32.Parse (s [0]));

			result.text = "ได้รับ " + s[0] + " เพชร";
		} else { //monster reward
			GameSparksManager.instance.AddMonster (reward);
			GameSparksManager.instance.SavePlayerData ();

			result.text = "ได้รับมอนสเตอร์แล้ว";
		}
			
		messageDialog.SetActive (true);
	}

	public void OpenLine(){
		Application.OpenURL ("https://line.me/R/ti/p/%40vqg9546l");
	}

	public void CloseDialog(){
		inputText.text = "";
		inputText2.text = "";

		couponPopup.SetActive (false);
		referPopup.SetActive (false);
		messageDialog.SetActive (false);
	}


	public void ClaimDailyGems(){
		if (GameSparksManager.instance.playerData.dailyGems.Value > -1) {
			//Already bought this package
			if (GameSparksManager.instance.playerData.isReceived.Value == 0) {
				GameSparksManager.instance.playerData.gems += 5;
				AnalyticsManager.instance.ReceiveGems ("daily gems package", 5);
				GameSparksManager.instance.playerData.dailyGems += 1;

				if (GameSparksManager.instance.playerData.dailyGems >= 15) {
					GameSparksManager.instance.playerData.dailyGems.Value = -1;
				}

				GameSparksManager.instance.playerData.isReceived.Value = 1;

				GameSparksManager.instance.SavePlayerData ();

				result.text = "คุณได้รับ 5 เพชร";
				messageDialog.SetActive (true);

				UIManager.instance.UpdateCurrencyTexts ();
			} else {
				result.text = "สามารถรับได้วันละครั้ง";
				messageDialog.SetActive (true);
			}
		} else {
			buyDailyGemsDialog.SetActive (true);
		}
	}

}
