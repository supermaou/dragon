﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class DailyGift : MonoBehaviour {

	List<string> dailyRewards = new List<string>(){
		"10.gems",
		"10000.gold",
		"fireegg.1",
		"100.energy",
		"5.gems",
		"wateregg.1",
		"10.gems",
		"20000.gold",
		"woodegg.1",
		"50.energy",
		"50000.gold",
		"lightegg.1",
		"5000.gold",
		"15.gems",
		"darkegg.1"
	};

	public Sprite gemIcon;
	public Sprite goldIcon;
	public Sprite spaceIcon;
	public Sprite energyIcon;

	public List<Transform> bars = new List<Transform>();

	void OnEnable(){
		int count = 1;

		foreach (Transform bar in bars) {
			foreach (Transform item in bar) {
				Text day = item.GetChild (0).GetChild (0).GetComponent<Text> ();

				Image icon = item.GetChild (1).GetComponent<Image> ();
				Text amount = item.GetChild (2).GetComponent<Text> ();

				day.text = "วันที่ " + count.ToString ();

				SetReward (dailyRewards [count - 1], icon, amount);

				if (count <= GameSparksManager.instance.playerData.loginCount.Value) {
					item.GetChild (3).gameObject.SetActive (true);
				}

				count++;
			}
		}
	}

	void SetReward(string shortCode, Image img, Text amount){
		string[] s = shortCode.Split ('.');

		if (s [1] == "gems") {
			img.sprite = gemIcon;
			img.transform.localPosition = new Vector3 (0f, 20f, 0f);

			amount.gameObject.SetActive (true);
			amount.text = s [0];

			SetSpriteToOriginalSize (img, 70f);
		} else if (s [1] == "gold") {
			img.sprite = goldIcon;
			img.transform.localPosition = new Vector3 (0f, 20f, 0f);

			amount.gameObject.SetActive (true);
			amount.text = s [0];

			SetSpriteToOriginalSize (img, 70f);
		} else if (s [1] == "spaces") {
			img.sprite = spaceIcon;
			img.transform.localPosition = new Vector3 (0f, 20f, 0f);

			amount.gameObject.SetActive (true);
			amount.text = s [0];

			SetSpriteToOriginalSize (img, 70f);
		} else if (s [1] == "energy") {
			img.sprite = energyIcon;
			img.transform.localPosition = new Vector3 (0f, 20f, 0f);

			amount.gameObject.SetActive (true);
			amount.text = s [0];

			SetSpriteToOriginalSize (img, 70f);
		} else {
			img.sprite = MonsterList.instance.GetThumbnail (shortCode);
			img.transform.localPosition = new Vector3 (0f, 0f, 0f);

			amount.gameObject.SetActive (false);

			SetSpriteToOriginalSize (img, 110f);
		}

	}

	void GetReward(string shortCode){
		string[] s = shortCode.Split ('.');

		if (s [1] == "gems") {
			int amount = Int32.Parse (s [0]);

			GameSparksManager.instance.playerData.gems += amount;
			UIManager.instance.UpdateCurrencyTexts ();

			AnalyticsManager.instance.ReceiveGems ("Daily Reward", amount);

			//GameSparksManager.instance.SavePlayerData (); Save data in CheckIn method

		} else if (s [1] == "gold") {
			int amount = Int32.Parse (s [0]);

			GameSparksManager.instance.playerData.gold += amount;
			UIManager.instance.UpdateCurrencyTexts ();

			//GameSparksManager.instance.SavePlayerData ();

		} else if (s [1] == "spaces") {
			int amount = Int32.Parse (s [0]);

			GameSparksManager.instance.playerData.maxInventory += amount;

			//GameSparksManager.instance.SavePlayerData ();

		} else if (s [1] == "energy") {
			int amount = Int32.Parse (s [0]);

			GameSparksManager.instance.playerData.stamina += amount;
			//GameSparksManager.instance.SavePlayerData ();
			//SaveLoad.Save ();

			UIManager.instance.UpdateStaminaText ();

		} else {
			GameSparksManager.instance.AddMonster (shortCode);
		}
	}

	public void CheckIn(){

		if (GameSparksManager.instance.playerData.dailyLogin.Value == 0) {
			GameSparksManager.instance.playerData.dailyLogin = new SafeInt (1); //check for daily login validity
			PersistentData.instance.AddQuestProgress("login"); //Add quest progress

			GetReward (dailyRewards [GameSparksManager.instance.playerData.loginCount.Value]);
			GameSparksManager.instance.playerData.loginCount += 1;

			StartCoroutine (CheckAnimate (GameSparksManager.instance.playerData.loginCount.Value));

			if (GameSparksManager.instance.playerData.loginCount.Value >= 15) {
				GameSparksManager.instance.playerData.loginCount.Value = 0;
			}

			GameSparksManager.instance.SavePlayerData ();
		}
	}

	IEnumerator CheckAnimate(int day){
		int count = 1;

		foreach (Transform bar in bars) {
			foreach (Transform item in bar) {
				if (count == day) {
					GameObject check = item.transform.GetChild (3).gameObject;
					check.SetActive (true);

					float time = 0.1f;
					float t = 0;
					while (t < time) {
						float scale = Mathf.Lerp (2f, 1f, t / time);
						check.transform.localScale = new Vector3 (scale,scale,1f);

						t += Time.deltaTime;
						yield return null;
					}

					check.transform.localScale = new Vector3 (1f, 1f, 1f);

					yield break;
				}

				count++;
			}
		}
	}

	void SetSpriteToOriginalSize(Image img, float target){
		float width = img.sprite.rect.width;
		float height = img.sprite.rect.height;

		if (height > width) {
			float multiplier = target / height;

			img.rectTransform.sizeDelta = new Vector2 (width * multiplier, target);
		} else {
			float multiplier = target / width;

			img.rectTransform.sizeDelta = new Vector2 (target, height * multiplier);
		}
	}
}
