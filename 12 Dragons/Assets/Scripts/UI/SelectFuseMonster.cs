﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectFuseMonster : MonoBehaviour {

	public GameObject framePrefab;
	public GameObject inuseBadge;
	public GameObject starPrefab;
	public GameObject levelText;

	List<GameObject> monsterList = new List<GameObject>();

	private Color inactiveColor = new Color(150f/255f,150f/255f,150f/255f);

	void OnEnable(){

		ShowInventory ();
	}

	void SetSpriteToOriginalSize(Image img, float target){
		float width = img.sprite.rect.width;
		float height = img.sprite.rect.height;

		if (height > width) {
			float multiplier = target / height;

			img.rectTransform.sizeDelta = new Vector2 (width * multiplier, target);
		} else {
			float multiplier = target / width;

			img.rectTransform.sizeDelta = new Vector2 (target, height * multiplier);
		}
	}

	void OnDisable(){
		foreach (Transform t in transform.GetChild(2).GetChild(0)) {
			Destroy (t.gameObject);
		}
	}

	void ShowInventory(){
		foreach (Transform t in transform.GetChild(2).GetChild(0)) {
			Destroy (t.gameObject);
		}

		for (int i = 0; i < GameSparksManager.instance.monsterData.Count; i++) {
			if (GameSparksManager.instance.monsterData [i].isLocked == 1) {
				continue;
			}

			GameObject g = new GameObject ();
			g.transform.SetParent (transform.GetChild (2).GetChild (0), false);

			Image im = g.AddComponent<Image> ();
			im.raycastTarget = false;

			g.AddComponent<Mask> ().showMaskGraphic = false;

			GameObject mon = new GameObject ();
			mon.transform.SetParent (g.transform, false);
			mon.transform.localPosition = new Vector3 (0f, 0f, 0f);

			Image img = mon.AddComponent<Image> ();
			img.sprite = MonsterList.instance.GetThumbnail (GameSparksManager.instance.monsterData [i].shortCode);
			SetSpriteToOriginalSize (img, 120f);

			//Instantiate frmae prefab
			GameObject frame = Instantiate(framePrefab) as GameObject;
			frame.transform.SetParent (g.transform, false);
			frame.transform.localPosition = new Vector3 (0f, 0f, 0f);
			frame.GetComponent<Image> ().raycastTarget = false;
			frame.SetActive (false);

			//Clone data
			MonsterController m = mon.AddComponent<MonsterController> ();
			m.CloneMonsterData (GameSparksManager.instance.monsterData [i]);

			LongPress lp = mon.AddComponent<LongPress> ();

			lp.onClick.AddListener (() => AddMonster2Fuse (g));
			lp.onLongPress.AddListener (() => UIManager.instance.ShowMonsterDetails (m.monsterData, 2));
			lp.interactable = true;

			GameObject levelTxt = Instantiate (levelText, g.transform, false);
			levelTxt.transform.localPosition = new Vector3 (0f, -80f, 0f);
			levelTxt.GetComponent<Text> ().text = "Level " + m.level.ToString ();

			/*
			//instantiate stars
			for (int x = 0; x < m.stars; x++) {
				GameObject s = Instantiate (starPrefab, g.transform, false);
				s.transform.localPosition = new Vector3 (((float)x - (float)m.stars / 2f + 0.5f) * 20f, -80f, 0f);
			}*/

			if (!IsNotInAnyTeam (GameSparksManager.instance.monsterData [i])) {
				lp.interactable = false;

				//change color
				lp.gameObject.GetComponent<Image>().color = inactiveColor;
			}

			if (UIManager.instance.monA.objectId == m.objectId) {
				lp.interactable = false;

				//change color
				lp.gameObject.GetComponent<Image>().color = inactiveColor;
			}

			monsterList.Add (g);

			//Check if in mons2fuse then set the frame
			foreach (MonsterController mm in UIManager.instance.mons2fuse) {
				if (m.objectId == mm.objectId) {
					frame.gameObject.SetActive (true);
					lp.onClick.RemoveAllListeners ();
					lp.onClick.AddListener (() => RemoveMonster2Fuse (g));
					break;
				}
			}

		}
	}

	void UpdateInventory(){
		foreach (GameObject g in monsterList) {

			MonsterController m = g.transform.GetChild(0).GetComponent<MonsterController> ();

			g.transform.GetChild (1).gameObject.SetActive (false);

			Image frameImg = g.transform.GetChild (1).gameObject.GetComponent<Image> ();
			frameImg.color = new Color (frameImg.color.r, frameImg.color.g, frameImg.color.b, 1f);

			LongPress lp = g.transform.GetChild(0).GetComponent<LongPress> ();
			lp.onClick.RemoveAllListeners ();
			lp.onClick.AddListener (() => AddMonster2Fuse (g));
			lp.interactable = true;

			//Check if in mons2fuse then set the frame
			foreach (MonsterController mm in UIManager.instance.mons2fuse) {
				if (m.objectId == mm.objectId) {
					g.transform.GetChild (1).gameObject.SetActive (true);
					lp.onClick.RemoveAllListeners ();
					lp.onClick.AddListener (() => RemoveMonster2Fuse (g));
					break;
				}
			}

			//Define inactive buttons and remove previous listener

			if (!IsNotInAnyTeam (m.monsterData)) {
				lp.interactable = false;

				//change color
				lp.gameObject.GetComponent<Image>().color = inactiveColor;
			}

			if (UIManager.instance.monA.objectId == m.objectId) {
				lp.interactable = false;

				//change color
				lp.gameObject.GetComponent<Image>().color = inactiveColor;
			}
		}
	}

	public void AddMonster2Fuse(GameObject g){
		if (UIManager.instance.mons2fuse.Count < 5) {
			g.transform.GetChild (1).gameObject.SetActive (true);

			//Add to UIManager
			MonsterController m = g.transform.GetChild(0).GetComponent<MonsterController> ();
			UIManager.instance.mons2fuse.Add (m);

			LongPress lp = g.transform.GetChild(0).GetComponent<LongPress> ();

			lp.onClick.RemoveAllListeners ();
			lp.onClick.AddListener (() => RemoveMonster2Fuse (g));

		}
	}

	public void RemoveMonster2Fuse(GameObject g){

		g.transform.GetChild (1).gameObject.SetActive (false);

		LongPress lp = g.transform.GetChild(0).GetComponent<LongPress> ();
		lp.onClick.RemoveAllListeners ();
		lp.onClick.AddListener (() => AddMonster2Fuse (g));
	
		//Remove from UIManager
		MonsterController m = g.transform.GetChild(0).GetComponent<MonsterController>();
		for (int i = 0; i < UIManager.instance.mons2fuse.Count; i++) {
			if (UIManager.instance.mons2fuse [i].objectId == m.objectId) {
				UIManager.instance.mons2fuse.RemoveAt (i);
				break;
			}
		}
			
	}

	public void Confirm(){
		UIManager.instance.ShowFuse ();
	}

	public bool IsNotInAnyTeam(MonsterData m){
		List<List<MonsterData>> data = GameSparksManager.instance.teamData;

		for (int i = 0; i < 5; i++) {
			for (int j = 0; j < data [i].Count; j++) {
				if (data [i] [j].objectId == m.objectId) {
					return false;
				}
			}
		}

		return true;
	}
}
