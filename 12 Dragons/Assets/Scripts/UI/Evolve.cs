﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public class Evolve : MonoBehaviour {

	private GameObject mon;
	public GameObject congratText;
	public Image[] materials;

	private int count = 0;

	void OnEnable(){
		count = 0;
		mon = Instantiate (MonsterList.instance.GetPrefab (UIManager.instance.monA.shortCode), new Vector3 (0f, 0f, 0f), Quaternion.identity);

		//StartCoroutine (EvolveAnimate ());
		for (int i = 0; i < UIManager.instance.monA.evolveMaterials.Count; i++) {
			float startX = Mathf.Cos (72*i * Mathf.Deg2Rad) * 300f;
			float startY = Mathf.Sin (72 * i * Mathf.Deg2Rad) * 300f + 200f;

			materials [i].transform.localPosition = new Vector3 (startX, startY, 0f);
			materials [i].transform.localScale = new Vector3 (1f, 1f, 1f);
			materials [i].gameObject.SetActive (true);
			materials [i].sprite = MonsterList.instance.GetThumbnail (UIManager.instance.monA.evolveMaterials [i]);
			SetSpriteToOriginalSize (materials [i], 150f);

			StartCoroutine(Manager(materials[i].gameObject,72f*i));
		}

		for (int i = UIManager.instance.monA.evolveMaterials.Count; i < 5; i++) {
			materials[i].gameObject.SetActive(false);
		}
	}

	void OnDisable(){
		if (mon != null) {
			Destroy (mon);
		}
	}

	IEnumerator Manager(GameObject g, float startAngle){
		yield return new WaitForSeconds (1f);

		StartCoroutine (ScaleDown (g,0.3f, 1f));
		StartCoroutine (MoveCircular (g, 300f, 2f,startAngle));
	}

	IEnumerator ScaleDown(GameObject g,float targetScale,float time){
		float t = 0;
		float start = g.transform.localScale.x;

		while (t < time) {
			t += Time.deltaTime;
			float scale = Mathf.Lerp (start, targetScale, t / time);
			g.transform.localScale = new Vector2 (scale, scale);
			yield return null;
		}
	}

	IEnumerator MoveCircular(GameObject g,float radius,float time,float startAngle){
		float counter = startAngle;
		float t = 0;
		float start = radius;

		while (t < time) {
			counter += Time.deltaTime * 100f;
			t += Time.deltaTime;
			float x = Mathf.Cos (counter * Mathf.Deg2Rad) * radius;
			float y = Mathf.Sin (counter * Mathf.Deg2Rad) * radius + 200f;
			g.transform.localPosition = mon.transform.localPosition + new Vector3 (x, y, 0f);

			radius = Mathf.Lerp (start, 0f, t / time);
			yield return null;
		}

		count++;
		if (count < UIManager.instance.monA.evolveMaterials.Count) {
			yield break;
		}

		Fading fade = GetComponent<Fading> ();
		float waitTime = fade.BeginFade (1);
		yield return new WaitForSeconds (waitTime);

		//show congrat text
		congratText.SetActive (true);

		//Remove all materials
		for (int i = 0; i < materials.Length; i++) {
			materials [i].gameObject.SetActive (false);
		}

		//Spawn the evolve form
		Destroy(mon);
		mon = Instantiate (MonsterList.instance.GetPrefab (UIManager.instance.evolveShortCode), new Vector3 (0f, -1f, 0f), Quaternion.identity);

		waitTime = fade.BeginFade (-1);

		yield return new WaitForSeconds (waitTime);

		//evolve complete
		StartCoroutine (EvolveComplete ());
	}

	IEnumerator EvolveComplete(){
		//Remove all materials
		for (int i = 0; i < UIManager.instance.monA.evolveMaterials.Count; i++) {

			MonsterData monsterToRemove = null;

			for (int j = 0; j < GameSparksManager.instance.monsterData.Count; j++) {
				if (GameSparksManager.instance.monsterData [j].objectId != UIManager.instance.monA.objectId && GameSparksManager.instance.monsterData [j].shortCode == UIManager.instance.monA.evolveMaterials [i]) {
					if (monsterToRemove == null) {
						monsterToRemove = GameSparksManager.instance.monsterData [j];
					} else {
						if (GameSparksManager.instance.monsterData [j].level < monsterToRemove.level) {
							monsterToRemove = GameSparksManager.instance.monsterData [j];
						}
					}
				}
			}

			GameSparksManager.instance.RemoveMonster (monsterToRemove.objectId);
			GameSparksManager.instance.monsterData.Remove (monsterToRemove);

			//Add monster data below will automatically change team data, so, dont have to change it here.
		}

		//update local variable
		//Set data before save to the server

		//check if there is evolve form and then evolve
		for (int i = 0; i < MonsterList.instance.monsterModels.Length; i++) {
			string monsterShortCode = MonsterList.instance.monsterModels [i].shortCode;
			string[] model = monsterShortCode.Split ('.');
			string[] thisMon = UIManager.instance.monA.shortCode.Split ('.');

			if (thisMon [0] == model [0] && Int32.Parse(thisMon [1]) == Int32.Parse(model [1]) - 1) {
				//There is an evolve form
				UIManager.instance.monA.shortCode = monsterShortCode;
				UIManager.instance.monA.exp = new SafeInt (0);
				UIManager.instance.monA.level = new SafeInt (1);

				//update data on server
				GameSparksManager.instance.AddMonster (UIManager.instance.monA.shortCode, 1,UIManager.instance.monA.team);
				GameSparksManager.instance.RemoveMonster(UIManager.instance.monA.objectId);

				GameSparksManager.instance.GetMonsterData(UIManager.instance.monA);

				//Wait while loading
				while (!PersistentData.instance.loadedMonsterData) {
					yield return null;
				}

				/*
				//Reset value and return to previous scene.
				UIManager.instance.resetInventory = 1;
				UIManager.instance.resetEditMonster = 1;
				UIManager.instance.resetSelectMonster = 1;*/

				yield return new WaitForSeconds (2f);

				//Add quest progress
				PersistentData.instance.AddQuestProgress("evolve");

				congratText.SetActive (false);
				UIManager.instance.HideEvolveAnimate ();
				gameObject.SetActive (false);

				break;
			}
		}
	}

	void SetSpriteToOriginalSize(Image img, float target){
		float width = img.sprite.rect.width;
		float height = img.sprite.rect.height;

		if (height > width) {
			float multiplier = target / height;

			img.rectTransform.sizeDelta = new Vector2 (width * multiplier, target);
		} else {
			float multiplier = target / width;

			img.rectTransform.sizeDelta = new Vector2 (target, height * multiplier);
		}
	}
}
