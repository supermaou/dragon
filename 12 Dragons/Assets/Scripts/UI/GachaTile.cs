﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GachaTile : MonoBehaviour {

	public int row;
	public int col;

	public Color originalColor;
	private Image img;

	void Awake(){
		img = GetComponent<Image> ();

		if ((row % 2 == 0 && col % 2 == 0) || (row % 2 == 1 && col % 2 == 1)) {
			originalColor = new Color (246f / 255f, 190f / 255f, 152f / 255f);
		} else if ((row % 2 == 1 && col % 2 == 0) || (row % 2 == 0 && col % 2 == 1)) {
			originalColor = new Color (167f / 255f, 97f / 255f, 51f / 255f);
		}
	}

	public void SetToOriginalColor(){
		img.color = originalColor;
	}
}
