﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GameSparks.Core;

public class worldBossResult : MonoBehaviour {

	public AudioClip successSound;

	public Text scoreIncrease;
	public Text gemReceive;
	public Text bestScoreText;

	// Use this for initialization
	IEnumerator Start () {

		StartCoroutine (CountTo (scoreIncrease, PersistentData.instance.worldBossScore, 0.5f));

		//Save new world boss score
		GameSparksManager.instance.LoadPlayerData(); //to check for latest data

		while (!GameSparksManager.instance.loadPlayer) { //Wait while loading the data
			yield return null;
		}

		GameSparksManager.instance.playerData.dailyRaid = new SafeInt (1);
		//GameSparksManager.instance.playerData.worldBossScore += PersistentData.instance.worldBossScore;
		GameSparksManager.instance.playerData.gems.Value += 1; // Add gem for participation
		AnalyticsManager.instance.ReceiveGems("WorldBoss Participation",1);

		PersistentData.instance.AddQuestProgress ("bossraid");
		PersistentData.instance.AddDailyQuestProgress ("worldBossPlay");

		if (PersistentData.instance.worldBossScore > GameSparksManager.instance.playerData.bestWorldBossScore.Value) {
			GameSparksManager.instance.playerData.bestWorldBossScore.Value = PersistentData.instance.worldBossScore;
			PostScore (PersistentData.instance.worldBossScore);
			SubmitTeam ();
			GameSparksManager.instance.SavePlayerData ();
		}

		StartCoroutine (CountTo (bestScoreText, GameSparksManager.instance.playerData.bestWorldBossScore.Value, 0.5f));
	}

	void OnEnable(){
		SoundManager.instance.PlayEffect (successSound);
	}
	
	public void Menu(){
		Initiate.Fade("main",new Color(0.1f,0.1f,0.1f),1.5f);
	}

	IEnumerator CountTo(Text txt,int target, float duration){
		int start = System.Int32.Parse (txt.text);
		for (float timer = 0; timer < duration; timer += Time.deltaTime) {
			float progress = timer / duration;
			int score = (int)Mathf.Lerp (start, target, progress);
			txt.text = score.ToString ();
			yield return null;
		}

		txt.text = target.ToString ();
	}

	void SubmitTeam(){
		List<string> team = new List<string> ();
		for (int i = 0; i < GameSparksManager.instance.teamData [GameSparksManager.instance.playerData.currentTeam].Count; i++) {
			team.Add (GameSparksManager.instance.teamData [GameSparksManager.instance.playerData.currentTeam] [i].shortCode);
		}

		List<int> levels = new List<int> ();
		for (int i = 0; i < GameSparksManager.instance.teamData [GameSparksManager.instance.playerData.currentTeam].Count; i++) {
			levels.Add (GameSparksManager.instance.teamData [GameSparksManager.instance.playerData.currentTeam] [i].level.Value);
		}

		List<int> skillLevels = new List<int> ();
		for (int i = 0; i < GameSparksManager.instance.teamData [GameSparksManager.instance.playerData.currentTeam].Count; i++) {
			skillLevels.Add (GameSparksManager.instance.teamData [GameSparksManager.instance.playerData.currentTeam] [i].skillLevel.Value);
		}

		new GameSparks.Api.Requests.LogEventRequest().SetEventKey("Submit_Team").SetEventAttribute("team",team).SetEventAttribute("levels",levels)
			.SetEventAttribute("skillLevels",skillLevels).SetDurable(true).Send((response) => {
				if (!response.HasErrors) {
					print("submit team success");
				} else {
					print("submit team Failed");
					GameSparksManager.instance.OnError();
				}
			});
	}

	void PostScore(int score){
		new GameSparks.Api.Requests.LogEventRequest ().SetEventKey ("Score_Event").SetEventAttribute ("Score_Attribute", score).SetDurable(true).Send ((response) => {
			if(!response.HasErrors){
				print("Post Score Success");
			}else{
				print("Post Score Failed");
				GameSparksManager.instance.OnError();
			}
		});
	}
}
