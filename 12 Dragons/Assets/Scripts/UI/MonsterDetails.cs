﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class MonsterDetails : MonoBehaviour {

	public Transform stars;

	GameObject monSprite;
	public Text monName;
	public Text monLevel;
	public Text monStat;
	public Text leaderSkillDesc;
	public Text activeSkillTitle;
	public Text activeSkillDesc;
	public Text skillLevel;

	public GameObject skillLevelBadge;

	public Image element;

	public GameObject sellMonsterPopup;

	int moneyGot = 0;

	private MonsterData mon;

	void OnEnable () {

		if (PersistentData.instance.detailStack == 1) {
			mon = UIManager.instance.monA;
		} else {
			mon = UIManager.instance.monB;
		}

		if (mon.stars == 6) {
			moneyGot = 8000;
		} else if (mon.stars == 5) {
			moneyGot = 4000;
		} else if (mon.stars == 4) {
			moneyGot = 2000;
		} else if (mon.stars == 3) {
			moneyGot = 1000;
		} else if (mon.stars == 2) {
			moneyGot = 500;
		} else {
			moneyGot = 250;
		}

		monSprite = Instantiate(MonsterList.instance.GetPrefab(mon.shortCode),new Vector3(0f,0.5f,0f),Quaternion.identity) as GameObject;

		monName.text = mon.thaiName;
		if (mon.level.Value < mon.maxLevel) {
			monLevel.text = "Level " + mon.level.ToString() + " / " + mon.maxLevel.ToString();
		} else {
			monLevel.text = "Level MAX";
		}
			
		mon.AssignAllSkills();

		string monStatText;

		monStatText = mon.tribe + "\n" + mon.attack.ToString () + "\n" + mon.hitPoint.ToString () + "\n" + mon.recovery.ToString ();

		leaderSkillDesc.text = mon.leaderSkillBehavior.aDescription;

		if (mon.cooldown.Value != 0 && mon.cooldown.Value < 100) {
			activeSkillTitle.text = "สกิลกดใช้ (คูลดาวน์ " + (mon.cooldown - mon.skillLevel + 1).ToString () + " เทิร์น)";
			skillLevelBadge.SetActive (true);
		} else {
			activeSkillTitle.text = "สกิลกดใช้";
			skillLevelBadge.SetActive (false);
		}

		activeSkillDesc.text = mon.activeSkillBehavior.aDescription;
		skillLevel.text = "เลเวล " + mon.skillLevel.ToString () + " / " + mon.maxSkillLevel.ToString ();

		monStat.text = monStatText;

		if (mon.element == "fire") {
			element.sprite = UIManager.instance.fire;
		} else if (mon.element == "water") {
			element.sprite = UIManager.instance.water;
		} else if (mon.element == "plant") {
			element.sprite = UIManager.instance.plant;
		} else if (mon.element == "light") {
			element.sprite = UIManager.instance.light;
		} else if (mon.element == "dark") {
			element.sprite = UIManager.instance.dark;
		}

		//Display stars
		for (int i = 0; i < 6; i++) {
			if (i < mon.stars) {
				stars.GetChild (i).gameObject.SetActive (true);
			} else {
				stars.GetChild (i).gameObject.SetActive (false);
			}
		}
	}

	void OnDisable(){
		Resources.UnloadUnusedAssets ();

		if (monSprite) {
			Destroy (monSprite);
		}
	}

	public void SellMonsterButton(){
		sellMonsterPopup.transform.GetChild (0).GetComponent<Text> ().text = "ต้องการจะขายมอนสเตอร์ตัวนี้ในราคา " + moneyGot.ToString () + " ทองหรือไม่ ถ้าขายแล้วมอนสเตอร์ตัวนี้จะหายไป";
		sellMonsterPopup.SetActive (true);
	}

	public void CloseDialog(){
		sellMonsterPopup.SetActive (false);
	}

	public void SellMonster(){
		//Remove it locally
		GameSparksManager.instance.monsterData.Remove(mon);

		//Remove it from team
		for (int i = 0; i < GameSparksManager.instance.teamData.Count; i++) {
			for (int j = 0; j < GameSparksManager.instance.teamData [i].Count; j++) {
				if (GameSparksManager.instance.teamData [i] [j] == mon) {
					GameSparksManager.instance.teamData [i].Remove (mon);
				}
			}
		}

		//remove it on server
		GameSparksManager.instance.RemoveMonster(mon.objectId);

		//make the inventory reset
		UIManager.instance.resetInventory = 1;
		UIManager.instance.resetEditMonster = 1;
		UIManager.instance.resetSelectMonster = 1;

		//Add gold to player
		GameSparksManager.instance.playerData.gold += moneyGot;
		moneyGot = 0;
		GameSparksManager.instance.SavePlayerData ();

		//back
		UIManager.instance.BackBtn();
	}
}
