﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CountDownText : MonoBehaviour {

	Text selfText;

	IEnumerator coroutine;

	void Awake(){
		selfText = GetComponent<Text> ();
	}
		
	void Start(){
		if (coroutine != null) {
			StopCoroutine (coroutine);
		}

		coroutine = CountDown ();

		StartCoroutine (coroutine);
	}

	void OnApplicationPause(bool pauseStatus){
		if (!pauseStatus) {
			StopCoroutine (coroutine);

			coroutine = CountDown ();

			StartCoroutine (coroutine);
		}
	}

	IEnumerator CountDown(){

		GameSparksManager.instance.GetStaminaAdded();

		while (!PersistentData.instance.loadedStamina) {
			yield return null;
		}

		//load data complete
		PersistentData.instance.loadedStamina = false;
		UIManager.instance.UpdateStaminaText ();

		if (GameSparksManager.instance.playerData.stamina >= GameSparksManager.instance.playerData.maxStamina.Value) {
			gameObject.SetActive (false);
		}

		while(true){
			float time = 60f; //time in seconds
			float t = 0f;

			while (t < time) {
				t += Time.deltaTime;
				float timeLeft = time - t;

				int minutes = (int)Mathf.Floor (timeLeft / 60f);
				int seconds = (int)timeLeft % 60;

				if (seconds >= 10) {
					selfText.text = minutes.ToString () + ":" + seconds.ToString ();
				} else {
					selfText.text = minutes.ToString () + ":0" + seconds.ToString ();
				}
				yield return null;
			}

			//may be hack by speedhack
			if (GameSparksManager.instance.playerData.stamina < GameSparksManager.instance.playerData.maxStamina.Value) {
				GameSparksManager.instance.playerData.stamina += 1;
				//SaveLoad.Save ();
				UIManager.instance.UpdateStaminaText ();
				GameSparksManager.instance.SavePlayerData ();

				//GameSparksManager.instance.GetStaminaAdded ();
			} else {
				gameObject.SetActive (false);
			}
		}
	}
}
