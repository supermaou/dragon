﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Fuse : MonoBehaviour {

	private GameObject mon;
	public GameObject successText;
	public Image[] materials;

	public GameObject messageDialog;

	private SafeInt skillLevelIncreased;
	private int count = 0;

	// Use this for initialization
	void OnEnable () {
		skillLevelIncreased.Value = 0;
		count = 0;
		mon = Instantiate (MonsterList.instance.GetPrefab (UIManager.instance.monA.shortCode), new Vector3 (0f, 0f, 0f), Quaternion.identity);

		for (int i = 0; i < UIManager.instance.mons2fuse.Count; i++) {
			float startX = Mathf.Cos (72 * i * Mathf.Deg2Rad) * 300f;
			float startY = Mathf.Sin (72 * i * Mathf.Deg2Rad) * 300f + 200f;

			materials [i].transform.localPosition = new Vector3 (startX, startY, 0f);
			materials [i].transform.localScale = new Vector3 (1f, 1f, 1f);
			materials [i].gameObject.SetActive (true);
			materials [i].sprite = MonsterList.instance.GetThumbnail (UIManager.instance.mons2fuse [i].shortCode);
			materials [i].color = Color.white;
			SetSpriteToOriginalSize (materials [i], 150f);

			//Play the animation
			StartCoroutine(Manager(materials[i].gameObject,72f*i));
		}

		for (int i = UIManager.instance.mons2fuse.Count; i < 5; i++) {
			materials[i].gameObject.SetActive(false);
		}
	}

	void OnDisable(){
		if (mon != null) {
			Destroy (mon);
		}
	}

	IEnumerator Manager(GameObject g, float startAngle){
		yield return new WaitForSeconds (1f);

		StartCoroutine (ScaleDown (g,0.3f, 1f));
		StartCoroutine (FadeOut (g, 1.5f));
		StartCoroutine (MoveCircular (g, 300f, 2f,startAngle));
	}

	IEnumerator ScaleDown(GameObject g,float targetScale,float time){
		float t = 0;
		float start = g.transform.localScale.x;

		while (t < time) {
			t += Time.deltaTime;
			float scale = Mathf.Lerp (start, targetScale, t / time);
			g.transform.localScale = new Vector2 (scale, scale);
			yield return null;
		}
	}

	IEnumerator FadeOut(GameObject g,float time){
		Color start = Color.white;
		Color end = new Color (1f, 1f, 1f, 0f);

		Image img = g.GetComponent<Image> ();

		float t = 0;
		while (t < time) {
			t += Time.deltaTime;
			img.color = Color.Lerp (start, end, t / time);
			yield return null;
		}
	}

	IEnumerator MoveCircular(GameObject g,float radius,float time,float startAngle){
		float counter = startAngle;
		float t = 0;
		float start = radius;

		while (t < time) {
			counter += Time.deltaTime * 100f;
			t += Time.deltaTime;
			float x = Mathf.Cos (counter * Mathf.Deg2Rad) * radius;
			float y = Mathf.Sin (counter * Mathf.Deg2Rad) * radius + 200f;
			g.transform.localPosition = mon.transform.localPosition + new Vector3 (x, y, 0f);

			radius = Mathf.Lerp (start, 0f, t / time);
			yield return null;
		}

		count++;
		if (count < UIManager.instance.mons2fuse.Count) {
			yield break;
		}
			
		Fading fade = GetComponent<Fading> ();
		float waitTime = fade.BeginFade (1);
		yield return new WaitForSeconds (waitTime);

		//show success text
		successText.SetActive(true);

		//Remove all materials
		for (int i = 0; i < materials.Length; i++) {
			materials [i].gameObject.SetActive (false);
		}

		waitTime = fade.BeginFade (-1);

		yield return new WaitForSeconds (waitTime);

		//fuse complete
		StartCoroutine(FuseComplete());
	}

	IEnumerator FuseComplete(){
		IncreaseExp (UIManager.instance.monA, TotalExpGain ());

		//Remove the monsters that was used to fuse
		for (int i = 0; i < UIManager.instance.mons2fuse.Count; i++) {
			string oid = UIManager.instance.mons2fuse [i].objectId;

			GameSparksManager.instance.monsterData.Remove (UIManager.instance.mons2fuse [i].monsterData);
			GameSparksManager.instance.RemoveMonster (oid);

			//add quest progress
			PersistentData.instance.AddQuestProgress("fuse");
		}

		yield return new WaitForSeconds (2f);

		//reset inventory and others
		UIManager.instance.resetInventory = 1;
		UIManager.instance.resetEditMonster = 1;
		UIManager.instance.resetSelectMonster = 1;
		UIManager.instance.mons2fuse.Clear ();
		UIManager.instance.HideFuseAnimate ();

		if (skillLevelIncreased.Value > 0 && UIManager.instance.monA.cooldown.Value != 0 && UIManager.instance.monA.cooldown.Value < 100) {
			messageDialog.transform.GetChild (0).GetComponent<Text> ().text = "เลเวลสกิลเพิ่มขึ้น " + skillLevelIncreased.ToString () + " ลดคูลดาวน์สกิลเหลือ "
			+ (UIManager.instance.monA.cooldown - UIManager.instance.monA.skillLevel + 1).ToString ();
			messageDialog.SetActive (true);
		}

		successText.SetActive (false);
		gameObject.SetActive (false);
	}

	void IncreaseExp(MonsterData m, int exp){
		SafeInt skillToIncrease = new SafeInt (SkillLevelIncrease ());

		for (int i = 0; i < GameSparksManager.instance.monsterData.Count; i++) {
			if (GameSparksManager.instance.monsterData [i] == m) {
				//Check if exp excess max exp
				if (GameSparksManager.instance.monsterData [i].exp + exp < GameSparksManager.instance.monsterData [i].maxExp) {
					GameSparksManager.instance.monsterData [i].exp += exp;
				} else {
					GameSparksManager.instance.monsterData [i].exp = GameSparksManager.instance.monsterData [i].maxExp;
				}

				//calculate new level and increase the stats
				int newLevel = m.CalculateLevel (GameSparksManager.instance.monsterData [i].exp.Value);
				m.SetStatToLevel (newLevel);
				GameSparksManager.instance.monsterData [i].SetStatToLevel (newLevel);

				//change at team member too!
				for (int a = 0; a < GameSparksManager.instance.teamData.Count; a++) {
					for (int b = 0; b < GameSparksManager.instance.teamData [a].Count; b++) {
						if (GameSparksManager.instance.teamData [a] [b] == m) {
							GameSparksManager.instance.teamData [a] [b].SetStatToLevel (newLevel);
						}
					}
				}

				//increase skill level if eligible
				if (skillToIncrease > 0) {
					if (m.skillLevel + skillToIncrease > m.maxSkillLevel) {
						skillLevelIncreased.Value = m.maxSkillLevel - m.skillLevel.Value;
						skillToIncrease = skillLevelIncreased ;
						m.skillLevel = new SafeInt (m.maxSkillLevel);
					} else {
						skillLevelIncreased = skillToIncrease;
						m.skillLevel = new SafeInt (m.skillLevel.Value + skillToIncrease.Value);
					}
				}

				//update the data to server
				GameSparksManager.instance.UpdateMonster (UIManager.instance.monA.objectId, UIManager.instance.monA.exp.Value, m.skillLevel.Value);

				return;
			}
		}
	}

	int SkillLevelIncrease(){
		int count = 0;
		for (int i = 0; i < UIManager.instance.mons2fuse.Count; i++) {
			if (UIManager.instance.mons2fuse [i].thaiName == UIManager.instance.monA.thaiName) {
				count++;
			}
		}

		return count;
	}

	int TotalExpGain(){
		SafeInt exp = new SafeInt (0);

		for (int i = 0; i < UIManager.instance.mons2fuse.Count; i++) {
			if (UIManager.instance.mons2fuse [i].element == UIManager.instance.monA.element) {
				exp = new SafeInt( exp.Value + Mathf.RoundToInt (UIManager.instance.mons2fuse [i].exp.Value * 0.5f * 1.5f));
				exp = new SafeInt(exp.Value + Mathf.RoundToInt (UIManager.instance.mons2fuse [i].startExp.Value * 0.5f * 1.5f));
			} else {
				exp = new SafeInt(exp.Value + Mathf.RoundToInt (UIManager.instance.mons2fuse [i].exp.Value * 0.5f));
				exp = new SafeInt(exp.Value + Mathf.RoundToInt (UIManager.instance.mons2fuse [i].startExp.Value * 0.5f));
			}
		}

		return exp.Value;
	}

	void SetSpriteToOriginalSize(Image img, float target){
		float width = img.sprite.rect.width;
		float height = img.sprite.rect.height;

		if (height > width) {
			float multiplier = target / height;

			img.rectTransform.sizeDelta = new Vector2 (width * multiplier, target);
		} else {
			float multiplier = target / width;

			img.rectTransform.sizeDelta = new Vector2 (target, height * multiplier);
		}
	}
}
