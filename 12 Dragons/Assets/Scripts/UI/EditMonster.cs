﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EditMonster : MonoBehaviour {

	public GameObject inuseBadge;
	public GameObject starPrefab;
	public GameObject levelText;

	List<GameObject> monsterList = new List<GameObject>();

	private Color inactiveColor = new Color(150f/255f,150f/255f,150f/255f);

	void OnEnable(){
		ShowInventory ();
	}

	void SetSpriteToOriginalSize(Image img, float target){
		float width = img.sprite.rect.width;
		float height = img.sprite.rect.height;

		if (height > width) {
			float multiplier = target / height;

			img.rectTransform.sizeDelta = new Vector2 (width * multiplier, target);
		} else {
			float multiplier = target / width;

			img.rectTransform.sizeDelta = new Vector2 (target, height * multiplier);
		}
	}

	void OnDisable(){
		foreach (Transform t in transform.GetChild(0)) {
			Destroy (t.gameObject);
		}
	}
	
	void ShowInventory(){
		foreach (Transform t in transform.GetChild(0)) {
			Destroy (t.gameObject);
		}
			
		for (int i = 0; i < GameSparksManager.instance.monsterData.Count; i++) {

			if (IsInTeam (GameSparksManager.instance.monsterData [i])) {
				continue;
			}

			GameObject g = new GameObject ();
			g.transform.SetParent (transform.GetChild (0), false);

			Image im = g.AddComponent<Image> ();
			im.raycastTarget = false;

			g.AddComponent<Mask> ().showMaskGraphic = false;

			GameObject mon = new GameObject ();
			mon.transform.SetParent (g.transform, false);
			mon.transform.localPosition = new Vector3 (0f, 0f, 0f);

			Image img = mon.AddComponent<Image> ();
			img.sprite = MonsterList.instance.GetThumbnail (GameSparksManager.instance.monsterData [i].shortCode);
			SetSpriteToOriginalSize (img, 120f);

			MonsterController m = mon.AddComponent<MonsterController> ();
			m.CloneMonsterData (GameSparksManager.instance.monsterData [i]);

			LongPress lp = mon.AddComponent<LongPress> ();
			lp.onClick.AddListener (() => UIManager.instance.ShowSwitchMonster (lp.gameObject));
			lp.onLongPress.AddListener (() => UIManager.instance.ShowMonsterDetails (m.monsterData, 2));
			lp.interactable = true;

			GameObject levelTxt = Instantiate (levelText, g.transform, false);
			levelTxt.transform.localPosition = new Vector3 (0f, -80f, 0f);
			levelTxt.GetComponent<Text> ().text = "Level " + m.level.ToString ();

			/*
			//instantiate stars
			for (int x = 0; x < m.stars; x++) {
				GameObject s = Instantiate (starPrefab, g.transform, false);
				s.transform.localPosition = new Vector3 (((float)x - (float)m.stars / 2f + 0.5f) * 20f, -80f, 0f);
			}
			
			//Check if there is duplicate name in the player team
			for (int j = 0; j < GameSparksManager.instance.teamData [GameSparksManager.instance.playerData.currentTeam].Count; j++) {
				MonsterData mm = GameSparksManager.instance.teamData [GameSparksManager.instance.playerData.currentTeam] [j];
				if (m.objectId == mm.objectId) {
					lp.interactable = false;

					lp.gameObject.GetComponent<Image> ().color = inactiveColor;
				}
			}*/

			monsterList.Add (g);
		}
	}

	bool IsInTeam(MonsterData m){
		for (int j = 0; j < GameSparksManager.instance.teamData [GameSparksManager.instance.playerData.currentTeam].Count; j++) {
			MonsterData mm = GameSparksManager.instance.teamData [GameSparksManager.instance.playerData.currentTeam] [j];
			if (m.objectId == mm.objectId) {
				return true;
			}
		}

		return false;
	}

	/*
	void UpdateInventory(){
		foreach(GameObject g in monsterList) {
			MonsterController m = g.transform.GetChild(0).GetComponent<MonsterController> ();

			LongPress lp = g.transform.GetChild(0).GetComponent<LongPress> ();
			lp.interactable = true;

			//Check if there is duplicate name in the player team
			for (int j = 0; j < GameSparksManager.instance.teamData [GameSparksManager.instance.playerData.currentTeam].Count; j++) {
				MonsterData mm = GameSparksManager.instance.teamData [GameSparksManager.instance.playerData.currentTeam] [j];
				if (m.objectId == mm.objectId) {
					lp.interactable = false;

					lp.gameObject.GetComponent<Image> ().color = inactiveColor;
				}
			}
		}
	}*/

	void HideInventory(){
		foreach(Transform t in transform.GetChild(0)){
			Destroy(t.gameObject);
		}
	}
}
