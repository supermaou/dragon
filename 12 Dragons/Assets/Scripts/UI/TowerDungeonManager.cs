﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSparks.Core;
using UnityEngine.UI;

public class TowerDungeonManager : MonoBehaviour {

	public Transform content;
	public GameObject dungeonPrefab;

	List<DungeonData> towerDungeons = new List<DungeonData>();

	public GameObject staminaRefillDialog;
	public GameObject fullInventoryDialog;
	public GameObject messageDialog;

	public Sprite gemIcon;
	public Sprite goldIcon;
	public Sprite energyIcon;
	public Sprite spaceIcon;

	void SetSpriteToOriginalSize(Image img, float target){
		float width = img.sprite.rect.width;
		float height = img.sprite.rect.height;

		if (height > width) {
			float multiplier = target / height;

			img.rectTransform.sizeDelta = new Vector2 (width * multiplier, target);
		} else {
			float multiplier = target / width;

			img.rectTransform.sizeDelta = new Vector2 (target, height * multiplier);
		}
	}

	void OnEnable(){
		LoadDungeons ();
	}

	void LoadDungeons(){
		new GameSparks.Api.Requests.LogEventRequest ().SetEventKey ("getTowerDungeons").Send ((response) => {
			if(!response.HasErrors){
				towerDungeons.Clear();

				List<GSData> dataList = response.ScriptData.GetGSDataList("towerDungeons");

				for(int i=0;i<dataList.Count;i++){
					DungeonData d = new DungeonData();

					d.floor = dataList[i].GetInt("floor").Value;
					d.stamina = dataList[i].GetInt("stamina").Value;
					d.reward = dataList[i].GetString("reward");
					d.background = 9;
					d.music = dataList[i].GetInt("music") ?? 0;
					d.dropRateNormal = 0;
					d.dropRateBoss = 0;
					d.gold = 0;
					d.exp = 0;

					towerDungeons.Add(d);

				}

				towerDungeons.Sort(
					delegate (DungeonData d2,DungeonData d1) {
						return d2.floor.CompareTo(d1.floor);
					}
				);

				ShowDungeons();

			}else{
				GameSparksManager.instance.OnError();
			}
		});
	}

	void ShowDungeons(){
		foreach (Transform t in content) {
			Destroy (t.gameObject);
		}

		for (int i = 0; i < towerDungeons.Count; i++) {
			GameObject g = Instantiate (dungeonPrefab) as GameObject;
			g.transform.SetParent (content, false);

			//set boss thumbnail
			string boss = GameSparksManager.instance.dungeonData[i].bossShortCode;
			Image img = g.transform.GetChild (0).GetComponent<Image> ();
			img.sprite = MonsterList.instance.GetThumbnail (boss);
			SetSpriteToOriginalSize (img, 100f);

			g.transform.GetChild (1).GetComponent<Text> ().text = "ชั้นที่ " + towerDungeons [i].floor.ToString ();
			g.transform.GetChild (2).GetComponent<Text> ().text = "พลังงานที่ใช้: " + towerDungeons[i].stamina;
			g.transform.GetChild (3).gameObject.SetActive (false);

			if (i == GameSparksManager.instance.playerData.tower.Value) {
				Button b = g.GetComponent<Button> ();

				int a = i;
				b.onClick.AddListener (() => StartGame (towerDungeons [a]));

				g.transform.GetChild (6).gameObject.SetActive (true);
				g.transform.GetChild (6).GetComponent<Text> ().text = "รางวัล: " + GetRewardText (towerDungeons [i].reward);

				g.transform.GetChild (7).gameObject.SetActive (true);
				Image rtn = g.transform.GetChild (7).GetComponent<Image> ();
				rtn.sprite = GetRewardThumbnail (towerDungeons [i].reward);
				SetSpriteToOriginalSize (rtn, 60f);

			} else {
				g.transform.GetChild (6).gameObject.SetActive (true);
				g.transform.GetChild (6).GetComponent<Text> ().text = "รับรางวัลไปแล้ว";
			}
		}
	}

	string GetRewardText(string shortCode){
		string[] s = shortCode.Split ('.');

		if (s [1] == "gems") {
			return s [0];
		} else if (s [1] == "gold") {
			return s [0];
		} else if (s [1] == "energy") {
			return s [0];
		} else if (s [1] == "spaces") {
			return s [0];
		}

		return "";
	}

	Sprite GetRewardThumbnail(string shortCode){
		string[] s = shortCode.Split ('.');

		if (s [1] == "gems") {
			return gemIcon;
		} else if (s [1] == "gold") {
			return goldIcon;
		} else if (s [1] == "energy") {
			return energyIcon;
		} else if (s [1] == "spaces") {
			return spaceIcon;
		} else { //monster case
			return MonsterList.instance.GetThumbnail(shortCode);
		}

		return null;
	}

	void StartGame(DungeonData d){
		if (!GS.Authenticated) {
			GameSparksManager.instance.OnError ();
			return;
		}

		if (GameSparksManager.instance.playerData.stamina < d.stamina) {
			staminaRefillDialog.SetActive (true);
			return;
			//the subtraction will occur in Load enemies data method below
		}

		if (GameSparksManager.instance.monsterData.Count >= GameSparksManager.instance.playerData.maxInventory.Value) {
			fullInventoryDialog.SetActive (true);
			return;
		}

		if (GameSparksManager.instance.teamData [GameSparksManager.instance.playerData.currentTeam].Count <= 0) {
			//no monster in team
			messageDialog.transform.GetChild(0).GetComponent<Text>().text = "ไม่มีมอนสเตอร์ในทีม";
			messageDialog.SetActive (true);

			return;
		}

		UIManager.instance.unclickableTransparentPanel.SetActive (true);

		new GameSparks.Api.Requests.LogEventRequest ().SetEventKey ("getTowerDungeonData").SetEventAttribute ("floor", d.floor).Send ((response) => {
			if (!response.HasErrors) {
				GSData data = response.ScriptData.GetGSData("towerData");

				d.wave = data.GetIntList("wave");

				List<GSData> enemyList = data.GetGSDataList("enemies");

				d.enemies.Clear();

				for(int j=0;j<enemyList.Count;j++){
					EnemyData e = new EnemyData();
					e.enemyShortCode = enemyList[j].GetString("name");
					e.turn = enemyList[j].GetInt("turn").Value;
					e.hitPoint = enemyList[j].GetInt("hitPoint").Value;
					e.attack = enemyList[j].GetInt("attack").Value;
					e.defense = enemyList[j].GetInt("defense").Value;
					e.lootChance = enemyList[j].GetInt("lootChance") ?? 0;
					e.enemySkill = enemyList[j].GetGSData("enemySkill");
					d.enemies.Add(e);
				}

				//Set dungeon in Persistence data
				PersistentData.instance.gameType = "tower";
				PersistentData.instance.currentDungeon = d;

				StartCoroutine (LoadEnemiesData ()); //cache enemies data to Persistence data gameobject;

			}else{
				print(response.Errors.JSON);
				GameSparksManager.instance.OnError();
			}
		});
	}

	IEnumerator LoadEnemiesData(){

		PersistentData.instance.enemiesData.Clear ();
		PersistentData.instance.enemiesLoadCount = 0;

		//extract data from EnemyData class and put it in MonsterData class and also get the data according to its shortCode
		int count = 0;
		for (int i = 0; i < PersistentData.instance.currentDungeon.wave.Count; i++) {
			PersistentData.instance.enemiesData.Add (new List<MonsterData> ());
			for (int j = 0; j < PersistentData.instance.currentDungeon.wave [i]; j++) {
				MonsterData m = new MonsterData ();
				m.shortCode = PersistentData.instance.currentDungeon.enemies [count].enemyShortCode;
				m.cooldown = new SafeInt (PersistentData.instance.currentDungeon.enemies [count].turn);
				m.hitPoint = new SafeInt(PersistentData.instance.currentDungeon.enemies [count].hitPoint);
				m.attack= new SafeInt(PersistentData.instance.currentDungeon.enemies [count].attack);
				m.defense= new SafeInt(PersistentData.instance.currentDungeon.enemies [count].defense);
				m.lootChance = new SafeInt(PersistentData.instance.currentDungeon.enemies [count].lootChance);
				m.enemySkill = PersistentData.instance.currentDungeon.enemies [count].enemySkill;
				GameSparksManager.instance.GetMonsterData (m, "enemy");
				PersistentData.instance.enemiesData [i].Add (m);
				count++;
			}
		}

		while (!PersistentData.instance.loadedMonsterData) {
			yield return null;
		}

		while (PersistentData.instance.enemiesLoadCount < PersistentData.instance.currentDungeon.enemies.Count) {
			yield return null;
		}

		//subtract stamina
		if (GameSparksManager.instance.playerData.stamina >= PersistentData.instance.currentDungeon.stamina) {
			GameSparksManager.instance.playerData.stamina -= PersistentData.instance.currentDungeon.stamina;
			GameSparksManager.instance.SavePlayerData ();
			/*
			if (SaveLoad.userData.stamina < 0) {
				SaveLoad.userData.stamina = 0;
			}
			SaveLoad.Save ();*/

			Initiate.Fade ("gameplay", new Color (0.1f, 0.1f, 0.1f), 1.5f);
		}
	}
}
