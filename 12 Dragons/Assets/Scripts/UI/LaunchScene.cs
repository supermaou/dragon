﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class LaunchScene : MonoBehaviour {

	public Button authenticateBtn;
	public Button fbAuthenticateBtn;

	public Button nameSubmitBtn;

	IEnumerator Start(){
		yield return new WaitForSeconds (0.5f);

		authenticateBtn.onClick.AddListener (() => DeviceLoginListener());
		fbAuthenticateBtn.onClick.AddListener (() => FacebookLoginListener());
		nameSubmitBtn.onClick.AddListener (() => GameSparksManager.instance.SubmitName ());
	}

	void DeviceLoginListener(){
		fbAuthenticateBtn.interactable = false;
		GameSparksManager.instance.AuthenticateDeviceBtn ();
	}

	void FacebookLoginListener(){
		authenticateBtn.interactable = false;
		GameSparksManager.instance.FacebookLoginBtn ();
	}
}
