﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GameSparks.Core;

public class MonsterController : MonoBehaviour {

	public MonsterData monsterData;

	public string objectId;

	public string shortCode;

	public int isLocked;

	public List<int> team;
	public List<int> order;

	public string monsterName;
	public string thaiName;

	//public int evolve;
	public List<string> evolveMaterials;

	public SafeInt level;
	public int stars;
	public SafeInt exp;
	public string element;

	public string tribe;

	public GSData leaderSkill;
	public GSData activeSkill;
	public SafeInt cooldown;

	public SafeInt minAttack;
	public SafeInt minHitPoint;
	public SafeInt minRecovery;

	public SafeInt maxAttack;
	public SafeInt maxHitPoint;
	public SafeInt maxRecovery;

	public float gfAttack;
	public float gfHitPoint;
	public float gfRecovery;

	public SafeInt startExp;
	public SafeInt maxExp;

	public float maxLevel;

	//data specific to this prefab
	public Sprite sprite;
	public Sprite thumbnail;

	//order in game
	public int orderInGame;

	public Ability leaderSkillBehavior;
	public Ability activeSkillBehavior;
	public SafeInt skillLevel;
	public int maxSkillLevel;

	public GameObject skillEffect; //for what??

	//data from calculation
	public SafeInt hitPoint;
	public SafeInt attack;
	public SafeInt recovery;

	//enemy variables
	public GSData enemySkill;
	public Ability enemySkillBehavior;
	public SafeInt defense;
	public SafeInt lootChance;

	public void SetSprite(){
		SpriteRenderer sr = GetComponent<SpriteRenderer> ();
		sr.sprite = sprite;
		sr.sortingOrder = 2;
	}

	public void SetSpriteToThumbnail(){
		SpriteRenderer sr = GetComponent<SpriteRenderer> ();
		sr.sprite = thumbnail;
		sr.sortingOrder = 2;
	}

	public void SetThumbnail(){
		GetComponent<Image> ().sprite = thumbnail;
	}

	public void CloneMonsterData(MonsterData template){
		this.monsterData = template;
		this.objectId = template.objectId;
		this.shortCode = template.shortCode;
		this.isLocked = template.isLocked;
		this.team = template.team;
		this.order = template.order;
		this.monsterName = template.monsterName;
		this.thaiName = template.thaiName;
		//this.evolve = template.evolve;
		this.evolveMaterials = template.evolveMaterials;
		this.level = template.level;
		this.stars = template.stars;
		this.exp = template.exp;
		this.element = template.element;
		this.tribe = template.tribe;

		this.minAttack = template.minAttack;
		this.minHitPoint = template.minHitPoint;
		this.minRecovery = template.minRecovery;

		this.hitPoint = template.hitPoint;
		this.attack = template.attack;
		this.recovery = template.recovery;

		this.maxAttack = template.maxAttack;
		this.maxHitPoint = template.maxHitPoint;
		this.maxRecovery = template.maxRecovery;

		this.gfAttack = template.gfAttack;
		this.gfHitPoint = template.gfHitPoint;
		this.gfRecovery = template.gfRecovery;

		this.leaderSkill = template.leaderSkill;
		this.activeSkill = template.activeSkill;
		this.cooldown = template.cooldown;
		this.skillLevel = template.skillLevel;
		this.maxSkillLevel = template.maxSkillLevel;

		this.startExp = template.startExp;
		this.maxExp = template.maxExp;

		this.maxLevel = template.maxLevel;

		this.defense = template.defense;
		this.lootChance = template.lootChance;

		//Set skills
		this.enemySkill = template.enemySkill;

		if (this.enemySkill == null) {
			AssignLeaderSkill ();
			AssignActiveSkill ();
		}

		if (this.enemySkill != null) {
			AssignEnemySkill ();
		}
	}

	public void AssignAllSkills(){ //Use for evolve scene
		AssignLeaderSkill();
		AssignActiveSkill ();
	}

	void AssignLeaderSkill(){ 
		if (leaderSkill == null) {
			leaderSkillBehavior = new NoSkill ();
			leaderSkillBehavior.Initialize (leaderSkill);

			return;
		}

		if (leaderSkill.GetString ("type") == "BuffSelf") {
			leaderSkillBehavior = new BuffSelf ();
		} else if (leaderSkill.GetString ("type") == "IncreaseMoveTime") {
			leaderSkillBehavior = new IncreaseMoveTime ();
		} else if (leaderSkill.GetString ("type") == "AttackIncreaseWhenFullHp") {
			leaderSkillBehavior = new AttackIncreaseWhenFullHp ();
		} else if (leaderSkill.GetString ("type") == "AttackIncreaseWhenHpBelow") {
			leaderSkillBehavior = new AttackIncreaseWhenHpBelow ();
		} else if (leaderSkill.GetString ("type") == "BonusGold") {
			leaderSkillBehavior = new BonusGold ();
		} else if (leaderSkill.GetString ("type") == "IncreaseDropChance") {
			leaderSkillBehavior = new IncreaseDropChance ();
		} else if (leaderSkill.GetString ("type") == "BurstRuneTypesBuff") {
			leaderSkillBehavior = new BurstRuneTypesBuff ();
		} else if (leaderSkill.GetString ("type") == "NotDieOneTime") {
			leaderSkillBehavior = new NotDieOneTime ();
		} else if (leaderSkill.GetString ("type") == "TeamAllElements") {
			leaderSkillBehavior = new TeamAllElements ();
		} else if (leaderSkill.GetString ("type") == "TribeBuff") {
			leaderSkillBehavior = new TribeBuff ();
		} else if (leaderSkill.GetString ("type") == "BuffHitPoint") {
			leaderSkillBehavior = new BuffHitPoint ();
		} else if (leaderSkill.GetString ("type") == "DamageBuffOnRuneCount") {
			leaderSkillBehavior = new DamageBuffOnRuneCount ();
		}


		if (leaderSkillBehavior != null) {
			leaderSkillBehavior.Initialize (leaderSkill);
		} else {
			print ("leader skill null");
		}
	}

	void AssignActiveSkill(){
		if (activeSkill == null) {
			activeSkillBehavior = new NoSkill ();
			activeSkillBehavior.Initialize (activeSkill);

			return;
		}

		if (activeSkill.GetString ("type") == "DealDamage") {
			activeSkillBehavior = new DealDamage ();
		} else if (activeSkill.GetString ("type") == "DealDamagePercent") {
			activeSkillBehavior = new DealDamagePercent ();
		} else if (activeSkill.GetString ("type") == "Heal") {
			activeSkillBehavior = new Heal ();
		} else if (activeSkill.GetString ("type") == "EnemyDebuff") {
			activeSkillBehavior = new EnemyDebuff ();
		} else if (activeSkill.GetString ("type") == "IncreaseCooldown") {
			activeSkillBehavior = new IncreaseCooldown ();
		} else if (activeSkill.GetString ("type") == "SwitchRune") {
			activeSkillBehavior = new SwitchRune ();
		} else if (activeSkill.GetString ("type") == "TemporaryBuff") {
			activeSkillBehavior = new TemporaryBuff ();
		}

		if (activeSkillBehavior != null) {
			activeSkillBehavior.Initialize (activeSkill);
		} else {
			print ("active skill null");
		}
	}

	void AssignEnemySkill(){
		if (enemySkill.GetString ("type") == "ChangeRune") {
			enemySkillBehavior = new ChangeRune ();
		} else if (enemySkill.GetString ("type") == "EnemyHeal") {
			enemySkillBehavior = new EnemyHeal ();
		} else if (enemySkill.GetString ("type") == "IncreaseAttack") {
			enemySkillBehavior = new IncreaseAttack ();
		} else if (enemySkill.GetString ("type") == "MorethanCombo") {
			enemySkillBehavior = new MorethanCombo ();
		} else if (enemySkill.GetString ("type") == "MorethanTypes") {
			enemySkillBehavior = new MorethanTypes ();
		} else if (enemySkill.GetString ("type") == "RuneCountRequire") {
			enemySkillBehavior = new RuneCountRequire ();
		} else if (enemySkill.GetString ("type") == "BanRune") {
			enemySkillBehavior = new BanRune ();
		}

		enemySkillBehavior.Initialize (enemySkill);
	}

	public void Clear(){
		this.objectId = null;
		this.monsterName = null;
		this.monsterData = null;
	}

	//return exp left require for next level
	public int ExpRequireForNextLevel(){
		if (level.Value >= maxLevel) {
			return 0;
		}
			
		float nextLevelExp = maxExp.Value * Mathf.Pow ((level.Value) / 98f, 2.5f);

		return Mathf.RoundToInt (nextLevelExp - exp.Value);
	}

	//return all exp require for next level
	public int TotalExpRequireForNextLevel(){
		float currentLevelExp = maxExp.Value * Mathf.Pow ((level.Value - 1f) / 98f, 2.5f);
		float nextLevelExp = maxExp.Value * Mathf.Pow ((level.Value) / 98f, 2.5f);

		return Mathf.RoundToInt (nextLevelExp - currentLevelExp);
	}

	//return level according to exp
	public int CalculateLevel(int exp){
		for (int i = 1; i < 100; i++) {
			int level = 1;
			int cumulativeExp = CumulativeExp (i);

			if (exp < cumulativeExp) {
				level = i - 1;
				if (level > maxLevel) {
					return (int)maxLevel;
				} else {
					return level;
				}
			}
		}

		return 1;
	}

	//return cumulative exp upto the level
	private int CumulativeExp(int level){
		float currentLevelExp = maxExp.Value * Mathf.Pow ((level - 1f) / 98f, 2.5f);
		return Mathf.RoundToInt (currentLevelExp);
	}

	public void SetStatToLevel(int level){
		this.level = new SafeInt (level);

		//set other stat here
		this.attack = new SafeInt (CurrentStat(minAttack.Value,maxAttack.Value,level,gfAttack));
		this.hitPoint = new SafeInt (CurrentStat (minHitPoint.Value, maxHitPoint.Value, level, gfHitPoint));
		this.recovery = new SafeInt (CurrentStat (minRecovery.Value, maxRecovery.Value, level, gfRecovery));
	}

	private int CurrentStat(int baseStat,int maxStat,int level, float gf){

		float stat = baseStat + (maxStat - baseStat) * Mathf.Pow ((level - 1f) / (maxLevel - 1f), gf);

		return Mathf.RoundToInt (stat);
	}

	public int StatDifference(int levelA, int levelB, int baseStat,int maxStat,float gf){
		//levelA higher than levelB

		float statA = baseStat + (maxStat - baseStat) * Mathf.Pow ((levelA - 1f) / (maxLevel - 1f), gf);
		float statB = baseStat + (maxStat - baseStat) * Mathf.Pow ((levelB - 1f) / (maxLevel - 1f), gf);

		return Mathf.RoundToInt (statA - statB);
	}
		
}
