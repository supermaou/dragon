﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using GameSparks.Core;
using Facebook.Unity;
using System;
using UnityEngine.EventSystems;

public class GameSparksManager : MonoBehaviour {

	public GameObject errorCanvas;
	public GameObject checkVersionCanvas;

	public static GameSparksManager instance = null;

	public PlayerData playerData;
	public List<DungeonData> dungeonData = new List<DungeonData> ();
	public List<DungeonData> dailyDungeons = new List<DungeonData> ();
	public List<QuestData> questData = new List<QuestData> ();
	public List<MonsterData> monsterData = new List<MonsterData> ();
	public List<List<MonsterData>> teamData = new List<List<MonsterData>> ();
	public List<string> achievementList = new List<string> ();

	private bool newPlayer;

	//Check for loading getmonster
	private int monsterLoaded = 0;
	private int monsterToLoad = 0;

	public GameObject namePopup;
	public Text inputText;

	//Loading parameter
	private bool isLatest = false;
	public bool loadPlayer = false;
	private bool loadDungeons = false;
	public bool loadDailyDungeons = false;
	private bool loadInventory = false;
	private bool loadQuest = false;

	public Text debugText;

	IEnumerator Start(){

		while(!isLatest || !loadPlayer || !loadDungeons || !loadDailyDungeons || !loadInventory || !loadQuest){
			debugText.text = "isLatest: " + isLatest.ToString() + "\nloadPlayer: " + loadPlayer.ToString() + "\nloadDungeons:" + loadDungeons.ToString() +
			"\nloadDailyDungeons: " + loadDailyDungeons.ToString() + "\nloadInventory: " + loadInventory.ToString() + "\nloadQuest: " + loadQuest.ToString();
			yield return null;
		}

		if (SceneManager.GetActiveScene ().name == "launchScene") {
			StartCoroutine (LoadNewScene ());
		}
	}

	void Awake() {
		if (instance == null) // check to see if the instance has a reference
		{
			instance = this; // if not, give it a reference to this class...
			DontDestroyOnLoad(this.gameObject); // and make this object persistent as we load new scenes
		} else // if we already have a reference then remove the extra manager from the scene
		{
			Destroy(this.gameObject);
		}

		SaveLoad.Load ();

		playerData = new PlayerData ();
	}

	void OnApplicationPause(bool pauseStatus){
		if (!pauseStatus) {
			if (GS.Available) {
				LoadPlayerData ();
				CheckVersion ();
			}
		}
	}

	void CheckVersion(){
		print ("Version: " + Application.version);

		new GameSparks.Api.Requests.LogEventRequest ().SetEventKey ("CheckVersion").SetEventAttribute ("version", Application.version).SetDurable(true).Send ((response) => {
			if (!response.HasErrors) {
				isLatest = response.ScriptData.GetBoolean("isLatest") ?? true;

				if(isLatest == false){
					ShowCheckVersion();
				}else{
					print("Check version complete");
				}

			} else {
				OnError();
			}
		});
	}

	void ShowCheckVersion(){
		GameObject g = Instantiate (checkVersionCanvas) as GameObject;

		g.transform.GetChild (1).GetChild (1).GetComponent<Button> ().onClick.AddListener (() => DownloadNewVersion());
	}

	void DownloadNewVersion(){
		#if UNITY_ANDROID
		Application.OpenURL("https://play.google.com/store/apps/details?id=" + Application.bundleIdentifier);
		#endif
	}

	public void AuthenticateDeviceBtn(){
		monsterToLoad = 0;
		monsterLoaded = 0;

		EventSystem.current.currentSelectedGameObject.GetComponent<Button> ().interactable = false;

		//check if already authenticate
		AuthenticateDevice ();
	}

	public void FacebookLoginBtn(){
		monsterToLoad = 0;
		monsterLoaded = 0;

		EventSystem.current.currentSelectedGameObject.GetComponent<Button> ().interactable = false;

		if (!FB.IsInitialized) {
			FB.Init(ConnectGameSparksToGameSparks, null);
		}else{
			FB.ActivateApp();
			ConnectGameSparksToGameSparks();
		}
	}

	private void ConnectGameSparksToGameSparks(){
		if (FB.IsInitialized) {
			FB.ActivateApp ();
			var perms = new List<string> (){ "public_profile", "email", "user_friends" };
			FB.LogInWithReadPermissions (perms, (result) => {
			
				if (FB.IsLoggedIn) {
					new GameSparks.Api.Requests.FacebookConnectRequest ().SetAccessToken (AccessToken.CurrentAccessToken.TokenString).SetDoNotLinkToCurrentPlayer (false).SetSwitchIfPossible (true).Send ((fbauth_response) => {
						if (!fbauth_response.HasErrors) {
							newPlayer = fbauth_response.NewPlayer.Value;
							AuthenticateSuccess(newPlayer);

						} else {
							OnError();
						}
					});
				} else {
					OnError();
				}
			
			});
		} else {
			FacebookLoginBtn ();
		}
	}

	void AuthenticateDevice(){
		new GameSparks.Api.Requests.DeviceAuthenticationRequest().Send((response) => {
			if (!response.HasErrors) {
				Debug.Log("Device Authenticated...");

				newPlayer = response.NewPlayer.Value;
				AuthenticateSuccess(newPlayer);

			} else {
				Debug.Log("Error Authenticating Device...");
				OnError();
			}
		});
	}

	void AuthenticateSuccess(bool newPlayer){
		GetUserDetails ();
	}

	void GetUserDetails(){
		new GameSparks.Api.Requests.AccountDetailsRequest().Send((response) => {
			if (!response.HasErrors) {
				string playerName = response.DisplayName;
				List <string> alist = response.Achievements;

				if(alist != null){
					foreach(string s in alist) {
						achievementList.Add(s);
					}
				}

				print("Player Name: " + playerName);

				if(playerName == "" || playerName == null){
					newPlayer = true;

					//Ask for naming
					namePopup.SetActive(true);

				}else if(newPlayer == false){
					CheckVersion ();
					LoadPlayerData();
					LoadDungeons();
					LoadDailyDungeons ();
					LoadPlayerInventory();
				}

			} else {
				OnError();
			}
		});
	}

	void ChangeDisplayName(){
		if (inputText.text != "" && !inputText.text.Contains(" ") && !inputText.text.Contains("|") && inputText.text.Length >= 3 && inputText.text.Length <= 15) {

			playerData.displayName = inputText.text;

			//set new display name for user
			new GameSparks.Api.Requests.ChangeUserDetailsRequest ().SetDisplayName (inputText.text).SetDurable(true).Send ((response) => {
				if(!response.HasErrors){
					print("name change success");
				}else{
					OnError();
				}

			});
		}
	}

	public void SubmitName(){
		namePopup.SetActive (false);

		AddStarterData ();
	}

	public void AddStarterData(){
		if (playerData.displayName == "") {
			ChangeDisplayName ();

			//It will bug if player can edit his name multiple times, it will give monster to player unlimited
			AddMonster ("flametiger.1", 1, new List<int> (new int[]{ 1, 0, 0, 0, 0 }), 0);
			AddMonster ("flyingfish.1", 1, new List<int> (new int[]{ 1, 0, 0, 0, 0 }), 1);
			AddMonster ("leafdino.1", 1, new List<int> (new int[]{ 1, 0, 0, 0, 0 }), 2);

			SavePlayerData ();

		}
	}

	void GenerateReferralCode(){
		new GameSparks.Api.Requests.LogEventRequest ().SetEventKey ("generateReferCode").Send ((response) => {
		
		});
	}

	public void SavePlayerData(){
		new GameSparks.Api.Requests.LogEventRequest ().SetEventKey ("Save_Data").SetEventAttribute ("playerData", playerData.ToJsonRequestData ()).SetDurable(true).Send ((response) => {
			if (!response.HasErrors) {
				if (newPlayer) {
					GenerateReferralCode();
					CheckVersion();
					LoadPlayerData ();
					LoadDungeons();
					LoadDailyDungeons ();
					LoadPlayerInventory ();
				}
					
			} else {
				Debug.Log ("Error Saving Player Data...");
				OnError();
			}
		});
	}

	public void LoadPlayerData(){
		loadPlayer = false;

		new GameSparks.Api.Requests.LogEventRequest().SetEventKey("Load_Data").Send((response) => {
			if (!response.HasErrors) {
				GSData data = response.ScriptData.GetGSData("player_Data").GetGSData("playerData");

				playerData.displayName = data.GetString("displayName") ?? "";
				playerData.bestWorldBossScore = new SafeInt(data.GetInt("bestWorldBossScore") ?? 0);
				playerData.xp = new SafeInt( data.GetInt("xp") ?? 0 );
				playerData.gold = new SafeInt( data.GetInt("gold").Value );
				playerData.gems = new SafeInt( data.GetInt("gems").Value );
				playerData.stamina = new SafeInt(data.GetInt("stamina") ?? data.GetInt("maxStamina").Value);
				playerData.maxStamina = new SafeInt(data.GetInt("maxStamina").Value);
				playerData.rank = new SafeInt(data.GetInt("rank").Value);
				playerData.maxInventory = new SafeInt(data.GetInt("maxInventory").Value);
				playerData.maxFriends = new SafeInt(data.GetInt("maxFriends") ?? 5);
				playerData.currentTeam = data.GetInt("currentTeam").Value;
				playerData.dailyLogin = new SafeInt(data.GetInt("dailyLogin").Value);
				playerData.loginCount = new SafeInt(data.GetInt("loginCount").Value);
				playerData.dungeon = new SafeInt(data.GetInt("dungeon").Value);
				playerData.tower = new SafeInt(data.GetInt("tower").Value);
				playerData.unclaimAchievement = data.GetStringList("unclaimAchievement");
				playerData.dailyGacha = new SafeInt(data.GetInt("dailyGacha") ?? 0);
				playerData.specialGacha = new SafeInt(data.GetInt("specialGacha") ?? 0);
				playerData.dailyDungeonCompleted = data.GetStringList("dailyDungeonCompleted") ?? new List<string>();

				playerData.friendRequests = data.GetStringList("friendRequests");
				playerData.friendList = data.GetStringList("friendList");
				playerData.friendEnergy = data.GetIntList("friendEnergy");
				playerData.friendDailySend = data.GetIntList("friendDailySend");

				playerData.login = new SafeInt(data.GetInt("login") ?? 0);
				playerData.clear = new SafeInt(data.GetInt("clear") ?? 0);
				playerData.gacha = new SafeInt(data.GetInt("gacha") ?? 0);
				playerData.evolve = new SafeInt(data.GetInt("evolve") ?? 0);
				playerData.fuse = new SafeInt(data.GetInt("fuse") ?? 0);
				playerData.beat = new SafeInt(data.GetInt("beat") ?? 0);
				playerData.dailydun = new SafeInt(data.GetInt("dailydun") ?? 0);
				playerData.bossraid = new SafeInt(data.GetInt("bossraid") ?? 0);

				playerData.worldBoss_reward = data.GetString("worldBoss_reward") ?? "";
				playerData.dailyRaid = new SafeInt(data.GetInt("dailyRaid") ?? 0);

				playerData.dailyGems = new SafeInt(data.GetInt("dailyGems") ?? -1);
				playerData.isReceived = new SafeInt(data.GetInt("isReceived") ?? 0);

				playerData.isBuyStarterPack = new SafeInt(data.GetInt("isBuyStarterPack") ?? 0);

				playerData.referReward = new SafeInt(data.GetInt("referReward") ?? 0);

				playerData.dailyQuestsToClaim = data.GetStringList("dailyQuestsToClaim") ?? new List<string>();
				playerData.dailyQuestsComplete = data.GetStringList("dailyQuestsComplete") ?? new List<string>();

				/*
				if(newPlayer){
					SaveLoad.userData.stamina = playerData.maxStamina.Value;
					SaveLoad.Save();
				}*/

				loadPlayer = true;

				LoadQuests();
			} else {
				Debug.Log("Error Loading Player Data...");
				OnError();
			}
		});
	}

	public void GetStaminaAdded(){
		new GameSparks.Api.Requests.LogEventRequest().SetEventKey("getStamina").Send((response) => {
			if (!response.HasErrors) {
				int staminaAdded = response.ScriptData.GetInt("staminaAdded").Value;

				if(GameSparksManager.instance.playerData.stamina + staminaAdded >= playerData.maxStamina.Value && staminaAdded > 0){
					if(GameSparksManager.instance.playerData.stamina < playerData.maxStamina.Value){
						GameSparksManager.instance.playerData.stamina.Value = playerData.maxStamina.Value;
					}
				}else if(staminaAdded > 0){
					GameSparksManager.instance.playerData.stamina += staminaAdded;
				}

				if(PersistentData.instance != null){
					PersistentData.instance.loadedStamina = true;
				}

				//SaveLoad.Save();
				GameSparksManager.instance.SavePlayerData();

			} else {
				Debug.Log("Error Geting Stamina...");
				OnError();
			}
		});
	}

	public void AddMonster(string shortCode ,int level = 1, List<int> team = default(List<int>), int order = 0){
		if (!MonsterList.instance.IsShortCodeValid (shortCode)) {
			return;
		}

		if (team == null) {
			team = new List<int> (new int[]{ 0, 0, 0, 0, 0 });
		}
		/*
		//Add to SaveLoad monster founded list
		if (!SaveLoad.userData.monsterFounded.Contains (shortCode)) {
			SaveLoad.userData.monsterFounded.Add (shortCode);
			SaveLoad.Save ();
		}*/

		new GameSparks.Api.Requests.LogEventRequest().SetEventKey("addMonster").SetEventAttribute("shortCode",shortCode).SetEventAttribute("level",level).SetEventAttribute("team",team)
		.SetEventAttribute("order",order).SetDurable(true).Send((response) => {
			if (!response.HasErrors) {
				LoadPlayerInventory();
				SaveLoad.userData.inventoryNoti++;
				SaveLoad.Save();
			} else {
				Debug.Log("Error Adding Monster...");
				OnError();
			}
		});
	}

	public void UpdateMonster(string oid,int exp,int skillLevel = 1,int isLocked = 0){
		new GameSparks.Api.Requests.LogEventRequest().SetEventKey("updateMonster").SetEventAttribute("oid",oid).SetEventAttribute("exp",exp).SetEventAttribute("skillLevel",skillLevel)
			.SetEventAttribute("isLocked",isLocked).SetDurable(true).Send((response) => {
			if (!response.HasErrors) {
				Debug.Log("Update Monster Complete...");
			} else {
				Debug.Log("Error Updating Monster...");
				OnError();
			}
		});
	}

	public void RemoveMonster(string oid){
		new GameSparks.Api.Requests.LogEventRequest().SetEventKey("removeMonster").SetEventAttribute("oid",oid).SetDurable(true).Send((response) => {
			if (!response.HasErrors) {
				Debug.Log("Remove monster success");
			} else {
				Debug.Log("Error Removing Monster...");
				OnError();
			}
		});
	}

	public void ChangeMonsterTeam(string oidToAdd,int team, int order , string oidToRemove = ""){
		new GameSparks.Api.Requests.LogEventRequest().SetEventKey("changeTeamMember").SetEventAttribute("oidToAdd",oidToAdd).SetEventAttribute("oidToRemove",oidToRemove).
		SetEventAttribute("order",order).SetEventAttribute("team",team).Send((response) => {
			if (!response.HasErrors) {
				Debug.Log("switching complete");
			} else {
				Debug.Log("Error Changing Monster...");
				OnError();
			}
		});
	}

	void LoadPlayerInventory(){
		new GameSparks.Api.Requests.LogEventRequest().SetEventKey("getPlayerInventory").Send((response) => {
			if (!response.HasErrors) {
				//Clear prior data
				monsterData.Clear();
				teamData.Clear();
				for (int i = 0; i < 5; i++) {
					teamData.Add (new List<MonsterData> ());
				}

				//Store data to lists
				List<GSData> dataList = response.ScriptData.GetGSDataList("MonsterData");

				for(int i=0;i<dataList.Count;i++){
					GSData data = dataList[i].GetGSData("monster");

					MonsterData m = new MonsterData();
					m.objectId = dataList[i].GetGSData("_id").GetString("$oid");
					m.shortCode = data.GetString("shortCode");
					m.isLocked = data.GetInt("isLocked") ?? 0;
					m.team = data.GetIntList("team");
					m.order = data.GetIntList("order");
					m.level = new SafeInt(data.GetInt("level").Value); 
					m.exp = new SafeInt(data.GetInt("exp").Value);
					m.skillLevel = new SafeInt(data.GetInt("skillLevel") ?? 1);

					GetMonsterData(m);
					monsterToLoad++;

					//Add monsters to team array
					for(int a = 0; a < m.team.Count; a++){
						if ( m.team[a] == 1 ) {
							teamData[a].Add(m);
						}
					}

					monsterData.Add(m);
				}

				//Not sure if this is the right place? You can start debug from here
				//Sort the order
				for(int i=0;i<teamData.Count;i++){
					teamData[i] = SortTeamData(teamData[i],i);
				}

				//Set order in case of some duplicate value of teamData's order
				for(int i=0;i<teamData.Count;i++){
					for(int j=0;j<teamData[i].Count;j++){
						teamData[i][j].order[i] = j;
					}
				}

				loadInventory = true;

			} else {
				Debug.Log("Error Loading Player Inventory...");
				OnError();
			}
		});
	}

	public void GetMonsterData(MonsterData mon, string tag = "",bool dependOnExp = true){ //for player's monster
		PersistentData.instance.loadedMonsterData = false;

		new GameSparks.Api.Requests.LogEventRequest().SetEventKey("getMonster").SetEventAttribute("shortCode",mon.shortCode).Send((response) => {
			if (!response.HasErrors) {
				GSData data = response.ScriptData.GetGSData("monster");

				mon.monsterName = data.GetString("name");
				mon.thaiName = data.GetString("thaiName");

				mon.element = data.GetString("element");
				mon.stars = data.GetInt("stars").Value;
				mon.tribe = data.GetString("tribe");

				mon.minAttack = new SafeInt(data.GetInt("minAttack").Value);
				mon.minHitPoint = new SafeInt(data.GetInt("minHitPoint").Value);
				mon.minRecovery = new SafeInt(data.GetInt("minDefense").Value);

				mon.maxAttack = new SafeInt(data.GetInt("maxAttack").Value);
				mon.maxHitPoint = new SafeInt(data.GetInt("maxHitPoint").Value);
				mon.maxRecovery = new SafeInt(data.GetInt("maxDefense").Value);

				mon.gfAttack = data.GetFloat("gfAttack").Value;
				mon.gfHitPoint = data.GetFloat("gfHitPoint").Value;
				mon.gfRecovery = data.GetFloat("gfDefense").Value;

				mon.leaderSkill = data.GetGSData("leaderSkill");
				mon.activeSkill = data.GetGSData("activeSkill");

				//mon.evolve = data.GetInt("evolve").Value;
				mon.evolveMaterials = data.GetStringList("evolveMaterials");

				mon.startExp = new SafeInt(data.GetInt("startExp").Value);
				mon.maxExp = new SafeInt(data.GetInt("maxExp").Value);

				if(mon.stars == 6){
					mon.maxLevel = 99f;
				}else if(mon.stars == 5){
					mon.maxLevel = 99f;
				}else if(mon.stars == 4){
					mon.maxLevel = 70f;
				}else if(mon.stars == 3){
					mon.maxLevel = 50f;
				}else if(mon.stars == 2){
					mon.maxLevel = 30f;
				}else if(mon.stars == 1){
					mon.maxLevel = 10f;
				}

				SetMaxSkillLevel(mon);

				if(tag == "enemy"){
					if(PersistentData.instance != null){
						PersistentData.instance.enemiesLoadCount++;
					}
				}else
				{

					mon.cooldown = new SafeInt(data.GetInt("cooldown").Value);

					//set stat for monster
					if(dependOnExp){
						mon.level = new SafeInt(mon.CalculateLevel(mon.exp.Value));
					}
					mon.SetStatToLevel(mon.level.Value);
					//monsterData = SortInventory();
					SortInventory();

					monsterLoaded++;
				}

				if(PersistentData.instance != null){
					PersistentData.instance.loadedMonsterData = true;
				}

			} else {
				Debug.Log("Error Get Monster Data...");
				print(response.Errors.JSON);
				OnError();
			}
		});
	}

	void SetMaxSkillLevel(MonsterData mon){
		string[] s = mon.shortCode.Split ('.');

		if (s [1] == "1") {
			mon.maxSkillLevel = 5;
		} else if (s [1] == "2") {
			mon.maxSkillLevel = 8;
		} else if (s [1] == "3") {
			mon.maxSkillLevel = 10;
		}
	}

	void LoadDungeons(){
		new GameSparks.Api.Requests.LogEventRequest().SetEventKey("getDungeons").Send((response) => {
			if (!response.HasErrors) {
				dungeonData.Clear();

				List<GSData> dataList = response.ScriptData.GetGSDataList("normalDungeons");

				for(int i=0;i<dataList.Count;i++){
					DungeonData d = new DungeonData();

					d.dungeonName = dataList[i].GetString("name");
					d.thaiName = dataList[i].GetString("thaiName");
					d.order = dataList[i].GetInt("order").Value;
					d.stamina = dataList[i].GetInt("stamina").Value;
					d.bossShortCode = dataList[i].GetString("boss");

					dungeonData.Add(d);
				}

				//sort dungeon data from min to max order
				dungeonData = SortDungeonData();

				LoadDungeonStars();

				loadDungeons = true;

			} else {
				Debug.Log("Error Loading Dungeon Data...");
				OnError();
			}
		});
	}

	public void LoadDailyDungeons(){

		loadDailyDungeons = false;

		new GameSparks.Api.Requests.LogEventRequest ().SetEventKey ("getTodayDungeons").Send ((response) => {
			if (!response.HasErrors) {
				loadDailyDungeons = true;
				dailyDungeons.Clear();

				List<GSData> dataList = response.ScriptData.GetGSDataList("dailyDungeons");

				for(int i=0;i<dataList.Count;i++){
					DungeonData d = new DungeonData();

					GSData dungeon = dataList[i].GetGSData("dungeon");

					d.type = "daily";
					d.dungeonName = dungeon.GetString("name");
					d.thaiName = dungeon.GetString("thaiName");
					d.onlyDays = dungeon.GetStringList("onlyDays");
					d.dropRateNormal = dungeon.GetInt("dropRateNormal") ?? 50;
					d.dropRateBoss = dungeon.GetInt("dropRateBoss") ?? 0;
					d.background = dungeon.GetInt("background") ?? 0;
					d.music = dungeon.GetInt("music") ?? 0;
					d.stamina = dungeon.GetInt("stamina").Value;
					//d.element = dungeon.GetString("element");

					int gold = dungeon.GetInt("gold") ?? 0;
					int maxGold = Mathf.RoundToInt(gold * 1.2f);
					int minGold = Mathf.RoundToInt(gold * 0.8f);

					d.gold = UnityEngine.Random.Range(minGold,maxGold);

					d.exp = dungeon.GetInt("exp").Value;
					d.wave = dungeon.GetIntList("wave");

					//Add enemies data to list
					List<GSData> enemyList = dungeon.GetGSDataList("enemies");
					for(int j=0;j<enemyList.Count;j++){
						EnemyData e = new EnemyData();
						e.enemyShortCode = enemyList[j].GetString("name");
						e.turn = enemyList[j].GetInt("turn").Value;
						e.hitPoint = enemyList[j].GetInt("hitPoint").Value;
						e.attack = enemyList[j].GetInt("attack").Value;
						e.defense = enemyList[j].GetInt("defense").Value;
						e.lootChance = enemyList[j].GetInt("lootChance") ?? 0;
						e.enemySkill = enemyList[j].GetGSData("enemySkill");
						d.enemies.Add(e);
					}

					dailyDungeons.Add(d);

				}

				if(dataList.Count > 1){
					if((dataList[0].GetLong("timeStamp") ?? 0) != SaveLoad.userData.timeStamp){ //New dungeons have arrived
						SaveLoad.userData.dailyDungeons.Clear (); //Clear dungeon that player have passed
						GameSparksManager.instance.playerData.dailyDungeonCompleted.Clear();
						GameSparksManager.instance.SavePlayerData();

						for (int i = 0; i < GameSparksManager.instance.dailyDungeons.Count; i++) {
							SaveLoad.userData.dailyDungeons.Add (GameSparksManager.instance.dailyDungeons [i].dungeonName);
						}	
					}

					SaveLoad.userData.timeStamp = dataList[0].GetLong("timeStamp") ?? 0;
					SaveLoad.Save();
				}

			} else {
				Debug.Log("Error Loading Dungeon Data...");
				OnError();
			}
		});
	}

	public void SaveDungeonStars(){
		new GameSparks.Api.Requests.LogEventRequest().SetEventKey("setDungeonStars").SetEventAttribute("stars",playerData.stars).SetDurable(true).Send((response) => {
			if (!response.HasErrors) {
				//Do nothing
				print("Set dungeon stars complete");
			} else {
				print(response.Errors.JSON);
				Debug.Log("Error Saving Dungeon Stars..."); //this may cause the problem
				OnError();
			}
		});
	}

	void LoadDungeonStars(){
		new GameSparks.Api.Requests.LogEventRequest().SetEventKey("getDungeonStars").Send((response) => {
			if (!response.HasErrors) {
				List<int> dungeonStars = new List<int>();
				if(response.ScriptData != null){
					//Always make sure dungeon data is sort from low to high order before calling this!
					dungeonStars = response.ScriptData.GetIntList("dungeonStars");

					for(int i=0;i<dungeonStars.Count;i++){
						dungeonData[i].stars = dungeonStars[i];
					}

					playerData.stars = dungeonStars;
				}else{
					print("you never earn any stars!");
				}
			} else {
				Debug.Log("Error Loading Dungeon Data...");
				OnError();
			}
		});
	}

	void LoadQuests(){
		new GameSparks.Api.Requests.LogEventRequest().SetEventKey("getQuestData").Send((response) => {
			if (!response.HasErrors) {
				questData.Clear();

				List<GSData> dataList = response.ScriptData.GetGSDataList("QuestData");

				for(int i=0;i<dataList.Count;i++){
					GSData data = dataList[i];

					QuestData q = new QuestData();
					q.questType = data.GetString("shortcode");
					q.targetValue = data.GetIntList("target");
					q.reward = data.GetStringList("reward");

					questData.Add(q);

					//Check quest progress
					int value = 0;
					if(q.questType == "login"){
						value = playerData.login.Value;
					}else if(q.questType == "clear"){
						value = playerData.clear.Value;
					}else if(q.questType == "gacha"){
						value = playerData.gacha.Value;
					}else if(q.questType == "evolve"){
						value = playerData.evolve.Value;
					}else if(q.questType == "fuse"){
						value = playerData.fuse.Value;
					}else if(q.questType == "beat"){
						value = playerData.beat.Value;
					}else if(q.questType == "cleardun"){
						value = playerData.dungeon.Value;
					}else if(q.questType == "dailydun"){
						value = playerData.dailydun.Value;
					}else if(q.questType == "bossraid"){
						value = playerData.bossraid.Value;
					}

					int j;
					for(j=0;j<q.targetValue.Count;j++){
						if( value < q.targetValue[j] ){
							q.currentTargetIndex = j;
							q.progress = value;
							break;
						}
					}

					//Remove the quest data if it complete all of the quest
					if(j == q.targetValue.Count){
						questData.Remove(q);
					}
				}

				loadQuest = true;

			} else {
				Debug.Log("Error Loading Quest Data...");
				OnError();
			}
		});
	}

	IEnumerator LoadNewScene(){

		if (!isLatest) {
			yield break;
		}
					
		//Test tutorial
		if(newPlayer){
			GoToTutorial();
			yield break;
		}
			
		while(!PersistentData.instance.loadedMonsterData){
			yield return null;
		}
			
		while (monsterLoaded < monsterToLoad) {
			yield return null;
		}
			
		//launch main scene
		newPlayer = false;
		SoundManager.instance.MuteEffectLoop();
		Initiate.Fade("main",new Color(0.1f,0.1f,0.1f),1.5f);
		//SceneManager.LoadScene("main");
	}

	void SortInventory(){
		monsterData.Sort (
			delegate (MonsterData m2, MonsterData m1) {
				int compareElement = m2.element.CompareTo(m1.element);
				if(compareElement == 0){
					int compareStars = m1.stars.CompareTo(m2.stars);
					if(compareStars == 0){
						int compareShortCode = m1.shortCode.CompareTo(m2.shortCode);
						if(compareShortCode == 0){
							return m1.level.Value.CompareTo(m2.level.Value);
						}
						return compareShortCode;
					}
					return compareStars;
				}
				return compareElement;
			}
		);
	}

	List<DungeonData> SortDungeonData(){
		List<DungeonData> newData = new List<DungeonData> ();
		List<DungeonData> oldData = dungeonData;

		while (oldData.Count > 0) {
			int minIndex = 0;
			int min = oldData [0].order;
			for (int i = 0; i < oldData.Count; i++) {
				if (oldData [i].order < min) {
					min = oldData [i].order;
					minIndex = i;
				}
			}

			newData.Add (oldData [minIndex]);
			oldData.RemoveAt (minIndex);
		}

		return newData;
	}

	List<MonsterData> SortTeamData(List<MonsterData> team, int teamNumber){
		List<MonsterData> oldData = team;
		List<MonsterData> newData = new List<MonsterData> ();

		while (oldData.Count > 0) {
			int minIndex = 0;
			int min = oldData [0].order [teamNumber];
			for (int i = 0; i < oldData.Count; i++) {
				if (oldData [i].order[teamNumber] < min) {
					min = oldData [i].order [teamNumber];
					minIndex = i;
				}
			}

			newData.Add (oldData [minIndex]);
			oldData.RemoveAt (minIndex);
		}

		return newData;
	}

	//Handle when error occur
	public void OnError(){
		GameObject g = Instantiate (errorCanvas) as GameObject;

		g.transform.GetChild (1).GetChild (1).GetComponent<Button> ().onClick.AddListener (() => Restart (g));
	}

	//Restart the game
	public void Restart(GameObject g){
		//Initiate.Fade ("launchScene", new Color (0.1f, 0.1f, 0.1f), 1.5f);
		Destroy(g);

		AuthenticateDevice ();
	}

	void GoToTutorial(){
		if (!GS.Authenticated) {
			GameSparksManager.instance.OnError ();
			return;
		}
			
		DungeonData d = GameSparksManager.instance.dungeonData [0];

		new GameSparks.Api.Requests.LogEventRequest ().SetEventKey ("GetDungeonData").SetEventAttribute("name", d.dungeonName).Send ((response) => {
			if(!response.HasErrors){
				GSData data = response.ScriptData.GetGSData("dungeonData");

				d.background = data.GetInt("background") ?? 0;
				d.music = data.GetInt("music") ?? 0;
				d.dropRateNormal = data.GetInt("dropRateNormal") ?? 50;
				d.dropRateBoss = data.GetInt("dropRateBoss") ?? 0;
				//d.element = data.GetString("element");

				int gold = data.GetInt("gold") ?? 0;
				int maxGold = Mathf.RoundToInt(gold * 1.2f);
				int minGold = Mathf.RoundToInt(gold * 0.8f);

				d.gold = UnityEngine.Random.Range(minGold,maxGold);

				d.exp = data.GetInt("exp") ?? 0;
				d.wave = data.GetIntList("wave");

				List<GSData> enemyList = data.GetGSDataList("enemies");

				d.enemies.Clear();

				for(int j=0;j<enemyList.Count;j++){
					EnemyData e = new EnemyData();
					e.enemyShortCode = enemyList[j].GetString("name");
					e.turn = enemyList[j].GetInt("turn").Value;
					e.hitPoint = enemyList[j].GetInt("hitPoint").Value;
					e.attack = enemyList[j].GetInt("attack").Value;
					e.defense = enemyList[j].GetInt("defense").Value;
					e.lootChance = enemyList[j].GetInt("lootChance") ?? 0;
					e.enemySkill = enemyList[j].GetGSData("enemySkill");
					d.enemies.Add(e);
				}

				//Set dungeon in Persistence data
				PersistentData.instance.gameType = "dungeon";
				PersistentData.instance.currentDungeon = d;

				StartCoroutine (LoadEnemiesData ()); //cache enemies data to Persistence data gameobject;

			}else{
				OnError();
			}
		});

	}

	IEnumerator LoadEnemiesData(){

		PersistentData.instance.enemiesData.Clear ();
		PersistentData.instance.enemiesLoadCount = 0;

		//extract data from EnemyData class and put it in MonsterData class and also get the data according to its shortCode
		int count = 0;
		for (int i = 0; i < PersistentData.instance.currentDungeon.wave.Count; i++) {
			PersistentData.instance.enemiesData.Add (new List<MonsterData> ());
			for (int j = 0; j < PersistentData.instance.currentDungeon.wave [i]; j++) {
				MonsterData m = new MonsterData ();
				m.shortCode = PersistentData.instance.currentDungeon.enemies [count].enemyShortCode;
				m.cooldown = new SafeInt (PersistentData.instance.currentDungeon.enemies [count].turn);
				m.hitPoint = new SafeInt(PersistentData.instance.currentDungeon.enemies [count].hitPoint);
				m.attack = new SafeInt(PersistentData.instance.currentDungeon.enemies [count].attack);
				m.defense = new SafeInt(PersistentData.instance.currentDungeon.enemies [count].defense);
				m.lootChance = new SafeInt(PersistentData.instance.currentDungeon.enemies [count].lootChance);
				m.enemySkill = PersistentData.instance.currentDungeon.enemies [count].enemySkill;
				GameSparksManager.instance.GetMonsterData (m, "enemy");
				PersistentData.instance.enemiesData [i].Add (m);
				count++;
			}
		}

		while (!PersistentData.instance.loadedMonsterData) {
			yield return null;
		}

		while (PersistentData.instance.enemiesLoadCount < PersistentData.instance.currentDungeon.enemies.Count) {
			yield return null;
		}

		//subtract stamina
		GameSparksManager.instance.playerData.stamina -= PersistentData.instance.currentDungeon.stamina;
		GameSparksManager.instance.SavePlayerData ();
		/*
		if (SaveLoad.userData.stamina < 0) {
			SaveLoad.userData.stamina = 0;
		}
		SaveLoad.Save ();*/

		//SceneManager.LoadScene ("gameplay");
		Initiate.Fade("gameplay",new Color(0.1f,0.1f,0.1f),1.5f);
	}

}
