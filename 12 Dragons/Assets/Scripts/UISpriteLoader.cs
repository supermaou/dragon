﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UISpriteLoader : MonoBehaviour {

	public string spriteName;

	private Image img;

	void Awake(){
		string resolution = "SD";

		if (Screen.width >= 500f) {
			resolution = "HD";
		}

		img = GetComponent<Image> ();
		img.sprite = Resources.Load<Sprite> ("UI/" + resolution + "/" + spriteName);
	}
}
