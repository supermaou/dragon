﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemDrop : MonoBehaviour {

	public AudioClip dropSound;
	public string type;
	public float xPosition;

	int bonusGold;

	// Use this for initialization
	void Awake () {
		StartCoroutine (ItemAnimate ());

		bonusGold = Mathf.RoundToInt (PersistentData.instance.currentDungeon.gold * GameplayManager.instance.goldBonusPercent / 100f);
	}

	IEnumerator ItemAnimate(){
		yield return new WaitForSeconds (0.2f);

		SoundManager.instance.PlayEffect (dropSound);
		StartCoroutine (SmoothMovement (new Vector3 (xPosition, 4.7f, 0f), 0.8f));
		StartCoroutine (ScaleDown (1f));
	}
	
	IEnumerator SmoothMovement(Vector3 end,float time){
		float t = 0;
		Vector3 startPos = transform.position;

		while (t < time) {
			t += Time.deltaTime;
			transform.position = Vector3.Lerp (startPos, end, t / time);
			yield return null;
		}

		if (PersistentData.instance.currentDungeon != null) {
			if (type == "coin") {
				int perEnemy = Mathf.RoundToInt ((float)(PersistentData.instance.currentDungeon.gold + bonusGold) / (float)PersistentData.instance.currentDungeon.enemies.Count);
				GameplayManager.instance.AddCoins (perEnemy);
			} else if (type == "egg") {
				GameplayManager.instance.AddEgg ();
			}
		}

		StartCoroutine (Fade (0.5f));
	}

	IEnumerator ScaleDown(float time){
		float t = 0;
		float start = transform.localScale.x;
		float end = 0.3f;

		while (t < time) {
			t += Time.deltaTime;
			float scale = Mathf.Lerp (start, end, t / time);
			transform.localScale = new Vector3 (scale, scale);
			yield return null;
		}
	}

	IEnumerator Fade(float time){
		SpriteRenderer sr = GetComponent<SpriteRenderer> ();
		float t = 0;
		Color start = sr.color;
		Color end = new Color (sr.color.r, sr.color.g, sr.color.b, 0f);

		while (t < time) {
			t += Time.deltaTime;
			sr.color = Color.Lerp (start, end, t / time);
			yield return null;
		}

		Destroy (gameObject);
	}
}
