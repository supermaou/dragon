﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class WarningTextAnimate : MonoBehaviour {

	private float time = 0.5f;
	private float startAlpha = 1f;
	private float stopAlpha = 0.2f;
	private Color start;
	private Color stop;

	private TextMeshProUGUI tmp;

	void Awake(){
		tmp = GetComponent<TextMeshProUGUI> ();
		start = new Color (tmp.color.r, tmp.color.g, tmp.color.b, startAlpha);
		stop = new Color (tmp.color.r, tmp.color.g, tmp.color.b, stopAlpha);
	}

	IEnumerator Start(){
		while (true) {
			float t = 0;
			while (t < time) {
				t += Time.deltaTime;
				tmp.color = Color.Lerp (start, stop, t / time);
				yield return null;
			}

			t = 0;
			while (t < time) {
				t += Time.deltaTime;
				tmp.color = Color.Lerp (stop, start, t / time);
				yield return null;
			}
		}
	}
}
