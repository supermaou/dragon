﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {

	// Use this for initialization
	void OnEnable () {
		StartCoroutine (DestroySelf ());
	}
	
	IEnumerator DestroySelf(){
		yield return new WaitForSeconds (1f);

		Destroy (gameObject);
	}
}
