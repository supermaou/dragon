﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TestScript : MonoBehaviour {

	private Image img;
	float fixWidth = 150f;

	void Awake(){
		img = GetComponent<Image> ();

		float width = img.sprite.rect.width;
		float height = img.sprite.rect.height;

		float multiplier = fixWidth / width;

		img.rectTransform.sizeDelta = new Vector2 (fixWidth, height * multiplier);
	}

}
