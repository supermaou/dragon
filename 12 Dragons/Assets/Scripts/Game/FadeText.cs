﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class FadeText : MonoBehaviour {

	private TextMeshProUGUI thisText;

	public float fadeTime = 0.3f;
	public float stayTime = 0.2f;

	void Awake(){
		thisText = GetComponent<TextMeshProUGUI> ();
	}

	void OnEnable(){
		thisText.color = new Color (thisText.color.r, thisText.color.g, thisText.color.b, 1f);

		StartCoroutine (Fade (fadeTime));
	}

	IEnumerator Fade(float time){
		yield return new WaitForSeconds (fadeTime + stayTime);

		float t = 0;
		while (t < time) {
			float alpha = Mathf.Lerp (1, 0, t / time);
			t += Time.deltaTime;

			thisText.color = new Color (thisText.color.r, thisText.color.g, thisText.color.b, alpha);
			yield return null;
		}

		gameObject.SetActive (false);
	}
}
