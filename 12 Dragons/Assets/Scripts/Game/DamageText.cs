﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DamageText : MonoBehaviour {

	//private TextMeshProUGUI thisText;
	private Text thisText;

	void Awake(){
		//thisText = GetComponent<TextMeshProUGUI> ();
		thisText = GetComponent<Text>();
	}

	// Use this for initialization
	void OnEnable () {
		thisText.color = new Color (thisText.color.r, thisText.color.g, thisText.color.b, 1f);
	}

	public void MoveUp(){
		StartCoroutine (SmoothMovement (gameObject, gameObject.transform.localPosition + new Vector3 (0f, 0.5f, 0f), 0.5f));
	}
	
	IEnumerator SmoothMovement(GameObject g,Vector3 end,float time){
		float t = 0;
		Vector3 startPos = g.transform.localPosition;

		while (t < time) {
			t += Time.deltaTime;
			g.transform.localPosition = Vector3.Lerp (startPos, end, t/time);
			yield return null;
		}

		StartCoroutine (Fade (0.2f));
	}

	IEnumerator Fade(float time){
		float t = 0;
		while (t < time) {
			float alpha = Mathf.Lerp (1, 0, t / time);
			t += Time.deltaTime;

			thisText.color = new Color (thisText.color.r, thisText.color.g, thisText.color.b, alpha);
			yield return null;
		}

		Destroy (gameObject);

		//gameObject.SetActive (false);
		//GameManager.instance.damageTextPool.Push (gameObject);
	}
}
