﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Jump : MonoBehaviour {

	private RectTransform rectTransform;
	private bool isJumping = false;

	void Awake(){
		rectTransform = GetComponent<RectTransform> ();
	}

	public void StartJump(){
		isJumping = true;
		StartCoroutine (JumpAnimate (0.5f));
	}

	public void StopJump(){
		isJumping = false;
	}

	IEnumerator JumpAnimate(float time){
		Vector3 start = rectTransform.position;
		Vector3 end = rectTransform.position + new Vector3 (0f, 10f, 0f);

		float t = 0;

		while (isJumping) {
			t = 0;
			while (t < time) {
				rectTransform.position = Vector3.Lerp (start, end, t / time);
				t += Time.deltaTime;
				yield return null;
			}

			t = 0;

			while (t < time) {
				rectTransform.position = Vector3.Lerp (end, start, t / time);
				t += Time.deltaTime;
				yield return null;
			}
		}

		rectTransform.position = start;
	}
}
