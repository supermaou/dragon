﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WarningAnimate : MonoBehaviour {

	private float time = 0.5f;
	private float startAlpha = 0f;
	private float stopAlpha = 0.3f;
	private Color start;
	private Color stop;

	private Image img;

	void Awake(){
		img = GetComponent<Image> ();
		start = new Color (img.color.r, img.color.g, img.color.b, startAlpha);
		stop = new Color (img.color.r, img.color.g, img.color.b, stopAlpha);
	}

	void OnEnable(){
		SoundManager.instance.PlayEffectLoop (GameplayManager.instance.warningSound);
	}

	void OnDisable(){
		SoundManager.instance.MuteEffectLoop (); //mute all effect
	}
		
	IEnumerator Start(){
		while (true) {
			float t = 0;
			while (t < time) {
				t += Time.deltaTime;
				img.color = Color.Lerp (start, stop, t / time);
				yield return null;
			}

			t = 0;
			while (t < time) {
				t += Time.deltaTime;
				img.color = Color.Lerp (stop, start, t / time);
				yield return null;
			}
		}
	}
}
