﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbilityData : MonoBehaviour {

	public AudioClip aSound;

	void Start(){
		SoundManager.instance.PlayEffect (aSound);
	}
}
