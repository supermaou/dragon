﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DragHandler : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler {

	public static GameObject itemBeingDragged;

	public Transform chargeBar;

	Vector3 startPosition;
	RectTransform rect;
	public bool interactable = false;

	private bool isCharge = false;

	void Awake(){
		rect = GetComponent<RectTransform> ();
	}

	public void OnBeginDrag(PointerEventData eventData){
		if (!interactable) {
			return;
		}

		itemBeingDragged = gameObject;
		startPosition = transform.position;

		GetComponent<CanvasGroup> ().blocksRaycasts = false;
		//StartCoroutine (Charge (0.5f));
	}

	public void OnDrag(PointerEventData eventData){

		if (!interactable) {
			return;
		}

		float clamp = Input.mousePosition.y;

		if (rect.anchoredPosition.y > 105f) {
			return;
		} else if (rect.anchoredPosition.y < -105f) {
			return;
		} else {
			transform.position = new Vector3 (startPosition.x, clamp, startPosition.z);
		}

	}

	public void OnEndDrag(PointerEventData eventData){
		if (!interactable) {
			return;
		}

		if (rect.anchoredPosition.y < -100f) {
			//GameManager.instance.Attack ();
		}

		isCharge = false;
		itemBeingDragged = null;
		transform.position = startPosition;
		GetComponent<CanvasGroup>().blocksRaycasts = true;
	}

	IEnumerator Charge(float time){
		isCharge = true;

		Image chargeBg = chargeBar.GetComponent<Image> ();
		chargeBg.color = new Color (chargeBg.color.r, chargeBg.color.g, chargeBg.color.b, 1f);
		Image chargeGauge = chargeBar.GetChild (0).GetComponent<Image> ();
		chargeGauge.color = new Color (chargeGauge.color.r, chargeGauge.color.g, chargeGauge.color.b, 1f);

		chargeBar.gameObject.SetActive (true);
		chargeGauge.gameObject.SetActive (true);

		chargeGauge.fillAmount = 0;

		float count = 0;
		while (isCharge) {
			chargeGauge.fillAmount = count / time;

			if (chargeGauge.fillAmount >= 1) {
				count = 0;
			}

			count += Time.deltaTime;
			yield return null;
		}

		yield return new WaitForSeconds (1f);

		StartCoroutine(FadeOut(chargeGauge,0.5f));
		StartCoroutine(FadeOut(chargeBg,0.5f));
	}

	IEnumerator FadeOut(Image img,float time){
		float t = 0;
		Color start = img.color;
		Color stop = new Color (img.color.r, img.color.g, img.color.b, 0f);

		while (t < time) {
			t += Time.deltaTime;
			img.color = Color.Lerp (start, stop, t / time);
			yield return null;
		}

		img.gameObject.SetActive (false);
	}

}
