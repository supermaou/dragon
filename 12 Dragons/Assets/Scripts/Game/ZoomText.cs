﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ZoomText : MonoBehaviour {

	private TextMeshProUGUI tmp;
	private bool isZoom = false;

	void Awake(){
		tmp = GetComponent<TextMeshProUGUI> ();
	}

	public void Zoom(){
		isZoom = true;
		StartCoroutine (ZoomAnimate (0.5f));
	}

	public void StopZoom(){
		isZoom = false;
	}

	IEnumerator ZoomAnimate(float time){
		float t = 0;
		float begin = 1f;
		float end = 1.2f;

		while (isZoom) {
			t = 0;
			while (t < time) {
				float scale = Mathf.Lerp (begin, end, t / time);
				transform.localScale = new Vector2 (scale, scale);
				t += Time.deltaTime;
				yield return null;
			}

			t = 0;
			while (t < time) {
				float scale = Mathf.Lerp (end, begin, t / time);
				transform.localScale = new Vector2 (scale, scale);
				t += Time.deltaTime;
				yield return null;
			}
				
		}

		transform.localScale = new Vector2 (1f, 1f);
	}
}
