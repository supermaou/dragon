﻿//  HiddenVars - RunTimeDebug Editor


//  Custom editor for RunTimeDebugObject. Should be kept in Editor folder.


using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Leguar.HiddenVars {

	[CustomEditor(typeof(RunTimeDebugObject))]
	public class RunTimeDebugEditor : Editor {

		private GUIStyle title=new GUIStyle();

		void OnEnable() {
			title=new GUIStyle();
			title.fontStyle=FontStyle.Bold;
		}

		public override void OnInspectorGUI() {

			RunTimeDebugObject runTimeDebugObject=(RunTimeDebugObject)(this.target); // There always should be only one object
			Dictionary<int,RunTimeDebugContent> allContent=runTimeDebugObject.getContentForDebug();

			if (allContent.Count==0) {
				EditorGUILayout.LabelField("No active HiddenVars instances");
				return;
			}

			foreach (int debugId in allContent.Keys) {
				EditorGUILayout.Separator();
				Dictionary<string,object> contentDict=allContent[debugId].getContent();
				int count=contentDict.Count;
				EditorGUILayout.LabelField(allContent[debugId].getName()+" ("+count+" value"+(count!=1?"s":"")+")",title);
				foreach (string key in contentDict.Keys) {
					object value=contentDict[key];
					EditorGUILayout.LabelField(getKeyString(key),getValueString(value));
				}
			}

		}

		private static string getKeyString(string key) {
			if (key==null) {
				return "(null key)"; // This should not happen
			}
			if (key.Length==0) {
				return "(empty key string)";
			}
			return key;
		}

		private static string getValueString(object value) {
			if (value==null) {
				return "(null value)"; // This should not happen
			}
			if (value is int) {
				return (value.ToString()+" (int)");
			}
			if (value is long) {
				return (value.ToString()+" (long)");
			}
			if (value is float) {
				return (value.ToString()+" (float)");
			}
			if (value is double) {
				return (value.ToString()+" (double)");
			}
			if (value is bool) {
				return (value.ToString()+" (bool)");
			}
			if (value is string) {
				string str=(string)(value);
				int len=str.Length;
				if (len==0) {
					return "(empty string)";
				}
				StringBuilder sb=new StringBuilder();
				if (len<=40) {
					sb.Append(str);
				} else {
					sb.Append(str,0,25).Append(" ... ").Append(str,len-10,10);
				}
				sb.Append(" (").Append(len).Append(" char");
				if (len!=1) {
					sb.Append('s');
				}
				sb.Append(')');
				return sb.ToString();
			}
			if (value is byte[]) {
				byte[] array=(byte[])(value);
				int len=array.Length;
				if (len==0) {
					return "(empty byte array)";
				}
				StringBuilder sb=new StringBuilder();
				if (len<=20) {
					for (int n=0; n<len; n++) {
						sb.Append(array[n]);
						if (n<len-1) {
							sb.Append(',');
						}
					}
				} else {
					for (int n=0; n<10; n++) {
						sb.Append(array[n]).Append(',');
					}
					sb.Append(" ... ");
					for (int n=len-5; n<len; n++) {
						sb.Append(array[n]);
						if (n<len-1) {
							sb.Append(',');
						}
					}
				}
				sb.Append(" (").Append(len).Append(" byte");
				if (len!=1) {
					sb.Append('s');
				}
				sb.Append(')');
				return sb.ToString();
			}
			return ("Unknown value type: "+value.GetType().ToString());
		}
		
	}

}
