﻿//    HiddenVars


using System;
using System.Collections.Generic;

namespace Leguar.HiddenVars {
	
	/// <summary>
	/// Main class for HiddenVars. Also only public class.
	/// </summary>
	public class HiddenVars : IDisposable {

		private HiddenVarsRep rep;
		private bool disposed;

		#if UNITY_EDITOR
		private int debugId;
		#endif

		/// <summary>
		/// Initializes a new instance of the HiddenVars class.
		/// </summary>
		public HiddenVars() : this(null) {
		}

		/// <summary>
		/// Initializes a new instance of the HiddenVars class with specific debug name.
		/// </summary>
		/// <param name="nameForDebug">
		/// Debug name for this instance, visible in "HiddenVars EditorOnly RunTimeDebug" game object in the Unity Editor when application is running.
		/// There can be multiple instances of HiddenVars with same debug name. If parameter is null, default name is generated.
		/// </param>
		/// <remarks>
		/// Debug name have no effect to actual functionality of this class and have meaning only when running application in Unity Editor.
		/// </remarks>
		public HiddenVars(string nameForDebug) {
			rep=new HiddenVarsRep();
			disposed=false;
			rtdAddInstance(nameForDebug);
		}

		/// <summary>
		/// Returns count of key/value pairs in this instance.
		/// </summary>
		public int Count {
			get {
				return rep.getCount();
			}
		}

		/// <summary>
		/// Gets or sets integer value with the specified key.
		/// </summary>
		/// <remarks>
		/// Functionality is identical compared to using GetInt and SetInt methods.
		/// </remarks>
		/// <param name="key">
		/// Key of the value to set or get
		/// </param>
		/// <value>
		/// Integer value
		/// </value>
		public int this[string key] {
			set {
				SetInt(key,value);
			}
			get {
				return GetInt(key);
			}
		}

		/// <summary>
		/// Add or replace existing value identified by key.
		/// </summary>
		/// <param name="key">
		/// Key of the integer value to add
		/// </param>
		/// <param name="value">
		/// Integer value to be hidden
		/// </param>
		public void SetInt(string key, int value) {
			rep.putByteArray(key,BitConverter.GetBytes(value),false);
			rtdSetValue(key,value);
		}

		/// <summary>
		/// Add or replace existing value identified by key.
		/// </summary>
		/// <param name="key">
		/// Key used to identify this value
		/// </param>
		/// <param name="value">
		/// Long value to be hidden
		/// </param>
		public void SetLong(string key, long value) {
			rep.putByteArray(key,BitConverter.GetBytes(value),false);
			rtdSetValue(key,value);
		}

		/// <summary>
		/// Add or replace existing value identified by key.
		/// </summary>
		/// <param name="key">
		/// Key of the float value to add
		/// </param>
		/// <param name="value">
		/// Float value to be hidden (may be NaN, positive infinity or negative infinity)
		/// </param>
		public void SetFloat(string key, float value) {
			rep.putByteArray(key,BitConverter.GetBytes(value),false);
			rtdSetValue(key,value);
		}

		/// <summary>
		/// Add or replace existing value identified by key.
		/// </summary>
		/// <param name="key">
		/// Key used to identify this value
		/// </param>
		/// <param name="value">
		/// Double value to be hidden (may be NaN, positive infinity or negative infinity)
		/// </param>
		public void SetDouble(string key, double value) {
			rep.putByteArray(key,BitConverter.GetBytes(value),false);
			rtdSetValue(key,value);
		}

		/// <summary>
		/// Add or replace existing value identified by key.
		/// </summary>
		/// <param name="key">
		/// Key of the boolean value to add
		/// </param>
		/// <param name="value">
		/// Boolean value to be hidden
		/// </param>
		public void SetBool(string key, bool value) {
			rep.putByteArray(key,BitConverter.GetBytes(value),false);
			rtdSetValue(key,value);
		}

		/// <summary>
		/// Add or replace existing value identified by key.
		/// </summary>
		/// <param name="key">
		/// Key of the string to add
		/// </param>
		/// <param name="value">
		/// String to be hidden (can not be null but length may be zero)
		/// </param>
		public void SetString(string key, string value) {
			int length=value.Length;
			byte[] strBytes=new byte[length*2];
			for (int n=0; n<length; n++) {
				byte[] chrBytes=BitConverter.GetBytes(value[n]);
				strBytes[n*2]=chrBytes[0];
				strBytes[n*2+1]=chrBytes[1];
			}
			rep.putByteArray(key,strBytes,false);
			rtdSetValue(key,value);
		}

		/// <summary>
		/// Add or replace existing value identified by key.
		/// </summary>
		/// <param name="key">
		/// Key of the value to add
		/// </param>
		/// <param name="value">
		/// Byte array to be hidden (can not be null but length may be zero)
		/// </param>
		public void SetByteArray(string key, byte[] value) {
			rep.putByteArray(key,value,true);
			rtdSetValue(key,value);
		}

		/// <summary>
		/// Get integer value identified by key.
		/// </summary>
		/// <param name="key">
		/// Key of the integer value to get
		/// </param>
		/// <returns>
		/// Integer value
		/// </returns>
		/// <exception cref="KeyNotFoundException">
		/// Value with specified key doesn't exist
		/// </exception>
		public int GetInt(string key) {
			return BitConverter.ToInt32(rep.getByteArray(key),0);
		}

		/// <summary>
		/// Get integer value identified by key or default value if key is not found.
		/// </summary>
		/// <param name="key">
		/// Key of the integer value to get
		/// </param>
		/// <param name="defaultValue">
		/// Default integer value to return if key does not exist
		/// </param>
		/// <returns>
		/// Integer value or default integer value
		/// </returns>
		public int GetInt(string key, int defaultValue) {
			try {
				return BitConverter.ToInt32(rep.getByteArray(key),0);
			}
			catch (KeyNotFoundException) {
				return defaultValue;
			}
		}

		/// <summary>
		/// Get long value identified by key.
		/// </summary>
		/// <param name="key">
		/// Key of the long value to get
		/// </param>
		/// <returns>
		/// Long value
		/// </returns>
		/// <exception cref="KeyNotFoundException">
		/// Value with specified key doesn't exist
		/// </exception>
		public long GetLong(string key) {
			return BitConverter.ToInt64(rep.getByteArray(key),0);
		}

		/// <summary>
		/// Get long value identified by key or default value if key is not found.
		/// </summary>
		/// <param name="key">
		/// Key of the long value to get
		/// </param>
		/// <param name="defaultValue">
		/// Default long value to return if key does not exist
		/// </param>
		/// <returns>
		/// Long value or default value
		/// </returns>
		public long GetLong(string key, int defaultValue) {
			try {
				return BitConverter.ToInt64(rep.getByteArray(key),0);
			}
			catch (KeyNotFoundException) {
				return defaultValue;
			}
		}

		/// <summary>
		/// Get float value identified by key.
		/// </summary>
		/// <param name="key">
		/// Key of the float value to get
		/// </param>
		/// <returns>
		/// Float value
		/// </returns>
		/// <exception cref="KeyNotFoundException">
		/// Value with specified key doesn't exist
		/// </exception>
		public float GetFloat(string key) {
			return BitConverter.ToSingle(rep.getByteArray(key),0);
		}
		
		/// <summary>
		/// Get float value identified by key or default value if key is not found.
		/// </summary>
		/// <param name="key">
		/// Key of the float value to get
		/// </param>
		/// <param name="defaultValue">
		/// Default float value to return if key does not exist
		/// </param>
		/// <returns>
		/// Float value or default float value
		/// </returns>
		public float GetFloat(string key, float defaultValue) {
			try {
				return BitConverter.ToSingle(rep.getByteArray(key),0);
			}
			catch (KeyNotFoundException) {
				return defaultValue;
			}
		}

		/// <summary>
		/// Get double value identified by key.
		/// </summary>
		/// <param name="key">
		/// Key of the double value to get
		/// </param>
		/// <returns>
		/// Double value
		/// </returns>
		/// <exception cref="KeyNotFoundException">
		/// Value with specified key doesn't exist
		/// </exception>
		public double GetDouble(string key) {
			return BitConverter.ToDouble(rep.getByteArray(key),0);
		}

		/// <summary>
		/// Get double value identified by key or default value if key is not found.
		/// </summary>
		/// <param name="key">
		/// Key of the double value to get
		/// </param>
		/// <param name="defaultValue">
		/// Default double value to return if key does not exist
		/// </param>
		/// <returns>
		/// Double value or default value
		/// </returns>
		public double GetDouble(string key, float defaultValue) {
			try {
				return BitConverter.ToDouble(rep.getByteArray(key),0);
			}
			catch (KeyNotFoundException) {
				return defaultValue;
			}
		}

		/// <summary>
		/// Get boolean value identified by key.
		/// </summary>
		/// <param name="key">
		/// Key of the boolean value to get
		/// </param>
		/// <returns>
		/// Boolean value
		/// </returns>
		/// <exception cref="KeyNotFoundException">
		/// Value with specified key doesn't exist
		/// </exception>
		public bool GetBool(string key) {
			return BitConverter.ToBoolean(rep.getByteArray(key),0);
		}
		
		/// <summary>
		/// Get boolean value identified by key or default value if key is not found.
		/// </summary>
		/// <param name="key">
		/// Key of the boolean value to get
		/// </param>
		/// <param name="defaultValue">
		/// Default boolean value to return if key does not exist
		/// </param>
		/// <returns>
		/// Boolean value or default boolean value
		/// </returns>
		public bool GetBool(string key, bool defaultValue) {
			try {
				return BitConverter.ToBoolean(rep.getByteArray(key),0);
			}
			catch (KeyNotFoundException) {
				return defaultValue;
			}
		}

		/// <summary>
		/// Get string identified by key.
		/// </summary>
		/// <param name="key">
		/// Key of the string to get
		/// </param>
		/// <returns>
		/// String
		/// </returns>
		/// <exception cref="KeyNotFoundException">
		/// Value with specified key doesn't exist
		/// </exception>
		public string GetString(string key) {
			byte[] bytes=rep.getByteArray(key);
			int strLength=bytes.Length/2;
			char[] strChars=new char[strLength];
			for (int n=0; n<strLength; n++) {
				strChars[n]=BitConverter.ToChar(bytes,n*2);
			}
			return (new string(strChars));
		}
		
		/// <summary>
		/// Get string identified by key or default value if key is not found.
		/// </summary>
		/// <param name="key">
		/// Key of the string to get
		/// </param>
		/// <param name="defaultValue">
		/// Default string value to return if key does not exist (may be null)
		/// </param>
		/// <returns>
		/// String or default string
		/// </returns>
		public string GetString(string key, string defaultValue) {
			try {
				return GetString(key);
			}
			catch (KeyNotFoundException) {
				return defaultValue;
			}
		}

		/// <summary>
		/// Get byte array identified by key.
		/// </summary>
		/// <param name="key">
		/// Key of the byte array to get
		/// </param>
		/// <returns>
		/// Byte array
		/// </returns>
		/// <exception cref="KeyNotFoundException">
		/// Value with specified key doesn't exist
		/// </exception>
		public byte[] GetByteArray(string key) {
			return rep.getByteArray(key);
		}

		/// <summary>
		/// Get byte array identified by key or default value if key is not found.
		/// </summary>
		/// <param name="key">
		/// Key of the byte array to get
		/// </param>
		/// <param name="defaultValue">
		/// Default byte array to return if key does not exist (may be null)
		/// </param>
		/// <returns>
		/// Byte array or default byte array
		/// </returns>
		public byte[] GetByteArray(string key, byte[] defaultValue) {
			try {
				return rep.getByteArray(key);
			}
			catch (KeyNotFoundException) {
				return defaultValue;
			}
		}
		
		/// <summary>
		/// Checks whether any type of value identified by key exists.
		/// </summary>
		/// <param name="key">
		/// Key to locate
		/// </param>
		/// <returns>
		/// True if key and value exists
		/// </returns>
		public bool ContainsKey(string key) {
			return rep.containsKey(key);
		}

		/// <summary>
		/// Remove any type of value identified by key.
		/// </summary>
		/// <param name="key">
		/// Key of the value to be removed
		/// </param>
		/// <returns>
		/// True if key was found and value removed, otherwise false
		/// </returns>
		public bool Remove(string key) {
			bool ret=rep.remove(key);
			rtdRemoveValue(key);
			return ret;
		}

		/// <summary>
		/// Remove all the keys and values.
		/// </summary>
		public void Clear() {
			rep.clear();
			rtdClear();
	    }

		/// <summary>
		/// Clear and destroy this instance. This method will also remove instance from RunTimeDebug object if running application in Unity Editor.
		/// After calling this method, instance is not usable any more and trying to add or read any values will cause exception.
		/// </summary>
		public void Dispose() {
			Dispose(true);
		}

		protected virtual void Dispose(bool disposing) {
			if (!disposed) {
				if (disposing) {
					rep.destroy();
					rep=null;
				}
				rtdRemoveInstance();
				disposed=true;
			}
		}

		~HiddenVars() {
			Dispose(false); // This will remove instance from debug object if running in Unity Editor
		}

		private void rtdAddInstance(string nameForDebug) {
			#if UNITY_EDITOR
			RunTimeDebugObject rtdObject=RunTimeDebugObject.getOrCreateInstance();
			if (rtdObject!=null) {
				debugId=rtdObject.addDebugDictionary(nameForDebug);
			}
			#endif
		}

		private void rtdSetValue(string key, object value) {
			#if UNITY_EDITOR
			RunTimeDebugObject rtdObject=RunTimeDebugObject.getInstance();
			if (rtdObject!=null) {
				rtdObject.setDebugDictionaryValue(debugId,key,value);
			}
			#endif
		}

		private void rtdRemoveValue(string key) {
			#if UNITY_EDITOR
			RunTimeDebugObject rtdObject=RunTimeDebugObject.getInstance();
			if (rtdObject!=null) {
				rtdObject.removeDebugDictionaryValue(debugId,key);
			}
			#endif
		}

		private void rtdClear() {
			#if UNITY_EDITOR
			RunTimeDebugObject rtdObject=RunTimeDebugObject.getInstance();
			if (rtdObject!=null) {
				rtdObject.clearDebugDictionary(debugId);
			}
			#endif
		}

		private void rtdRemoveInstance() {
			#if UNITY_EDITOR
			if (!RunTimeDebugObject.isDestroyedByUser()) {
				RunTimeDebugObject.getInstance().removeDebugDictionary(debugId);
			}
// This will cause error to log at least in Unity 4, if this method was called by GC thread
//			RunTimeDebugObject rtdObject=RunTimeDebugObject.getInstance();
//			if (rtdObject!=null) {
//				rtdObject.removeDebugDictionary(debugId);
//			}
			#endif
		}

	}

}
